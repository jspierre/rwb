<?php
/** 
 * BACKGROUND TASKS CRON PROCESSOR
 * @author John St. Pierre
 * @ver 0.1
 *
 */

/*** error reporting on ***/
error_reporting(E_ALL);

/*** define the site path constant ***/
$site_path = realpath(dirname(__FILE__));
define ('SITE_PATH', $site_path);


/*** include the init.php file ***/
require(SITE_PATH.'/includes/init.php');
require(SITE_PATH.'/application/background.class.php');
require(SITE_PATH.'/application/bgtasks.class.php');

$bgtasks = new background($registry);
$bgtasksran = $bgtasks->doBackgroundTasks();
if ($bgtasksran) {
	$explanation = "BGTASKS attempted to run all scheduled tasks successfully.";
}
else {
	$explanation = "BGTASKS found no tasks to run and halted successfully.";
}
$registry->logging->logEvent($registry->config['logging_cat_viewmsg'],$explanation);

exit();



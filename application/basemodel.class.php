<?php

Abstract Class baseModel {

	protected $registry;
    protected $userip;

	function __construct($registry) {
		$this->registry = $registry;
		if (isset($_SERVER['REMOTE_ADDR'])) {
        	$this->userip = $_SERVER['REMOTE_ADDR'];
        }
        else {
        	$this->userip = "127.0.0.1";
        }
	}


}

?>
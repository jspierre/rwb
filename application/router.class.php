<?php
/** 
 * MAIN ROUTER FILE
 * Configures and calls all controllers
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
 class router {
 
	/* Public Variables */
	public $registry;
	public $output;
	protected $model;
	
 
	/* Constructor */
	public function __construct($registry) {
		
		/* Set up the registry in this class */
		$this->registry = $registry;

		
		/* If this is supposed to be an SSL connection and it isn't, let's do that now */
		if ($this->registry->config['ssl'] == TRUE) {
			if (!isset($_SERVER['HTTPS'])) {
				$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				header("Location: $url");
				exit;
			}
		}

		/* Are we supposed to use sessions?  If so, let's turn them on now! */
		if($this->registry->config['sessions'] == TRUE) {
			session_start();
		}
	}
	
	
	/* CONTROLLER MAIN - performs the main controller functions by figuring out what to do and where to go */
	public function doRoute() {
			
		/* Now, let's pull apart the URL sent by our user to get the requested /controllers class  */
		/* If the URL is in a subdirectory, we need to pull that out because it isn't part of the true request */
		$subdirectory = (count(explode('/', $this->registry->config['url']))-3);
		$pageRequest = $_SERVER['REQUEST_URI'];
		$pageArray = explode('/', $pageRequest);

		// How many pieces are there to the URI Request?
		$numsubs = (count(explode('/', $_SERVER['REQUEST_URI'])));
		
		// If there's a trailing slash in the URI, we need to remove 1 from the number of pieces as that's a false piece
		// And also remove the last item from the $pageArray as it is a blank
		if (substr($_SERVER['REQUEST_URI'],-1) == "/") { 
			$numsubs --; 
			array_pop($pageArray);
		}

		// If there is no controller listed, we need to set the class to nothing, at least for now...

		if ($numsubs-$subdirectory > 1) { 
			$controllerspot = 1 + $subdirectory; 
			$class=strtolower($pageArray[$controllerspot]);
		}
		else {
			$class = "";
		}

		/* Now, let's pull any extra pieces off the end of the URL and set them as variables we */
		/* Can pass to our controller for its use in an array called $passedVars */
		/* If there were no passed variables, let's set the $passedVars to FALSE */
		// This is the number of pieces we want to 'pop' off of the front of the array
		if ($numsubs-$subdirectory > 1) {
			$x = $numsubs - ($controllerspot+1); 
		}
		else {
			$x = 0;
		}

		// This piece loops through popping off pieces until we're just left with the variables
		// If $x == 0, we just pass back FALSE
		if ($x > 0) {
			$passedVars = $pageArray;
			for ($i=0; $i < $controllerspot+1; $i++) { 
				array_shift($passedVars);
			}
		}
		else {
			$passedVars = false;
		}

		/* Let's also add the passed variables into the registry - our controller may never use it, but we add */
		/* It just in case, it is up to the controller to use the variables or not from the end of the URL */
		$this->registry->passedVars = $passedVars;

		/* If no controller was given, we set this so it will send them to the main page */
		if ($class=="") { $class = strtolower($this->registry->config['firstpage']); }
		
		/* Load the requested class into the registry - could change later, but we need to know what it is for now */
		$this->registry->controller = $class;

									
		/* Run security processes before letting the user do anything */
		if ($this->registry->security->doSecurity() == FALSE) {
			exit();
		}

		
		/* If the user is logged in and trying to access the home page, let's send him to HIS home page as expected instead of generic (log in) */
		if ($this->registry->security->isLoggedIn()) {
			/*if (isset($_SESSION['requestedurl'])) { 
				$requestedurl = $_SESSION['requestedurl']; 
				unset($_SESSION['requestedurl']);
				$url = $this->registry->config['url']."/".$requestedurl;
				header("Location: $url");
				exit();
			}
			else*/ if ($class==$this->registry->config['firstpage']) {
				$class=$this->registry->config['mainpage'];
			 }
		}
			

		/* If the controller exists, let's use it and get its return value, else show the 404 Page instead*/
		if ($this->routeExists() == FALSE) { 
			if ($this->registry->config['security_404attacks'] == TRUE) {$this->registry->security->reportAttack();}
			$addon = "";
			if ($this->registry->config['sessions']==TRUE && isset($_SESSION['username'])) { $addon = "BY USER %%USER%% "; }
			$this->registry->logging->logEvent($this->registry->config['logging_cat_error404'],"ATTEMPT ".$addon."TO ACCESS NON-EXISTENT CONTROLLER: ".$class." FROM %%IP%%");
			$class = strtolower($this->registry->config['404page']); 
			$pageArray[0]=$pageArray[1];
		}
		if (file_exists(SITE_PATH."/controller/system/".$class.".php")) {
			require(SITE_PATH."/controller/system/".$class.".php"); 	
		}
		else {
			require(SITE_PATH."/controller/".$class.".php"); 
		}
		$this->registry->controller = $class;
		$this->registry->pageArray = $pageArray;
		$outercall = new $class($this->registry);
		$returnvalue = $outercall->index();
				
		/* Now, we send the final return value to our View to let us create the page for our user! */
		$this->registry->template->getPage($returnvalue);
	
		/* If set, write any return message to the log */
		if(isset($returnvalue['logmessage'])) { $this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$returnvalue['logmessage']); }

		/* Clear out any returncodes used */
		if(isset($_SESSION['returncode'])) { unset($_SESSION['returncode']); }
		
		/* All Done! */
		return true;
	}
	
	/* Checks to see if the requested route (controller) php file actually exists. */
	/* Returns true if it exists, returns false if it doesn't exist */
	public function routeExists() {
		$class = $this->registry->controller;
		if (file_exists(SITE_PATH."/controller/".$class.".php") || file_exists(SITE_PATH."/controller/system/".$class.".php")) { 
			return true;
		}
		else {
			return false;
		}
	}

	

/* END OF CLASS */
}
 

 
?>
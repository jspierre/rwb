<?php

/** 
 * LOGGING CLASS
 * Handles all logging functions
 * @author John St. Pierre
 * @ver 0.1
 *
 * The ONLY public method is logEvent($category, $shortmsg, $longmsg) - See logging.inc.php to set up your categories
 *
 */
 
 class logging {
 
	private $registry;
	private $subarray;
	private $subject;
	private $premessage;
	private $shortmsg;
	private $shortmessage;
	private $longmsg;
	private $longmessage;
	private $postmessage;
	private $loglevel;
	private $attack;
	private $notifyuser;
	private $llname;
	private $llapplog;
	private $llcommgroup;
	private $llcommother;
	private $tempdata;
 
	/* Constructor */
	public function __construct($registry) {

		/* First, let's set the configuration so other routines can access it */
		$this->registry = $registry;
		
	}
	
	
	/* logEvent() */
	/* The main logging method - it acts as a controller to send instructions to other methods within this class */
	public function logEvent($category='default', $shortmessage=FALSE, $longmessage=FALSE, $subject=FALSE) {
	
		/* If the user does not want to use notifications, we can stop here and save the overhead */
		if ( $this->registry->config['notifications'] == FALSE ) { return true; }

		/* First, let's set all of our private properties through the getVariables() function */
		/* If that function fails, we simply return false as the configuration file has settings that won't let us continue */
		if ($shortmessage) { $this->shortmessage = $shortmessage; }
		if ($longmessage) { $this->longmessage = $longmessage; }
		if ($subject) {$this->subject = $subject; }
		if ( $this->getVariables($category) == FALSE) { return false; }
		
		/* OK, now if applog is set to true, let's send this in our system log as requested */
		if ($this->llapplog == TRUE) { $this->addToLog($this->shortmessage); }
		
		/* OK, now, if any commgroups exists, let's look at the commgroups and do what each one wants */
		if ( $this->llcommgroup !== FALSE ) {
			/* First, if this is not an array, let's turn it into one */
			if ( is_array($this->llcommgroup) == FALSE ) {
				$this->llcommgroup = array($this->llcommgroup);
			}
			/* Next, let's loop through each item in this array and take action on each one */
			foreach ($this->llcommgroup as $comm) { $this->doCommunicator($comm); }
		}
		
		/* We're done with commgroups, if any 'other' communications rules exist (llcommother), let's go through those as well */
		if ($this->llcommother !== FALSE) {
			/* First, this must be an array to be a true commother */
			if ( is_array($this->llcommother) == TRUE ) {
				/* Next, let's loop through each item in this array and take action on each one */
				foreach ($this->llcommother as $comm=>$data) { $this->doCommunicator($comm,$data); }
			}
		}
		
		/* Now, if we've chosen to alert the user, let's get that information and send them notice as well */
		/* Of course, only if there's actually a user logged in */
		if ($this->notifyuser == TRUE) {
			if (isset($_SESSION['username'])) {	$this->doCommunicator('uSEr'.$_SESSION['username']); }
		}
		
		/* Finally, if this constitutes a security breach attempt, we log it as an attack as instructed */
		if ($this->attack == TRUE) {
			$this->registry->security->reportAttack();
		}
	}

	/* getVariables($category) */
	/* Pulls all the variables sent to us and sets private properties with the correct values for use by all */
	/* of the other methods. */
	private function getVariables($category) {
		/* First check if the category doesn't exist */
		if ( !array_key_exists($category, $this->registry->config['logging_categories']) ) {
			/* If it doesn't, if the default category exists set that as the category instead */
			if ( array_key_exists('default', $this->registry->config['logging_categories'] )) {
				$category = 'default';
			}
			/* If no default exists, return false and do nothing else */
			else {
				return false;
			}
		}
		
		/* Now that we have a valid category, we find out what's in it */
		$this->subarray = $this->registry->config['logging_categories'][$category];
		if ($this->subject == false || $this->subject == "") { $this->subject = $this->subarray['subject']; };
		$this->premessage = $this->subarray['premessage'];
		$this->shortmsg = $this->subarray['shortmsg'];
		$this->longmsg = $this->subarray['longmsg'];
		$this->postmessage = $this->subarray['postmessage'];
		$this->loglevel = $this->subarray['loglevel'];
		$this->attack = $this->subarray['attack'];
		$this->notifyuser = $this->subarray['notifyuser'];
		
		/* And we need the data for the log level set - check to see if the level set doesn't actually exist */
		if ( !array_key_exists($this->loglevel, $this->registry->config['logging_levels']) ) {
			/* If it doesn't and a default exists and is valid, we'll take that */
			if ( array_key_exists($this->registry->config['logging_level_default'], $this->registry->config['logging_levels']) ) {
				$loglevel = $this->registry->config['logging_level_default'];
			}		
			/* If even that falls through, we return false and stop processing */
			else {
				return false;
			}
		}
		
		/* Now that we have a valid log level, let's find out what's in that */
		$subarray2 = $this->registry->config['logging_levels'][$this->loglevel];
		$this->llname = $subarray2['name'];
		$this->llapplog = $subarray2['applog'];
		$this->llcommgroup = $subarray2['commgroup'];
		$this->llcommother = $subarray2['commother'];
		
		/* Let's set our messages - if no $shortmessage or $longmessage is set, we'll use the default set in the category */
		if (!$this->shortmessage) { $this->shortmessage = $this->shortmsg; }
		if (!$this->longmessage) { $this->longmessage = $this->longmsg; }
		
		/* Now, let's send our message through the data parser to replace %%VARIABLES%% with their actual values */
		$this->subject = $this->varParser($this->subject);
		$this->premessage = $this->varParser($this->premessage);
		$this->shortmessage = substr($this->varParser($this->shortmessage),0,4000);		/* Limited to 120 characters */
		$this->longmessage = $this->varParser($this->longmessage);
		$this->postmessage = $this->varParser($this->postmessage);
		
		/* All done! */
		return true;
	}
	
	
	
	/*	doCommunicator - The actual communications portion of this class - takes the prepared data, finds the proper send method, 
	/*	and performs the send based on the data given it */
	private function doCommunicator($comm,$data=FALSE) {
		/* If this is a single value, and not a user send, we do the routine for a commgroup */
		if ( ($data == FALSE) && (substr($comm,0,4) != 'uSEr') ) {
			/* First, if the commgroup doesn't exist, let's just go back */
			if ( array_key_exists($comm, $this->registry->config['logging_groups'] )) {
				$grouparray = $this->registry->config['logging_groups'][$comm];
				$this->doMethod($grouparray);
			}
			else {
				return false;
			}
			return true;
		}
		
		/* If the data variable is set, we do the routine for a commother */
		elseif ( $data !== FALSE ) {
			$grouparray['method'] = $comm;
			$grouparray['data'] = $data;
			$this->doMethod($grouparray);
		}
		
		/* If this is a user variable, we do the routing for a user notice */
		elseif ( substr($comm,0,4) == 'uSEr' ) {
			foreach ($this->registry->config['logging_user'] as $usermethod) {
				$grouparray['method'] = $usermethod['notify_method'];
				$user = substr($comm,4);
				$grouparray['data'] = $this->getUserValue($user,$usermethod);
				if ($grouparray['data'] <> "") { $this->doMethod($grouparray); }
			}
		}

		/* Otherwise, something is wrong and we fail */
		else {
			return false;
		}

		/* Everything went okay - return true */
		return true;
	}
	

	/* doMethod($grouparray) */
	/* Performs the actual method call for the communications method chosen. */
	private function doMethod($grouparray) {
		$method = $grouparray['method'];
		$this->tempdata = $grouparray['data'];
		if ($this->{$method}() == FALSE) {
			return false;
		}
		else {
			return true;
		}
	}
	
		
	
	/* PARSING - replaces %%VARIABLES%% with the actual value and returns the new string.  If no %%VARS%% exist in the string, same string is returned */
	private function varParser($text) {

		/* IP ADDRESS PARSER */
		if (isset($_SERVER['REMOTE_ADDR'])) {
			$text=str_replace('%%IP%%', $_SERVER['REMOTE_ADDR'], $text);
		}
		
		/* USER NUMBER PARSER */
		if (isset($_SESSION['usernum'])) {
			$text=str_replace('%%USERNUM%%', $_SESSION['usernum'], $text);
		}
		
		/* USER NAME PARSER */
		if (isset($_SESSION['username'])) {
			$text=str_replace('%%USER%%', $_SESSION['username'], $text);
		}
		
		/* SUBJECT PARSER */
		$text=str_replace('%%SUBJECT%%', $this->subject, $text);
		
		/* PRE-MESSAGE PARSER */
		$text=str_replace ('%%PREMESSAGE%%', $this->premessage, $text);
		
		/* SHORT MESSAGE PARSER */
		$text=str_replace ('%%SHORTMESSAGE%%', $this->shortmessage, $text);
		
		/* LONG MESSAGE PARSER */
		$text=str_replace ('%%LONGMESSAGE%%', $this->longmessage, $text);
		
		/* POST-MESSAGE PARSER */
		$text=str_replace ('%%POSTMESSAGE%%', $this->postmessage, $text);
		
		/* ALL DONE - RETURN THE PARSED TEXT! */
		return $text;	
		
	}
	
	
	/* getUserValue */
	/* Pulls the value required based on app settings and returns it to the caller */
	private function getUserValue($user, $usermethod) {
		$table = $usermethod['db_table'];
		$match = $usermethod['db_match_column'];
		$active = $usermethod['db_active_column'];
		$value = $usermethod['db_value_column'];
		$sql = "SELECT usernum FROM ".$this->registry->config['logtable']." WHERE username = '$user' LIMIT 1";
		$result = $this->registry->db->oneRowQuery($sql);
		$usernum = $result['usernum'];
		$sql = "SELECT $value FROM $table WHERE $match = '$usernum' AND ($active = '1' OR $active = 'true')";
		$result = $this->registry->db->query($sql);
		$x = $result->num_rows;
		for ($i=1; $i <= $x; $i++) {
			$row = $this->registry->db->getRow();
			$data[$i-1] = $row[$value];
		}
		return $data;		
	}
	
	
	
	/* LOGGING - Adds an entry to the logfile if needed and if logging is turned on */
	private function addToLog($explanation) {
		if ($this->registry->config['logging'] == TRUE) {
			$filename =  $this->registry->config['logging_location'];
			if (file_exists($filename)) {
				$file=fopen($filename,"a");
			}
			else {
				$file=fopen($filename,"w");
			}
			$today = date("Y-m-d",time());
			$now = date("H:i:s",time());
			$towrite = $today."\t".$now."\t".$explanation."\n";
			fwrite ($file, $towrite);
			fclose ($file);
			return true;
		}
		return true;
	}
	
	
	
	
	
	
	
	/*******************************************************************************************************************************************/
	/* SENDER METHODS
	/* THESE ARE THE METHODS THAT CAN BE ADDED TO IF YOU WANT TO ADD ADDITIONAL COMMUNICATIONS METHODS.
	/* FOR INSTANCE, TEXT OR FAX OR SOME OTHER METHOD
	/* YOU WOULD WRITE THE FUNCTION AND THEN REFERENCE ITS NAME IN THE logging.inc.php FILE AS 'method' WHEN CALLED FOR
	/* THESE TAKE -NO- PARAMETERS.  INSTEAD, CALL THE PROPERTIES ALREADY SET FOR YOU WITHIN THIS CLASS 
	/* (e.g., $this->premessage, $this->longmessage, $this->tempdata etc.
	/* $this->tempdata holds the actual 'data' that needs to be sent to your method as an array (e.g. an array of email addresses to send to 
	/*******************************************************************************************************************************************/

	/* emailSend() */
	/* Sends email to each recipient listed in a non-keyed array in $this->tempdata */
	private function emailSend() {
		$message = $this->premessage.$this->longmessage.$this->postmessage;
		$this->registry->mail->sendMail($this->tempdata,'',$this->subject, $message);
		return true;
	}
	
	
	
	
	
	/* END OF CLASS */

}
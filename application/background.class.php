<?php

/* BACKGROUND TASKS CLASS */

class background {

	private $registry;

	
	public function __construct($registry) {
		
		/* First, let's set the configuration so other routines can access it */
		$this->registry = $registry;
			
		/* If this script is being called remotely, kill it. */
		if (isset($_SERVER['REMOTE_ADDR'])) {
			$this->registry->security->reportAttack();
			$explanation = "ATTEMPT TO RUN BACKGROUND TASKS FROM INTERNET! FROM ".$_SERVER['REMOTE_ADDR'];
			$this->registry->logging->logEvent($this->registry->config['logging_cat_unuathaccess'], $explanation);
			$url = $this->registry->config['url'];
			header("Location: $url");
			exit();
		}
	}


	public function doBackgroundTasks() {
		/* First, select our tasks from the database */
		$sql="SELECT * FROM background_tasks WHERE isactive = 1";
		$tasks=$this->registry->db->query($sql);
		$taskrows = $tasks->num_rows;
		if ($taskrows > 0) {
			for ($i=1; $i <= $taskrows; $i++) {
				$taskarray[$i]=$this->registry->db->getRow();
			}
			for ($i=1; $i <= $taskrows; $i++) {
				$bgtasknum=$taskarray[$i]['bgtasknum'];
				$modulename=$taskarray[$i]['modulename'];
				$friendlyname=$taskarray[$i]['friendlyname'];
				$minutes=$taskarray[$i]['minutes'];
				$lastrun=$taskarray[$i]['lastrun'];
				$period='PT'.$minutes."M";
				$nextrun= new DateTime($lastrun);
				$nextrun->add(new DateInterval($period));
				$now = new DateTime("now");
				if ($nextrun<=$now) {
					$bgtaskfile=SITE_PATH."/application/bgtasks/".$modulename.".php";
					if (file_exists($bgtaskfile)) { 
						require($bgtaskfile);
						$bgtask = strtolower($modulename);
						$bgrun = new $bgtask($this->registry);
						$bgrun->doTask();
						$todays_date = date("Y-m-d H:i:s");
						$sql="UPDATE background_tasks SET lastrun='$todays_date' WHERE bgtasknum='$bgtasknum'";
						$this->registry->db->query($sql);
						$explanation = "BGTASKS ran: ".$modulename." (".$friendlyname.") successfully.";
						$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
					}
					else {
						$explanation = "BGTASKS failed to run module: ".$friendlyname." - ".$bgtaskfile." does not exist.";
						$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
					}
				}
			}
			return true;
		}
		else {
			return false;
		}
	}
}


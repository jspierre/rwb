<?php

Abstract Class baseController {

	protected $registry;
	protected $api;
    protected $model;
    protected $submodel;

	function __construct($registry) {
		$this->registry = $registry;
	}

	/**
	 * @all controllers must contain an index method
	 */
	abstract function index();

}

?>
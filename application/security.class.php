<?php

/** 
 * SECURITY CLASS
 * Handles all security functions
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
 class security {
 
	private $registry;
 
	/* Constructor */
	public function __construct($registry) {
		
		/* First, let's set the configuration so other routines can access it */
		$this->registry = $registry;
	
	}
	
	public function doSecurity($pass=true) {
		/* If Security is not enabled, we skip all checks and return TRUE */
		/* If it is enabled, we run each check that is configured and return */
		/* TRUE if ALL security checks pass and FALSE if ANY security check fails */
		/*																		 */
		/* NOTE: YOU CAN FORCE A FAIL IN YOUR CONTROLLER BY CALLING $this->registry->security->doSecurity(false); */
		/* THIS WILL LOG AN ATTACK AND SHOW A DENIED ACCESS SCREEN. */
		if($this->registry->config['security'] == TRUE) {	
		
			/* FIRST, ARE WE IN MAINTENANCE MODE? IF SO, WE NEED TO DO THAT REGARDLESS */
			$checkmode = $this->checkMode();
			$class=$this->registry->controller;
			$maintpage=$this->registry->config['maintpage'];
			if ($class==$maintpage) { $maint = true; } else { $maint = false; }
			if ($checkmode > 0 && $maint == false) {
				if (!isset($_SESSION['displaymaint'])) { $_SESSION['displaymaint'] = 0; }
				// If system is "DOWN", just stop right there and do nothing else
				if ($checkmode == 3) { exit(); }
				// If admin logins are allowed, and the maintenance page was already displayed once, let them go
				// Otherwise, call the maintenance page 
				if ($_SESSION['displaymaint'] == 0 || $checkmode > 1)  {
					$url = $this->registry->config['url']."/".$this->registry->config['maintpage'];
					if ($checkmode==1) { $_SESSION['displaymaint'] = 1; }
					header("Location: $url");
					exit();
				}
			}

			/* CHECK THE IP ADDRESS TO MAKE SURE IT IS REAL */
			if ($this->checkIP() == FALSE) { 
				return FALSE; 
			} 
			
			/*  IF THE PAGE REQUIRES THE USER TO BE LOGGED IN, LET'S MAKE SURE HE IS */
			$loginexempt = FALSE;
			foreach ($this->registry->config['loginexempt'] as $exempt) {	
				if ($this->registry->controller==$exempt) { 
					$loginexempt=TRUE;
					$c=$this->registry->controller;
					if ($this->isAllowed($c) == false) {$loginexempt = FALSE;}
				} 
			}
			
			/* First, if the page is NOT exempt from logins, let's run security to both check */
			/* that the user is logged in AND that his session is ok */
			if($loginexempt == FALSE && $this->registry->config['logins'] == TRUE) { 
				if ($this->check_session($pass,$loginexempt) == TRUE) {
					return TRUE; 
				}
				else {
					return FALSE;
				}
			}
			/* If the page IS exempt from login requirement, but a user is already logged in anyway, */
			/* We STILL need to check his security */
			else {
				if ($this->isLoggedIn()) {
						if ($this->check_session($pass,$loginexempt) == TRUE) {
							return TRUE; 
						}
						else {
							return FALSE;
						}
				}
				else {
					return TRUE;
				}
			}
		}
	}
	
	/* This function checks to see if there is a user logged in or not.  If yes, return TRUE.  If not, return FALSE */
	public function isLoggedIn() {
		if($this->registry->config['sessions'] == TRUE) {
			if (isset($_SESSION['userip']) && isset($_SESSION['username']) && isset ($_SESSION['usernum'])) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return true;
		}
	}
	
 
 	// This function checks the current run status of the entire system
 	private function checkMode() {
 		$sql = "SELECT runstatus FROM settings_system LIMIT 1";
 		$result = $this->registry->db->oneRowQuery($sql);
 		$runstatus = $result['runstatus'];
 		return $runstatus;
 	}
	

	/* SECURITY - Checks User's IP against the ban list to see if they can access */
	private function checkIP() {
		$userip = $_SERVER['REMOTE_ADDR'];
		if ($this->registry->config['devmode'] == FALSE) {
			$pri_addrs = array( '10.0.0.0|10.255.255.255', '172.16.0.0|172.31.255.255', '192.168.0.0|192.168.255.255', '169.254.0.0|169.254.255.255', '127.0.0.0|127.255.255.255');
			$long_ip = ip2long($userip);
			if($long_ip != -1) {
				foreach($pri_addrs AS $pri_addr) {
					list($start, $end) = explode('|', $pri_addr);
					if($long_ip >= ip2long($start) && $long_ip <= ip2long($end)) {
						$explanation = "User attempt to access from Private IP Address. IP Address: %%IP%%";
						$this->registry->logging->logEvent($this->registry->config['logging_cat_secipfail'],$explanation);
						return false;
					}
				}
			}
		}
		$sql = "SELECT bannumber FROM security_bans WHERE ipaddress='$userip'";
		$result = $this->registry->db->query($sql);
		if (($result->num_rows)>0) {
			$explanation = "User attempt to access from Banned IP Address. IP Address: ".$userip;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_secipban'],$explanation);
			return false;
		}
		return true;
	}
		

	/* SECURITY - Reports an attack if security is turned on */
	public function reportAttack() {
		if ($this->registry->config['security'] == TRUE) {
			/* Get IP and Set Date */
			$userip = $_SERVER['REMOTE_ADDR'];
			$dadate = date("Y-m-d H:i:s",time());
			
			/* Get max allowed attempts */
			$ipmax = $this->registry->config['security_attempts'];

			/* Pull any prior attacks */
			$sql = "SELECT attacknum, attempts FROM security_attacks WHERE ipaddress='$userip' LIMIT 1";
			$result= $this->registry->db->query($sql);
			$numrows = $result->num_rows;
				
			/* If there are priors, increase total number by one */
			if ($numrows>0) {
				$row = $result->fetch_assoc();
				$attacknum = $row['attacknum'];
				$attempts = $row['attempts'];
				$attempts++;
				$sql = "UPDATE security_attacks SET attempts='$attempts', lastattempt='$dadate' WHERE attacknum='$attacknum' LIMIT 1";
				$result= $this->registry->db->query($sql);
			}
			/* Otherwise, add to the table as new attacker */
			else {
				$attempts = 1;
				$sql = "INSERT INTO security_attacks (ipaddress, attempts, lastattempt) VALUES ('$userip', '$attempts', '$dadate')";
				$result= $this->registry->db->query($sql);
			}
				
				
			/* If the number of priors now is more than max allowed, set ban for IP address */
			if ($attempts > $ipmax) {
				$sql = "SELECT bannumber FROM security_bans WHERE ipaddress='$userip'";
				$numrows=$this->registry->db->queryGetRows($sql);
				if ($numrows==0) {
					$explanation = "NEWLY BANNED IP ADDRESS: %%IP%% ADDED TO BANNED TABLE!.";
					$this->registry->logging->logEvent($this->registry->config['logging_cat_secipbannew'],$explanation);
					$sql = "INSERT INTO security_bans ( ipaddress, bandate ) VALUES ('$userip', '$dadate')";
					$result= $this->registry->db->query($sql);
				}
			}
			
			/* It's up to the calling code to log the attack if desired, so we end here */
			return true;
		}
		return true;
	}
	
	/* This function: Checks the user's session to make sure it is still valid and hasn't expired, makes sure
	/*                the user's access level hasn't change in some way, and makes sure the user's password hasn't
	/*				  expired - IT DOES NOT PERFORM IP-BASED SECURITY CHECKS - those are handled by the checkIP() function.  */
	/* NOTE: YOU CAN FORCE A FAIL IN YOUR CONTROLLER BY CALLING $this->registry->security->doSecurity(false); */
	/* THIS WILL LOG AN ATTACK AND SHOW A DENIED ACCESS SCREEN. */
	private function check_session($pass=true, $loginexempt=false) {	
		if ($pass==false) {
			$module = $this->registry->controller;
			$explanation = "UNAUTHORIZED ACCESS ATTEMPT BY %%USER%% FROM %%IP%% MODULE: ".$module. "- FORCED DENIED BY MODULE";
			$this->registry->logging->logEvent($this->registry->config['logging_cat_acldeny'],$explanation);
			echo '<html><body>Access Denied. <a href = "'.BASE_URL.'"> Click here to continue.</body></html>';
			return false;
		}


		// FIRST, IS THE USER TRYING TO LOG IN OR OUT?  IF SO, WE CAN 
		// SKIP ALL OF THIS!
		$class=$this->registry->controller;
		if ($class == $this->registry->config['loginurl'] || $class == $this->registry->config['logouturl']) { return true;	}

		// IS THE USER TRYING TO VERIFY 2FA?  IF SO, WE SKIP THIS AS WELL!
		if ($class == 'tfa' && isset($_SESSION['twofactor_pass']) && $_SESSION['twofactor_pass'] == false) { return true; }
		
		// NEXT, DOES THE PAGE TO ACCESS EVEN EXIST?  IF NOT, WE CAN SKIP THIS AND SHOW A 404 ERROR
		$module = $this->registry->controller;
		if ($this->registry->router->routeExists() == FALSE) {
			return true;
		}
	
		// LET'S STORE WHERE THE USER WAS TRYING TO GO IN CASE HE NEEDS TO LOG IN FIRST
		// UNLESS HE WAS JUST TRYING TO GO TO THE MAIN PAGE OR IF HE'S ALREADY LOGGED IN.
		if (!$this->registry->isLoggedIn()) {
			if ($class !== $this->registry->config['firstpage'] && $class !== $this->registry->config['mainpage'] && $class !== $this->registry->config['loginurl'] && $class !== "") { 
				if($this->registry->config['ssl']) { $link = "https"; } else { $link = "http"; }			  
				$link .= "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
				$_SESSION['requestedurl'] = $link; 
				$explanation="NON-LOGGED IN USER REQUESTED SPECIFIC URL: ".$link;
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
			}
		}

		// FIRST, WE CHECK TO SEE IF ANYTHING REMOTELY RESEMBLING A SESSION
		// IS ACTIVE.  IF NOT, BACK TO THE LOGIN SCREEN FOR YOU!
		if (!isset($_SESSION['userip']) || empty($_SESSION['userip'])) {
			if ($_SESSION['returncode'] !== 2) { 
				if (isset($_SESSION['requestedurl'])) { $requestedurl = $_SESSION['requestedurl']; } else { $requestedurl = false; }
				session_destroy(); 
				session_start();
				if ($requestedurl) { $_SESSION['requestedurl'] = $requestedurl; }
			}
			$url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
			header("Location: $url");
			exit();
			}
		
		// NOW WE CHECK TO SEE IF THE SESSION IP ADDRESS
		// MATCHES THE CURRENT IP ADDRESS.  IF NOT,
		// BACK TO THE LOGIN SCREEN FOR YOU!
		$userip = $_SERVER['REMOTE_ADDR'];
		if ($userip <> $_SESSION['userip']) {
			$explanation="POSSIBLE SPOOF ATTEMPT ON SESSION FOR ".$_SESSION['username']." - IP expected: ".$_SESSION['userip'].", got: ".$userip;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_secipfail'],$explanation);
			$this->updateSession(false);
			session_destroy();
			$url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
			header("Location: $url");
			exit();
		}
		

		// OK, SO FAR SO GOOD. NOW, LET'S CHECK THE SESSION
		// TIME TO SEE IF WE'VE EXPIRED.
		$sessionmax=$this->registry->config['sessiontime'];
		$dadate = date("Y-m-d H:i:s",time());
		$now = new DateTime($dadate);
		$then = new DateTime($_SESSION['usertime']);
		$diff = $then->diff($now);
		$totalmins = ((($diff->d)*1440)+(($diff->h)*60)+($diff->i));
		if ($totalmins >= $sessionmax) {
			$explanation="LOGOUT DUE TO EXPIRE OF SESSION FOR ".$_SESSION['username']." FROM ".$_SESSION['userip'];
			$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
			$tfa = new mTwoFactor($this->registry);
			$usernum = $_SESSION['usernum'];
			$tfa->invalidate2FA($usernum);
			$this->updateSession(false);
			session_destroy();
			session_start();
			$_SESSION['returncode'] = 17;
			$url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
			header("Location: $url");
			exit();
		}
		
		// NOW WE CHECK TO SEE IF THE USER IS STILL ACTIVE, IS NOT BEING FORGED, AND NOT LOCKED OUT
		// WE'LL ALSO PULL HIS ACL FOR USE IN THE NEXT STEP
		$usernum=$_SESSION['usernum'];
		//$sql = "SELECT username, passexpires, useractive, userlocked, aclgroup FROM ".$this->registry->config['logtable']." WHERE usernum='$usernum' LIMIT 1";
		$sql = "SELECT logins_users.username, logins_users.passexpires, logins_users.useractive, logins_users.userlocked, logins_users.forcelogout,
		logins_users.aclgroup, logins_users.showsysbanner, security_acl_groups.isadmin, security_acl_groups.isactive AS aclgroupactive 
		FROM logins_users LEFT JOIN security_acl_groups ON logins_users.aclgroup = security_acl_groups.groupindex WHERE 
		logins_users.usernum=$usernum LIMIT 1";
		$result=$this->registry->db->query($sql) or die ('ERROR: DB ACCESS ERROR IN SESSION CHECK METHOD');
		$row=mysqli_fetch_array($result);
		$username = $row['username'];
		$active = $row['useractive'];
		$lockedout = $row['userlocked'];
		$empacl = $row['aclgroup'];
		$pwexpires=$row['passexpires'];
		$forcelogout=$row['forcelogout'];
		$aclgroupactive=$row['aclgroupactive'];
		$isadmin=$row['isadmin'];
		$showsysbanner = $row['showsysbanner'];
		if (empty($row['deptisactive'])) { $deptisactive = 1; } else { $deptisactive=$row['deptisactive']; }

		//If the user is being forced to logout, let's do that now
		if ($forcelogout == 1) {
			$sql = "UPDATE logins_users SET logins_users.forcelogout = 0 WHERE logins_users.usernum=$usernum LIMIT 1";
			$result=$this->registry->db->query($sql) or die ('ERROR: DB ERROR WHILE UPDATING FORCED LOGOUT FOR USER');
			$explanation="LOGOUT DUE TO FORCED LOGOUT FOR ".$_SESSION['username']." FROM ".$_SESSION['userip'];
			$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
			$this->updateSession(false);
			session_destroy();
			session_start();
			$_SESSION['returncode'] = 17;
			$url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
			header("Location: $url");
			exit();
		}

		// If maintenance mode is active and the user is not an admin, let's log them off
		$checkmode = $this->checkMode();
		if ($checkmode > 0) {
			if (($checkmode == 1 && $isadmin == 0) || $checkmode > 1) {
				$explanation="LOGOUT DUE TO SYSTEM MAINTENANCE FOR ".$_SESSION['username']." FROM ".$_SESSION['userip'];
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				$this->updateSession(false);
				session_destroy();
				session_start();
				$_SESSION['returncode'] = 17;
				$url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
				header("Location: $url");
				exit();
			}
		}

		// Let's check for the God User and set it in the registry
		$sql = "SELECT goduser FROM settings_system WHERE settingindex = 1 LIMIT 1";
		$result=$this->registry->db->query($sql);
		if ($result) {
			$row = $this->registry->db->getRow();
			if ($row['goduser']=="" || empty($row['goduser'])) {
				$goduser = 0;
			}
			else {
				$goduser = $row['goduser'];
			}
			if ($goduser == $usernum) {
				$isgod = true;
				$this->registry->isgod = true;
				$this->registry->goduser = $goduser;
			}
			else {
				$isgod = false;
				$this->registry->isgod = false;
				$this->registry->goduser = $goduser;
			}
		}



		/* If the username in the session and the username pulled from the database don't match, this is an attack. */
		if ($username <> $_SESSION['username']) {
			$explanation="POSSIBLE SPOOF ATTEMPT ON SESSION FOR ".$_SESSION['username']." - Username Expected for User # ".$usernum.": ".$username.", but got: ".$_SESSION['username'];
			$this->registry->logging->logEvent($this->registry->config['logging_cat_secsessfail'],$explanation);
			$this->updateSession(false);
			session_destroy();
			$url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
			header("Location: $url");
			exit();
		}
		if (($active == 0 || $lockedout == 1 || $aclgroupactive == 0 || $deptisactive == 0) && !$isgod) {
		if ($active == 0) {
			$reason="ACCOUNT INACTIVE";
			$logcat = 'logging_cat_userinactive';
		}
		elseif ($lockedout == 1) {
			$reason="LOCKED OUT";
			$logcat = 'logging_cat_userlocked';
		}
		elseif ($aclgroupactive == 0) {
			$reason="ACL GROUP DEACTIVATED";
		}
		elseif ($deptisactive == 0) {
			$reason="DEPARTMENT DEACTIVATED";
		}
		$explanation = "ATTEMPT TO LOG IN WHILE ".$reason. ". USER ".$_SESSION['username']." FROM ".$_SESSION['userip'];
		$this->registry->logging->logEvent($this->registry->config[$logcat],$explanation);
		$this->updateSession(false);
		session_destroy();
		session_start();
		$_SESSION['returncode'] = "2";
		$url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
		header("Location: $url");
		exit();
		}
		
		// OKAY, EVERYTHING IS ALMOST PERFECT, NOW IF THE TIME TO CHECK TO SEE IF THE USER HAS 2FA ACTIVE
		// AND IF THEY NEED TO VERIFY THAT RIGHT NOW OR NOT. OF COURSE, IF 2FA IS NOT TURNED ON FOR THE SYSTEM,
		// WE SKIP THIS STEP
		if ($this->registry->config['twofactor'] == TRUE && $this->registry->config['logins'] == TRUE) {
			if (!isset($_SESSION['twofactor_pass']) || $_SESSION['twofactor_pass'] == false) {
					$tfa = new mTwoFactor($this->registry);
					if ($tfa->check2FANeeded($usernum)) {
						$url = $this->registry->config['url']."/tfa/verify";
						header("Location: $url");
						exit();
					}
			}
		}

		// OUR NEXT CHECK WILL BE TO OBTAIN THE USER'S ACL GROUP AND MAKE SURE HE
		// STILL HAS ACCESS TO THE CONTROLLER HE'S TRYING TO ACCESS
		// WE DO THAT BY GRABBING THE ACL REQUIRED AND STATUS FOR THIS MODULE NAME
		// AND COMPARING IT TO THE ONE WE JUST STORED.
		if ($this->registry->config['acl'] == TRUE) {
			if ($this->checkACL() == FALSE && !$isgod) {
				echo '<html><body>Access Denied. <a href = "'.BASE_URL.'"> Click here to continue.</body></html>';
				return false;
			}
		}


		// ONE FINAL CHECK IS THIS USER'S PASSWORD EXPIRED??
		// IF SO, LET'S GET THAT UPDATED
		// IF THE USER IS ALREDY TRYING TO CHANGE HIS PASSWORD, LET'S NOT SEND HIM TO CHANGE IT AGAIN - THAT WOULD CREATE AN ENDLESS LOOP!
		// ALSO, IF THE USER JUST WANTS TO ACCESS A LOGIN-EXEMPT PAGE (LIKE T&Cs), LET'S NOT FORCE HIM TO CHANGE PASSWORD, THAT WOULD BE UNETHICAL
		if (($this->registry->config['pwexpires'] !== FALSE) && ($this->registry->controller != $this->registry->config['pwchgurl'])) {
			$pageRequest = $_SERVER['REQUEST_URI'];
			$pageArray = explode('/', $pageRequest);
			$class=$this->registry->controller;
			$pwchg=$this->registry->config['pwchgurl'];
			if ($class <> $pwchg && ($loginexempt == false || $class == $this->registry->config['firstpage'])) {
				$todayday = date("Y-m-d", time());
				$now = new DateTime($todayday);
				$then = new DateTime($pwexpires);
				if ($now >= $then && !$isgod){
					$explanation = "PASSWORD EXPIRED - SENT TO UPDATE FOR ".$_SESSION['username']." FROM ".$_SESSION['userip'];
					$this->registry->logging->logEvent($this->registry->config['logging_cat_userpass'],$explanation);
					$_SESSION['usertime'] = $dadate;
					$_SESSION['returncode'] = 10;
					$url = $this->registry->config['url']."/".$this->registry->config['pwchgurl'];
					header("Location: $url");
					exit();
				}
			}
		}
			
			// If the user has a system banner to see, let's get that now
	        if ($showsysbanner == 1) {
	            $sql = "SELECT systembanner FROM settings_system LIMIT 1";
	            $result = $this->registry->db->oneRowQuery($sql);
	            $systembanner = $result['systembanner'];
	            $sql = "UPDATE logins_users SET showsysbanner = 0 WHERE usernum = $usernum LIMIT 1";
	            $result=$this->registry->db->query($sql);
	            $_SESSION['systembanner'] = $systembanner;
	            $explanation = "SHOWED SYSTEM BANNER TO USER %%USER%% FROM %%IP%%: ".$systembanner;
				$this->registry->logging->logEvent($this->registry->config['logging_cat_success'],$explanation);
	        }

			// ALRIGHT, EVERYTHING CHECKS OUT!  LET'S LOG IT
			// SET SOME VARIABLES, EXTEND THE SESSION TIME
			// FOR THE USER AND GIVE CONTROL BACK TO THE MAIN 
			// PROGRAM
			$_SESSION['usertime']=$dadate;
			// $module = basename($_SERVER['REQUEST_URI']);
			$module = $this->registry->controller;
			if ($module=="") {$module = $this->registry->config['firstpage'];}
			$explanation = "CONTROLLER ACCESS ALLOWED - USER %%USER%% FROM %%IP%% CONTROLLER: ".$module;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_success'],$explanation);
			$this->updateSession(true,$module);
			return true;
			
	}
	
	/************************************************************************************************************/
	/* METHOD:	updateSession($alive)																			*/
	/* PURPOSE:	Updates the User Session Table with whether the user's session is still alive or not 			*/
	/* INPUT:	$alive - TRUE if session is still alive, FALSE if not											*/
	/* RETURNS:	TRUE if udpate succeeded; FALSE if it did not 													*/
	/************************************************************************************************************/
	public function updateSession($alive, $module="-none-") {
		if ($alive) { $isalive = 1; } else { $isalive = 0; }
		if (isset($_SESSION['usernum'])) { $usernum = $_SESSION['usernum']; } else { return false; }
		$usertime = $_SESSION['usertime'];
		$userip = $_SESSION['userip'];
		$sql = "SELECT * FROM logins_users_session WHERE usernum = $usernum LIMIT 1";
		$result = $this->registry->db->queryGetRows($sql);
		if ($result == 0) {
			$sql = "INSERT INTO logins_users_session (usernum, sessionstart, ipaddress, module, isalive) VALUES 
			($usernum, '$usertime', '$userip', '$module', $isalive)";
		} 
		elseif ($result == 1) {
			$sql = "UPDATE logins_users_session SET sessionstart = '$usertime', ipaddress = '$userip', module = '$module', isalive = $isalive WHERE usernum = $usernum LIMIT 1";
		}
		else {
			return false;
		}
		$result = $this->registry->db->query($sql);
		if ($result) { return true; } else { return false; }
	}




	
	/************************************************************************************************************/
	/* METHOD:	checkACL()																						*/
	/* PURPOSE:	Checks the ACL to see if the user has access to the requested Controller 						*/
	/* INPUT:	None																							*/
	/* RETURNS:	TRUE if user has access; FALSE if user does not have access 									*/
	/************************************************************************************************************/	
	public function checkACL() {
		// God user skips all checks
		if ($this->registry->isgod) {
			return true;
		}

		// Get the controller name
		$module = $this->registry->controller;

		/* If the module is exempt from ACL checking (login/logout/main or Login exempt pages), stop here */
		if ($module == $this->registry->config['loginurl'] || $module == $this->registry->config['logouturl'] || $module == $this->registry->config['mainpage'] || array_search($module,$this->registry->config['loginexempt']) !== false ) {
			$exempt=true;
		}
		else {
			$exempt=false;
		}

		/* PULL THE MODULE INFORMATION */
		$sql = "SELECT moduleindex, allowall, denyall FROM ".$this->registry->config['acl_modules']." WHERE controller = '$module' LIMIT 1";
		$result = $this->registry->db->query($sql);
		$numrows = $result->num_rows;

		/* If the module doesn't have an ACL entry, AND it actually exists, don't allow access (otherwise allow for 404 error) */
		if ($numrows == 0 && $exempt==false) {
			$explanation = "UNAUTHORIZED ACCESS ATTEMPT BY %%USER%% FROM %%IP%% - NO RULES EXIST - MODULE: ".$module;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_acldeny'], $explanation);
			return FALSE;
		}
		

		/* If the module does have an ACL entry, find out if Allow-All or Deny-All are set */
		$row=$this->registry->db->getRow();
		$modulenum = $row['moduleindex'];
		$allowall = $row['allowall'];
		$denyall = $row['denyall'];

		/* If allowall is set, let's set the exempt variable back to TRUE */
		if ($allowall == 1 && $denyall == 0) {
			$exempt=true;
		}
		
		
		/* If Deny-All is set, log it and return false */
		if ( $denyall == 1 ) {
			$explanation = "ATTEMPT TO ACCESS DENY-ALL MODULE BY ".$_SESSION['username']." FROM ".$_SESSION['userip']." MODULE: ".$module;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_acldeny'], $explanation);
			return false;
		}
		
	
		/* If neither of those apply, see if the user has an ACL Set. */
		$usernum = $_SESSION['usernum'];
		$sql = "SELECT ".$this->registry->config['logtable'].".aclgroup, ".$this->registry->config['acl_groups'].".groupname, ".$this->registry->config['acl_groups'].".isactive FROM ".$this->registry->config['logtable']." LEFT JOIN ".$this->registry->config['acl_groups']." ON ".$this->registry->config['logtable'].".aclgroup = ".$this->registry->config['acl_groups'].".groupindex WHERE usernum = '$usernum' LIMIT 1";
		$acl = $this->registry->db->oneRowQuery($sql);

		/* If an ACL Group is not set, fail and return false */
		if (is_null($acl['aclgroup']) && $exempt==false) {
			$explanation = "USER HAS NO ACL SET - ACCESS DENIED TO %%USER%% FROM %%IP%% MODULE: ".$module;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_aclmissing'],$explanation);
			return false;
		}
		
		/* If ACL Group is not active, fail and return false */
		if ($acl['isactive'] != 1 && !$exempt) {
			$explanation = "ATTEMPT TO ACCESS BUT ACL GROUP IS NOT ACTIVE FOR %%USER%% FROM %%IP%% MODULE: ".$module;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_aclmissing'],$explanation);
			return false;
		}
		
		/* Pull any Allow Access rules about the user's group */
		$groupnum=$acl['aclgroup'];
		$sql = "SELECT * FROM ".$this->registry->config['acl_access']." WHERE groupnum = '$groupnum' AND modulenum = '$modulenum' LIMIT 1";
		$result = $this->registry->db->query($sql);
	
		/* If there are no Allow rules then access is denied */
		if ($result->num_rows < 1 && $exempt==false) {
			$groupname = $acl['groupname'];
			$explanation = "UNAUTHORIZED ACCESS ATTEMPT BY %%USER%% FROM %%IP%% MODULE: ".$module.", ACL GROUP: ".$groupname;
			$this->registry->logging->logEvent($this->registry->config['logging_cat_acldeny'],$explanation);
			return false;
		}
		
		/* If there is an Allow rule, but the rule is not active, access is denied */
		if ($result->num_rows > 0) { 
			$access = $this->registry->db->getRow();
			if ($access['ruleactive'] == 0 && $exempt==false) {
				$explanation = "UNAUTHORIZED ACCESS ATTEMPT BY %%USER%% FROM %%IP%% MODULE: ".$module.", ACL GROUP: ".$groupname;
				$this->registry->logging->logEvent($this->registry->config['logging_cat_acldeny'],$explanation);
				return false;
			}
			else {
				$this->getACLFunctions($groupnum, $modulenum);
				return true;
			}
		}
		else {
				$this->getACLFunctions($groupnum, $modulenum);
				return true;
		}
		
		/* If nothing else matches and we fall through to here, deny access */
		return false;
	}

	public function getACLFunctions($groupnum, $modulenum=1) {
		// This method pulls the granular details of module access for use by the module itself if needed.
		// It stores any allowed 'methods' in a session variable that the module itself can access
		if (isset($_SESSION['acl_functions'])) {
			unset($_SESSION['acl_functions']);
		}
		if (!is_numeric($modulenum) || empty($modulenum)) { $modulenum =1; }
		// First, let's get the functions everyone has access to:
		$sql = "SELECT * FROM security_acl_functions WHERE functionactive = 1 AND allowall = 1";
		$result = $this->registry->db->query($sql);
		$numrows = $result->num_rows;
		if ($numrows == 0) {
			$_SESSION['acl_functions'] = false;
		}
		else {
			$rows = $this->registry->db->getAllRows();
			foreach ($rows as $row) {
				$func=$row['funcname'];
				$aclfunc[$func]=true;
			}
			$_SESSION['acl_functions'] = $aclfunc;
		}
		$sql = "SELECT security_acl_functions_access.*, security_acl_functions.* FROM security_acl_functions_access LEFT JOIN security_acl_functions ON security_acl_functions_access.functionnum = security_acl_functions.funcnum WHERE (security_acl_functions_access.groupnum = $groupnum OR security_acl_functions.allowall = 1) AND (security_acl_functions.modulenum = $modulenum OR security_acl_functions.modulenum = 1) AND security_acl_functions_access.ruleactive = 1 AND security_acl_functions.functionactive = 1";
		//$sql = "SELECT $funcrules.*, $functions.* FROM $funcrules LEFT JOIN $functions ON $funcrules.functionnum = $functions.funcnum WHERE ($funcrules.groupnum = $groupnum OR $functions.allowall = 1) AND ($functions.modulenum = $modulenum OR $functions.modulenum = 1) AND $funcrules.ruleactive = 1 AND $functions.functionactive = 1";
		$result = $this->registry->db->query($sql);
		$numrows = $result->num_rows;
		if ($numrows > 0) {
			$rows = $this->registry->db->getAllRows();
			foreach ($rows as $row) {
					$func=$row['funcname'];
					$aclfunc[$func]=true;
			}
			$_SESSION['acl_functions'] = $aclfunc;
		}
		return true;
	}


	public function sanitizePost($keephtml=false) {
	    if ($_POST) { 
			foreach ($_POST as $key => $val) {
				if(is_array($val)) {
					foreach ($val as $key2 => $val2) {
						$val2 = trim($val2);
			    		$val2 = $this->registry->db->sanitize($val2);
				    	$_POST[$key][$key2] = $val2;
					}
				}
				else {
		    		$val = trim($val);
		    		$val = $this->registry->db->sanitize($val);
			    	$_POST[$key] = $val;
			    }
    	 	}
			//if ($keephtml == false ) { 
			//	$_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS); }
    		return true;
		}
		return false;
	}

	public function sanitizeArray($data, $keephtml=false) {
		foreach ($data as $key => $val) {
	    	if (!is_array($data[$key])) {
	    		$val = trim($val);
	    		$val = $this->registry->db->sanitize($val);
		    	$data[$key] = $val;
		    }
		    //if ($keephtml == false) { $cleandata = filter_var_array($data, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS ); }
	     }
	    return $data;
	}

	public function sanitizeString($string, $keephtml=false) {
		$string = trim($string);
		$string = $this->registry->db->sanitize($string);
		//if ($keephtml == false) { $cleandata = filter_var($string, FILTER_SANITIZE_STRING); }
		return $string;
	}

	public function unSanitize($string) {
		//$string2 = str_replace(array("\r\n", "\r", "\n"), "<br />", "$string");
		$string2 = stripslashes("$string");
		return $string2;
	}


	public function upperPost() {
		// This function takes every item in a POST and changes all strings to UPPER CASE.  
		// Be careful with this.  If you are using comparisons on mixed case, you will have
		// Problems.
	    foreach ($_POST as $key => $val) {
	        if (is_string($_POST[$key])) { $_POST[$key] = strtoupper($val); }
	     }
	    return true;
	}

	public function upperArray($data) {
		// This function takes every item in an array of key=>value data and changes all 
		// strings to UPPER CASE.  
		// Be careful with this.  If you are using comparisons on mixed case, you will have
		// Problems.
	    foreach ($data as $key => $val) {
	        if (is_string($data[$key])) { $data[$key] = strtoupper($val); }
	     }
	    return $data;	
	}
	
	public function isAllowed($module) {
		// This function takes a module name and returns FALSE if the "DenyAll" flag is set and TRUE otherwise
		$sql = "SELECT moduleindex, allowall, denyall FROM ".$this->registry->config['acl_modules']." WHERE controller = '$module' LIMIT 1";
		$result = $this->registry->db->query($sql);
		$numrows = $result->num_rows;

		/* If nothing is returned, we can't determine if allowed or not, so we default to DENIED */
		if ($numrows == 0) { return false; }
		
		/* Pull the row from the database and get the actual data */
		$row=$this->registry->db->getRow();
		$modulenum = $row['moduleindex'];
		$allowall = $row['allowall'];
		$denyall = $row['denyall'];

		/* If denyall is set, it is denied, else we allow */
		if ($denyall == 1) { return false; }
		
		return true;
	}
	
	// Does a quick check to see if the user is allowed to use a certain function within a module.
	// Returns true if yes, false if no.  This can easily be done manually by checking the $_SESSION['acl_functions']
	// Variable, but this is a quicker method to do that.
	public function checkFunction($funcname) {
		$funcname = strtolower($funcname);
		if (isset($_SESSION['acl_functions'][$funcname]) || $this->registry->isgod) { return true; }
		else { return false; }
		return false;
	}

	// Pulls the username, first, last name, and email address of the usernum given, or returns FALSE if invalid
	// result will be $result['username'], $result['firstname'], $result['lastname']
	public function getUserName($usernum=false) { 
		if ($usernum) {
			$sql = "SELECT logins_users.username, logins_users_details.firstname, logins_users_details.lastname, 
			logins_users_details.emailaddress FROM logins_users LEFT JOIN logins_users_details ON
			logins_users.usernum = logins_users_details.usernum WHERE logins_users.usernum = $usernum LIMIT 1";
			$result = $this->registry->db->oneRowQuery($sql);
			if ($result) { return $result; } else { return false; }
		}
		else {
			return false;
		}
	}

	// Get users per function - returns a list of usernum, username, first name, last name, and emailaddress 
	// of all users who have access to the given function.  Returns false on error or if none.
	public function getUserPerFunction($funcname=false) {
		if ($funcname == false) { return false; }
		$sql = "SELECT security_acl_functions_access.*, security_acl_functions.*, logins_users.usernum, logins_users.username, 
			logins_users_details.firstname, logins_users_details.lastname, logins_users_details.emailaddress FROM
			security_acl_functions_access LEFT JOIN security_acl_functions ON security_acl_functions_access.functionnum =
			security_acl_functions.funcnum LEFT JOIN logins_users ON security_acl_functions_access.groupnum = 
			logins_users.aclgroup LEFT JOIN logins_users_details ON logins_users.usernum = logins_users_details.usernum
			WHERE funcname = '$funcname' AND logins_users.usernum IS NOT NULL";
		$result = $this->registry->db->query($sql);
		if ($result && $result->num_rows > 0) {
			$allusers = $this->registry->db->getAllRows();
			return $allusers;
		}
		else {
			return false;
		}
	}


/* END OF CLASS */
}

?>
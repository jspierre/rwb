<?php

Class registry {

	/*** SETS $registry->config with all configuration variables ***/
	/*** allows set and get of 'global' variables without globals ***/

	private $vars = array();
	
	public $baseURL;
	public $cc;
	public $config = array();
	public $controller;
	public $db;
	public $logging;
	public $pageArray;
	public $security;
	public $passedVars;
	public $isgod;
	public $goduser;	



	public function __construct() {
		require (SITE_PATH.'/includes/configurator.php');
		$this->config = $app_config;
		date_default_timezone_set($this->config['timezone']);
		$this->baseURL = $this->config['baseurl'];
		$this->isgod = false;
	}
 

 
	public function __set($index, $value) {
        $this->vars[$index] = $value;
	}


	public function __get($index) {
        return $this->vars[$index];
	}
	
	public function getDateTime() {
		$currentDate = new DateTime("now");
		$formattedDate = $currentDate->format('Y-m-d H:i:s');
		return $formattedDate;
	}
	
	public function goHome() {
		$url = $this->config['url'];
		header("Location: $url");
		exit();
	}

	public function redirect($controller) {
		$url = $this->config['url']."/".$controller;
		header("Location: $url");
		exit();
	}

	public function goPostLogout() {
		// This controls where the user goes after a successful logout.
		$preurl = $this->config['url'];
		$postlogout = $this->config['postlogout'];
		$url = $preurl."/".$postlogout;
		header("Location: $url");
		exit();	
	}

	public function isLoggedIn() {
		// Returns true if current user is logged in, or false if not
		if (isset($_SESSION['userip']) && isset($_SESSION['username']) && isset($_SESSION['usernum'])) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
}

?>
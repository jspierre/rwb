<?php

/** 
 * EMAIL CLASS
 * Handles all external emailing functions
 * @author John St. Pierre
 * @ver 0.1
 *
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
class email {
 
	private $registry;
 
	/* Constructor */
	public function __construct($registry) {
		
		/* First, let's set the configuration so other routines can access it */
		$this->registry = $registry;
	
	}
	
	
	/********************************************************************************************************************************************/
	/* Function to Send NON-HTML email.  																										*/
	/*		The ONLY required field is $to 																										*/
	/*		$to - either a single address or an array of email addresses to send to 															*/
	/*		$toname - either a single name or array of names that match the email addresses you are sending to in the same order in the array 	*/
	/********************************************************************************************************************************************/
	public function sendMail($to,$toname=FALSE,$subject=" ",$body=" ",$from=FALSE,$fromname=FALSE,$attachment=FALSE) {
		require_once($this->registry->config['smtp_loc'])."/Exception.php";	
		require_once($this->registry->config['smtp_loc'])."/PHPMailer.php";	
		require_once($this->registry->config['smtp_loc'])."/SMTP.php";	
		$mail = new PHPMailer(true);
		$mail->IsSMTP(); 													// telling the class to use SMTP
		try {
			$mail->Host       = $this->registry->config['smtp_server']; 	// SMTP server
			$mail->SMTPDebug  = 0;                     						// enables SMTP debug information (for testing set to 2, else use 0)
			$mail->SMTPAuth   = $this->registry->config['smtp_auth'];		// enable SMTP authentication
			$mail->Port       = $this->registry->config['smtp_port'];		// set the SMTP port for the server
			$mail->XMailer	  = $this->registry->config['smtp_xmailer'];	// Set the X-Mailer Header in the email
			
			/* If Authorization was enabled, get auth info */
			if ($this->registry->config['smtp_auth'] == TRUE) {
				$mail->Username   = $this->registry->config['smtp_user'];	// SMTP account username
				$mail->Password   = $this->registry->config['smtp_pass'];	// SMTP account password
			}
		  
	
			/* Set up the From, Subject, Body and any Attachments */
			if ($from == FALSE) { $from = $this->registry->config['smtp_from']; }
					if ($fromname == FALSE) { $fromname = $this->registry->config['smtp_frname']; }
					$mail->SetFrom($from, $fromname);
		    		$mail->Subject = $subject;
					$mail->Body = $body;
					if ($attachment !== FALSE) {
					if (is_array($attachment)) {
						foreach ($attachment as $file) {
							$mail->AddAttachment($file);      
						}
					}
						else {
							$mail->AddAttachment($attachment); 
						}
					}
			
			/* Now, loop through each recipient and send a separate email for each */
			if (!is_array($to)) { $to=array($to); }
			$x=0;
			foreach($to as $recipient) {
				if(!is_array($toname)) {
					$toname=array($toname);
				}
				
				if (isset($toname[$x])) {
					$name = $toname[$x];
				}					
				else {
					$name = "";
				}
				$mail->ClearAllRecipients();
				$mail->AddAddress($recipient, $name);
				$mail->Send();
			}
			
			/* All Done! */
			return true;
		} 
		catch (phpmailerException $e) {
			echo $e->errorMessage(); 
			return false;
		} 
		catch (Exception $e) {
			echo $e->getMessage();   
			return false;
		}	
	}
	
	/********************************************************************************************************************************************/
	/* Function to Send HTML email.  																											*/
	/* NOTE: *** FOR A NICE HTML EMAIL BOILERPLATE TO USE THAT WORKS WITH MOST CLIENTS, SEE /thirdparty/emailboilerplate						*/
	/*		The ONLY required field is $to 		-  You DO NOT need specify an $altbody, PHPMailer will create one automatically for you			*/
	/*		$to - either a single address or an array of email addresses to send to 															*/
	/*		$toname - either a single name or array of names that match the email addresses you are sending to in the same order in the array 	*/
	/********************************************************************************************************************************************/
	public function sendHTMLMail($to,$toname=FALSE,$subject=" ",$body=" ",$from=FALSE,$fromname=FALSE,$attachment=FALSE,$altbody=FALSE) {
		require_once($this->registry->config['smtp_loc'])."/Exception.php";	
		require_once($this->registry->config['smtp_loc'])."/PHPMailer.php";	
		require_once($this->registry->config['smtp_loc'])."/SMTP.php";	
		$mail = new PHPMailer(true);
		$mail->IsSMTP(); 													// telling the class to use SMTP
		try {
			$mail->Host       = $this->registry->config['smtp_server']; 	// SMTP server
			$mail->SMTPDebug  = 0;                     						// enables SMTP debug information (for testing)
			$mail->SMTPAuth   = $this->registry->config['smtp_auth'];		// enable SMTP authentication
			$mail->Port       = $this->registry->config['smtp_port'];		// set the SMTP port for the server
			$mail->XMailer	  = $this->registry->config['smtp_xmailer'];	// Set the X-Mailer Header in the email
			
			/* If Authorization was enabled, get auth info */
			if ($this->registry->config['smtp_auth'] == TRUE) {
				$mail->Username   = $this->registry->config['smtp_user'];	// SMTP account username
				$mail->Password   = $this->registry->config['smtp_pass'];	// SMTP account password
			}
			
			/* Set up the From, Subject, Body and any Attachments */
			if ($from == FALSE) { $from = $this->registry->config['smtp_from']; }
			if ($fromname == FALSE) { $fromname = $this->registry->config['smtp_frname']; }
			$mail->SetFrom($from, $fromname);
			$mail->Subject = $subject;
			if ($altbody !== FALSE) {
				$mail->AltBody = $altbody;
			}
			$mail->MsgHTML($body);
			if ($attachment !== FALSE) {
				if (is_array($attachment)) {
					foreach ($attachment as $file) {
						$mail->AddAttachment($file);      
					}
				}
				else {
					$mail->AddAttachment($attachment); 
				}
			}
		  
			
			/* Now, loop through each recipient and send a separate email for each */
			if (!is_array($to)) { $to=array($to); }
			$x=0;
			foreach($to as $recipient) {
				if(!is_array($toname)) {
					$toname=array($toname);
				}
				
				if (isset($toname[$x])) {
					$name = $toname[$x];
				}					
				else {
					$name = "";
				}
				$mail->ClearAllRecipients();
				$mail->AddAddress($recipient, $name);
				$mail->Send();
			}
			
			/* All Done! */
			return true;
		}
		catch (phpmailerException $e) {
			//echo $e->errorMessage(); 
			return false;
		} 
		catch (Exception $e) {
			//echo $e->getMessage();   
			return false;
		}	
	}
	

}


    



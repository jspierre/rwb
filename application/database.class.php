<?php
/** 
 * MAIN DATABASE FILE
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
class database {

	/* Private Variables */
	private $dbconfig;
	private $encrypted;
	private $salty;
	private $salt;
	private $seasoned;
	private $pepper;
	private $connected;
	private $dbc;
	private $registry;
	public $result;
		
	/* CONSTRUCTOR - Gets the database setup values passed to it, Sets the Pepper for salting from the Config File */
	public function __construct($registry) {
		$this->registry = $registry;
		$this->dbconfig = $registry->config;	
		$this->pepper = $this->dbconfig['pepper'];
		$this->connected = FALSE;
	}
	

	/**
	 * @function	connect()		Makes a database connection
	 * @param 		none
	 * @return		handle $dbase	Database Connection Link
	 *
	 */
	protected function connect() {
		$this->dbc=new mysqli($this->dbconfig['dbhost'], $this->dbconfig['dbuser'], $this->dbconfig['dbpass'], $this->dbconfig['dbname']);
		if ($this->dbc->connect_error) {
			die ('Could not establish a database connection. Error No. '.$this->dbc->connect_errno.', Msg: '.$this->dbc->connect_error);
		}
		$this->connected = TRUE;
		return true;
	}
	
	/**
	 * @function	oneRowQuery($sql)	Performs a Query and Returns a Single Row from the Result 
	 * @param		str $sql			The SQL SELECT statement to execute 
	 * @returns		mixed $result		Returns an assoc array containing the selected row or FALSE if nothing was returned
	 *
	 */	
	public function oneRowQuery($sql) {
		$this->connect();
		$result=$this->dbc->query($sql);
		$longresult = $this->getdump($result);
		$dblog = "\nSQL PASSED:\n".$sql."\n\nSQL RESPONSE:\n".$longresult."\n\n";
		$this->dblog($dblog);
		if ($result) {
			if ($result->num_rows > 0) {
				$this->dbc->close();
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function query($sql) {
		$this->connect();
		$this->result=$this->dbc->query($sql);
		$result=$this->result;
		$longresult = $this->getdump($result);
		$dblog = "\nSQL PASSED:\n".$sql."\n\nSQL RESPONSE:\n".$longresult."\n\n\\n";
		$this->dblog($dblog);
		return $this->result;
	}

	public function multiQuery($sql) {
		$this->connect();
		$this->result=$this->dbc->multi_query($sql);
		$result=$this->result;
		$longresult = $this->getdump($result);
		$dblog = "\nSQL PASSED:\n".$sql."\n\nSQL RESPONSE:\n".$longresult."\n\n\\n";
		$this->dblog($dblog);
		return $this->result;
	}

	
	public function getRow() {
		return $this->result->fetch_assoc();
	}
	
	public function getAllRows() {
		if ($this->result->num_rows > 0) {
			for ($i=1; $i<= $this->result->num_rows; $i++) {
				$resultArray[$i] = $this->getRow();
			}
			return $resultArray;
		}
		else {
			return false;
		}
	}
	
	public function getAffected() {
		return $this->dbc->affected_rows;
	}
	
	public function getInsertId() {
		return $this->dbc->insert_id;
	}
	
	public function queryGetRows($sql) {
		$this->connect();
		$result=$this->dbc->query($sql);
		$this->dbc->close();
		if ($result) {
			return $result->num_rows;	
		}
		else {
			return 0;
		}
	}
	
	public function sanitize($dirtystring) {
		$this->connect();
		$cleanstring=mysqli_real_escape_string($this->dbc,$dirtystring);
		$this->dbc->close();
		return $cleanstring;
	}


	public function validateEmail($email) {
		// This function performs several validity tests on an email
		// address before accepting it as an actual email address
		// Returns the sanitized email addy if email checks out, false if not
		$chkemail = filter_var($email, FILTER_SANITIZE_EMAIL);
		$userip=$_SERVER['REMOTE_ADDR'];
		if (!filter_var($chkemail, FILTER_VALIDATE_EMAIL)) { return false;	}
		return $chkemail;
	}
	

	/* This is a helper function for the encrypt password methods.  Leave it alone */
	private function season($password) {
		$this->seasoned = $this->salt.$password.$this->pepper;
	}
	
	/* 	FUNCTION	->encrypt($password)
		REQUIRES	$password - The password  to be encrypted
		RETURNS		An array containing the SHA512-encrypted password ['pw'] and the salt ['salt']
		PURPOSE		Provides the initial encryption of the supplied password.  To be used on password that have never been encrypted before.
	*/
	public function encrypt($password) {
		$this->salty = rand(1776000,9999000)*rand(999,99999);
		$this->salt=md5($this->salty);
		$this->season($password);
		$this->encrypted=hash('sha512',$this->seasoned);
		$passarray['pw']=$this->encrypted;
		$passarray['salt']=$this->salty;
		return $passarray;
	}

	/*	FUNCTION	->recrypt($password, $usersalt)
		REQUIRES	$password=The password to encrypt, $usersalt=The salt to use to encrypt
		RETURNS		The SHA512-encrypted password. Similar to ->encrypt, but uses the salt you supply.  
		PURPOSE		Provides a way to encrypt a supplied password so that it can be compared to the one stored in the database.  You have to
					use the exact same Salt to do this, which is why you supply the salt, which you've read from the user's database record.
	*/
	public function recrypt($password, $usersalt) {
		$this->salt=md5($usersalt);
		$this->season($password);
		$this->encrypted=hash('sha512',$this->seasoned);
		return $this->encrypted;
	}

	/*	FUNCTION	->validateDate($date)
		REQUIRES	$date = a date as filled in by a user in a text field
		RETURNS		a proper Database formatted YYYY-MM-DD date if it can be figured out or FALSE if the date is bad or cannot be determined
		PURPOSE		Provides a way to validate user-entered dates while allowing the user maximum flexibility on entering the data, but
					provides a database-useful date format string in return 
	*/
	public function validateDate($date) {
		$thisyear = date('Y');
		// If they used the word "today", then we'll let it slide
		if (strtolower($date) == "today") {
			$date = date("Y-m-d");
		}

		// If the date field is ten characters, they used delimiters, so
        // We'll break it apart.  Dashes, periods, or slashes will work
        // If we can't figure out what they did, error.
        if (strlen($date) == 10) {
            $dates = explode('-', $date);
            if ($date == $dates[0]) { 
                $dates = explode('/', $date);
                if ($date == $dates[0]) { 
                    $dates = explode('.', $date);
                    if ($date == $dates[0]) {            
                        return false;
                    }
                }
            }
        }
        // If the date field is eight characters, they didn't use delimiters
        // We'll try to understand their input anyway
        elseif (strlen($date) == 8) {
        	if (intval(substr($date, 0, 2)) > 18) {
            	$dates[0] = substr($date, 0, 4);
            	$dates[1] = substr($date, 4, 2);
            	$dates[2] = substr($date, 6, 2);
            }
            else {
            	$dates[0] = substr($date, 0, 2);
            	$dates[1] = substr($date, 2, 2);
            	$dates[2] = substr($date, 4, 4);	
            }
        }
        // If the date field is six characters, they didnt' use a four-digit
        // Year.  We'll try to work with this, too, but assume an American formatted date
        elseif (strlen($date) == 6) {
            $dates[1] = substr($date, 0, 2);
            $dates[2] = substr($date, 2, 2);
            $dates[0] = substr($thisyear,0,2).substr($date, 4, 2);
            if ($dates[0] > $thisyear) {
            	$dates[0] = "19".substr($date, 4, 2);
            }
        }
        else {
        	return false;
        }

        // Now, let's see if they formatted the date American or MySQL style
        // If American, let's move it back around
        if (strlen($dates[0]) < 4) {
        	if (strlen($dates[2]) == 4) {
        		$dates[3] = $dates[0];
        		$dates[0] = $dates[2];
        		$dates[2] = $dates[1];
        		$dates[1] = $dates[3];
        	}
        }

        // Now, let's try to understand the date we were given
        // If the first part of the date is 4 digits, and is greater than 1900, then we've got a year
        if (strlen($dates[0]) < 4 || $dates[0] < 1900) {
        	return false;
        }

        // Now, let's see if the month makes sense
        if (strlen($dates[1]) != 2 || $dates[1] < 1 || $dates[1] > 12) {
        	return false;
        }

        // Finally, does the day make sense
        if (strlen($dates[2]) !=2 || $dates[2] < 1 || $dates[2] > 31) {
        	return false;
        }

        // Okay, everything checks out.  Now let's check for some weird things
        
        // If the month is February, the days can't be greater than 29
        if ($dates[1]==2) {
        	if ($dates[2] == 29) {
        		$year = $dates[0];
        		$isleap = ((($year % 4) == 0) && ((($year % 100) != 0) || (($year %400) == 0)));
        		if ($isleap == false) {
        			return false;
        		}
        	}
        	if ($dates[2] > 28) {
        		return false;
        	}
        }

        // If the month is April, June, September, or November, make sure
        // The date isn't greater than 30
        if ($dates[1]==4 || $dates[1]==6 || $dates[1]==9 || $dates[1]==11) {
        	if ($dates[2] > 30) {
        		return false;
        	}
        }

        // Okay, everything checks out.  Let's make a MySQL-happy date
        // And return it
        $validDate = $dates[0].'-'.$dates[1].'-'.$dates[2];
        return $validDate;
	}





	/*	FUNCTION	->validateUSAZip($zip_code)
		REQUIRES	$zip_code = a zipcode as filled in by a user in a text field
		RETURNS		TRUE if the data supplied is a valid US Zip code and FALSE if it is note
		PURPOSE		Provides a way to validate user-entered zipcodes; does not check to see if the zipcode is a real US 
					Post Office ZipCode, but rather runs a regex to simply check the format and numerals
	*/
	public function validateUSAZip($zip_code) {
	  if(preg_match("/^([0-9]{5})(-[0-9]{4})?$/i",$zip_code)) {
	    return true;
	  }
	  else {
	    return false;
	  }
	}




	/*	FUNCTION	->validatePhone($phonenumber)
		REQUIRES	$phonenumber = a phonenumber as supplied by a user in a text field
		RETURNS		a MYSQL-friendly 10-digit phone number free of spaces, dashes, etc.
		PURPOSE		Provides a way to validate user-entered phone numbers and stardize them to a simple 10 digit format					
	*/
	public function validatePhone($phonenumber) {
		// First thing to do is to strip everything but the numbers
		$phone = preg_replace('/\D+/', '', $phonenumber);
		// Next, check to make sure it is ten digits long
		if (strlen($phone) != 10) {
			return false;
		}
		else {
			return $phone;
		}
	}

	/*	FUNCTION	->getExchangeRate($country)
		REQUIRES	$country = a 3-letter country code
		RETURNS		a single DECIMAL (8,2) number representing the exchagne rate from USD to that currency or FALSE if failed.
		PURPOSE		Provides most recent exchange rate for a given country, bgtasks 'getexchangerates' must be active for this to work
	*/
	public function getExchangeRate($country) {
		if(strlen($country) != 3) { return false; }
		$sql = "SELECT * FROM currency_exchange_rates WHERE country = '$country' LIMIT 1";
		$result = $this->oneRowQuery($sql);
		if ($result) {
			$rate = $result['exchangerate'];
			return $rate;
		}
		else {
			return false;
		}
	}


	/*	FUNCTION	->runOtherMethod($modelname, $method)
		REQUIRES	$modelname = the name of the Model to be loaded for the query
		REQUIRES 	$method = a string containing the method name (getTheThings)
		REQUIRES 	$parameters = an array containing all necessary parameters to be called (e.g., [0] true, [1] false, [2] 26)
		RETURNS		whatever the called method returns - see the original method
		PURPOSE		Provides an easy way to run a method from another Model without needing to instantiate and call the model
					This is especially useful in places like the View, where it is not proper to perform those types of calls
		NOTE 		The ... in the method tells PHP to "UNPACK" the array into a list of parameters.
	*/
	public function runOtherMethod($modelname=false, $method=false, $parameters=false ) {
		if ($modelname==false || $method==false || $parameters==false) { return false; }
		$nump = count($parameters);
		if ($nump == 0 ) { return false; }
		$model = new $modelname($this->registry);
		$result = $model->$method(...$parameters);
		return $result;
	}
	
	/*	FUNCTION	->dblog($dblogging)
		REQUIRES	$dblog = the string to be logged to the database log file
		PURPOSE		Logs verbose database calls and results to a separate database log file only if database logging is turned on.
		NOTE 		Database logging is turned on within the database configuration settings file.
	*/
	private function dblog($dblog) {
		if ($this->dbconfig['dblogging'] == true) {
			$filename =  $this->registry->config['dbglogging_location'];
			if (file_exists($filename)) {
				$file=fopen($filename,"a");
			}
			else {
				$file=fopen($filename,"w");
			}
			$today = date("Y-m-d",time());
			$now = date("H:i:s",time());
			$towrite = "--------------------------------------------------------------------------------\n";
			$towrite .= $today."\t".$now."\n".$dblog."\n";
			$towrite .= "--------------------------------------------------------------------------------\n";
			fwrite ($file, $towrite);
			fclose ($file);
			return true;
		}
		return false;
	}
	
	/*	FUNCTION	->getdump($thevar)
		REQUIRES	$thevar = the variable to be var_dumped to a string
		RETURNS		the var_dumped string
		PURPOSE		Takes a variable and runs it through var_dump to a string instead of to screen
	*/
	private function getdump($thevar) {
		ob_start();
    	var_dump($thevar);
      	$dblog = ob_get_clean();
		return $dblog;
	}


}


 
?>
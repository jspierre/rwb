<?php

Abstract Class bgtasks {

	protected $registry;

	function __construct($registry) {
		$this->registry = $registry;
	}

	/**
	 * @all bgtasks must contain a doTask method
	 */
	abstract function doTask();

}

?>
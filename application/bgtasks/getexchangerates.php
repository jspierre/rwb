<?php

/* This class pulls new exchange rates from the API at exchangerate-api.com, then drops*/
/* the current table of rates and replacese it with the new rates */

class getexchangerates extends bgtasks {

	public function doTask() {
		$contents = fopen("https://open.exchangerate-api.com/v6/latest", "r");
		$json = stream_get_contents($contents);
		fclose($contents);
		$data = json_decode($json, true);
		if (isset($data['rates']) && $data['rates']) {
			$rates = $data['rates'];
			$sql = "DELETE FROM currency_exchange_rates; ";
			foreach ($rates as $c => $r) {
				$sql .= "INSERT INTO currency_exchange_rates (country, exchangerate) VALUES ('$c', $r); ";
			}
			//echo $sql;
			//exit();
			$result = $this->registry->db->multiQuery($sql);
			if ($result) {
				$explanation = "GetExchangeRates RAN and all rates were updated SUCCESSFULLY.";
			}
			else {
				$explanation = "GetExchangeRates RAN but there was an error when attempting to update the database";	
			}
		}
		else {
			$explanation = "GetExchangeRates RAN but there was an error with pulling the data from the API";
		}
		$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
		return true;
	}

}
<?php

/* This class checks for any emails that are ready to be sent (per date/time to be sent) */
/* and creates the entries for each individual email in the database for another background */
/* task to send later. */

class sysemailqueue extends bgtasks {

	public function doTask() {
		$model = new mEmails($this->registry);
		// Find out if there are any new system emails needing to be queued
		// If not, log it and exit gracefully
		$getemails = $model->checkNewEmails();
		if ($getemails == false) {
			$explanation = "System Email Queue RAN but no waiting emails were found to queue.";
			$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
			return true;
		}
		// If there were emails to be queued, run through each one and grab the 
		// individual email addresses needed and set them up in the queue for proper
		// sending by another bg process
		foreach ($getemails as $getemail) {
			// First, get the emails and so on needed for each email to be sent
			$groupnum = $getemail['groupnum'];
			$optinonly = $getemail['optinonly'];
			$ccadmins = $getemail['ccadmins'];
			$mailnum = $getemail['mailnum'];
			$subjectline = $this->registry->security->sanitizeString($getemail['subjectline']);
			$emailbody = $this->registry->security->sanitizeString($getemail['emailbody']);
			$numexpected = 0;
			if ($getemail['sendfrom'] == 0) { $emailfrom = $this->registry->config['systememail']; } else { $emailfrom = $this->registry->config['siteemail']; }
			$emails = $model->getEmailsQueue($groupnum, $optinonly, $ccadmins);
			// Now loop through each email address and add it to the database
			foreach ($emails as $email) {
				$emailto = $email['emailaddress'];
				$firstname = ucwords(strtolower($email['firstname']));
				$fullname = ucwords(strtolower($email['firstname']))." ".ucwords(strtolower($email['lastname']));
				$subject = str_replace('%%FIRSTNAME%%', $firstname, $subjectline); 
				$subject = str_replace('%%FULLNAME%%', $fullname, $subject);
				$body=str_replace('%%FIRSTNAME%%', $firstname, $emailbody);
				$body=str_replace('%%FULLNAME%%', $fullname, $body);
				if ($model->queueEmail($mailnum, $emailfrom, $emailto, $subject, $body) == false) {
					$explanation = "EMAIL QUEUE FAILED FOR MAILNUM ".$mailnum." TO ".$emailto. " FROM ".$emailfrom." SUBJ: ".$subject;
					$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
				}
				else {
					$numexpected++;
				}
			}
			// Now we update the status and stats of the email so that it shows in progress
			// If successful, we log same
			if ($model->updateEmailStatus($mailnum, 2, $numexpected)) {
				$explanation = "QUEUE RUN SUCCESFUL FOR MAILNUM ".$mailnum." WITH SUBJECT '".$getemail['subjectline']."'";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
			}
			// If failed, we rollback everything and log the error
			else {
				$model->emptyQueue($mailnum);
				$explanation = "QUEUE RUN FAILED FOR MAILNUM ".$mailnum." WITH SUBJECT '".$getemail['subjectline']."'";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
			}
		}
		return true;
	}

}

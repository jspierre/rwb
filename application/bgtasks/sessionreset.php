<?php

/* This class checks for users that have been logged in without activity for greater than the  */
/* session time allows and should be logged out and updates their online status to offline  */

class sessionreset extends bgtasks {

	public function doTask() {
        $session = $this->registry->config['sessiontime'];
        if (!$session) { 
            $explanation = "Session Reset RAN but there was an error with the main.inc.php configuration file for 'sessiontime'";
        }
        else {
            $datetime = date("Y-m-d H:i:s");
            $timestamp = strtotime($datetime);
            $time = $timestamp - ($session * 60);
            $cleartime = date("Y-m-d H:i:s", $time);
            $sql = "UPDATE logins_users_session SET isalive = 0 WHERE (lastalive < '$cleartime')";
            $result = $this->registry->db->query($sql);
            if ($result) { 
                $affect = $this->registry->db->getAffected();
                if ($affect > 0) {
                    $explanation = "Session Reset RAN and UNSET ".$affect." Inactive Sessions.";
                }
                else {
                    $explanation = "Session Reset RAN but no stale sessions were found.";
                }
            }
            else {
                $explanation - "Session Reset RAN but an error occured during Database Query.";
            }
            $this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
		    return true;
        }
        
	}
	
}

?>
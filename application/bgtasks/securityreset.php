<?php

/* This class checks and removes attacks from the security_attacks table once the lastattempt was made */
/* Over nn Hours ago as set in the config/main.inc.php file under 'security_reset' */

class securityreset extends bgtasks {

	public function doTask() {
		$hours = $this->registry->config['security_reset'];
		if ($hours < 1) { echo "Triggered"; return true;	}
		$calc = new DateTime("now");
		$period = "PT".$hours."H";
		$calc->sub(new DateInterval($period));
		$compare = $calc->format('Y-m-d H:i:s');
		$sql = "DELETE FROM security_attacks WHERE (lastattempt < '$compare')";
		$this->registry->db->query($sql);
		$affect = $this->registry->db->getAffected();
		if ($affect > 0) {
			$explanation = "Security Reset RAN and removed ".$affect." STALE ATTACKS from the database.";
			
		}
		else {
			$explanation = "Security Reset RAN but no stale attacks were found.";
		}
		$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
		return true;
	}
	
}
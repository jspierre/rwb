<?php

/* This class checks the main log file and, if it is over the size in MB defined in logging.inc.php, */
/* Then the file is archived and a new log file begins */

class logrotate extends bgtasks {

	private $model = "";

	public function doTask() {
		$notifyon = $this->registry->config['notifications'];
		$loggingon = $this->registry->config['logging'];

		// If logging is turned off, there is nothing to do, so just stop
		if ($notifyon == false || $loggingon == false) {
			$explanation = "LogRotate BG job ran, but logging IS OFF.  Exiting without action.";
			$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
			return true;
		}

		// Get variables needed
		$logloc = $this->registry->config['logging_location'];
		$size = $this->registry->config['log_rotate_size'];
		$bytesize = $size * 1000000;

		// Create the model
        $this->model = new mSystem($this->registry);

		// Find out the size of the file
		if (file_exists($logloc)) {
			if (filesize($logloc) > $bytesize) {
				// File is too big, let's archive it
		        if ($this->model->archiveLog()) {
	                $explanation = "### SYSTEM ### - SYSTEM LOG ARCHIVED BY LOGROTATE BG JOB";
	                $this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
					return true;
	            }
	            // Archive Failed
	            else {
	            	$explanation = "### SYSTEM ### - ERROR WHILE RUNNING LOGROTATE - SYSTEM ERROR WHILE ATTEMPTING TO ROTATE!";
	            	$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
					return false;    
	            }
			}
			// File isn't big enough to rotate yet
			else {
				$explanation = "### SYSTEM ### - LOGROTATE BG JOB RAN BUT LOG IS NOT ELIGIBLE FOR ROTATION YET (MIN SIZE = $size MB).";
	            $this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
				return false;
			}
		}
		// File doesn't exist??
		else {
			$explanation = "### SYSTEM ### - ERROR WHILE RUNNING LOGROTATE - LOG FILE DOES NOT EXIST!";
	        $this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
			Return false;
		}		
	}
}
<?php

/* This class checks for any emails that are ready to be sent (per date/time to be sent) */
/* and creates the entries for each individual email in the database for another background */
/* task to send later. */

class sysemailsend extends bgtasks {

	public function doTask() {
		$model = new mEmails($this->registry);
		// Find out if there are any emails needing to be sent
		// If not, log it and exit gracefully
		$getemails = $model->checkSendEmails();
		if ($getemails == false) {
			$explanation = "System Email Send RAN but no queued emails were found to send.";
			$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
			return true;
		}
		// If there were emails to be sent, run through each one and try to send it
		foreach ($getemails as $getemail) {
			// First, get the emails and so on needed for each email to be sent
			$queuenum = $getemail['queuenum'];
			$mailnum = $getemail['mailnum'];
			$emailfrom = $getemail['emailfrom'];
			$fromname = $this->registry->config['sitename'];
			$emailto = $getemail['emailto'];
			//$subjectline = $this->registry->security->unSanitize($getemail['subjectline']);
			//$emailbody = $this->registry->security->unSanitize($getemail['emailbody']);
			$subjectline = $getemail['subjectline'];
			$emailbody = $getemail['emailbody'];
			// Now, try to send
			$mail = new email($this->registry);
            if ($mail->sendMail($emailto,false,$subjectline,$emailbody,$emailfrom,$fromname)) {
            	// If the email sent successfully, remove it from the send queue
            	if (!$model->removeSend($queuenum)) { 
            		$explanation = "ERROR: Could not remove email ".$queuenum." from queue table! BACKGROUND RUN ABORTED!";
					$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation); 
					exit();
				}
				// Now, let's check to see if we've finished sending all emails for this mailnum
            	if ($model->countSend($mailnum) == 0) {
            		// If so, we need to update the mailnum status to complete in the database
            		if (!$model->updateEmailStatus($mailnum, 0)) {
            			$explanation = "ERROR: Could not update status to complete of  Mailnum ".$mailnum."!";
						$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation); 
            		}
            	}
            	// Now, we just need to update the number of emails sent for the mailnum
            	if (!$model->updateEmailStatus($mailnum, false, false, 1)) {
            		$explanation = "ERROR: Could not update numsent of  Mailnum ".$mailnum."!";
					$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
            	}
            }
            else {
            	// IF the email did not send successfully, log it and move on
            	$explanation = "ERROR: Could not SEND queued email ".$queuenum."!";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
            }
		}
	}

}


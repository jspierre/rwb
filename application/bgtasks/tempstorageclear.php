<?php

/* This class checks and removes all temp files from the configured temporary storage */
/* location as configured in storage.inc.php that are older than the configured time */

class tempstorageclear extends bgtasks {

	public function doTask() {
		$dir = $this->registry->config['storage_TEMPLOC'];
		$dir = substr($dir, 0, -1);
		$min = $this->registry->config['storage_DELINTERVAL'];
		if ($min < 1) { echo "Triggered"; return true;	}
		// NOTE: $deleted will contain an array of every file deleted, but we only use this to count the number of files
		$deleted = $this->delete_older_than($dir, $min*60);
		$affect = count($deleted);
		if ($affect > 0) {
			$explanation = "TempStorageClear RAN and removed ".$affect." STALE FILES from temp storage.";
		}
		else {
			$explanation = "TempStorageClear RAN but no stale files were found.";
		}
		$this->registry->logging->logEvent($this->registry->config['logging_cat_viewmsg'],$explanation);
		return true;
	}


	public function delete_older_than($dir, $max_age) {
		$list = array();	  
		$limit = time() - $max_age;
		$dir = realpath($dir);

		if (!is_dir($dir)) { return false; }

		$dh = opendir($dir);
		if ($dh === false) { return false; }

		while (($file = readdir($dh)) !== false) {
			$file = $dir . '/' . $file;
			if (!is_file($file)) {
			  continue;
			}
			if (filemtime($file) < $limit) {
			  $list[] = $file;
			  unlink($file);
			}
		}

		closedir($dh);
		return $list;
	}


	
}
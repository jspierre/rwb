<?php
/** 
 * MAIN VIEWS FILE
 * Configures and calls all subviews
 * @author John St. Pierre
 * @ver 0.1
 *
 *	Views uses the $request array to tell it how to render your page.  Values are:
 *	$request['type'] - type can be 'text' (will simply output the text you set), 'html' (will build an html page from templates - you'll probably always use this)
 *		for type 'html', the following variables are available and will be inserted in this order if they exist (none are required):
 *			$request['headerfile'] (file containing html to be used as the header for the page)
 *			$request['headertext'] (string containing all/some of the html to be displayed for the header)
 *			$request['headerinclude'] (file containing additional html/javascript to be inserted into the headerfile when called - OPTIONAL and rarely used)
 *			$request['bodyfile'] (file containing html with the body of the page)
 *			$request['bodytext']  (string containing all/some of the html to be displayed for the body)
 *			$request['footertext'] (string containing all/some of the html to be displayed for the footer)
 *			$request['footerfile'] (file containing html with the footer of the page)
 *			***NOTE: ALL 'headerfile', 'bodyfile', and 'footerfile' files must be located in views/ or they can't be inserted in a view!
 *	$request['variables'] - an array of variables that you want passed to your page.
 *
 *  $this->input is the entire array of the $request sent to us as explained above
 *  $this->config is the entire array of all of the configuration files
 *  $this->logged in is either TRUE (the user is logged in) or FALSE (the user is not logged in)
 *  $this->currentclass contains the final part of the URI that sent us here (e.g., 'login')
 *  $pagename = Another way to call $this->currentclass
 *  $sitename = The name of the site as configured in the config files
 *  $url = The main URL of the site
 */
 
 class template {
 
	private $input;
	private $loggedin;
	public $config;
	public $currentclass;
	private $registry;
	public $usstates;
 
	/* Constructor */
	public function __construct($registry) {
		
		/* First, let's set the configuration so other routines can access it */
		$this->registry = $registry;
		
		/* Now, let's set the configuration so other methods can access it */
		$this->config = $this->registry->config;
				
	}
	
 
 /* Depending upon the type of request made, this function outputs the HTML to screen desired */
	public function getPage($request) {
		$this->loggedin = $this->registry->security->isLoggedIn();
		$this->currentclass = $this->registry->controller;
		$pagename = $this->currentclass;
		$this->input = $request;
		$request = $this->input;
		$sitename = $this->registry->config['sitename'];
		$url = $this->registry->config['url'];
		if (isset($request['variables'])) { $values = $request['variables']; }
		$type = strtolower($request['type']);
		switch ($type) {
			case "die":
				break;
			case "text":
				echo $request['displaytext'];
				break;
			case "xml-twilio":
				echo '<?xml version="1.0" encoding="UTF-8"?>';
				echo "\n";
				echo '<Response>';
				echo "\n";
				foreach ($request['xml'] as $xml) {
					echo "  ".$xml."\n";	
				}
				echo '</Response>';
				break;
			case "html":
				if (isset($request['headerfile'])) { require(SITE_PATH.'/view/'.$request['headerfile']);	}
				if (isset($request['headertext'])) { echo $request['headertext']; }
				if (isset($request['bodyfile'])) { require(SITE_PATH.'/view/'.$request['bodyfile']);	}
				if (isset($request['bodytext'])) { echo $request['bodytext']; }
				if (isset($request['footertext'])) { echo $request['footertext']; }
				if (isset($request['footerfile'])) { require(SITE_PATH.'/view/'.$request['footerfile']);	}
				break;
			default:
				echo "ERROR: NO OUTPUT TYPE WAS DEFINED FOR VIEW!";
		}
		return;
	}

	// This function simply returns a formatted phone number that's easier for humans to read
	// Options for type are PARENTHESIS, e.g., (123) 456-7890; DASHES, e.g., 123-456-789
	public function prettyPhone($phone=false, $type="DEFAULT") {
		if ($type=="DEFAULT") { 
			$type = $this->registry->config['phone_format'];
		}
		if ($phone) {
			if (strlen($phone)==10) {
				$area = substr($phone, 0,3);
				$prefix = substr($phone, 3, 3);
				$number = substr($phone, 6, 4);
				if ($type == "PARENTHESIS") {
					$phonenumber = "(".$area.") ".$prefix."-".$number;
				}
				elseif ($type=="DASHES") {
					$phonenumber = $area."-".$prefix."-".$number;
				}
				else {
					$phonenumber = $phone;
				}
			}
			return $phonenumber;
		}
		else {
			return false;
		}
	}

	public function getUSStates($type="OPTION", $state=false) {
		$type = strtoupper($type);
		if ($state==false) { 
			$state = $this->registry->config['us_state'];
			if ($state == "") { $state = "AL"; }
		}
		else { $state = strtoupper ($state); }
		$state_list = array('AL'=>"Alabama",'AK'=>"Alaska",  'AZ'=>"Arizona",  'AR'=>"Arkansas",  'CA'=>"California",  
			'CO'=>"Colorado",  'CT'=>"Connecticut",  'DE'=>"Delaware",  'DC'=>"District Of Columbia",  'FL'=>"Florida",  
			'GA'=>"Georgia", 'HI'=>"Hawaii",  'ID'=>"Idaho",  'IL'=>"Illinois",  'IN'=>"Indiana",  'IA'=>"Iowa",  
			'KS'=>"Kansas",  'KY'=>"Kentucky",  'LA'=>"Louisiana",  'ME'=>"Maine",  'MD'=>"Maryland",  
			'MA'=>"Massachusetts", 'MI'=>"Michigan",  'MN'=>"Minnesota",  'MS'=>"Mississippi",  'MO'=>"Missouri",  
			'MT'=>"Montana", 'NE'=>"Nebraska", 'NV'=>"Nevada", 'NH'=>"New Hampshire", 'NJ'=>"New Jersey", 
			'NM'=>"New Mexico", 'NY'=>"New York", 'NC'=>"North Carolina", 'ND'=>"North Dakota", 'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  'OR'=>"Oregon", 'PA'=>"Pennsylvania",  'RI'=>"Rhode Island",  'SC'=>"South Carolina",  
			'SD'=>"South Dakota", 'TN'=>"Tennessee",  'TX'=>"Texas",  'UT'=>"Utah",  'VT'=>"Vermont",  'VA'=>"Virginia",  
			'WA'=>"Washington",  'WV'=>"West Virginia",  'WI'=>"Wisconsin",  'WY'=>"Wyoming");
		
		// OPTION 1 - "OPTION" - Outputs a OPTION Html line with the $state given selected automatically
		// You just need to wrap it in <select></.select> statements, with all of your desired options
		if ($type == "OPTION") {
			foreach ($state_list as $abbr=>$name) {
				if ($state == $abbr) {
					echo '<option value="'.$abbr.'" selected>'.$abbr.'</option>';
				}
				else {
					echo '<option value="'.$abbr.'">'.$abbr.'</option>';
				}
			}
			return true;
		}
		// OPTION 2 - "LIST" - Just returns the list of states as an array
		elseif ($type == "LIST") {	
			return $state_list;
		}

	// If anything went wrong and no option was selected, just return FALSE
	return false;
	}



	public function getTimes($type="OPTION", $time=false, $allownone=true) {
		$type = strtoupper($type);
		if ($time==false) {
			if ($allownone) {
				$time = "25";
			}
			else {
				$time = "0";
			}
		}
		$time_list = array ('0'=>"MIDNIGHT", '1'=>"1:00 AM", '2'=>"2:00 AM", '3'=>"3:00 AM", '4'=>"4:00 AM", '5'=>"5:00 AM", 
			'6'=>"6:00 AM", '7'=>"7:00 AM", '8'=>"8:00 AM", '9'=>"9:00 AM", '10'=>"10:00 AM", '11'=>"11:00 AM", '12'=>"NOON",
			'13'=>"1:00 PM", '14'=>"2:00 PM", '15'=>"3:00 PM", '16'=>"4:00 PM", '17'=>"5:00 PM", '18'=>"6:00 PM", 
			'19'=>"7:00 PM", '20'=>"8:00 PM", '21'=>"9:00 PM", '22'=>"10:00 PM", '23'=>"11:00 PM", '24'=>"Open All Day", '25'=>"Closed All Day");

		// OPTION 1 - "OPTION" - Outputs an OPTION HTML line with the $time given selected automatically
		// You just need to wrap it in <select></select> statements, with all of your desired options
		if ($type == "OPTION") {
			foreach ($time_list as $value=>$name) {
				if ($time == $value) {
					echo '<option value="'.$value.'" selected>'.$name.'</option>';
				}
				else {
					echo '<option value="'.$value.'">'.$name.'</option>';
				}
			}
			return true;
		}
		// If anything went wrong and no option was selected, just return FALSE
		return false;
	}

}

<?php
/** 
 * MAIN STORAGE CLASS
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
class storage {

	/* Private Variables */
	private $stconfig;
	private $registry;
		
	/* CONSTRUCTOR - Gets the database setup values passed to it, Sets the Pepper for salting from the Config File */
	public function __construct($registry) {
		$this->stconfig = $registry->config;
		$this->registry = $registry;
		//require_once($this->stconfig['storage_AWS_LOC']);
	}
	

	/*
	HOW THE RETURNPAYLOAD IS USED IN THIS CLASS:

		$returnPayload[0]   BOOL - was the operation successful? (true/false)
		$returnPayload[1]   INT - Level of severity (0=None [default], 1=Success, 2=Info, 3=Warning, 4=Danger,  
		                          5=BLACK). Note the level can correspond to color on messages given to users.
		$returnPayload[2]   STRING - a short description of the success or failure (mostly for logging)
		$returnPayload[3]   STRING- The message the user will see in the alert box (if developer chooses to use)
		$returnPayload[4]   STRING:  
								For Storage: will contain a single item: ‘xxxxxxxxxxx.xxx’, containing the name that the 
								file was stored under if successful, FALSE if failed
								
								For Retrieval: will contain the filename in temporary storage that has been retrieved if 
								successful.  If not successful, will simply contain FALSE                                                                        

								For Moving: will contain TRUE if successful, or FALSE if failed.

	*/

	
	



	/**
	 * @function	putLocal			Saves the given file from temporary storage into local storage (as defined in config)
	 * @param		str $loc 			Tne name assigned to the location in the configuration file
	 * @param     	str $file 			The file to be stored (file only - must be in temp storage)
	 * @param       bool $keepname 		TRUE = Keep the original file name upon store; FALSE (default) = Assign a random file name
	 * @param 		bool $keepfile 		TRUE = Keep the original file; FALSE (default) = Delete the original file once stored
	 * @returns		mixed $returnPayload Returns a $returnPayload as described above
	 *
	 */	
	public function putLocal($loc, $file, $keepname=false, $keepfile=false) {
		$error = 0;
		$e="";
		if ($loc=="" || $file == "" || is_bool($keepname) == false || is_bool($keepfile) == false) {
			$error = 1;
			$e .= "5986524 ";
		}
		else {
			// Let's check the filename to make sure it's a real file
			$srcpath = $this->stconfig['storage_TEMPLOC'];
			$srcfile = $srcpath.$file;
			// DOES THE SOURCE FILE EVEN EXIST?
			if (file_exists($srcfile) == false) { $error = 1; $e .= " 5899989 "; }
			// IS THE FILENAME AN ACTUAL FILE?
			$filename = $this->detectFileName($file);				
			if ($filename == false) { $error = 1; $e .= " 5982563"; }

			// Let's get the storage path
			$pathdata = $this->getStoragePath(0, $loc);
			$path = $pathdata['locloc'];
			if ($path == false) { $error = 1; $e .= " 5897428"; }

			// Do we need a random file name? If so, let's pull that
			if ($keepname==false) { $storefilename = $this->getRandomFileName($filename); } else { $storefilename = $filename; }
			if ($storefilename == false) { $error = 1; $e .= " 5894379"; }

			// Now, let's put everything together for the destination file name
			$destfile = $path.$storefilename;

			// Does the file already exist in the destination? If so, that's an error
			if (file_exists($destfile)) { $error = 1; $e .= " 5896189"; }

			// If there were no errors, Let's try to copy the file now
			if ($error == 0) {
				if (copy($srcfile, $destfile))  {
					// And then, let's delete the original file if requested
					// If deletion fails, this is a soft failure and will just be logged
					if ($keepfile == false) {
						if (unlink($srcfile) == false) {
							$explanation = "FILE ERROR! COULD NOT DELETE $file - PLEASE REMOVE MANUALLY!";
							$this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
						}
					}
				}
				else { $error = 1; $e .= " 5892476"; }
			}
		}

		// If we had an error, let's send that back.  If not, let's send back the successpayload
		if ($error == 1) {
			$returnPayload[0] = false;
			$returnPayload[1] = 4;
			$returnPayload[2] = "putLocal FAIL: storage.class.php -> putLocal missing required parameters to function. ERROR $e";
			$returnPayload[3] = "An internal error has occured. Please notify your system administrator. ERROR $e";
			$returnPayload[4] = false;
		}
		else {
			$returnPayload[0] = true;
			$returnPayload[1] = 1;
			$returnPayload[2] = "putLocal SUCCESS: Stored $file as $destfile";
			$returnPayload[3] = "SUCCESS! File Uploaded Successfully.";
			$returnPayload[4] = $storefilename;
		}

		return $returnPayload;

	// END OF putLocal()
	}
	


	/**
	 * @function	getLocal			Retrieves the given file from local storage and places it in temporary storage as defined in config
	 * @param		str $loc 			Tne name assigned to the storage location in the configuration file
	 * @param     	str $file 			The name of the file to be retrieved (may be a previously assigned random name)
	 * @param       str $newname 		The name to rename the file being retrieved or FALSE (default) if this is not necessary
	 * @returns		mixed $returnPayload Returns a $returnPayload as described above
	 *
	 */	
	public function getLocal($loc, $file, $newname=false) {
		$error = 0;
		$e = "";
		if ($loc=="" || $file == "") {
			$error = 1;
			$e .= " 1859685";
		}
		else {
			// Let's get the source and destination paths
			$srcpathdata = $this->getStoragePath(0, $loc);
			$srcpath = $srcpathdata['locloc'];
			if ($srcpath == false) { $error = 1; $e .= " 1859428"; }
			$destpath = $this->stconfig['storage_TEMPLOC'];

			// Let assign the source path and file name
			$sourcefile = $srcpath.$file;

			// Now, put  together the destination file name 
			if ($newname == false) { $destfile = $destpath.$file; } else { $destfile = $destpath.$newname; }

			// Does the file already exist in the destination? If so, delete it because it is a temporary file
			if (file_exists($destfile)) { 
				if (unlink($destfile) == false) { $error = 1; $e .= " 1852359"; }
			}

			// Let's try to copy the file now
			if (copy($sourcefile, $destfile) == false)  { $error = 1; $e .= " 5892476"; }
		}

		// If we had an error, let's send that back.  If not, let's send back the successpayload
		if ($error == 1) {
			$returnPayload[0] = false;
			$returnPayload[1] = 4;
			$returnPayload[2] = "getLocal FAIL: storage.class.php -> getLocal missing required parameters to function. ERROR $e";
			$returnPayload[3] = "An internal error has occured. Please notify your system administrator. ERROR $e";
			$returnPayload[4] = false;
		}
		else {
			$returnPayload[0] = true;
			$returnPayload[1] = 1;
			$returnPayload[2] = "getLocal SUCCESS: Stored $file as $destfile";
			$returnPayload[3] = "SUCCESS! File Retrieved Successfully.";
			$returnPayload[4] = basename($destfile);
		}

		return $returnPayload;

	// END OF getLocal()
	}


	/**
	 * @function	putS3				Saves the given file from temporary storage into AWS S3 Storage (as defined in config)
	 * @param		str $loc 			Tne name assigned to the location in the configuration file
	 * @param     	str $file 			The file to be stored (file only - must be in temp storage)
	 * @param       bool $keepname 		TRUE = Keep the original file name upon store; FALSE (default) = Assign a random file name
	 * @param 		bool $keepfile 		TRUE = Keep the original file; FALSE (default) = Delete the original file once stored
	 * @returns		mixed $returnPayload Returns a $returnPayload as described above
	 * NOTE: 		If the file already exists at the destination, it will be overwritten!!
	 */	
	public function putS3($loc, $file, $keepname=false, $keepfile=false) {
		$error = 0;
		$e="";
		if ($loc=="" || $file == "" || is_bool($keepname) == false || is_bool($keepfile) == false) {
			$error = 1;
			$e .= "1584796 ";
		}
		else {
			// Let's check the filename to make sure it's a real file
			$srcpath = $this->stconfig['storage_TEMPLOC'];
			$srcfile = $srcpath.$file;
			// DOES THE SOURCE FILE EVEN EXIST?
			if (file_exists($srcfile) == false) { $error = 1; $e .= " 1586352 "; }
			// IS THE FILENAME AN ACTUAL FILE?
			$filename = $this->detectFileName($file);				
			if ($filename == false) { $error = 1; $e .= " 1583257"; }

			// Let's get the S3 Location Info
			$infoarray = $this->getStoragePath(1, $loc);
			if ($infoarray == false) { $error = 1; $e .= " 1582116"; } 
			else {
				$bucket = $infoarray['bucket'];
				$region = $infoarray['region'];
				$key = $infoarray['key'];
				$secret = $infoarray['secret'];
			}			

			// Do we need a random file name? If so, let's pull that
			if ($keepname==false) { $storefilename = $this->getRandomFileName($filename); } else { $storefilename = $filename; }
			if ($storefilename == false) { $error = 1; $e .= " 1587768"; }

			// If there were no errors, Let's try to copy the file now
			if ($error == 0) {
				require_once ($this->registry->config['storage_AWS_LOC']);
				$s3 = new Aws\S3\S3Client([
					'region'  => $region,
					'version' => 'latest',
					'credentials' => ['key' => $key, 'secret' => $secret]
				]);

				try {
					$s3->putObject([
						'Bucket' => $bucket,
						'Key'    => $storefilename,
						'SourceFile' => $srcfile
					]);
				}
				catch (Throwable $ee) {
					$error = 1;
					$e .= "1581776";
				}
			}

			if ($error == 0) {
				// And then, let's delete the original file if requested
				// If deletion fails, this is a soft failure and will just be logged
				if ($keepfile == false) {
					if (unlink($srcfile) == false) {
						$explanation = "FILE ERROR! COULD NOT DELETE $file - PLEASE REMOVE MANUALLY!";
						$this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
					}
				}
			}
		}

		// If we had an error, let's send that back.  If not, let's send back the successpayload
		if ($error == 1) {
			$returnPayload[0] = false;
			$returnPayload[1] = 4;
			$returnPayload[2] = "putS3 FAIL: storage.class.php -> putS3 had critical errors and could not function. ERROR $e";
			$returnPayload[3] = "An internal error has occured. Please notify your system administrator. ERROR $e";
			$returnPayload[4] = false;
		}
		else {
			$returnPayload[0] = true;
			$returnPayload[1] = 1;
			$returnPayload[2] = "putS3 SUCCESS: Stored $file as $storefilename";
			$returnPayload[3] = "SUCCESS! File Uploaded Successfully.";
			$returnPayload[4] = $storefilename;
		}

		return $returnPayload;

	// END OF putS3()
	}


/**
	 * @function	getS3				Retrieves the given file from AWS S3 and places it in temporary storage as defined in config
	 * @param		str $loc 			Tne name assigned to the storage location in the configuration file
	 * @param     	str $file 			The name of the file to be retrieved (may be a previously assigned random name)
	 * @param       str $newname 		The name to rename the file being retrieved or FALSE (default) if this is not necessary
	 * @returns		mixed $returnPayload Returns a $returnPayload as described above
	 *
	 */	
	public function getS3($loc, $file, $newname=false) {
		$error = 0;
		$e = "";
		if ($loc=="" || $file == "") {
			$error = 1;
			$e .= " 6385419";
		}
		else {
			// Let's get the S3 Location Info
			$infoarray = $this->getStoragePath(1, $loc);
			if ($infoarray == false) { $error = 1; $e .= " 6384796"; } 
			else {
				$bucket = $infoarray['bucket'];
				$region = $infoarray['region'];
				$key = $infoarray['key'];
				$secret = $infoarray['secret'];
			}		
			
			// Now get our destination path
			$destpath = $this->stconfig['storage_TEMPLOC'];

			// Now, put  together the destination file name 
			if ($newname == false) { $destfile = $destpath.$file; } else { $destfile = $destpath.$newname; }

			// Does the file already exist in the destination? If so, delete it because it is a temporary file
			if (file_exists($destfile)) { 
				if (unlink($destfile) == false) { $error = 1; $e .= " 6385514"; }
			}

			// If there were no errors, Let's try to copy the file now
			if ($error == 0) {
				require_once ($this->registry->config['storage_AWS_LOC']);
				$s3 = new Aws\S3\S3Client([
					'region'  => 'us-east-1',
					'version' => 'latest',
					'credentials' => ['key' => $key, 'secret' => $secret]
				]);

				try {
					$s3->getObject([
						'Bucket' => $bucket,
						'Key'    => $file,
						'SaveAs' => $destfile
					]);
				}
				catch (Throwable $ee) {
					$error = 1;
					$e .= " 6387197";
				}
			}
		}
		// If we had an error, let's send that back.  If not, let's send back the successpayload
		if ($error == 1) {
			$returnPayload[0] = false;
			$returnPayload[1] = 4;
			$returnPayload[2] = "getS3 FAIL: storage.class.php -> a fatal error has occurred. ERROR $e";
			$returnPayload[3] = "An internal error has occured. Please notify your system administrator. ERROR $e";
			$returnPayload[4] = false;
		}
		else {
			$returnPayload[0] = true;
			$returnPayload[1] = 1;
			$returnPayload[2] = "putLocal SUCCESS: Stored $file as $destfile";
			$returnPayload[3] = "SUCCESS! File Stored Locally Successfully.";
			$returnPayload[4] = basename($destfile);
		}

		return $returnPayload;

	// END OF getS3()
	}







	/**
	 * @function	clearTemp			Deletes a file or all files from Temporary Storage as defined in Configuration file
	 * @param     	str $file 			The name of the file to be deleted from temporary storage. If FALSE, delete ALL temp files
	 * @returns		bool 				True if successful, false if not. Note that there is also a background task already built 
	 *									that can be scheduled to delete files older than xx minutes.  See bgtasks.
	 */	
	public function clearTemp($file=false) {
		// Let's pull the temporary storage location from config and create the full path
		$path =  $this->stconfig['storage_TEMPLOC'];
		
		// If we're clearing all files, we need to do that
		if ($file == false) {
			$error = 0;
			$dfiles = glob($path.'*'); 
			foreach($dfiles as $dfile) { if(is_file($dfile)) { if (unlink($dfile) == false) { $error = 1; } } }
			if ( $error = 1 ) { return false; } else { return true; }
		}
		else {
			$filename = $path.$file;
		}

		// If the file actually exists, let's delete it, otherwise, let's fail
		if (file_exists($filename)) {
			if (unlink($filename) == false) { return false; } else { return true; }
		}
		
	// END clearTemp
	}


	/**
	 * @function	validateImage		Determines whether an IMAGE in temporary storage is an image and if it is of the proper type 
	 * 									and within the max size allowed. Also will resize the image if desired to fit your specs
	 * @param     	str $filename		The name of the file in temporary storage to be checked.
	 * @param 		int $filesize 		(OPTIONAL) The maximum size of the file in bytes
	 * @param 		int $addlext 		(OPTIONAL) This method only checks for JPG, GIF, or PNG.  add an addl image constant here 
	 * 									if desired (see https://www.php.net/manual/en/function.exif-imagetype.php)
	 * @param 		bool $resize		Should the system attempt to resize the image if it is over the resize max? (true/false)
	 * @param       int $resizemax		The maximum size that the file should be resized to, in bytes
	 * @param 		int $rescale  		Rescale the image to a max of X pixels wide
	 * @returns		array $result		Common PayLoad is returned
	 *									
	 */	
	public function validateImage($filename=false, $filesize=false, $resize=false, $resizemax = false, $addlext=false, $rescale=false) {
		// First, if we didn't name a file, this is not good - send em back
		if ($filename == false) { $result[0]=false; $result[1]="File not specified."; $result[2]=false; return $result; }

		// Next, let's get the place where this image is stored and make sure it actually exists
		$path =  $this->stconfig['storage_TEMPLOC'];
		$filepath = $path.$filename;
		$resizepath = $path."RSZ_".$filename;
		if (!file_exists($filepath))  { $result[0]=false; $result[1]="File not found."; $result[2]=false; return $result; }

		// Okay, is the file of an image type we'll accept?
		$imgtype = exif_imagetype($filepath);
		if ($imgtype==false || ($imgtype != IMAGETYPE_GIF && $imgtype != IMAGETYPE_JPEG && $imgtype != IMAGETYPE_PNG)) {
			if ($imgtype && $addlext) { 
				if ($imgtype != $addlext) {
					$addl = substr($addlext, 11);
					$result[0]=false; 
					$result[1]=4; 
					$result[2]="ERROR: File type not supported (GIF, JPG, PNG and $addl only)";
					$result[3]="ERROR: File type not supported (GIF, JPG, PNG and $addl only)";
					return $result; 
				}
			}
			else {
				$result[0]=false;
				$result[1]=4;
				$result[2]="ERROR: File type not supported (GIF, JPG, and PNG only)"; 
				$result[3]="ERROR: File type not supported (GIF, JPG, and PNG only)";
				return $result; 
			}
		}
		if ($imgtype == IMAGETYPE_PNG) { $result2 = "PNG"; } else if ($imgtype == IMAGETYPE_JPEG) { $result2 = "JPG"; } 
		elseif ($imgtype == IMAGETYPE_GIF) {$result2 = "GIF"; } else if ($imgtype == $addlext) { $result = substr($addlext, 11); }
		else { $result2 = "Unknown"; }

		// Great - the image type is good, now let's check the file size
		if ($filesize) {
			if (filesize($filepath) > $filesize) {
				if ($filesize > 1000000000) { $maxsize = round($filesize/1000000000)." GB"; }
				else if ($filesize > 1000000) { $maxsize = round($filesize/1000000)." MB"; }
				else if ($filesize > 1000) { $maxsize = round($filesize/1000)." KB"; }
				else { $maxsize = round($filesize/1000)." Bytes"; }
				$result[0]=false; 
				$result[1]=4;
				$result[2]="ERROR: File size exceeds maximum allowed of $maxsize"; 
				$result[3]="ERROR: File size exceeds maximum allowed of $maxsize"; 
				$result[4]=$result2; 
				return $result;
			}
			else { $currfilesize = filesize($filepath); }
		}

		// Let's make the image a decent size


		// Okay, the image is good.  Let's see if we need to resize
		if ($resize) {
			$resized = false;
			$source = $filepath;
			if ($currfilesize > $resizemax) {
				// Let's calculate the difference
				$sizediff = round(($resizemax/$currfilesize)*100);
				if ($sizediff > 60) { $quality = 95; }
				elseif ($sizediff > 50) { $quality = 90; }
				elseif ($sizediff > 40) { $quality = 85; }
				else {$quality = 85; }

				$info = getimagesize($filepath);

				if ($info['mime'] == 'image/jpeg') { $image = imagecreatefromjpeg($source); }

				elseif ($info['mime'] == 'image/gif') {	$image = imagecreatefromgif($source); }

				elseif ($info['mime'] == 'image/png') { $image = imagecreatefrompng($source); }

				if ($rescale) {
					$newimage = imagescale($image, $rescale, -1);
				}
				else {
					$newimage = $image;
				}


				if (!imagejpeg($newimage, $resizepath, $quality)) {
					$result[0] = false;
					$result[1]=4;
					$result[2]="ERROR: FILE COULD NOT BE RESIZED PROPERLY!"; 
					$result[3]="ERROR: An Internal File Error Has Occured. Please contact your administrator. Error 999376"; 
					$result[4]= false;
					return $result;
				}
				else {
					$resized = true;
				}
			}
		}
		
		
		// OKAY, all done - the image is good.  Let's return that!
		$result[0]=true;
		$result[1]=1;
		$result[2]="File validated."; 
		$result[3]="File validated."; 
		if ($resized) { $result2 = "RESIZED"; }
		$result[4]=$result2; 
		return $result; 
	}


	/**
	 * @function	resizeImage			Determines whether an image in temporary storage is too big and then attempts to reduce
	 * 									the image down to a more manageable size
	 * @param     	str $filename		The name of the file in temporary storage to be checked.
	 * @param 		int $filesize 		The maximum size the file can be.
	 * @returns		array $result		Common PayLoad returned
	 * NOTE: THIS METHOD ASSUMES A PROPERLY FORMATTED FILE - USE $this->validateImage FIRST before using this method!
	 *									
	 */	
	 public function resizeImage($filename=false, $filesize=false) {
	 	// Get current size of file
		
	 
	 }


	/*
	***********************************************************************************************************
	************************************** HELPER METHODS APPEAR BELOW ****************************************
	*/



	// This function pulls the file name from a path
	public function detectFileName($file) {
		if (substr($file, -1) == "/" || substr($file, -1) == chr(92)) {
			return false;
		}
		$filename = basename($file);
		return $filename;
	}

	// This function searches the config file for the actual path to the local storage location
	public function getStoragePath($type, $loc, $urlonly=false) {
		if ($type == 0) {
			if (isset($this->stconfig['storage_LOC_LOCS'][$loc])) {
				$sp = $this->stconfig['storage_LOC_LOCS'][$loc];
				if ($sp == "") { return false; }
			}
			else { return false; }
			if ($urlonly) { $sp = $sp['url']; }
			return $sp;
		}

		if ($type == 1) {
			if (isset($this->stconfig['storage_AWS_LOCS'][$loc])) {
				$sp = $this->stconfig['storage_AWS_LOCS'][$loc];
				if ($sp == "") { return false; }
			}
			else { return false; }
			if ($urlonly) { $sp = $sp['url']; }
			return $sp;
		}

		// If none of the above matched, return an error
		return false;				
	}

	// This function returns a random file name with the same extension as the original
	public function getRandomFileName($filename) {
		// Let's see if there is a file extension or not
		$exploded = explode(".", $filename);
		if ($exploded[0] = $filename) {
			$extension = "";
		}
		$pos = count($exploded)-1;
		$extension = $exploded[$pos];

		// How many characters to create?
		$rnc = $this->stconfig['storage_NORANDOM'];

		// Let's create the random characters
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
   		$randomString = ''; 
   		for ($i = 0; $i < $rnc; $i++) { 
        	$index = rand(0, strlen($characters) - 1); 
        	$randomString .= $characters[$index]; 
    	} 

    	if ($randomString == "") { return false; }

    	// Okay, now add the extension back on
    	$randomFilename = $randomString.".".$extension;

    	// Let's return the random File Name
    	return $randomFilename;		
	}

	
// END CLASS
}


 
?>
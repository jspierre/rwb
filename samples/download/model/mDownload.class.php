<?php

class mDownload extends baseModel {
 

 	// Check ACL and Download table for file requested.  
 	// Return FALSE if file doesn't exist or access denied
 	// Return filename otherwise
	public function getDownloadFile($filerequest, $loginnum) {
		// First pull User's ACL Group
		$sql = "SELECT aclgroup FROM logins_users WHERE usernum = $loginnum LIMIT 1";
		$row1=$this->registry->db->oneRowQuery($sql);
		$aclgroup = $row1['aclgroup'];

		// Now see if the file exists in the table.
		$sql = "SELECT download_files.*, download_acl.dlacl_id FROM download_files 
		LEFT JOIN download_acl ON download_files.dl_id = download_acl.dl_id WHERE 
		download_files.dl_shortname='$filerequest' AND download_files.dl_active = 1 
		AND download_acl.dl_aclgroup = $aclgroup LIMIT 1";
		$row2=$this->registry->db->oneRowQuery($sql);
		if ($row2) {
			$filename = $row2['dl_filename'];
			return $filename;
		}
		else {
			return false;
		}
	}



	// Get all the data about one file, including acl data, and return it
	// If unsuccessful, returns FALSE
	public function getFileData($dl_id) {
		$sql = "SELECT * FROM download_files WHERE dl_id = $dl_id LIMIT 1";
		$filedata = $this->registry->db->oneRowQuery($sql);
		if ($filedata == false) { return false; }
		$sql = "SELECT dl_aclgroup FROM download_acl WHERE dl_id = $dl_id";
		$this->registry->db->query($sql);
        $groups = $this->registry->db->getAllRows();
        if ($groups == false) { return false; }
        $filedata['acl'] = $groups;
        return $filedata;
	}

	// Get list of files that the user has access to and return them in an array
	// Or return false for no files. $onlyactive determines whether or not to pull
	// just Active files (for user functions) or ALL files (for admin functions)
	public function getFileList($loginnum, $onlyactive=true) {
		// First pull User's ACL Group
		$sql = "SELECT aclgroup FROM logins_users WHERE usernum = $loginnum LIMIT 1";
		$row1=$this->registry->db->oneRowQuery($sql);
		$aclgroup = $row1['aclgroup'];
		$sqlactive="";
		if ($onlyactive) {
			$sqlactive = "download_files.dl_active = 1 AND ";
		}

		// Now get all details of files that are allowed for him, sorted by name
		$sql = "SELECT download_files.*, download_acl.dlacl_id FROM download_files 
		LEFT JOIN download_acl ON download_files.dl_id = download_acl.dl_id WHERE ".
		$sqlactive."download_acl.dl_aclgroup = $aclgroup ORDER BY 
		download_files.dl_displayname ASC";
		$result=$this->registry->db->query($sql);
		$files=$this->registry->db->getAllRows();
		return $files;
	}




	// Add or edit a downloadable file 
	// Returns the dl_id of the file if successful, or FALSE otherwise
	public function addEditFile($isnew=true, $posted, $files=false) {
		// We can't add a new file without the files array being present
		if ($isnew==true && $files==false) {
			return false;
		}

		// First, we sanitize the posted data
		$cleanpost = $this->registry->security->sanitizeArray($posted);

		// Now, we extract all of our variables
		extract ($cleanpost);
		if ($files != false) {	extract ($files); }

		// Now, let's send the text ones through the MySQL filter
		$dl_displayname = $this->registry->db->sanitize($dl_displayname);
		$dl_description = $this->registry->db->sanitize($dl_description);

		// Just make sure nothing goofy is going on
		if ($dl_active > 1 || $dl_active < 0) {
			$dl_active = 0;
		}


		// *** LANGUAGE FOR EDITS ONLY - CODE STOPS AFTER THIS FOR EDITS ***
		// If this is an edit, we're pretty much done - let's call the DB
		if ($isnew==false) {
			// First, we update the main table.  There's only 3 editable columns
			$sql = "UPDATE download_files SET dl_displayname='$dl_displayname', dl_description='$dl_description', 
				dl_active=$dl_active WHERE dl_id = $dl_id LIMIT 1";
			if ($this->registry->db->query($sql)) {
				// Now Update ACL
				// To do this, we first delete all ACL entries
				$sql = "DELETE FROM download_acl WHERE dl_id = $dl_id";
				$this->registry->db->query($sql);
				// Now, we put it all back as newly submitted
				foreach ($aclgroup as $acl) {
					$sql = "INSERT INTO download_acl (dl_id, dl_aclgroup) VALUES ($dl_id, $acl)";
					$this->registry->db->query($sql);
				}
				// And we're all done!
				return $dl_id;
			}
			else {
				return false;
			}
		}

		// Now, get missing variables
		$dl_shortname = $this->shortName($dl_displayname);
		$dl_added = date("Y-m-d H:i:s");

		// Now, we need to get info about the uploaded files
		$dl_orig_filename = $dl_file['name'];
		$extension = strtolower(pathinfo($dl_orig_filename, PATHINFO_EXTENSION));
		$dl_filename = $dl_shortname.".".$extension;
		$dl_filetype = strtoupper($extension);


		// Now, we add the new data to the database
		$sql = "INSERT INTO download_files (dl_shortname, dl_displayname, dl_description, dl_orig_filename, dl_filename, 
			dl_filetype, dl_added, dl_active) VALUES ('$dl_shortname', '$dl_displayname', '$dl_description', '$dl_orig_filename', 
			'$dl_filename', '$dl_filetype', '$dl_added', $dl_active)";
		if ($this->registry->db->query($sql)) {
			$dl_id = $this->registry->db->getInsertId();
		}
		else {
			return false;
		}

		// Now, we add the ACL data to the appropriate table
		foreach ($aclgroup as $acl) {
			$sql = "INSERT INTO download_acl (dl_id, dl_aclgroup) VALUES ($dl_id, $acl)";
			$this->registry->db->query($sql);
		}

		// Now, we move the temporary file to its permanent home
		// If it fails, we fail the whole thing and delete the table data
		$tempname = $dl_file['tmp_name'];
		$location = $this->registry->config['file_location'];
		$destination = $location."/".$dl_filename;
		if (move_uploaded_file($tempname, $destination)) {
			return $dl_id;
		}
		else {
			// Remove all table data - acl will auto delete due to foreign key
			$sql = "DELETE FROM download_files WHERE dl_id = $dl_id LIMIT 1";
			$this->registry->db->query($sql);
			return false;
		}
	}



	// Create a unique shortname for the file based on random characters and the
	// date and time
	public function shortName($longname) {
		$datetime = date("YmdHis");
		$longname = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $longname));
		$length = strlen($longname);
		if ($length<11) {
			$x = 11-$length;
			$extrastring=substr(md5(rand()), 0, $x);
			$longname.=$extrastring;
		}
		else {
			$longname = substr($longname,0,11);
		}
		$shortname = $longname.$datetime;
		return $shortname;
	}


}

?>

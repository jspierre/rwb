This module allows the downloading of files by users. The downloads are limited access based upon the user groups assigned to them by the uploader.  You can control who can upload and who can only download as well. 

1. Move the controller and model to their respective directories in the main app
2. Move the entire 'download' directory contained in the /view directory to the view director in the main app
3. If the folder /fileshare does not exist in your main directory, add it and copy the .htaccess and index.php file from /samples to it (for security)
4. Import both .sql files from the sql directory into your database
5. Log in and go to "ACL Module Management" and add a new Module called download.  This Module Description is: Files-Download. 
6. got to "ACL Function Management" and add a new Function called 'downloadmgmt' that is for the 'download' module.  This function allows: Add and Edit Downloadable Files
7. Still in the "ACL Function Management" and add a new Function called 'mycommunity' that is for the 'appindex' module.  This function allows: MENU: My Community
7. Go to "ACL Group Management" and add the module and functions to user groups as you see fit.
8. Add a new menu item for this new feature wherever you like. RECOMMENDED:
	Go to the main /view/header.php and add this near the other dropdown menus like it:

	<?php
        if ($this->registry->security->checkFunction("mycommunity")) { ?>
          <li class="nav-item dropdown">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Community</a>
              <div class="dropdown-menu" aria-labelledby="dropdown02">
                <a class="dropdown-item" href="<?= BASE_URL ?>download">Download Community Files</a>
              </div>
          </li>
          <?php } ?>


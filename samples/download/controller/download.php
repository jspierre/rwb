<?php

class download extends baseController {

	public function index() {
		$this->model = new mDownload($this->registry); 
		
		// If any POSTed data was sent, we need to sanitize it
		$this->registry->security->sanitizePost(); 
		
		if ($this->registry->passedVars) {
			$filerequest = strtolower($this->registry->passedVars[0]);
			if ($this->getDownload($filerequest)) {
				return true;
			}
			else {
				echo '<html><body>Access Denied. <a href = "'.BASE_URL.'"> Click here to continue.</body></html>';
				exit;
			}
		}
		elseif (isset($_POST['submit']) && $_POST['submit'] == "new") {
			return $this->showDownload(true);
		}
		// Call to Save a new download submission
		elseif (isset($_POST['submit']) && isset($_FILES) && $_POST['submit'] == "savenew") {
			$dl_id = $this->model->addEditFile(true, $_POST, $_FILES);
			if ($dl_id == false) {
				return $this->showDownload(false);
			}
			else {
				return $this->showDownload(false, $dl_id, false);
			}
		}
		// Call to Save an UPDATE to download submission
		elseif (isset($_POST['submit']) && $_POST['submit'] == "update") {
			$dl_id = $this->model->addEditFile(false, $_POST, false);
			if ($dl_id == false) {
				return $this->showDownload(false);
			}
			else {
				return $this->showDownload(false, $dl_id, false);
			}
		}
		// Call to Show Screen for Editing an Existing Download Submission
		elseif (isset($_POST['submit']) && substr($_POST['submit'],0,3) == "UNX") {
			$dl_id = substr($_POST['submit'],3);
			return $this->showDownload(false, $dl_id, true);
		}
		else {
			$loginnum = $_SESSION['usernum'];
			$files= $this->model->getFileList($loginnum, false);
			$rvalue['variables']['files'] = $files;
			$rvalue['type']='html';
			$rvalue['variables']['title']="Download Important Files - MyStJulienPlace.org";
			$rvalue['headerfile']="header.php";
			$rvalue['bodyfile'] = "download/body_show_downloads.php";
			$rvalue['footerfile']="footer.php";
			return $rvalue;
		}
		
	}
	
	public function getDownload($filerequest) {
		// Send the request to the model to get the file info
		$loginnum = $_SESSION['usernum'];
		$filedata = $this->model->getDownloadFile($filerequest, $loginnum); 
		// Get back the actual file location, or FALSE if it doesn't exist or no access
		if ($filedata) {
			$location = $this->registry->config['file_location'];
			$file = $location."/".$filedata;
			if (file_exists($file)) {
			$fp = fopen($file, 'rb');
				header("Content-Type: application/octet-stream");
				header("Content-Disposition: attachment; filename=$filedata");
				header("Content-Length: " . filesize($file));
				fpassthru($fp);
				$explanation = "FILE DOWNLOAD BY %%USER%% FROM %%IP%%: ".$filerequest;
				$this->registry->logging->logEvent($this->registry->config['logging_cat_success'],$explanation);
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}


	public function showDownload($newDownload, $dl_id=false, $isdisplay=false) {
		if ($newDownload == false && $dl_id == false) {
			// Return Code for an Unsuccessful update/add
			$rvalue['variables']['returncode'] = 0;
		}
		elseif ($newDownload == false) {
			// Return Code for a successful update/add
			if ($isdisplay == false) {	$rvalue['variables']['returncode'] = 1; }
			$rvalue['variables']['dl_id'] = $dl_id;
			// Pull Data to fill in database
			$filedata = $this->model->getFileData($dl_id);
			$rvalue['variables']['filedata'] = $filedata;
		}
		$rvalue['type']='html';
		$rvalue['variables']['title']="Download Files Management - Condunity";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "download/body_update_download.php";
		$rvalue['footerfile']="footer.php";
		$securemodel = new mSecurity($this->registry);
		$aclgroups = $securemodel->getAllACLGroups();
		$rvalue['variables']['aclgroups'] = $aclgroups;
		if ($newDownload==true) {
			$rvalue['variables']['actiontype'] = "NEW";
		}
		else  {
			$rvalue['variables']['actiontype'] = "EDIT";
		}
		return $rvalue;
	}






}

?>

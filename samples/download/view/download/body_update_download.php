
<form class="form-horizontal" action="<?= BASE_URL ?>download" method="post" name="editdownload" id="editdownload" enctype="multipart/form-data">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  //var_dump($_SESSION);
  //var_dump($_POST);
  //exit();
  //var_dump($values);
  
  if (isset($values['aclgroups'])) {
    $aclgroups = $values['aclgroups'];
  }
 
  // Determine what type of thing we are doing - new, display, or edit?
  if (isset($values['actiontype'])) {
    $actiontype = $values['actiontype'];
  }
  else {
    $actiontype = "NEW";
  }

  if (isset($values['group']['actiontype']) && $values['group']['actiontype'] == "UPDATE") {
      echo '<h3 class="panel-title">Update Existing Download</h3>'; 
    }
    else {
      echo '<h3 class="panel-title">New Download Entry Form</h3>';
    }
?>
</div>
<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($values['returncode'])) {
                if ($values['returncode'] > 0) { 
                    $color = "alert-success"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        
                switch ($values['returncode']) {
                    case 1:
                        echo "Download Added/Updated. Please check settings to make sure all is correct!";
                        break;
                    case 0:
                        echo "ERROR: File was NOT uploaded - see your system administrator for help if needed.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '</div>';
            }                    
?>        


<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($values['dl_id']) && $actiontype != "NEW") {
  $dl_id = $values['dl_id'];
  echo '<input type="hidden" name="dl_id" value ='.$dl_id.' />';
  echo '<input type="hidden" name="dl_shortname" value ='.$dl_shortname.' />';
  if (isset($values['filedata'])) { extract($values['filedata']); };
}
?>


<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="dl_id">Download #</label>  
    <div class="col-md-2">
      <input id="dl_id" name="dl_id" type="text" placeholder="" class="form-control input-md" value="<?= $dl_id?>" disabled >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="dl_shortname">System Name</label>  
    <div class="col-md-2">
      <input id="dl_shortname" name="dl_shortname" type="text" placeholder="" class="form-control input-md" value="<?= $dl_shortname?>" disabled >
    </div>
  </div>
</div>

<div class="form-group">
   <div class="row">
    <label class="col-md-2 control-label" for="dl_displayname">Short Description</label>  
    <div class="col-md-6">
      <input id="dl_displayname" name="dl_displayname" type="text" placeholder="" class="form-control input-md" maxlength="50" value="<?= $dl_displayname?>" required >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="dl_description">Long Description</label>  
    <div class="col-md-5">
      <textarea rows="2" cols = "76" name="dl_description" id="dl_description" class="form-control" required><?= $dl_description ?></textarea> 
    </div>
  </div>
</div>

<?php
  if ($actiontype=="NEW") {
    ?>
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="dl_file">Select File</label>
    <div class = "col-md-5">
      <input type="file" name="dl_file" id="dl_file">
    </div>
  </div>
</div>
  <?php
}
?>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="dl_active">Download Active?</label>
    <div class="col-md-2">
      <select id="dl_active" name="dl_active" class="form-control" >
        <option value="1" <?php if ($dl_active==1) { echo "selected"; }?>>YES</option>
        <option value="0" <?php if ($dl_active==0 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>NO</option>
      </select>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="permissions">Permissions:</label>  
    <?php
      $x=0;
      foreach ($aclgroups as $aclgroup) {
        if ($aclgroup['isactive'] == 0) { $disabled = "disabled"; } else { $disabled = ""; }
        if ($x==0) {echo '<div class = "col-md-2">';}
        else {
          echo '  <div class="row">';
          echo '  <label class="col-md-2 control-label"></label>';
          echo '    <div class = "col-md-2">'; 
        }
        echo '    <div class="checkbox">'."\n";
        if (isset($acl)) {
          $checked = "";
          foreach ($acl as $aclcheck) {
            if ($aclgroup['groupindex'] == $aclcheck['dl_aclgroup']) { $checked = "checked"; }
          }
        }
        echo '      <input type = "checkbox" name="aclgroup[]" value='.$aclgroup['groupindex'].' '.$checked.' '.$disabled.' >';
        if ($disabled == "disabled") {
          echo '          '.$aclgroup['groupname'];  
        }
        else {
          echo '          <strong>'.$aclgroup['groupname']."</strong>";
        }
        echo '      </input>'."\n"; 
        echo "    </div>\n";
        echo "  </div>\n";
        echo "</div>\n";

        $x++;

      }
    ?>
  </div>
</div>
<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-4">
      <?php if ($actiontype == "NEW") {
              unset($_SESSION['modulenum']);
              ?><button id="submit" name="submit" value="savenew" class="btn btn-primary">ADD NEW DOWNLOAD</button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>download" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">UPDATE DOWNLOAD</button>
              <button id="submit" name="submit" value="download" class="btn btn-primary">CANCEL</button>
              <?php
            }?>
      
    </div>
  </div>
</div>

</fieldset>
</form>



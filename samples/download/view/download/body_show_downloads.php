
<div class="panel panel-primary">

  <div class="panel-heading">
    <h3 class="panel-title">File Storage Download Area</h3>
  </div>

</div>

  <form action="<?= BASE_URL ?>download" method="post" name="showdownloads" id="download_form">
    <table class="table table-bordered table-hover table-responsive">
      <thead>
        <tr>
            <th class="cond-fn">File Name</th>
            <th>Description</th>
            <?php if ($this->registry->security->checkFunction("downloadmgmt")) { ?> <th>Status</th> <?php } ?>
            <th>Download</th>
            <?php if ($this->registry->security->checkFunction("downloadmgmt")) { ?> <th>Action</th> <?php } ?>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['files']) && $values['files']) {
      $rows = $values['files'];  
      foreach ($rows as $row) {
        if ($row['dl_active'] == 1) {$astatus = "ACTIVE";} else {$astatus="INACTIVE";}      
        if ($this->registry->security->checkFunction("downloadmgmt")) {$edit=true;} else {$edit=false;}
        if ($row['dl_active'] == 1 || $edit == true) {
          ?>
          <tr>
            <td><?= $row['dl_displayname'] ?></td>
            <td><?= $row['dl_description'] ?></td>
            <?php if ($this->registry->security->checkFunction("downloadmgmt")) { ?> <td><?= $astatus ?></td> <?php } ?>
            <td><a class="btn btn-primary" href="<?= BASE_URL ?>download/<?= $row['dl_shortname'] ?>" role="button">DOWNLOAD</a></td>
            <?php if ($this->registry->security->checkFunction("downloadmgmt")) { 
              ?> <td><button id="submit" name="submit" class="btn btn-primary" value="UNX<?= $row['dl_id'] ?>">EDIT</button></td> <?php
              } ?>
          </tr>
        <?php
        }
      }
    }
    else {
      ?>
      <tr>
        <td colspan="5">Sorry, no files are available for you to download at this time.  Please contact an administrator 
          if you believe this message is wrong.</td>
      </tr>
      <?php
    }
  ?>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <?php if ($this->registry->security->checkFunction("downloadmgmt")) { 
          ?><button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW DOWNLOAD</button> <?php
        } ?>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>

</div>
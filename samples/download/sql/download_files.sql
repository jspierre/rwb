-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2019 at 05:05 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rwb`
--

-- --------------------------------------------------------

--
-- Table structure for table `download_files`
--

CREATE TABLE `download_files` (
  `dl_id` int(8) NOT NULL COMMENT 'The auto ID of the downloadable file',
  `dl_shortname` varchar(25) NOT NULL COMMENT 'The internal ID name of the file to identify it in the controller (no spaces, all lowercase)',
  `dl_displayname` varchar(50) NOT NULL DEFAULT '' COMMENT 'The name as displayed to the end user',
  `dl_description` varchar(255) NOT NULL DEFAULT '' COMMENT 'A longer description of the file''s contents',
  `dl_orig_filename` varchar(255) NOT NULL DEFAULT '' COMMENT 'The original file name from the client machine',
  `dl_filename` varchar(255) NOT NULL DEFAULT '' COMMENT 'The actual filename of the file on the file system',
  `dl_filetype` varchar(50) NOT NULL DEFAULT '' COMMENT 'The file extension for the file',
  `dl_added` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Date and time the file was added',
  `dl_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Is the file active and available to DL? (0=No, 1=YES)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Downloadable Files Table';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `download_files`
--
ALTER TABLE `download_files`
  ADD PRIMARY KEY (`dl_id`),
  ADD KEY `dlshortname` (`dl_shortname`),
  ADD KEY `dlactive` (`dl_active`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `download_files`
--
ALTER TABLE `download_files`
  MODIFY `dl_id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'The auto ID of the downloadable file', AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

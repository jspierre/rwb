-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2019 at 05:05 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rwb`
--

-- --------------------------------------------------------

--
-- Table structure for table `download_acl`
--

CREATE TABLE `download_acl` (
  `dlacl_id` int(8) NOT NULL COMMENT 'The auto ID of the ACL Rule for a DL File',
  `dl_id` int(8) NOT NULL COMMENT 'The download ID of the File in Question from the _files table',
  `dl_aclgroup` int(6) NOT NULL COMMENT 'The ACL Group that is granted access to this file'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The ACL rules for the download_files table';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `download_acl`
--
ALTER TABLE `download_acl`
  ADD PRIMARY KEY (`dlacl_id`),
  ADD UNIQUE KEY `dl_id` (`dl_id`,`dl_aclgroup`),
  ADD KEY `dlid` (`dl_id`),
  ADD KEY `dl_aclgroup` (`dl_aclgroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `download_acl`
--
ALTER TABLE `download_acl`
  MODIFY `dlacl_id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'The auto ID of the ACL Rule for a DL File';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `download_acl`
--
ALTER TABLE `download_acl`
  ADD CONSTRAINT `downloadaclgroup` FOREIGN KEY (`dl_aclgroup`) REFERENCES `security_acl_groups` (`groupindex`),
  ADD CONSTRAINT `downloadidacl` FOREIGN KEY (`dl_id`) REFERENCES `download_files` (`dl_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

### How to use samples ###

The samples in this folder were fully working before being archived, so they should work with minimal effort on your part.

1. Within the samples' folder, there will likely be a controller, model, view, and sql folder and perhaps a readme file.
2. Copy the file(s) from the controller folder to the main app's /controller folder
3. Copy the file(s) from the model folder to the main app's /model folder
4. Copy the file(s) from the view folder into the main app's /view folder.  NOTE: If the view files are in their own directory within the /view folder, move the ENTIRE FOLDER, not just the file it contains.
    EXAMPLE: samples/download/view contains a folder called dowload inside of which are two view files (located at samples/download/view/download)  move the /download folder and the two files it contains to /view.  IF YOU DO NOT, THE MODULE WILL FAIL TO RUN PROPERLY
5. Now, go into your database engine of choice (or use PHPMySQL or similar) and IMPORT into your main database each and evey .sql file contained in the sql folder 
6. Check the readme file in the sample folder to tell you what Modules and Functions you need to add to your system in order to grant access to the new Feature to your users.



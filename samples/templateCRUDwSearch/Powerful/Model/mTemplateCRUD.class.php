<?php

   /*
    Payload Return (USE FOR ALL non-"list-only" returns IN THE MODEL!!)
            $returnPayload[0]   BOOL - was the operation successful? (true/false)
            $returnPayload[1]   INT - Level of severity (0=None [default], 1=Success, 2=Info, 3=Warning, 4=Danger, 5=BLACK)
            $returnPayload[2]   STRING - a short description of the success or failure
            $returnPayload[3]   STRING- The message the user will see in the alert box if shown
            $returnPayload[4]   ARRAY - All of the data that came from the VIEW on the FORM, so it can 
                                be redisplayed along with the error at top OR the data from the database
                                of those same fields for display
    */

class mHallmarkArtist extends baseModel {

    // Fill this in to keep logs, etc. up to date.
    public $classname = "mHallmarkArtist";


    // This function gets ONE specific record
    public function getRecordData($record) {
        $sql = "SELECT * FROM collections_artists_hallmark WHERE artistno = $record LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars) {
            $returnPayload[0] = true;
            $returnPayload[1] = 1;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record SUCCESS";
            $returnPayload[3] = "";
            $returnPayload[4] = $vars;
        }
        else { 
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record FAILURE";
            $returnPayload[3] = "Database retrieval failure. Try again or contact Support.";
            $returnPayload[4] = $vars;
        }
        return $returnPayload;
    }

   
    // This simple function returns ALL Active Artists' names and index numbers for use in other modules
    public function getAllArtists() {
        $sql = "SELECT artistno, firstname, lastname, displayname FROM collections_artists_hallmark WHERE isactive = 1 ORDER BY lastname, firstname ASC";
        $this->registry->db->query($sql);
        $vars=$this->registry->db->getAllRows();
        return $vars; 
    }



    // $posted is every field needed to update the necessary record
    // $action can be "ADD" or "UPDATE".  Anything else results in error
    public function updateRecordData($posted, $action=false) {
        

        // If $action remains false or is not valid, let's error out now.
        if ($action == false || ($action != "ADD" && $action != "UPDATE")) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "updateRecordData for ".$this->classname." called with missing or incorrect ACTION ($action).";
            $returnPayload[3] = "There was a fatal error calling the database.  Please contact support.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }

        // First, extract all the data to individual variables
        // We sanitized it in the controller, so there's no need to do it again.
        extract($posted);

        // If any fields need to be lowercase or uppercase, here is where to add that code
        $firstname = trim(strtoupper($firstname));
        $lastname = trim(strtoupper($lastname));
        $displayname = trim(ucwords(strtolower($displayname)));

        // Check for missing fields
        $error = 0;
        $errorreason = "";
        if ($firstname == "") { $errorreason .= "First Name is a Required Field. "; $error = 1; }
        if ($lastname == "") { $errorreason .= "Last Name is a Required Field. "; $error = 1; }
        if ($displayname == "") { $errorreason .= "Display Name is a Required Field. "; $error = 1; }
        if ($error == 1) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "Required fields were missing.";
            $returnPayload[3] = $errorreason;
            $returnPayload[4] = $posted;
            return $returnPayload;
        }

        /* 
            if (isset($emailaddress)) {$emailaddress = strtolower($emailaddress); $data['emailaddress'] = $emailaddress; } 
            */

        // Now do any data validation needed that can be done without a DB call.  For example, cleaning up
        // and verifying email addresses, etc.  e.g.,:
            if ($yearstart > 0 && $yearretire > 0 ) {
                if ($yearretire < $yearstart) {
                    $returnPayload[0] = false;
                    $returnPayload[1] = 4;
                    $returnPayload[2] = "Start Year is after Retire Year - Improper Input";
                    $returnPayload[3] = "Start Year must be BEFORE Retire Year - Please try again.";
                    $returnPayload[4] = $posted;
                    return $returnPayload;
                }
            }

            // If you set a complete year, then iscomplete must also be set to 1
            if ($deathdate != "" && $deathdate != "0000-00-00" && $isdeceased == 0) {
                $isdeceased = 1;
            }

        // Now we check for any duplicates that you'd like to check for before adding.  For example:
           if (isset($record) && $record > 0) { $extrasql = "AND (artistno <> $record"; $es = true; } else { $extrasql = ""; $es = false; }
           $sql = "SELECT artistno FROM collections_artists_hallmark WHERE (firstname = '$firstname' AND lastname = '$lastname') OR (displayname LIKE '$displayname') $extrasql LIMIT 1";
           //echo $sql;
           //exit();
            $exists = $this->registry->db->oneRowQuery($sql);
            if ($exists) {
                $duplicateurl = BASE_URL."hallmarkartmgmt/".$exists['artistno'];
                $returnPayload[0] = false;
                $returnPayload[1] = 4;
                $returnPayload[2] = "ADD ARTIST FAILED: Duplicate ID found for $firstname $lastname as ".$exists['artistno'].".";
                $returnPayload[3] = "ERROR: DUPLICATE ARTIST DETECTED.  Please see <a href=".'"'.$duplicateurl.'"'."> existing Artist # ".$exists['artistno']."</a>.";
                $returnPayload[4] = $posted;
                return $returnPayload;
            }
        

        // Set any other variables or final touches (default or extra db values, etc) here
        if (isset($deathdate) && $deathdate != "" && $deathdate != "0000-00-00") {
            $dateofdeath = $this->registry->db->validateDate($deathdate);
        }
        else {
            $dateofdeath = '0000-00-00';
        }

        $enteredon = $this->registry->getDateTime();
        $usernum = $_SESSION['usernum'];

        // Now, we set the SQL code to either add a new record or update the existing record depending upon
        // what was sent
        if ($action == "ADD") {
            $sql = "INSERT INTO collections_artists_hallmark (firstname, lastname, displayname, birthmonth, birthday, birthyear,
                yearstart, yearretire, isactive, isdeceased, deathdate, gender, bio, internalnotes, enteredby, enteredon, lastupdateby) VALUES 
            ('$firstname', '$lastname', '$displayname', $birthmonth, $birthday, $birthyear, $yearstart, $yearretire, 
            $isactive, $isdeceased, '$dateofdeath', $gender, '$bio', '$internalnotes', $usernum, '$enteredon', $usernum)";
        }
        elseif ($action == "UPDATE") {
            $sql = "UPDATE collections_artists_hallmark SET firstname = '$firstname', lastname = '$lastname', displayname = '$displayname',
            birthmonth = $birthmonth, birthday = $birthday, birthyear = $birthyear, yearstart = $yearstart, yearretire = $yearretire,
            isactive = $isactive, isdeceased = $isdeceased, deathdate = '$dateofdeath', gender = $gender, bio = '$bio', 
            internalnotes = '$internalnotes', lastupdateby = $usernum WHERE artistno = $record LIMIT 1";
        }

        // We run the transaction - if the insert/update fails, we send back an error
        if($this->registry->db->query($sql) == false) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "INSERT/UPDATE FAILED: ".$this->classname." - DATABASE ERROR when SQL ran";
            $returnPayload[3] = "FAILED: An error has occurred.  Please contact your system administrator.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }
        
        // If this was an ADD we pull the new record number to give back to the system
        if ($action == "ADD") { 
            $record = $this->registry->db->getInsertId(); 
            $posted['record'] = $record;
        }

        // That's all there is to it! Let's send back the data and be done!
        $returnPayload[0] = true;
        $returnPayload[1] = 1;
        $returnPayload[2] = "$action RECORD completed successfully ".$this->classname." - RECORD # $record";
        $returnPayload[3] = "$action completed successfully (Record# $record) - please check the data below.";
        $returnPayload[4] = $posted;
        return $returnPayload;
    }




    /*  ********************************************************************** */
    /*  ***************** SEARCH METHOD FOR SEARCH CONTROLLER **************** */
    /*  ********************************************************************** */
    // You will need to adjust your SQL here to fit your needs for the search
    // You would also add other wiring to do different limiters or types of search
    public function doSearch($searchterms, $active=0, $searchall=0) {
        $terms = explode(" ", $searchterms);
        $numterms = count($terms);
        $x=0;
        $sql = "SELECT * FROM collections_artists_hallmark WHERE ";
         if ($active==1) {
            $sql .= "isactive = 1 AND ";
        }
        foreach ($terms as $term) {
            $x++;
            if ($searchall==1) {
                $sql .= "(artistno LIKE '%$term%' OR firstname LIKE '%$term%' OR lastname 
                    LIKE '%$term%' OR displayname LIKE '%$term%' OR yearstart LIKE '%$term%' OR yearretire LIKE '%term%')";
            }
            else {
                $sql .= "(firstname LIKE '%$term%' OR lastname LIKE '%$term%' OR displayname LIKE '%term')";                
            }
            if ($x!==$numterms) {
                $sql .= " AND ";
            }
        }
        /*  **********************************************************************************
            Add additional code here to limit the search further or perform other cool things 
            **********************************************************************************
        if ($searchall==2 || $searchall==3 || $searchall==4 || $searchall==5 || $searchall==6) { 
            $sql.=" ORDER BY resident_units.unitnumber";
        }
        else {
            $sql.=" ORDER BY lastname, firstname ASC";
        }
        */
        $sql.=" ORDER BY lastname, firstname ASC";
        //echo $sql;
        //die();
        $result=$this->registry->db->query($sql);
        if ($result) {
            return $result;
        }
        return false;
    }




    public function getClassStyles() {
        $sql = "SELECT * FROM courses_styles ORDER BY stylename ASC";
        $result=$this->registry->db->query($sql);
        if ($result->num_rows > 0) {
            $result=$this->registry->db->getAllRows();
            return $result;
        }
        return false;
    }


}


?>
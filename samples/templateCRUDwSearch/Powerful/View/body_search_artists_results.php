
<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>
<div class="panel panel-primary">

  <div class="panel-heading pb-4">

      <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    // And now, let's set our page on-screen header
    $header = $thingname." Record Search Results";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
    ?>
    <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>

  </div>

</div>

  <form action="<?= BASE_URL ?>hallmarkartmgmt" method="post" name="hallmarkartmgmt" id="client_form">
    <input type="hidden" name="searchform" value="searchform" />
    <table class="table table-bordered table-hover table-responsive-lg" id="myTable2">
      <thead>
        <tr>
            <th onclick="sortTable(0)"><a href="#">Artist #</a></th>
            <th onclick="sortTable(1)"><a href="#">First Name</a></th>
            <th onclick="sortTable(2)"><a href="#">Last Name</a></th>
            <th onclick="sortTable(3)"><a href="#">Start Year</a></th>
            <th onclick="sortTable(4)"><a href="#">Retire Year</th>
            <th onclick="sortTable(5)"><a href="#">Living</th>
            <th onclick="sortTable(5)"><a href="#">Status</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['results'])) {
      $rows = $values['results'];  
    }
    foreach ($rows as $row) {
      if ($row['isactive'] == 1) {$astatus = "Active"; } else {$astatus = "Inactive"; }
      if ($row['isdeceased'] == 1) {$lstatus = "Deceased";} else {$lstatus="Living";}
      if ($row['yearretire'] == 0) { $yearretire = "Not Retired"; } else { $yearretire = $row['yearretire']; }
      ?>
      <tr>
            <td><?= $row['artistno'] ?></td>
            <td><?= $row['firstname'] ?></td>
            <td><?= $row['lastname'] ?></td>
            <td><?= $row['yearstart'] ?></td>
            <td><?= $yearretire ?></td>
            <td><?= $lstatus ?></td>
            <td><?= $astatus ?></td>
            <td><button id="submit" name="submit" class="btn btn-primary" value="select-<?= $row['artistno'] ?>">SELECT</button></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="research" class="btn btn-primary">BACK TO SEARCH</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             






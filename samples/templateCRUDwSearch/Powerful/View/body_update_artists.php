<!-- datetime picker -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script>
  $( function() {
    $("#datepicker" ).datepicker({ minDate: 0 });
    if ($('input[type=checkbox]').prop('checked')) {
      $("#datepicker").datepicker().datepicker('setDate', new Date());
    }
  } );
</script>


<form class="form-horizontal" action="<?= BASE_URL ?>hallmarkartmgmt" method="post" name="hallmarkartmgmt" id="artist_form">
<fieldset>
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  
// Let's get all of our variables extracted so we can work with them
extract($values);
if (isset($returnPayload[4]) && !empty($returnPayload[4])) {
  extract ($returnPayload[4]);
}

// Now, let's set some inputs so we'll have them again later
// Replace $coursenum with the index variable in your table here
// We'll also 
if (isset($record) && empty($artistno)) { 
  $artistno = $record; 
}
if (isset($record)) {
  echo '<input type="hidden" name="record" value="'.$record.'">';
}
if (!empty ($artistno) && empty($datecreated)) { $datecreated = "Just Now"; }

// Is this an error situation? If so, edit needs to be re-opened
if (isset($returnPayload[0]) && $returnPayload[0] == false) {
  if (empty($artistno)) { $action = "ADD"; } else { $action = "EDIT"; }
}


// This is the Headline on the page 
if ($action=="ADD") {
  $header = "Add New ".$thingname." Form";
}
elseif ($action=="EDIT") {
  $header = "Editing ".$thingname." #".$record;
}
else {
  $header = "Displaying ".$thingname." #".$record;
}
echo '<h3 class="panel-title">'.$header.'</h3>'; 
echo '<p><small>NOTE: Items in <strong>bold</strong> are Required.</small></p>';
?>

</div>
<div class="panel-body">
             
<?php
// Now, let's deal with any return messages we need to display.  These are always
// located in the $returnPayload array
if (isset($returnPayload)) {
    if (empty($returnPayload[3])) {$message = false; } else { $message = $returnPayload[3];}
  switch ($returnPayload[1]) {
    case 1:
      $color="alert-success";
      break;
    case 2:
      $color="alert-info";
      break;
    case 3:
      $color="alert-warning";
      break;
    case 4:
      $color="alert-danger";
      break;
    case 5:
      $color="alert-dark";
      break;
    default:
      $color="alert-primary";
      break;
  }
}
if ($message) {
  echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
  echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
  echo $message;
  echo '</div>';
}
?>        



<?php 
// This is for testing only, comment out for production
error_reporting(E_ALL & ~E_NOTICE);
?>

<?php
// This is the button section.  It will display certain additional buttons
// based upon access level and current task.  
if ($action=="DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("hallartmgmte")) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning">Edit <?= $thingname ?></button>
        <?php 
        }
      ?>
</div>
<?php
}
?>

<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="artistno"><abbr title="This is the assigned internal Artist Number - it cannot be changed.">Artist No.</abbr></label>  
    <div class="col-md-2">
      <input id="artistno" name="artistno" type="text" placeholder="" class="form-control input-md" value="<?= $artistno ?>" disabled>
      <input id="artistno" name="artistno" type="hidden" value="<?= $artistno ?>">
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="firstname"><abbr title="The properly spelled last name of the artist"><strong>First Name</strong></abbr></label>  
    <div class="col-md-6">
      <input id="firstname" name="firstname" type="text" maxlength="100" placeholder="" class="form-control input-md" value="<?= $firstname ?>" required <?php if ($action == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="lastname"><abbr title="The properly spelled last name of the artist"><strong>Last Name</strong></abbr></label>  
    <div class="col-md-6">
      <input id="lastname" name="lastname" type="text" maxlength="100" placeholder="" class="form-control input-md" value="<?= $lastname ?>" required <?php if ($action == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="displayname"><abbr title="The way the artist's name should be displayed in text, webpages, etc (including nicknames etc.)"><strong>Display Name</strong></abbr></label>  
    <div class="col-md-6">
      <input id="displayname" name="displayname" type="text" maxlength="255" placeholder="" class="form-control input-md" required value="<?= $displayname ?>" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  >
    </div>
  </div>
</div>

<div class="form-group">
    <div class="row">
      <label class="col-md-2 control-label" for="isactive"><abbr title="Is this artist active for use by items and collections? Setting to INACTIVE will make this artist unavailable for NEW items added to the master database, but will not affect existing items.">System Status</abbr></label>
      <div class="col-md-2">
       <select id="isactive" name="isactive" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
         <option value="1" <?php if ($isactive==1) { echo "selected"; }?>>Active</option>
         <option value="0" <?php if ($isactive==0 && ($action !== "ADD")) { echo "selected"; }?>>Inactive</option>
        </select>
       </div>
      <label class="col-md-2 control-label" for="gender"><abbr title="What gender, if any, does this artist identify with?">Gender Identity</abbr></label>
      <div class="col-md-2">
       <select id="gender" name="gender" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
         <option value="0" <?php if ($gender==0 && ($action !== "ADD")) { echo "selected"; }?>>Undisclosed</option>
         <option value="1" <?php if ($gender==1) { echo "selected"; }?>>Male</option>
         <option value="2" <?php if ($gender==2) { echo "selected"; }?>>Female</option>
         <option value="3" <?php if ($gender==3) { echo "selected"; }?>>Non-Binary</option>
        </select>
       </div>
    </div>

  <div class="row">
    <label class="col-md-2 control-label" for="birthday"><abbr title="List the birth month, day, and year of this artist if known">Birthdate</abbr></label> 
    <div class="col-md-2">
      <select id="birthmonth" name="birthmonth" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($birthmonth==0 && ($action !== "ADD")) { echo "selected"; }?>>-Month-</option>
        <option value="1" <?php if ($birthmonth==1) { echo "selected"; }?>>01- January</option>
        <option value="2" <?php if ($birthmonth==2) { echo "selected"; }?>>02- February</option>
        <option value="3" <?php if ($birthmonth==3) { echo "selected"; }?>>03- March</option>
        <option value="4" <?php if ($birthmonth==4) { echo "selected"; }?>>04- April</option>
        <option value="5" <?php if ($birthmonth==5) { echo "selected"; }?>>05- May</option>
        <option value="6" <?php if ($birthmonth==6) { echo "selected"; }?>>06 - June</option>
        <option value="7" <?php if ($birthmonth==7) { echo "selected"; }?>>07- July</option>
        <option value="8" <?php if ($birthmonth==8) { echo "selected"; }?>>08- August</option>
        <option value="9" <?php if ($birthmonth==9) { echo "selected"; }?>>09- September</option>
        <option value="10" <?php if ($birthmonth==10) { echo "selected"; }?>>10- October</option>
        <option value="11" <?php if ($birthmonth==11) { echo "selected"; }?>>11- November</option>
        <option value="12" <?php if ($birthmonth==12) { echo "selected"; }?>>12- December</option>
      </select>
    </div>
    <div class="col-md-2">
      <select id="birthday" name="birthday" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($birthday==0 && ($action !== "ADD")) { echo "selected"; }?>>-Day-</option>
        <?php
        for ($i=1; $i < 32; $i++) { 
          if ($i < 10) { $addzero = "0"; } else { $addzero = ""; }
          ?> 
          <option value="<?= $i ?>" <?php if ($birthday==$i) { echo "selected"; }?>><?= $addzero ?><?= $i ?></option>  
          <?php
        }
        ?>
      </select>
    </div>
    <div class="col-md-2">
      <select id="birthyear" name="birthyear" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($birthyear==0 && ($action !== "ADD")) { echo "selected"; }?>>-Year-</option>
        <?php
        for ($i=1900; $i < 2011; $i++) { 
          if ($i < 10) { $addzero = "0"; } else { $addzero = ""; }
          ?> 
          <option value="<?= $i ?>" <?php if ($birthyear==$i) { echo "selected"; }?>><?= $addzero ?><?= $i ?></option>  
          <?php
        }
        ?>
      </select>
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="isdeceased"><abbr title="Is this artist alive or dead?">Life Status</abbr></label>
    <div class="col-md-2">
     <select id="isdeceased" name="isdeceased" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
       <option value="0" <?php if ($isdeceased==0 && ($action !== "ADD")) { echo "selected"; }?>>Alive</option>
       <option value="1" <?php if ($isdeceased==1) { echo "selected"; }?>>DECEASED</option>
      </select>
    </div>
    <label class="col-md-2 control-label" for="deathdate"><abbr title="If the artist is deceased, the date that they passed away in MM/DD/YYYY format.">Date of Death</abbr></label>  
    <div class="col-md-2">
      <input id="datepicker" name="deathdate" type="text" placeholder="" class="form-control input-md datepicker" value="<?= $deathdate ?>" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="yearstart"><abbr title="The year that the artist began working for Hallmark as an artist">Hallmark Start Year</abbr></label>  
    <div class="col-md-2">
      <select id="yearstart" name="yearstart" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($yearstart==0 && ($action !== "ADD")) { echo "selected"; }?>>-Year-</option>
        <?php
        $thisyear = date("Y") + 1;
        for ($i=1910; $i < $thisyear; $i++) { 
          if ($i < 10) { $addzero = "0"; } else { $addzero = ""; }
          ?> 
          <option value="<?= $i ?>" <?php if ($yearstart==$i) { echo "selected"; }?>><?= $addzero ?><?= $i ?></option>  
          <?php
        }
        ?>
      </select>
    </div>
    <label class="col-md-2 control-label" for="yearretire"><abbr title="The year that the artist began working for Hallmark as an artist">Hallmark Retire Year</abbr></label>  
    <div class="col-md-2">
      <select id="yearretire" name="yearretire" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($yearretire==0 && ($action !== "ADD")) { echo "selected"; }?>>Not Retired</option>
        <?php
        for ($i=1910; $i < $thisyear; $i++) { 
          if ($i < 10) { $addzero = "0"; } else { $addzero = ""; }
          ?> 
          <option value="<?= $i ?>" <?php if ($yearretire==$i) { echo "selected"; }?>><?= $addzero ?><?= $i ?></option>  
          <?php
        }
        ?>
      </select>
    </div>
  </div>

  <?php if ($this->registry->security->checkFunction("hallartmgmtl")) { ?>
  <?php $getuser = $this->registry->security->getUserName($lastupdateby); ?>
  <?php $betterdate = explode(" ", $lastupdate); $lastdate = $betterdate[0]; $lasttime = $betterdate[1]; ?>
  <?php $lastchange = "By User # ".$lastupdateby." (".$getuser['firstname']." ".$getuser['lastname'].") on ".$lastdate." at ".$lasttime ?>
  <div class="row">
    <label class="col-md-2 control-label" for="lastchange"><abbr title="Data regarding who and when the last change happened to this record">Last Change</abbr></label>
    <div class="col-md-6">
      <input id="lastchange" name="lastchange" type="text" placeholder="" class="form-control input-md datepicker" value="<?= $lastchange ?>" disabled >
    </div>
  </div>
<?php } ?>

</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="bio"><abbr title="A full professional bio of the artist, not to exceed 65500 characters.">Artist Biography</abbr></label>  
    <div class="col-md-6">
      <textarea id="bio" name="bio" rows="5" placeholder="" maxlength="65500" class="form-control input-md" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  ><?= $bio ?></textarea>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="internalnotes"><abbr title="Internal/Private Notes about this artist that cannot be seen by customers - only staff can see this - max of 65500 characters.">Internal Notes</abbr></label>  
    <div class="col-md-6">
      <textarea id="internalnotes" name="internalnotes" rows="5" placeholder="" maxlength="65500" class="form-control input-md" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  ><?= $internalnotes ?></textarea>
    </div>
  </div>
</div>



<?php
// AND NOW HERE ARE OUR BOTTOM BUTTONS....
if ($action == "DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("crsedit")) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning">Edit <?= $thingname ?></button>
        <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("usernotes")) { ?>
          <button id="submit" name="submit" value="notes" class="btn btn-sm btn-primary"><?= $thingname ?> Notes</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("userlog")) { ?>
          <button id="submit" name="submit" value="log" class="btn btn-sm btn-primary"><?= $thingname ?> Log</button>
      <?php 
        }
      ?>
</div>
<?php
}
?>

<!-- Button -->
<?php if ($action !== "DISPLAY") {
  ?>
<div class="form-group">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-4">
      <?php if ($action == "ADD") {
              ?><button id="submit" name="submit" value="add" class="btn btn-primary">Add New <?= $thingname ?></button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">Update <?= $thingname ?></button>
              <button id="submit" name="submit" value="display" class="btn btn-primary">CANCEL</button>
              <?php
            }?>
    </div>
</div>
<?php 
}

?>

</fieldset>
</form>



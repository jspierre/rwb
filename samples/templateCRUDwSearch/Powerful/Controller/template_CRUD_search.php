<?php

class hallmarkartsrch extends baseController {


    // ********************************************************************************************                      
    // These private variables are ALL you should have to set any time you instantiate this class.
    // UNLESS you need you need to do additonal types of searches, then see ~line 181
    // ********************************************************************************************

    // This variable is the name of the model that you'll be working with. 
    private $modelname = "mHallmarkArtist";

    // Location and name of your VIEW files (one for search, one for results)
    private $viewloc = "hallmark/body_search_artists.php";
    private $resultloc = "hallmark/body_search_artists_results.php";

    // These variables contain some additional data to pass to your View.
    // $pagetitle - shows up in the browser tab for initial serach; 
    // $resulttitle - shows up in the browser tab for result pages;
    // $thingname - when viewing a record, THING # NNN will
    //    be shown.  Change $thingname to "USER" or "COURSE" or whatever as needed.
    private $pagetitle = "Hallmark Artist Search";  
    private $resulttitle = "Hallmark Artist Search Results";
    private $thingname = "Hallmark Artist";

    // ********************************************************************************************                      
    // ********************************************************************************************
    // ********************************************************************************************


    public function index() {
        /* First, we call in the model we'll need */
        $mname = $this->modelname;
        $this->model = new $mname($this->registry);
        $skipsanitize=false;

        // First, see if this is a special case triggered by GET data
        if (isset($this->registry->passedVars[0])) {
            $_POST['submit'] = "search";
            $skipsanitize=true;
        }

        // Next, sanitize all POSTed data
        if ($_POST && $skipsanitize==false) {
            $this->registry->security->sanitizePost();   
        }

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit']) && $_POST['submit'] == "search") {
            if(isset($_SESSION['searchterms'])) { unset($_SESSION['searchterms']); }
            return $this->doSearchRecords();
        }
        else {
            if (isset($_SESSION['lastsearch']) && $_SESSION['lastsearch'] == true) {
                unset($_SESSION['lastsearch']);
                return $this->doSearchRecords();       
            }
            else {
                return $this->getSearchRecords();
            }
        }
    }

    public function getSearchRecords() {
        $rvalue['type']='html';
        $rvalue['variables']['title']=$this->pagetitle;
        $rvalue['variables']['thingname'] = $this->thingname;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = $this->viewloc;
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }

    public function doSearchRecords() {
        if (!isset($_SESSION['searchterms'])) {
            $this->registry->security->sanitizePost();
            $vars=$_POST;
            extract ($_POST);
            $thedate = date("Y-m-d H:i:s");
            $searchterm = trim($searchterm);
            $searchterm = filter_var($searchterm, FILTER_SANITIZE_STRING);
            $safeterm = $this->registry->db->sanitize($searchterm);
            $_SESSION['searchterms'] = array($safeterm, $active, $searchall);
        }
        else {
            $safeterm = $_SESSION['searchterms'][0];
            $searchterm = $safeterm;
            $active = $_SESSION['searchterms'][1];
            $searchall = $_SESSION['searchterms'][2];
        }

        $result = $this->model->doSearch($safeterm, $active, $searchall);
        if ($result->num_rows < 1) {
            $_SESSION['returncode'] = 5;
            $_SESSION['searchterm'] = $searchterm;
            $_SESSION['searchactive'] = $active;
            $_SESSION['searchall'] = $searchall;
            return $this->getSearchRecords();
        }   
        else {
        $rows=$this->registry->db->getAllRows();
        $rvalue['type']='html';
        $rvalue['variables']['title']=$this->pagetitle;
        $rvalue['variables']['thingname'] = $this->thingname;
        $rvalue['variables']['results']=$rows;
        if (isset($_POST['searchall'])) { $rvalue['variables']['searchall']=$_POST['searchall']; } else { $rvalue['variables']['searchall']=1; }
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = $this->resultloc;
        $rvalue['footerfile']="footer.php";
        return $rvalue;      
    }
    }

}
<?php

   /*
    Payload Return (USE FOR ALL non-"list-only" returns IN THE MODEL!!)
            $returnPayload[0]   BOOL - was the operation successful? (true/false)
            $returnPayload[1]   INT - Level of severity (0=None [default], 1=Success, 2=Info, 3=Warning, 4=Danger, 5=BLACK)
            $returnPayload[2]   STRING - a short description of the success or failure
            $returnPayload[3]   STRING- The message the user will see in the alert box if shown
            $returnPayload[4]   ARRAY - All of the data that came from the VIEW on the FORM, so it can 
                                be redisplayed along with the error at top OR the data from the database
                                of those same fields for display
    */

class mHallmarkMaster extends baseModel {

    // Fill this in to keep logs, etc. up to date.
    public $classname = "mHallmarkMaster";


    // This function gets ONE specific record
    public function getRecordData($record) {
        $error = 0;
        $sql = "SELECT * FROM collections_masters_hallmark WHERE masterno = $record LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars == false) { $error = 1; }
        if($vars['isrepaint']==1) {
            $getrepaint = $this->getRepaintData($vars['repaintof']);
            if ($getrepaint) {
                $vars['repaintname'] = $getrepaint['itemname'];
                $vars['repaintcode'] = $getrepaint['upccode'];
                $vars['repaintdate'] = $getrepaint['releasedate'];
            }
            else { $error = 2; }
        }

        // Get multiple artists if they exist for this item
        $sql = "SELECT * FROM collections_artists_multi_hallmark WHERE masterno = $record";
        $extravars=$this->registry->db->query($sql);
        if ($extravars == false) { $error = 3; }
        if ($extravars->num_rows > 0) { 
            $thelines = $this->registry->db->getAllRows();
            foreach ($thelines as $lines) {
                $vars['multiartist'][] = $lines['artistno'];
            }
        }

        // Now deal with the return payloads
        if ($vars && $error == 0) {
            $returnPayload[0] = true;
            $returnPayload[1] = 1;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record SUCCESS";
            $returnPayload[3] = "";
            $returnPayload[4] = $vars;
        }
        else { 
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record FAILURE - ERROR $error ";
            $returnPayload[3] = "Database retrieval failure. Try again or contact Support.";
            $returnPayload[4] = $vars;
        }
        return $returnPayload;
    }

    // This method simply pulls limited information from the database about one specific
    // item to be used when needed for repaints, etc.
    public function getRepaintData($record) {
        $sql = "SELECT upccode, itemname, releasedate FROM collections_masters_hallmark WHERE masterno = $record LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars) { return $vars; } else { return false; }
    }



    // $posted is every field needed to update the necessary record
    // $action can be "ADD" or "UPDATE".  Anything else results in error
    public function updateRecordData($posted, $action=false) {
        

        // If $action remains false or is not valid, let's error out now.
        if ($action == false || ($action != "ADD" && $action != "UPDATE")) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "updateRecordData for ".$this->classname." called with missing or incorrect ACTION ($action).";
            $returnPayload[3] = "There was a fatal error calling the database.  Please contact support.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }

        // Let's call some variables that may not be called later so we don't get errors
        $priorvalue=0; $seriesno = "NULL";  $valueby="NULL"; $valuedate = "NULL"; 

        // First, extract all the data to individual variables
        // We sanitized it in the controller, so there's no need to do it again.
        extract($posted);

        // If any fields need to be lowercase or uppercase, here is where to add that code
        $itemname = trim(ucwords(strtolower($itemname)));

        // Check for missing fields
        $error = 0;
        $errorreason = "";
        if ($upccode == "") { $errorreason .= "UPC Code is a Required Field. "; $error = 1; }
        if (strlen($upccode) < 12) { $errorreason .= "UPC Code must be 12 digits. "; $error = 1; }
        if ($itemname == "") { $errorreason .= "Item Name is a Required Field. "; $error = 1; }
        if (is_numeric($artistno) && isset($multiartist) && count($multiartist) > 0) { $errorreason .= "Cannot choose both an Primary Artist and Additional Artists! Choose VARIOUS ARTISTS if you wish to select several artists. "; $error = 1; }
        if (($artistno == "NULL" || $artistno=="ZERO") && !isset($multiartist)) { $errorreason .= "At least one artist must be selected. "; $error = 1; }
        if ($origmsrp < 0 ) { $errorreason .= "Original MSRP cannot be a negative number. "; $error = 1; }
        if (!is_numeric($origmsrp) && $currentvalue != "") { $errorreason .= "Original MSRP must be a number. "; $error = 1; }
        if (substr_count($origmsrp,'.') > 1) { $errorreason .= "Original MSRP can only have one decimal point. "; $error = 1; }
        if ($currentvalue < 0 ) { $errorreason .= "Current Value cannot be a negative number. "; $error = 1; }
        if (!is_numeric($currentvalue) && $currentvalue != "") { $errorreason .= "Current Value must be a number. "; $error = 1; }
        if (substr_count($currentvalue,'.') > 1) { $errorreason .= "Current Value can only have ONE decimal. "; $error = 1; }
        if ($inseries > 0 && $seriesno ==0 ) { $errorreason .= "Number In Series cannot have a value when 'No Series' has been selected. "; $error = 1; }
        if ($error == 1) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "Required fields were missing.";
            $returnPayload[3] = $errorreason;
            $returnPayload[4] = $posted;
            return $returnPayload;
        }

       

        // Now do any data validation needed that can be done without a DB call.  For example, cleaning up
        // and verifying email addresses, etc.  e.g.,:
         /* 
            if (isset($emailaddress)) {$emailaddress = strtolower($emailaddress); $data['emailaddress'] = $emailaddress; } 
            */


            // If you set isviewable to 0 then isactive will also become 0
            if ($isviewable == 0) { $isactive = 0; }
            if ($seriesno == "") { $seriesno = "NULL"; }
            if ($valuedate != "NULL") {$valuedate = "'".$valuedate."'";}

        // Now we check for any duplicates that you'd like to check for before adding.  
            if (isset($record) && $record > 0) { $extrasql = "AND (masterno <> $record"; $es = true; } else { $extrasql = ""; $es = false; }
            if (isset($isrepaint) && $isrepaint == 1) { 
                if ($es) { $extrasql = " AND masterno <> $repaintof AND isrepaint = 0)"; } else { $extrasql = " AND (masterno <> $repaintof AND isrepaint = 0)"; }
            }
            else { if ($es) { $extrasql = ")"; } }      
           $sql = "SELECT masterno FROM collections_masters_hallmark WHERE (upccode = '$upccode' OR itemname = '$itemname') $extrasql LIMIT 1";
           //echo $sql;
            $exists = $this->registry->db->oneRowQuery($sql);
            if ($exists) {
                $duplicateurl = BASE_URL."hallmarkmastmgmt/".$exists['masterno'];
                $returnPayload[0] = false;
                $returnPayload[1] = 4;
                $returnPayload[2] = "$action MASTER ITEM FAILED: Duplicate ID found for UPC $upccode as item no. ".$exists['masterno'].".";
                $returnPayload[3] = "ERROR: DUPLICATE ITEM DETECTED. Please see <a href=".'"'.$duplicateurl.'"'."> existing Item # ".$exists['masterno']."</a>.";
                $returnPayload[4] = $posted;
                return $returnPayload;
            }
            // Did the value change?  If so, we need to know so we can mark when and who did it
            // Also, did a pending item change? Let's update that.
            $vc = 0;
            $wasactive = 0;
            $wasentered = 0;
            if (isset($record) && $record > 0) { 
                $sql = "SELECT currentvalue, valuedate, valueby, priorvalue, isactive, enteredby FROM collections_masters_hallmark 
                WHERE masterno = $record LIMIT 1"; }
                $valuechange = $this->registry->db->oneRowQuery($sql);
                if ($valuechange) { 
                    $wasactive = $valuechange['isactive'];
                    $wasentered = $valuechange['enteredby'];
                    if ($valuechange['currentvalue'] != $currentvalue) { 
                        $vc = 1; 
                        $priorvalue = $valuechange['currentvalue'];
                        $valuedate = "'".$this->registry->getDateTime()."'";
                    } 
                    else { 
                        $valuedate = $valuechange['valuedate']; 
                        $valueby = $valuechange['valueby'];
                        $priorvalue = $valuechange['priorvalue'];
                        if ($valuedate == "") { $valuedate = "NULL"; }
                        if ($valueby == "" ) { $valueby = "NULL"; }
                        if ($valuedate != "NULL") {$valuedate = "'".$valuedate."'";}
                    }
            }
        

        // Set any other variables or final touches (default or extra db values, etc) here
        $enteredon = $this->registry->getDateTime();
        $usernum = $_SESSION['usernum'];
        $enteredby = $usernum;
        if ($vc == 1) { $valuedate == $enteredon; $valueby = $usernum; }
        if ($origmsrp == "") { $origmsrp = 0; }
        if ($currentvalue == "") { $currentvalue = 0; }
        if ($artistno == "ZERO" && isset($multiartist) && count($multiartist) > 1) { $artistno = "NULL"; }
        if ($artistno == "ZERO" && isset($multiartist) && count($multiartist) == 1) { $artistno = $multiartist[0]; unset($multiartist); }
        if (is_numeric($artistno) && isset($multiartist) && count($multiartist) == 1 && $artistno == $multiartist[0]) { unset($multiartist); }
        if ($artistno == "NULL" && isset($multiartist) && count($multiartist) == 1) { $artistno = $multiartist[0]; unset($multiartist); }
        if (!isset($isrepaint) && !isset($repaintof) && $action == "ADD") { $isrepaint = 0; $repaintof = "NULL"; }
        if (!isset($itemnotes)) { $itemnotes = ""; }

        // If we rejected a pending item, we just delete
        if ($isactive == 3 && $wasactive == 2) {
            $action = "DELETE";
        }

        // Now, we set the SQL code to either add a new record or update the existing record depending upon
        // what was sent
        $passfinal = false;
        if ($action == "ADD") {
            $sql = "INSERT INTO collections_masters_hallmark (upccode, itemname, artistno, seriesno, inseries, isrepaint, repaintof, 
                releasedate, description, origmsrp, currentvalue, valuedate, valueby, isactive, isviewable, itemnotes, enteredby, 
                enteredon, lastchangedby) VALUES ('$upccode', '$itemname', $artistno, $seriesno, $inseries, $isrepaint, $repaintof, 
                $releasedate, '$description', $origmsrp, $currentvalue, $valuedate, $valueby, $isactive, $isviewable, '$itemnotes', 
                $enteredby, '$enteredon', $usernum)";  
        }
        elseif ($action == "UPDATE") {
            $sql = "UPDATE collections_masters_hallmark SET upccode = '$upccode', itemname = '$itemname', artistno = $artistno,
                seriesno = $seriesno, inseries = $inseries, releasedate = $releasedate, description = '$description', origmsrp = $origmsrp,
                currentvalue = $currentvalue, priorvalue = $priorvalue, valuedate = $valuedate, valueby = $valueby, isactive = $isactive, 
                isviewable = $isviewable, itemnotes = '$itemnotes', lastchangedby = $usernum WHERE masterno = $record LIMIT 1";
        }
        elseif ($action == "DELETE") {
            $passfinal = true;
            $sql = "DELETE FROM collections_items_hallmark WHERE masterno = $record";
            $result = $this->registry->db->query($sql);
            if ($result) {
                $sql = "DELETE FROM collections_artists_multi_hallmark WHERE masterno = $masterno";
                $result2 = $this->registry->db->query($sql);
            }
            else {
                $dbok = false; 
            }
            if ($result2) {
                $sql = "DELETE FROM collections_masters_hallmark WHERE masterno = $record LIMIT 1";        
                $result3 = $this->registry->db->query($sql);   
            }
            else {
                $dbok = false;
            }
            if ($result3) { $dbok = true; } else { $dbok = false; }
        }
                
                
        
        //echo $sql;
        //exit();

        // We run the transaction - if the insert/update fails, we send back an error
        if ($passfinal) {
            if (!$dbok) {
                $returnPayload[0] = false;
                $returnPayload[1] = 4;
                $returnPayload[2] = "DELETE FAILED: ".$this->classname." - DATABASE ERROR when SQL ran";
                $returnPayload[3] = "FAILED: An error has occurred.  Please contact Support.";
                $returnPayload[4] = $posted;
                return $returnPayload;    
            }
        }
        elseif ($this->registry->db->query($sql) == false) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "INSERT/UPDATE FAILED: ".$this->classname." - DATABASE ERROR when SQL ran";
            $returnPayload[3] = "FAILED: An error has occurred.  Please contact your system administrator.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }
        
        // If this was an ADD we pull the new record number to give back to the system
        if ($action == "ADD") { 
            $record = $this->registry->db->getInsertId(); 
            $posted['record'] = $record;
        }

        // If multiple artists were NOT selected, let's delete that from the multi table
        if (!isset($multiartist) && $passfinal==false) {
            if (isset($masterno)) {
                $sql = "DELETE FROM collections_artists_multi_hallmark WHERE masterno = $masterno";
                $this->registry->db->query($sql);
            }
        }

        // Okay, if that was successful, and if multiple artists were selected, we need to clean up the database with this info.
        if (isset($multiartist) && $multiartist != "" && $passfinal == false) {
            if (count($multiartist) == 1 && $multiartist[0]="") { $skip = 1; }
            else {
                // First, let's remove all prior entries from the table if there are any
                if (isset($masterno)) {
                    $sql = "DELETE FROM collections_artists_multi_hallmark WHERE masterno = $masterno";
                    $this->registry->db->query($sql);
                }
                else { $masterno = $record; }
                // Now, let's insert the new ones
                foreach ($multiartist as $artist) {
                    if ($artist != "") {
                        $sql = "INSERT INTO collections_artists_multi_hallmark (masterno, artistno) VALUES ($record, $artist)";
                        $this->registry->db->query($sql);       
                    }
                }
            }   
        }

        // Do we need to send any emails?  Let's do that.
        if ($wasactive == 2 && ($isactive == 1 || $isactive == 3)) {
            $email = new mEmails($this->registry);
            if ($isactive == 1) {
                if ($pendreason == "") { $pendreason = "Thank you!";}
                $body = "Hi from MyPowerCollector!\n\nThanks for suggesting the Hallmark Collection item '$itemname'.\n\nAfter review, we are pleased to announce that we have ACCEPTED your suggestion and it\nis now live on our sytem. Additionally, our moderator had these remarks:\n$pendreason\n\nThank you for helping to make MyPowerCollector even better!\n\n\nSincerely,\n\nMyPowerCollector\n\n\n\nNote: This email cannot be replied to. If you need help, please contact MyPowerCollector\nSupport at support@mypowercollector.com.";
                $subject = "MyPowerCollector: Suggestion Approved!";
                $result = $email->sendSingleEmail($wasentered, 0, $subject, $body);
            }
            if ($isactive == 3) {
                $body = "Hi from MyPowerCollector!\n\nThanks for suggesting the Hallmark Collection item '$itemname'.\n\nAfter review, we unfortunately had to reject your suggestion for the following reason(s):\n$pendreason\n\nWe appreciate your effort, though, and we sincerely thank you for trying to help to\nmake MyPowerCollector even better!\n\n\nSincerely,\n\nMyPowerCollector\n\n\n\nNote: This email cannot be replied to. If you need help, please contact MyPowerCollector\nSupport at support@mypowercollector.com.";
                $subject = "MyPowerCollector: Suggestion Rejected";
                $result = $email->sendSingleEmail($wasentered, 0, $subject, $body);
            }
        }
        // Do we need to notify anyone of pending items?
        if ($action == "ADD" && $isactive == 2) {
            // Get all users who can approve pending items
            $userinf = new mUser($this->registry);
            $pendusers = $userinf->getUsersByFunction("revpending");
            if ($pendusers && (count($pendusers) > 0)) {
                $email = new mEmails($this->registry);
                foreach ($pendusers as $penduser) {
                    $body = "Hi from MyPowerCollector!\n\nA new suggestion has been made for the Hallmark Collection by a user. The item is:\n'$itemname' - Master Item # $record\n\nPlease log in at your earliest convenience to review and either approve or reject this suggestion.\n\n\nSincerely,\n\nMyPowerCollector.";
                    $subject = "MyPowerCollector: New User Suggestion Received";
                    $result = $email->sendSingleEmail($penduser['usernum'], 0, $subject, $body);        
                }
            }
        }

        // That's all there is to it! Let's send back the data and be done!
        $returnPayload[0] = true;
        $returnPayload[1] = 1;
        $returnPayload[2] = "$action RECORD completed successfully ".$this->classname." - RECORD # $record";
        if ($isactive == 2) {
            $returnPayload[3] = "SUGGESTION SUBMITTED successfully - a moderator will review your suggestion and be in touch.";
        }
        else {
            $returnPayload[3] = "$action completed successfully (Record# $record) - please check the data below.";    
        }
        $returnPayload[4] = $posted;
        if ($action == "DELETE") { $returnPayload[4]['submit'] = "delete"; }
        return $returnPayload;
    }

    // Pulls all available photo information and sends it back to the calling program,
    // or sends error information otherwise.
    public function getMasterPhotos($masterno=false) {
        if ($masterno == false) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "GETMASTERPHOTOS FAILED: ".$this->classname." - NO PARAMETERS SENT TO METHOD";
            $returnPayload[3] = "FAILED: An error has occurred.  Please contact your system administrator.";
            $returnPayload[4] = false;
            return $returnPayload; 
        }
        $sql = "SELECT * FROM collections_images_hallmark WHERE masterno = $masterno AND (isdeleted=0 AND isflagged=0 AND isstricken=0)";
        $result = $this->registry->db->query($sql);
        if ($result) {
            if ($result->num_rows > 0) {
                $photos = $this->registry->db->getAllRows();
            }
            else {
                $photos=false;
            }
        }
        else {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "GETMASTERPHOTOS FAILED: ".$this->classname." - ERROR ON DB QUERY";
            $returnPayload[3] = "FAILED: An error has occurred.  Please contact your system administrator.";
            $returnPayload[4] = false;
            return $returnPayload; 
        }
        $returnPayload[0] = true;
        $returnPayload[1] = 1;
        $returnPayload[2] = "GET PHOTOS completed successfully ".$this->classname;
        $returnPayload[3] = "Photos Retrieved Successfully.";
        $returnPayload[4] = $photos;
        return $returnPayload;
    }

    // Stores data about a file uploaded for a master photo
    public function putMasterPhotos($data) {
        extract($data);
        $masterno = $record;
        $usernum = $_SESSION['usernum'];
        $userip = $_SESSION['userip'];
        $thisdate = $this->registry->getDateTime();
        $primarycheck = $this->getMasterPhotos($masterno);
        if ($primarycheck[0] == true && $primarycheck['4'] == false) { $isprimary = 1; } else { $isprimary = 0; }
        $sql = "INSERT INTO collections_images_hallmark (masterno, storetype, storeloc, imagename, originalname,  
            imagenote, uploadby, uploaddate, uploadip, isprimary, lastchangedby) VALUES ($masterno, $storetype, '$storeloc',
            '$imagename', '$origname', '$imagenotes', $usernum, '$thisdate', '$userip', $isprimary, $usernum)";
        $result = $this->registry->db->query($sql);
        if ($result) { return true; } else { return false; }
    }

    // This method updates the images table with new info as desired
    // It DOES NOT actually touch the image files themselves, only the db
    public function updatePhotos ($masterno, $imageno, $action, $reason="") {
        if ($action=="DELETE") {
            $sql = "UPDATE collections_images_hallmark SET isdeleted=1 WHERE imageno = $imageno LIMIT 1";
            if($this->registry->db->query($sql)) { return true; } else { return false; }
        }
        elseif ($action=="PRIMARY") {
            $sql =  "UPDATE collections_images_hallmark SET isprimary=0 WHERE masterno = $masterno";
            if($this->registry->db->query($sql)) { 
                $sql ="UPDATE collections_images_hallmark SET isprimary=1 WHERE imageno = $imageno LIMIT 1";
                if($this->registry->db->query($sql)) { return true; } else { return false; }
            } 
            else { return false; }
        }
        elseif ($action=="FLAG") {
            $sql = "UPDATE collections_images_hallmark SET isflagged=1, reason='$reason' WHERE imageno = $imageno LIMIT 1";
            if($this->registry->db->query($sql)) { return true; } else { return false; }
        }
        elseif ($action=="STRIKE") {
            $sql = "UPDATE collections_images_hallmark SET isstricken=1, reason='$reason' WHERE imageno = $imageno LIMIT 1";
            if($this->registry->db->query($sql)) { return true; } else { return false; }
        }
        else {
            return false;
        }
    }

    // This method pulls the primary photo for the given master record and returns the 
    // completely formatted URL necessary for display and its caption or returns false otherwise
    public function getPrimaryPhoto($masterno) {
        // Get the primary photo if one exists
        $sql = "SELECT * FROM collections_images_hallmark WHERE masterno = $masterno AND (isprimary = 1 AND isdeleted = 0 AND isflagged = 0 AND isstricken = 0) LIMIT 1";
        $primaryphoto = $this->registry->db->oneRowQuery($sql);
        if ($primaryphoto) {
            // Get the URL etc of the primary photo if it exists
            $storetype = $primaryphoto['storetype'];
            $storeloc = $primaryphoto['storeloc'];
            $imagename = $primaryphoto['imagename'];
            $imagenote = $primaryphoto['imagenote'];
            $locdata = $this->registry->storage->getStoragePath($storetype, $storeloc);
            if ($locdata) {
                $url = $locdata['url'].$imagename;
                $response['url'] = $url;
                $response['imagenote'] = $imagenote;
                return $response;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    // This function simply looks for repaints of the selected item and returns the number found
    // or 0 (zero) if none
    public function getNumRepaints($masterno) {
        $sql = "SELECT masterno FROM collections_masters_hallmark WHERE repaintof=$masterno AND isrepaint=1";
        $repaints = $this->registry->db->query($sql);
        if ($repaints) {
            $numrepaints = $repaints->num_rows;
        }
        else {
            $numrepaints = 0;
        }
        return $numrepaints;
    }

    /*  ********************************************************************** */
    /*  ***************** SEARCH METHOD FOR SEARCH CONTROLLER **************** */
    /*  ********************************************************************** */
    // You will need to adjust your SQL here to fit your needs for the search
    // You would also add other wiring to do different limiters or types of search
    public function doSearch($searchterms, $active=0, $searchall=0) {
        $terms = explode(" ", $searchterms);
        $numterms = count($terms);
        $x=0;

        // First, search the multiartists - we'll just add these to the main search as positive hits
        if ($searchall == 1) {
            $y=0;
            $sql = "SELECT collections_artists_multi_hallmark.*, collections_artists_hallmark.* FROM collections_artists_multi_hallmark LEFT JOIN 
                        collections_artists_hallmark ON collections_artists_multi_hallmark.artistno = collections_artists_hallmark.artistno WHERE 
                        collections_artists_hallmark.isactive = 1 AND ";
            foreach ($terms as $term) {
                $y++;
                $sql .= "(firstname LIKE '%$term%' OR lastname LIKE '%$term%' OR displayname LIKE '%$term%')";
                 if ($y!==$numterms) { $sql .= " AND "; }
            }
            $subresult=$this->registry->db->query($sql);
            $orstmt = "";
            $z=0;
            if($subresult && $subresult->num_rows > 0) {
                $itemrows = $this->registry->db->getAllRows();
                $numits = count($itemrows);
                $orstmt .= "(";
                foreach ($itemrows as $itemrow) {
                    $z++;
                    $orstmt .= "collections_masters_hallmark.masterno = ".$itemrow['masterno'];
                    if ($z !== $numits) { $orstmt .= " OR "; } 
                }
                $orstmt .=")";
            }
        }

        $sql = "SELECT collections_masters_hallmark.*, collections_masters_hallmark.isactive AS masactive, collections_artists_hallmark.firstname, 
                    collections_artists_hallmark.lastname,     collections_series_hallmark.seriesname, collections_series_hallmark.akaname, 
                    fp.storetype, fp.storeloc, fp.imagename, fp.imagenote FROM collections_masters_hallmark LEFT JOIN collections_artists_hallmark 
                    ON collections_masters_hallmark.artistno = collections_artists_hallmark.artistno LEFT JOIN collections_series_hallmark ON 
                    collections_masters_hallmark.seriesno = collections_series_hallmark.seriesno LEFT JOIN (SELECT fp.masterno, fp.storetype, 
                    fp.storeloc, fp.imagename, fp.imagenote FROM collections_images_hallmark fp WHERE (fp.isprimary=1 AND fp.isdeleted=0 AND 
                    fp.isflagged=0 AND fp.isstricken=0)) fp ON collections_masters_hallmark.masterno = fp.masterno WHERE ";
        if ($active==1) {
            $sql .= "collections_masters_hallmark.isactive = 1 AND ";
        }
        if ($active == 3) {
            $sql .= "collections_masters_hallmark.isactive = 2 AND ";
        }
        elseif ($active != 2) {
            $sql .= "collections_masters_hallmark.isviewable = 1 AND ";
        }
        if ($orstmt == "") { $orstmt = false; } else { $sql .= "(".$orstmt." OR "; }
        foreach ($terms as $term) {
            $x++;
            if (strtoupper($term) == "VARIOUS" && $searchall==1) { $varsql = "collections_masters_hallmark.artistno IS NULL OR"; } else { $varsql = ""; }
            if ($searchall==1) {
                $sql .= "($varsql collections_masters_hallmark.masterno LIKE '%$term%' OR upccode LIKE '%$term%' OR itemname LIKE '%$term%' OR releasedate LIKE '%$term%' OR 
                    firstname LIKE '%$term%' OR lastname LIKE '%$term%' OR displayname LIKE '%$term%' OR CONCAT (firstname, ' ' , lastname) LIKE '%$term%' OR 
                    seriesname LIKE '%$term%' OR akaname LIKE '%$term%')";
            }
            else {
                $sql .= "(itemname LIKE '%$term%' OR upccode LIKE '%$term%')";                
            }
            if ($x!==$numterms) {
                $sql .= " AND ";
            }
        }
        if ($orstmt !== "" && $orstmt) { $sql .= ")"; }
        $sql.=" ORDER BY itemname ASC";
        //echo $sql;
        //die();
        $result=$this->registry->db->query($sql);
        if ($result) {
            return $result;
        }
        return false;
    }



}


?>
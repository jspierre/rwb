<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
  $(function() {
    if($('#artistno').val() !== "NULL" ) {
      $('.hideme').hide();
    }
     else {
        $('.hideme').show(); 
    }
});
</script>

<script>
   $(function() {
        $('#artistno').change(function(){
          if($(this).val() !== "NULL" ) {
              $('.hideme').hide();
          }
          else {
             $('.hideme').show(); 
          }
        });
    });
</script>

 <script>
  $(document).ready(function(){
    // Make sure mobile number is only numbers
    $(".upccode").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
           // Allow: Ctrl+A, Command+A
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
           // Allow: home, end, left, right, down, up
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
    });
    });
</script>

<script>
  $(document).ready(function(){
    // Make sure mobile number is only numbers
    $(".moneys").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl+A, Command+A
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
           // Allow: home, end, left, right, down, up
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
    });
    });
</script>



<form class="form-horizontal" action="<?= BASE_URL ?>hallmarkmastmgmt" method="post" name="hallmarkmastmgmt" id="master_form">
<fieldset>
<div class="panel panel-primary">
<div class="panel-heading">
<?php

// Let's get all of our variables extracted so we can work with them
extract($values);
if (isset($returnPayload[4]) && !empty($returnPayload[4])) {
  extract ($returnPayload[4]);
}
if (isset($returnPayload[5]) && !empty($returnPayload[5])) {
  $artists = $returnPayload[5];
}
if (isset($returnPayload[6]) && !empty($returnPayload[6])) {
  $series = $returnPayload[6];
}

// Now, let's set some inputs so we'll have them again later
// Replace $coursenum with the index variable in your table here
// We'll also 
if (isset($record) && empty($masterno)) { 
  $masterno = $record; 
}
if (isset($record)) {
  echo '<input type="hidden" name="record" value="'.$record.'">';
}

// Is this an error situation? If so, edit needs to be re-opened
if (isset($returnPayload[0]) && $returnPayload[0] == false) {
  if (empty($masterno)) { $action = "ADD"; } else if (isset($submit) && $submit=="update") { $action = "EDIT"; }
}

// This is the Headline on the page 
if ($action=="ADD") {
  $header = "Add New ".$thingname." Form";
}
elseif ($action=="EDIT") {
  $header = "Editing ".$thingname." #".$record;
}
else {
  $header = "Displaying ".$thingname." #".$record;
}
echo '<h3 class="panel-title">'.$header.'</h3>';
 if (isset($_SESSION['collection']) && isset($_SESSION['collection_details'])) {
      $header2 = "[ COLLECTION: ".$_SESSION['collection_details']['collname']." ACTIVE ]";
      $collactive = true;
      $collurl = BASE_URL."hallmarkitemsearch";
      echo '<a href="'.$collurl.'""><h6 class="btn btn-sm btn-outline-info">'.$header2.'</h6></a>'; 
      $collno = $_SESSION['collection'];
      if ($action != "ADD") {
        $numincollection = $this->registry->db->runOtherMethod("mHallmarkItems", "getNumInCollection", array($collno, $masterno));
        if ($numincollection < 1 || $numincollection == false) { $numincollection = "None"; }
        $header3 = "[ $numincollection Already in Collection ]";
        echo '<h6 class="btn btn-sm btn-outline-primary spacer-left">'.$header3.'</h6>'; 
      }
      if (isset($suggesting) && $suggesting ==true && $action == "ADD" ) {
       echo '<h6 class="btn btn-sm btn-outline-primary spacer-left">SUGGESTING NEW ITEM</h6>';  
      }
    }
    else {
      $collactive = false;
    }  
echo '<p><small>NOTE: Items in <strong>bold</strong> are Required.</small></p>';
?>

</div>
<div class="panel-body">
             
<?php
// Now, let's deal with any return messages we need to display.  These are always
// located in the $returnPayload array
if (isset($returnPayload)) {
    if (empty($returnPayload[3])) {$message = false; } else { $message = $returnPayload[3];}
  switch ($returnPayload[1]) {
    case 1:
      $color="alert-success";
      break;
    case 2:
      $color="alert-info";
      break;
    case 3:
      $color="alert-warning";
      break;
    case 4:
      $color="alert-danger";
      break;
    case 5:
      $color="alert-dark";
      break;
    default:
      $color="alert-primary";
      break;
  }
}
if ($message) {
  echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
  echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
  echo $message;
  echo '</div>';
}
?>        



<?php 
// This is for testing only, comment out for production
error_reporting(E_ALL & ~E_NOTICE);
?>

<?php
// This is the button section.  It will display certain additional buttons
// based upon access level and current task.  
if ($action=="DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("hallmdmgmte") && $isactive <> 3) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning btn-space">Edit <?= $thingname ?></button>
        <?php 
        }
      ?>
      <?php
        // NOTE: THIS ALLOWS ONLY ONE REPAINT PER ITEM - TO ALLOW MULTIPLE PER ITEM, JUST CHANGE $allow_multiple_repaints
        // DIRECTLY BELOW FROM false TO true
        $allow_multiple_repaints = false;
        if ($this->registry->security->checkFunction("hallmdmgmtar") && $isrepaint==0 && $isactive < 2 && ($allow_multiple_repaints == true || (isset($numrepaints) && $numrepaints == 0))) { 
          if (!isset($numrepaints) || $numrepaints == false ) { 
            $warnrepaint = "Are you sure? This will add a new REPAINTED version of this item to the database (none exist yet)!"; 
          }
          else {
            if ($numrepaints == 1) {
              $warnrepaint = "THERE IS ALREADY A REPAINT OF THIS ITEM EXISTING! Are you sure you want to create another one?";  
            }
            else {
             $warnrepaint = "THERE ARE ALREADY $numrepaints REPAINTS OF THIS ITEM! Are you sure you want to create another one?";   
            }
            
          }
          ?>
          <button id="submit" name="submit" value="repaint" title="Add a New Repaint Item based on this item" class="btn btn-sm btn-warning btn-space" onClick="return confirm('<?= $warnrepaint ?>')">Create Repaint</button>
        <?php 
        }
      ?>
       <?php
        if ($this->registry->security->checkFunction("hallmdmgmtp") && $isactive < 2) { ?>
          <button id="submit" name="submit" value="photos" title="Upload and Edit Photos and Mark a Photo as Primary for this item" class="btn btn-sm btn-warning btn-space">Update Master Photos</button>
        <?php 
        }
        elseif ($isactive < 2) { ?>
          <button id="submit" name="submit" value="photos" title="View the Master Photographs for this Item" class="btn btn-sm btn-primary btn-space">View Master Photos</button>
        <?php
        }
      ?>
      <?php
        if (isset($_SESSION['collection']) && isset($_SESSION['collection_details'])) {
          if ($_SESSION['collection_details']['usernum'] == $_SESSION['usernum']) { $ownacct = true; } else { $ownacct = false; }
          if ($numincollection > 0) {$extra="NOTE: You already have ".$numincollection." in your collection."; }
          if (($this->registry->security->checkFunction("halladdcoll") && $ownacct) || $this->registry->security->checkFunction("halladdcolla")) { ?>
          <button id="submit" name="submit" value="addtocollection" title="Add this item to your collection" class="btn btn-sm btn-warning btn-space" onClick="return confirm('Are you sure you want to add this item to your collection? It will be added immediately. <?= $extra ?>')">ADD TO MY COLLECTION</button>
        <?php 
          }
        }
      ?>
</div>
<?php
}
?>

<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="masterno"><abbr title="This is the assigned internal Master Item Number - it cannot be changed.">Master Item No.</abbr></label>  
    <div class="col-md-2">
      <input id="masterno" name="masterno" type="text" placeholder="" class="form-control input-md" value="<?= $masterno ?>" disabled>
      <input id="masterno" name="masterno" type="hidden" value="<?= $masterno ?>">
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="upccode"><abbr title="The twelve digit UPC code associated with this item"><strong>Item UPC Code</strong></abbr></label>  
    <div class="col-md-2">
      <input id="upccode" name="upccode" type="text" maxlength="12" placeholder="" class="form-control input-md upccode" value="<?= $upccode ?>" required <?php if ($action == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>

<?php 
  if ($primaryphoto && $action=="DISPLAY") { ?>
  <div class="row">
    <label class="col-md-2 control-label" for="primaryphoto"><abbr title="The Primary Photo selected for this item. Additional photos may be available.">Primary Photo</abbr></label>
    <div class = "col-md-4">
     <button id="submit" name="submit" value="photos" class="btn btn-sm btn-info"><img class="imgprimary img-thumbnail" title="<?= $primaryphoto['imagenote'] ?>" src="<?= $primaryphoto['url'] ?>" ></button>
    </div>
  </div>
  <?php } ?>

  <div class="row">
    <label class="col-md-2 control-label" for="itemname"><abbr title="The properly spelled and capitalized OFFICIAL NAME of the item"><strong>Official Item Name</strong></abbr></label>  
    <div class="col-md-6">
      <input id="itemname" name="itemname" type="text" maxlength="255" placeholder="" class="form-control input-md" value="<?= $itemname ?>" required <?php if ($action == "DISPLAY") { echo "disabled"; } elseif ($action == "ADD" && $isrepaint==1) { echo "readonly"; } ?> >
    </div>
  </div>


  <?php 
    if($isrepaint==1) { 
      if ($repaintdate == 0) { $repaintphrase = "$repaintname"; } else { $repaintphrase = "$repaintname   ($repaintdate)"; }
      if ($action == "ADD") { ?> 
        <input type="hidden" name="isrepaint" value="1">
        <input type="hidden" name="repaintof" value="<?= $repaintof ?>"> 
        <input type="hidden" name="repaintname" value="<?= $repaintname ?>"> 
        <input type="hidden" name="repaintdate" value="<?= $repaintdate ?>"> 
        <?php if (isset($suggesting) && $suggesting ==true) { ?> <input type="hidden" name="suggesting" value="1"> <?php } else { ?> <input type="hidden" name="suggesting" value="0"> <?php } ?>
        <?php } 
      $repainturl = BASE_URL."hallmarkmastmgmt/".$repaintof;
    ?>
  <div class="row">
    <label class="col-md-2 control-label" for="repaintphrase"><abbr title="This item is a repainted version of the original item, which is listed here. Click the Name to see the item.">Repaint Of</abbr></label>  
    <div class="col-md-6">
      <a href="<?= $repainturl ?>"><input id="repaintphrase" name="repaintphrase" type="text" placeholder="" class="form-control input-md" value="<?= $repaintphrase ?>"  disabled ></a>
    </div>
  </div>
  <?php  }  ?>

<?php
   if ($this->registry->security->checkFunction("hallmdmgmts") && (!isset($suggesting) || $suggesting == false)) { ?>
    <div class="row">
      <label class="col-md-2 control-label" for="isactive"><abbr title="Is this item active for use in collections? Setting to INACTIVE will make this series unavailable for NEW items added to the master database, but will not affect existing items.">System Status</abbr></label>
      <div class="col-md-2">
       <select id="isactive" name="isactive" class="form-control" <?php if ($action == "DISPLAY" || ($isactive==2 && $this->registry->security->checkFunction("revpending")==false)) { echo "disabled"; } ?>>
         <?php if ($isactive ==2) { $aphrase = "ACCEPTED"; } else { $aphrase = "Active"; } ?>
         <option value="1" <?php if ($isactive==1) { echo "selected"; }?>><?= $aphrase ?></option>
         <?php if ($isactive <> 2) { ?> <option value="0" <?php if ($isactive==0 && ($action !== "ADD")) { echo "selected"; }?>>INACTIVE</option> <?php  } ?>
         <?php if ($isactive == 2) { ?>
          <option value="2" <?php if ($isactive==2 && ($action !== "ADD")) { echo "selected"; }?>>PENDING</option>
          <option value="3" <?php if ($isactive==3 && ($action !== "ADD")) { echo "selected"; }?>>REJECTED</option>
         <?php } ?>
        </select>
       </div>
      <label class="col-md-2 control-label" for="isviewable"><abbr title="Is this item viewable? Can it be seen at all? If set to UNVIEWABLE it will not be usable in collections and will DISAPPEAR from personal collections and everywhere else.">Viewable Status</abbr></label>
      <div class="col-md-2">
       <select id="isviewable" name="isviewable" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
         <option value="1" <?php if ($isviewable==1) { echo "selected"; }?>>Viewable</option>
         <option value="0" <?php if ($isviewable==0 && ($action !== "ADD")) { echo "selected"; }?>>UNVIEWABLE</option>
        </select>
       </div>
 </div>
 <?php } 
  elseif (isset($suggesting) && $suggesting == true) {
    ?> <input type="hidden" name="isactive" value="2"> <input type="hidden" name="isviewable" value="1"> <?php 
  }  
  else {
    ?> <input type="hidden" name="isactive" value="2"> <input type="hidden" name="isviewable" value="1"> <?php 
  } ?>

<?php 
if ($isactive == 2 && $this->registry->security->checkFunction("revpending")) { ?>
  <div class="row">
    <label class="col-md-2 control-label" for="pendreason"><abbr title="The reason why you are rejecting or accepting this item - this will be sent to the User who entered this. It will not be stored."><strong>Change Reason</strong></abbr></label>  
    <div class="col-md-6">
      <input id="pendreason" name="pendreason" type="text" maxlength="255" placeholder="" class="form-control input-md" value="<?= $pendreason ?>" required <?php if ($action == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>
<?php } ?>


    <div class="row">
    <label class="col-md-2 control-label" for="artistno"><abbr title="The primary artist for this series">Primary Item Artist</abbr></label>  
    <div class="col-md-6">
       <?php if ($artistno > 0 && $action == "DISPLAY") { ?> <a href = "#artistModal" data-toggle="modal" data-target="#artistModal"> <?php } ?>
       <select id="artistno" name="artistno" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="ZERO">-Select-</option>
        <option value="NULL" <?php if ($artistno=="" && ($action !== "ADD" || ($action=="ADD" && $isrepaint==1))) { echo "selected"; }?>>VARIOUS ARTISTS</option>
        <?php
        foreach ($artists as $artist) {
          if ($action == "DISPLAY") { ?>
           <option value = "<?= $artist['artistno'] ?>" <?php if ($artistno==$artist['artistno']) { echo "selected"; }?>><?= $artist['displayname'] ?> </option>
          <?php 
          }
          else { ?>
          <option value = "<?= $artist['artistno'] ?>" <?php if ($artistno==$artist['artistno']) { echo "selected"; }?>><?= $artist['firstname'] ?> <?= $artist['lastname'] ?> </option>
          <?php
        } 
      } ?>
      </select><?php if ($artistno > 0) { ?> </a> <?php } ?>
    </div>
  </div> 

<div class="hideme">
<?php if ((isset($multiartist) && count($multiartist) > 0) || $action != "DISPLAY") { ?>
  <div class="row" >
    <label class="col-md-2 control-label" for="multiartist"><abbr title="Any additional artists associated with the item (CTRL+CLICK to select multiple)">Additional Artists</abbr></label>  
    <div class="col-md-6">
      <?php 
      if ($action == "DISPLAY" && count($multiartist) > 0) {
        $noartists = count($multiartist);
        $countartists = 0;
        foreach ($multiartist as $singleartist) {
            foreach ($artists as $artist) {
              if ($artist['artistno'] == $singleartist) { $multinames .= $artist['displayname']; }
            }
            $countartists ++;
            if ($countartists < $noartists) { 
              if ($countartists == $noartists -1 && $noartists > 2) { $multinames .= ", and "; } else { $multinames .= ", "; }
            }
        }
        ?>
        <textarea id="description" name="description" rows="5" placeholder="" maxlength="65500" class="form-control input-md" disabled><?= $multinames ?></textarea>
        <?php
      }
      else {
        ?>
         <select id="multiartist" name="multiartist[]" class="form-control"  multiple <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <!-- <option value="" <?php if ($multiartist=="" && ($action !== "ADD")) { echo "selected"; }?>>NO ADDITIONAL ARTISTS</option> -->
        <?php
        foreach ($artists as $artist) {
          $s = "";
          foreach($multiartist as $singleartist) {
            if ($artist['artistno'] == $singleartist) { $s = "selected"; }
          }
          ?>
          <option value = "<?= $artist['artistno'] ?>" <?= $s ?>><?= $artist['firstname'] ?> <?= $artist['lastname'] ?></option>
          <?php
        } ?>
      </select>  
      <?php
      } ?>
    </div>
  </div> <?php
  } ?>
</div>
  <div class="row">
    <label class="col-md-2 control-label" for="seriesno"><abbr title="The series, if any, that this item is associated with. If none, do not select anything.">Item Series (if any)</abbr></label>  
    <div class="col-md-6">
      <?php if ($seriesno > 0 && $action == "DISPLAY") { ?> <a href = "#seriesModal" data-toggle="modal" data-target="#seriesModal"> <?php } ?>
       <select id="seriesno" name="seriesno" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="" <?php if ($seriesno=="" && ($action !== "ADD")) { echo "selected"; }?>>-Select-</option>
        <?php
        foreach ($series as $serie) {
          ?>
          <option value = "<?= $serie['seriesno'] ?>" <?php if ($seriesno==$serie['seriesno']) { echo "selected"; }?>><?= $serie['seriesname'] ?></option>
          <?php
        } ?>
      </select><?php if ($seriesno > 0) { ?> </a> <?php } ?>
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="inseries"><abbr title="The number of the item within the series (1 in series, 2 in series, etc.).  If none or unknown, do not select anything.">Number in Series</abbr></label>  
    <div class="col-md-2">
      <select id="inseries" name="inseries" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($inseries==0 && ($action !== "ADD")) { echo "selected"; }?>>-Select-</option>
        <?php
        $thisyear = date("Y") + 1;
        for ($i=1; $i < 51; $i++) { 
          ?> 
          <option value="<?= $i ?>" <?php if ($inseries==$i) { echo "selected"; }?>><?= $i ?></option>  
          <?php
        }
        ?>
      </select>
    </div>
    <label class="col-md-2 control-label" for="releasedate"><abbr title="The year that the item was originally released. If unknown, do not select anything.">Item Release Year</abbr></label>  
    <div class="col-md-2">
      <select id="releasedate" name="releasedate" class="form-control" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($releasedate==0 && ($action !== "ADD")) { echo "selected"; }?>>-Year-</option>
        <?php
        $thisyear = date("Y") + 1;
        for ($i=1973; $i < $thisyear; $i++) { 
          if ($i < 10) { $addzero = "0"; } else { $addzero = ""; }
          ?> 
          <option value="<?= $i ?>" <?php if ($releasedate==$i) { echo "selected"; }?>><?= $addzero ?><?= $i ?></option>  
          <?php
        }
        ?>
      </select>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="description"><abbr title="A thorough description of the item including subject, colors, materials used, and any other pertinent details collectors may need or want to know.">Item Description</abbr></label>  
    <div class="col-md-6">
      <?php if ($action=="ADD") { if(isset($description)) {$description = stripslashes($description);} } ?>
      <textarea id="description" name="description" rows="5" placeholder="" maxlength="65500" class="form-control input-md" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  ><?= $description ?></textarea>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="origmsrp"><abbr title="The original retail price of the item - if unknown, leave blank or enter 0.">Original Retail Price</abbr></label>  
    <div class="input-group col-md-2">
      <div class="input-group-prepend">
        <span class="input-group-text">$</span>
      </div>
      <input id="origmsrp" name="origmsrp" type="text" maxlength="10" placeholder="" class="form-control input-md moneys" value="<?= $origmsrp ?>" <?php if ($action == "DISPLAY") { echo "disabled"; } ?> >
    </div>
    <?php if (isset($suggesting) && $suggesting == true) { ?>
      <input type = "hidden" name = "currentvalue" value = "">
    <?php } 
    else { ?>
    <label class="col-md-2 control-label" for="currentvalue"><abbr title="The estimated dollar value of the item currently based on market value. If you don't know or unsure, leave blank or enter 0.">Est. Current Value</abbr></label>  
    <div class="input-group col-md-2">
      <div class="input-group-prepend">
        <span class="input-group-text">$</span>
      </div>
      <input id="currentvalue" name="currentvalue" type="text" maxlength="10" placeholder="" class="form-control input-md moneys" value="<?= $currentvalue ?>" <?php if ($action == "DISPLAY") { echo "disabled"; } ?> >
    </div>
    <?php } ?>
  </div>


<?php 
  $lastchange = "";
  if ($this->registry->security->checkFunction("hallmastmgmtl")) {
    $getuser = $this->registry->security->getUserName($valueby);
    $lastchange = "By User # ".$valueby." (".$getuser['firstname']." ".$getuser['lastname'].") From $".$priorvalue." ";
  }
  $betterdate = explode(" ", $valuedate); $lastdate = $betterdate[0]; $lasttime = $betterdate[1];
  $lastchange .= "On ".$lastdate." at ".$lasttime;
  if ($lastchange == "On  at " || $lastchange == "By User #  ( ) From $0.00 On  at ") { $lastchange = "-No Data-"; }
?> 
  <div class="row">
    <label class="col-md-2 control-label" for="lastchange"><abbr title="When the last change happened to the Current Value">Value Last Changed</abbr></label>
    <div class="col-md-6">
      <input id="lastchange" name="lastchange" type="text" style="font-size:14px;" placeholder="" class="form-control input-md" value="<?= $lastchange ?>" disabled >
    </div>
  </div>
</div>

<div class="form-group">
  <?php 
  if ($this->registry->security->checkFunction("hallmastmgmtl")) {
    $getuser = $this->registry->security->getUserName($lastchangedby); 
    $betterdate = explode(" ", $lastchangedon); $lastdate = $betterdate[0]; $lasttime = $betterdate[1];
    $lastchange = "By User # ".$lastchangedby." (".$getuser['firstname']." ".$getuser['lastname'].") on ".$lastdate." at ".$lasttime;
    if ($lastchange == "By User #  ( ) on  at ") { $lastchange = "-No Data-"; } 
  ?>
  <div class="row">
    <label class="col-md-2 control-label" for="lastchange"><abbr title="Data regarding who and when the last change happened to this record">Record Last Changed</abbr></label>
    <div class="col-md-6">
      <input id="lastchange" name="lastchange" type="text" style="font-size:14px;" placeholder="" class="form-control input-md datepicker" value="<?= $lastchange ?>" disabled >
    </div>
  </div>
  <?php } ?>

<?php
   if ($this->registry->security->checkFunction("hallmdmgmts")) { 
      if ($action=="ADD") { if(isset($itemnotes)) {$itemnotes = stripslashes($itemnotes);} } 
    ?>
  <div class="row">
    <label class="col-md-2 control-label" for="itemnotes"><abbr title="Internal/Private Notes about this series that cannot be seen by users - only staff can see this - max of 65500 characters.">Internal Notes</abbr></label>  
    <div class="col-md-6">
      <textarea id="itemnotes" name="itemnotes" rows="5" placeholder="" maxlength="65500" class="form-control input-md" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  ><?= $itemnotes ?></textarea>
    </div>
  </div>
</div>
<?php } ?>


<?php
// AND NOW HERE ARE OUR BOTTOM BUTTONS....
if ($action == "DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("crsedit")) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning">Edit <?= $thingname ?></button>
        <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("usernotes")) { ?>
          <button id="submit" name="submit" value="notes" class="btn btn-sm btn-primary"><?= $thingname ?> Notes</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("userlog")) { ?>
          <button id="submit" name="submit" value="log" class="btn btn-sm btn-primary"><?= $thingname ?> Log</button>
      <?php 
        }
      ?>
</div>
<?php
}
?>

<!-- Button -->
<?php if ($action !== "DISPLAY") {
  ?>
<div class="form-group">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8">
      <?php if ($action == "ADD") {
              if (isset($suggesting) && $suggesting == true) { ?>
              <button id="submit" name="submit" value="add" class="btn btn-warning">Suggest New <?= $thingname ?></button>
              <?php }
              else { ?>
              <button id="submit" name="submit" value="add" class="btn btn-warning">Add New <?= $thingname ?></button>
              <?php } ?>
                <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">Update <?= $thingname ?></button>
              <button id="submit" name="submit" value="display" class="btn btn-primary">CANCEL</button>
              <?php
            }?>
    </div>
</div>
<?php 
}

?>

</fieldset>
</form>

<!-- Modal -->
<div class="modal fade" id="artistModal" tabindex="-1" role="dialog" aria-labelledby="Artist Information" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Artist Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php $location = BASE_URL."hallmarkartmgmt/".$artistno."/X" ?>
       <iframe src="<?= $location ?>" title="Artist Information" width ="100%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="seriesModal" tabindex="-1" role="dialog" aria-labelledby="Series Information" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Series Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php $location = BASE_URL."hallmarksermgmt/".$seriesno."/X" ?>
       <iframe src="<?= $location ?>" title="Series Information" width ="100%"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>



<div class="panel panel-primary">

  <div class="panel-heading pb-4">

      <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    // And now, let's set our page on-screen header
    $header = $thingname." Record Search Results";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
     if (isset($_SESSION['collection']) && isset($_SESSION['collection_details'])) {
      $header2 = "[ COLLECTION: ".$_SESSION['collection_details']['collname']." ACTIVE ]";
      $collactive = true;
      $collurl = BASE_URL."hallmarkitemsearch";
      echo '<a href="'.$collurl.'""><h6 class="btn btn-sm btn-outline-info">'.$header2.'</h6></a>'; 
    }
    else {
      $collactive = false;
    }  
    ?>
    <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>
  </div>
</div>

  <form action="<?= BASE_URL ?>hallmarkmastmgmt" method="post" name="hallmarkmastmgmt" id="client_form">

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="research" class="btn btn-sm btn-primary">BACK TO SEARCH</button>
        <a class="btn btn-sm btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>



    <input type="hidden" name="searchform" value="searchform" />
    <table class="table table-bordered table-hover table-responsive-sm" id="myTable2">
      <thead>
        <tr>
            <th onclick="sortTable(0)" class="hideable"><a href="#">Master #</a></th>
            <th onclick="sortTable(1)" class="hideable"><a href="#">UPC Code</a></th>
            <th onclick="sortTable(2)"><a href="#">Item Name</a></th>
            <th onclick="sortTable(3)" class="hideable"><a href="#">Released</th>
            <th onclick="sortTable(4)"><a href="#">Artist</th>
            <th cnclick="sortTable(5)" class="hideable"><a href="#">Status</th>
            <th>Image</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['results'])) {
      $rows = $values['results'];  
    }
    foreach ($rows as $row) {
      if ($row['masactive'] == 1) {$astatus = "Active"; } elseif ($row['masactive'] == 2) { $astatus = "PENDING"; } else { $astatus = "INACTIVE"; }
      if ($row['artistno'] == "") { $aname = "VARIOUS"; } else { $aname = $row['firstname']." ".$row['lastname']; }
      if ($row['releasedate'] == 0) {$release = "UNKNOWN"; } else { $release = $row['releasedate']; }
      if ($row['isrepaint'] == 1) { $itnm = $row['itemname']." (REPAINT)"; } else { $itnm = $row['itemname']; }
      if ($row['storetype']) {
        $storetype = $row['storetype']; $storeloc = $row['storeloc']; $imagename = $row['imagename']; $imagenote = $row['imagenote'];
        $urlprefix = $this->registry->storage->getStoragePath($storetype, $storeloc, true);
        $imageurl = $urlprefix.$imagename;
      }
      else { $imageurl = false; }
      ?>
      <tr>
            <td class="hideable"><?= $row['masterno'] ?></td>
            <td class="hideable"><?= $row['upccode'] ?></td>
            <td><?= $itnm ?></td>
            <td class="hideable"><?= $release ?></td>
            <td><?= $aname ?></td>
            <td class="hideable"><?= $astatus ?></td>
            <td><?php if ($imageurl) { ?> <img class="lazy" data-src="<?= $imageurl ?>" title="<?= $imagenote ?>" > <?php } else { ?> <p class="noimg"></p>  <?php } ?></td>
            <td><button id="submit" name="submit" class="btn btn-primary" value="select-<?= $row['masterno'] ?>">SELECT</button></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="research" class="btn btn-primary">BACK TO SEARCH</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             


<script type="text/javascript" src="<?= BASE_URL ?>js/jquery.lazy.min.js"></script>
<script>
  $(function() {
        $('.lazy').Lazy({
          enableThrottle: true,
          throttle: 500,
          visibleOnly: true,
          onError: function(element) {
            console.log('error loading ' + element.data('src'));
          }
        });
    });
</script>





<form class="form-horizontal" action="<?= BASE_URL ?>hallmarkmastsrch" method="post" name="hallmarkmastsrch" id="user_form">
<fieldset>
<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
    <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    // And now, let's set our page on-screen header
    $header = "Searching ".$thingname." Records";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
    if (isset($_SESSION['collection']) && isset($_SESSION['collection_details'])) {
      $header2 = "[ COLLECTION: ".$_SESSION['collection_details']['collname']." ACTIVE ]";
      $collactive = true;
      $collurl = BASE_URL."hallmarkitemsearch";
      echo '<a href="'.$collurl.'""><h6 class="btn btn-sm btn-outline-info">'.$header2.'</h6></a>'; 
    }
    else {
      $collactive = false;
    }  
    ?>
    <hr />
</div>
<div class="panel-body">
             
  <?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                $searchterm = $_SESSION['searchterm'];
                $active = $_SESSION['searchactive'];
                $searchall = $_SESSION['searchall'];
                unset ($_SESSION['searchterm'], $_SESSION['searchactive']);
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        ;
                switch ($_SESSION['returncode']) {
                    case 5:
                        echo "ERROR: No records were found.  Please try searching again.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '</div>';
            }
            else {
              $active = 1;
              $searchall = 1;
            }
            
          
?>        

<?php 
    // This is used during dev and can be commented our in production
    error_reporting(E_ALL & ~E_NOTICE);
?>


<!-- Text input-->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="searchterm">Search Term</label>  
    <div class="col-md-4">
    <input id="searchterm" name="searchterm" type="text" placeholder="Keywords or click SEARCH to list all" class="form-control input-md" value="<?= $searchterm?>" autofocus>
    </div>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <?php if ($this->registry->security->checkFunction("isstaff")) { ?> 
  <div class="row">
    <label class="col-md-2 control-label" for="active">Status</label>
    <div class="col-md-4">
      <select id="active" name="active" class="form-control">
        <option value="1" <?php if ($active==1) { echo "selected"; }?>>Show Only Active <?= $thingname ?>s</option>
        <option value="0" <?php if ($active==0) { echo "selected"; }?>>Show ALL <?= $thingname ?>s</option>
        <?php if ($this->registry->security->checkFunction("searchpending")) { ?> 
        <option value="3" class="admin" <?php if ($active==3) { echo "selected"; }?>>Show Only PENDING Items</option>
        <?php } ?>
        <?php if ($this->registry->security->checkFunction("searchunviewable")) { ?> 
        <option value="2" class="admin" <?php if ($active==2) { echo "selected"; }?>>Show ALL incl. UNVIEWABLE</option>
        <?php } ?>
      </select>
    </div>
  </div>
  <?php } ?>
  <div class="row">
    <label class="col-md-2 control-label" for="searchall">Limit Search</label>
    <div class="col-md-4">
      <select id="searchall" name="searchall" class="form-control">
        <option value="1" <?php if ($searchall==1) { echo "selected"; }?>>Search All Possible Fields</option>
        <option value="0" <?php if ($searchall==0) { echo "selected"; }?>>Search Only Item Names &amp; UPC Codes</option>
      </select>
    </div>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8">
      <button id="submit" name="submit" value="search" class="btn btn-primary">SEARCH</button>
      <?php if($collactive) {
        
        ?> <a class="btn btn-primary" href="<?= $collurl ?>" role="button">RETURN TO MY COLLECTION</a>  <?php
      }
      else {
        ?> <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a> <?php
      } ?>
      
      <?php
        if ($this->registry->security->checkFunction("hallmdmgmta")) { ?>
          <a class="btn btn-warning" href="hallmarkmastmgmt" role="button">ADD NEW MASTER ITEM</a>
        <?php 
        }
        elseif ($this->registry->security->checkFunction("hallmdmgmtas")) { ?>
          <a class="btn btn-warning" href="hallmarkmastmgmt" role="button">SUGGEST A NEW MASTER ITEM</a>
        <?php 
        }
      ?>
      
    </div>
  </div>
</div>


</fieldset>
</form>



<?php

class hallmarkmastmgmt extends baseController {

    /*  
       Payload Return (comes from MODEL)
            $returnPayload[0]   BOOL - was the operation successful? (true/false)
            $returnPayload[1]   INT - Level of severity (0=None [default], 1=Success, 2=Info, 3=Warning, 4=Danger, 5=BLACK)
            $returnPayload[2]   STRING - a short description of the success or failure
            $returnPayload[3]   STRING- The message the user will see in the alert box if shown
            $returnPayload[4]   ARRAY - All of the data that came from the VIEW on the FORM, so it can 
                                be redisplayed along with the error at top OR the data from the database
                                of those same fields for display
    */

    // ********************************************************************************************                      
    // These private variables are ALL you should have to set any time you instantiate this class.
    // UNLESS you need you need to send extra data to your view, then see ~line 184
    // ********************************************************************************************

    // These three variables contain the ACL "Function Names" of the functions that give access 
    // to each of these functions.  Set them before you continue.  If you don't have ACL functions,
    // create them through the Web Interface.  If you don't have security turned on, it won't matter. 
    private $acl_display = "hallmdmgmtd";
    private $acl_add = "hallmdmgmta";
    private $acl_edit = "hallmdmgmte";

    // This variable allows the user to "suggest" a new add - they can add, but a "SUGGESTION" flag
    // will be set
    private $acl_suggest = "hallmdmgmtas";

    // This variable is the name of the model that you'll be working with. 
    private $modelname = "mHallmarkMaster";

    // This is the name of the search controller, if any, that you are coming from
    // If you are not working with a search, set this to the controller that you
    // want anyone who presses "BACK" to go to.
    private $searchname = "hallmarkmastsrch";

    // Location and name of your VIEW file
    private $viewloc = "hallmark/body_update_master.php";

    // These variables contain some additional data to pass to your View.
    // $pagetitle - shows up in the browser tab; $thingname - when viewing a record, THING # NNN will
    //    be shown.  Change $thingname to "USER" or "COURSE" or whatever as needed.
    private $pagetitle = "Hallmark Master Item Management";  
    private $thingname = "Hallmark Master Item";


    // ********************************************************************************************                      
    // ********************************************************************************************
    // ********************************************************************************************



    public function index() {
        // First, we call in the model we'll need
        $mname = $this->modelname;
        $this->model = new $mname($this->registry);

        // Next, initialize some variables just in case
        $record = false;
        $action = "ADD";
        $posted = false;

        // Let's check for display only data.  This would come in the form of 
        // Passed Variables from the registry.  We read the variable and store it
        // as $record. We also set $edit to false to let the system know this is
        // display only.
        if ($this->registry->passedVars) {
            $record = $this->registry->passedVars[0];
            $action = "DISPLAY";
        }

        // Now we can deal with POSTed data, if any.
        // If $record contains a record number, then EITHER $edit or $update can 
        // be TRUE, but NOT BOTH, so we'll automatically help out with that to avoid errors. 
        if (isset($_POST) && !empty($_POST)) {     
            // First, we sanitize the POST data
            $this->registry->security->sanitizePost(); 
            $posted = $_POST;

            // Next, we determine what action needs to be taken based 
            // on the POST data 
            if (isset($_POST['record']) && $_POST['record'] != false) {
                $record = $_POST['record'];
            }
            if (isset($_POST['action'])) {
                $action = $_POST['action'];
            }
            else {
                $action = "ADD";
            }

            // If buttons were pushed without an edit (from Display, for instance)
            // The submit value will tell us what the user wants to do. 
            // Let's deal with that next.
            if (isset($_POST['submit'])) {

                if ($_POST['submit'] == "edit") {
                    $action = "EDIT";
                }
                if ($_POST['submit'] == "display") {
                    $action = "DISPLAY";
                }
                 if ($_POST['submit'] == "update") {
                    $action = "UPDATE";
                }
                if ($_POST['submit'] == "repaint") {
                    $action = "ADD";
                }
                if ($_POST['submit'] == "search") {
                    $_SESSION['lastsearch'] = true;
                    $this->registry->redirect($this->searchname);
                }
                if ($_POST['submit'] == "photos") {
                    $masterno = $_POST['masterno'];
                    if ($masterno == 0 || $masterno == false || $masterno == "") {
                        $action = "DISPLAY";
                    }
                    else { return $this->getPhotos($masterno); }
                }
                if ($_POST['submit'] == "uploadphoto") {
                    $masterno = $_POST['record'];
                    $result = $this->uploadPhotos();
                    return $this->getPhotos($masterno, $result);
                }
                if ($_POST['submit'] == "addtocollection") {
                    $masterno = $_POST['record'];
                    $addurl = "hallmarkitemmgmt/ATC".$masterno;
                    $this->registry->redirect($addurl);
                }
                if (substr($_POST['submit'],0,2) == "ux") {
                    $action = substr($_POST['submit'], 2,1);
                    $masterno = $_POST['record'];
                    $imageno = substr($_POST['submit'], 3);
                    $reason = $_POST['reason'];
                    $result = $this->changePhoto($masterno, $imageno, $action, $reason);
                    return $this->getPhotos($masterno, $result);
                }
                if ($_POST['submit'] == "research") {
                    $_SESSION['lastsearch'] = false;
                    $this->registry->redirect($this->searchname);
            }
                if ((strlen($_POST['submit']) >7) && (substr($_POST['submit'], 0,6) == "select")) {
                    $selectpieces = explode('-', $_POST['submit']);
                    if (is_numeric($selectpieces[1]) && $selectpieces[1] > 0) {
                        $record = $selectpieces[1];
                        $action = "DISPLAY";
                    }
                }
            }
        }
        return $this->getCrudView($record,$action,$posted);
    }


    // $record = the record number (index number) of the database item to pull, may be false if none
    // $action = the action to take (ADD, DISPLAY, EDIT, UPDATE)
    // $posted = an array of all of the variables to be pushed through the model for add/update.
    public function getCrudView ($record=false, $action="ADD", $posted=false) {
        // First, we need to do some security.  Let's make sure that the user has the permission
        // to do a display, edit or an add.  These group policies are set in the Function 
        // Management Module and is the "Function Name" variable there.  Set the names of the 
        // functions at the top of this class
        $permission_display = false;
        $permission_add = false;
        $permission_edit = false;
        $suggesting = false;
        if (isset($_SESSION['acl_functions'][$this->acl_display]) || $this->registry->isgod) {
            $permission_display = true;
        }
        if (isset($_SESSION['acl_functions'][$this->acl_add]) || $this->registry->isgod) {
            $permission_add = true;
        }
        elseif (isset($_SESSION['acl_functions'][$this->acl_suggest]) || $this->registry->isgod) {
            $permission_add = true;
            $suggesting = true;
        }
        if (isset($_SESSION['acl_functions'][$this->acl_edit]) || $this->registry->isgod) {
            $permission_edit = true;
        }
        if (($action == "DISPLAY" && $permission_display == false) || ($action == "ADD" && $permission_add == false) || ($action == "UPDATE" && $permission_edit == false)) {
            $this->registry->security->doSecurity(false);
            exit();
        }

        //var_dump($action);
        //var_dump($posted);
        //exit();


        // Let's initialize the returnPayload, just in case we don't have one
        $returnPayload[0] = true;
        $returnPayload[1] = "";
        $returnPayload[2] = "";
        $returnPayload[3] = 0;
        $returnPayload[4] = "";

        // If $action = UPDATE, then we need to send the data to the model to be updated
        // If we're doing a DISPLAY or EDIT, we need to pull the record
        // Otherwise, we're doing an add, which really doesn't require anything.
        if ($action == "DISPLAY" || $action == "EDIT" || ($action == "ADD" && isset($posted['submit']) && $posted['submit'] == "repaint")) {
            $returnPayload = $this->model->getRecordData($record);
            // If this worked, get the primary photo and any repaints
            if ($returnPayload[0] == true) {
                $returnPayload[4]['primaryphoto'] = $this->model->getPrimaryPhoto($record);
                $returnPayload[4]['numrepaints'] = $this->model->getNumRepaints($record);
            }
            if ($posted && $posted['submit'] == "repaint") {
                $returnPayload[4]['isrepaint'] = 1;
                $returnPayload[4]['repaintof'] = $record;
                $returnPayload[4]['repaintname'] = $returnPayload[4]['itemname'];
                $returnPayload[4]['repaintdate'] = $returnPayload[4]['releasedate'];
                $returnPayload[4]['masterno'] = "";
                $returnPayload[4]['upccode'] = "";
                $returnPayload[4]['origmsrp'] = 0;
                $returnPayload[4]['priorvalue'] = 0;
                $returnPayload[4]['currentvalue'] = 0;
                $returnPayload[4]['releasedate'] = 0;
                $returnPayload[4]['valuedate'] = "";
                $returnPayload[4]['valueby'] = "";
                $returnPayload[4]['lastchangedon'] = "";
                $returnPayload[4]['lastchangedby'] = "";
                $record="";
            }
        }
        if ($action == "UPDATE" || ($action == "ADD" && $posted != false && $posted['submit'] != "repaint")) {
            $returnPayload = $this->model->updateRecordData($posted, $action);
            if ($returnPayload[0] == true) {
                if ($action == "ADD") { $record = $returnPayload[4]['record']; }
                if ($returnPayload[4]['submit'] == "delete") {
                    $newPayload = false;
                }
                else {
                    $newPayload = $this->model->getRecordData($record);
                    if ($newPayload[0] == true) { $returnPayload[4] = $newPayload[4]; }
                }
            }
            // If this worked, get the primary photo
            if ($returnPayload[0] == true) {
                $returnPayload[4]['primaryphoto'] = $this->model->getPrimaryPhoto($record);
            }
            $action = "DISPLAY";
        }

        
        // Before we finish, let's log anything worth logging
        if (!empty($returnPayload[2])) {
            $this->registry->logging->logEvent($this->registry->config['logging_cat_success'],$returnPayload[2]);   
        }

        // We need the artists as a list to use in our payloads, so:
        $artistmodel = new mHallmarkArtist($this->registry);
        $artists = $artistmodel->getAllArtists();
        $returnPayload[5] = $artists;
        $seriesmodel =new mHallmarkSeries($this->registry);
        $series = $seriesmodel->getAllSeries();
        $returnPayload[6] = $series;

        // Okay, now we send the data to the VIEW to handle displaying everything.
        // Remember when dealing with VIEWS that ['variables'] end up in the view
        // simply as $values, so you address them in the VIEW as $values['action']
        // for instance.
        $rvalue['type']='html';
        $rvalue['variables']['action']= $action;
        $rvalue['variables']['record']= $record;
        $rvalue['variables']['suggesting'] = $suggesting;
        $rvalue['variables']['title']=$this->pagetitle;
        $rvalue['variables']['thingname']=$this->thingname;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = $this->viewloc;
        $rvalue['footerfile']="footer.php";
        $rvalue['variables']['returnPayload'] = $returnPayload;
        // IF you need to send extra variables to the VIEW, here is the place to do it:
        //$classstyles = $this->model->getClassStyles();
        //$rvalue['variables']['classstyles']=$classstyles;
        return $rvalue;
    }


    public function getPhotos($masterno, $returnPayload=false) {
        $getphotos = $this->model->getMasterPhotos($masterno);
        $collector = new mCollector($this->registry); 
        $getlimits = $collector->getPlanLimits();
        $photolimit = $getlimits['maxmasterimg'];
        if ($getphotos[1] == true && $getphotos[4] !== false) { $photos = $getphotos[4]; } else { $photos = false; }
        $rvalue['type']='html';
        $rvalue['variables']['title']="Master Item Photos";
        $rvalue['variables']['photos']=$photos;
        $rvalue['variables']['masterno']=$masterno;
        $rvalue['variables']['maxmasterimg'] = $photolimit;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "hallmark/body_update_photos.php";
        $rvalue['footerfile']="footer.php";
        if ($returnPayload) { $rvalue['variables']['returnPayload'] = $returnPayload; }

        return $rvalue; 
    }

    public function uploadPhotos() {
        // LET'S DEFINE WHERE ALL PHOTOS FOR HALLMARK WILL BE STORED. YOU CAN
        // CHANGE THIS AT ANY TIME WITHOUT EFFECTING EXISTING IMAGES
        // $storetype = 0 is LOCAL -- $storetype = 1 is AWSS3
        $storetype = 1;
        $storeloc = "MPCAWS01";
        $maxsize = 9000000;
        $maxfinalsize = 1500000;


        // Let's make sure we can move whatever was uploaded to temp storage
        $error = 0; $e = "";
        $destination = $this->registry->config['storage_TEMPLOC'];
        $origname = $_FILES['filename']['name'];
        $origfile = $_FILES['filename']['tmp_name'];
        $destfile = $destination.$origname; 
        if (move_uploaded_file($origfile, $destfile)) {
            // It's moved, so now let's make sure the file is valid and resize it if it is too big,
            $goodfile = $this->registry->storage->validateImage($origname, $maxsize, true, $maxfinalsize);
            if ($goodfile[0] == false) { return $goodfile; }
            else { if ($goodfile[0] == true && $goodfile[4] == "RESIZED") { $origname = "RSZ_".$origname; } }
        } 

        // Now, what about number of images?
        // How many are already there?  If it exceeds the max allowed, stop the user right there.
        $primarycheck = $this->model->getMasterPhotos($_POST['record']);
        if ($primarycheck[0] == true && $primarycheck[4]) { 
            $numimages = count($primarycheck['4']);
            $collector = new mCollector($this->registry); 
            $getlimits = $collector->getPlanLimits();
            $photolimit = $getlimits['maxmasterimg'];
            if ($numimages >= $photolimit) {
                $returnPayload[0] = false;
                $returnPayload[1] = 4;
                $returnPayload[2] = "UPLOADPHOTOS FOR MASTER FAILED - MAX IMAGES EXCEEDED";
                $returnPayload[3] = "FAILED: You have reached the maximum allowed images for this item ($photolimit).";
                $returnPayload[4] = false;
                return $returnPayload; 
            }
        }
        
        if ($storetype==0) {
            if($error==0) { $returnPayload = $this->registry->storage->putLocal($storeloc,$origname); }
        }
        else if ($storetype == 1) {
            if ($error == 0) { $returnPayload = $this->registry->storage->putS3($storeloc,$origname); }
        }
        else {
           $error=1; $e .="835192 ";
        }
        if ($error == 0 && $returnPayload[0] == false) {
            return $returnPayload;
        }
        if ($error == 0) {
            $values = $_POST;
            $values['origname'] = $origname;
            $values['storetype'] = $storetype;
            $values['storeloc'] = $storeloc;
            $values['imagename'] = $returnPayload[4];
            $dbstore = $this->model->putMasterPhotos($values);
            if ($dbstore) { 
                $this->registry->storage->clearTemp($origname);
                return $returnPayload; 
            } 
            else { $error=1; $e .="835421 ";}
        }
        if ($error==1) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "uploadPhotos FAIL in hallmarkmastmgmt: ERROR $e";
            $returnPayload[3] = "An internal error has occured. Please notify your system administrator. ERROR $e";
            $returnPayload[4] = false;
            return $returnPayload;
        }
        return false;

    }

    public function changePhoto($masterno=false, $imageno=false, $action=false, $reason="") {
        if ($action=="d") {$action = "DELETE"; }
        elseif ($action=="p") {$action = "PRIMARY"; }
        elseif ($action=="f") {$action = "FLAG"; }
        elseif ($action=="s") {$action = "STRIKE"; }
        else {$action=false;}
        $error=0; $e="";
        if ($masterno == false) { $error = 1; $e .= "334812 "; }
        if ($action==false) { $error = 1; $e .= "334992 "; } 
        if ($imageno==false) { $error = 1; $e .= "334351 "; }
        if ($error == 0 && $this->model->updatePhotos($masterno, $imageno, $action, $reason)) {
            $returnPayload[0] = true;
            $returnPayload[1] = 1;
            $returnPayload[2] = "changePhoto SUCCESS in hallmarkmastmgmt";
            $returnPayload[3] = "SUCCESS: Image updated successfully.";
            $returnPayload[4] = false;
            return $returnPayload;
        }
        $returnPayload[0] = false;
        $returnPayload[1] = 4;
        $returnPayload[2] = "changePhoto FAIL in hallmarkmastmgmt: ERROR $e";
        $returnPayload[3] = "ERROR: An internal error has occured. Please notify your system administrator. ERROR $e";
        $returnPayload[4] = false;
        return $returnPayload;

    }

    public function pullPhoto($storetype, $storeloc, $filename) {
        if ($storetype == 0) {
            $result = $this->registry->storage->getLocal($storeloc, $filename);
            if ($result[0] == false) { return false; }
            return true;
        }
        if ($storetype == 1) {
            $result = $this->registry->storage->getS3($storeloc, $filename);
            if ($result[0] == false) { return false; }
            return true;   
        }
        return false; 
    }

}
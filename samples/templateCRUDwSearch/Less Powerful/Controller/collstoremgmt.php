<?php

class collstoremgmt extends baseController {

    // IMPORTANT: Change the name of the class ______ extends above first to match the name of your file exactly!

    // This class displays a list of items that can be added to, edited, or deleted.  Simply set the variables below
    // Edit the Model to fit your database needs, Edit the Views and you're ready to go!

    // Then Set these for your particular case:
    // The name of the Model you'll need to Call
    private $modelname = "mCollector";
    // A Three-letter Code to indicate that a particular record is needed. Can be anything (or leave as-is)
    private $tlc = "SNX";
    // The name of the permission (function) required to be able to delete a record.
    private $delfunc = "strgdelete";
    // What are the things being managed here, in singular (Collection, Record, File, Picture)
    private $thing = "Storage Location";
    // What is the name of the Body File to be shown (view) for the initial list screen. If in a subfolder, put that as well
    private $listbody = "collector/body_show_storage.php";
    // What is the name of the Body File to be shown (view) for the add/edit/display screen.  If in a subfolder, put that as well
    private $dispbody = "collector/body_update_storage.php";

    // ***** THERE SHOULD BE NO NEED TO TOUCH BELOW THIS LINE - AT FIRST ******

    public function index() {
        /* First, we call in the model we'll need */
        $this->model = new $this->modelname($this->registry);
        
        // If any POSTed data was sent, we need to sanitize it
        $this->registry->security->sanitizePost(); 

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if (substr($_POST['submit'],0,3) == $this->tlc) {
                $funcnum = substr($_POST['submit'],3);
                return $this->getUpdateFunction($funcnum,'EDIT');
            }
            if ($_POST['submit'] == "new") {
                return $this->getUpdateFunction(false, "NEW", false);
            }
            if ($_POST['submit'] == "savenew") {
                return $this->doUpdateFunction('NEW');
            }
            if ($_POST['submit'] == "update") {
                if (isset($_SESSION['funcnum'])) {
                    $funcnum = $_SESSION['funcnum'];
                    return $this->doUpdateFunction('UPDATE');
                }
            }
            if ($_POST['submit'] == "delete" && $this->registry->security->checkFunction($this->delfunc)) {
                $funcnum = $_SESSION['funcnum'];
                $funcname = $_POST['storename'];
                if($this->model->deleteStorageLocation($funcnum)) {
                    $explanation = $this->thing." DELETED. USER: %%USER%% ".$this->thing.": $funcnum ($funcname)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 2;
                }
                else {
                    $explanation = $this->thing." DELETE FAILED. USER: %%USER%% ".$this->thing.": $funcnum ($funcname)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 5; 
                }
                return $this->getAllFunctions();
            }
            if ($_POST['submit'] == "funcmgmt") {
                   return $this->getAllFunctions();
            }
        }
        else {
             if ($this->registry->passedVars) {
                if (is_numeric($this->registry->passedVars[0])) {
                    return $this->getUpdateFunction($this->registry->passedVars[0], 'EDIT');
                }
            }
            return $this->getAllFunctions();
        }
    }

    // This method returns all modules available for editing and displays them in a list
    // on screen to be chosen from.  IT NEVER shouls moduleindex #1 as that is for internal
    // Use only.
    public function getAllFunctions() {
        $usernum = $_SESSION['usernum'];
        $locations = $this->model->getUserStorageLocs($usernum);
        $numallowed = $this->model->getPlanLimits($usernum);
        $rvalue['type']='html';
        $rvalue['variables']['title']=$this->thing." Management Tool";
        $rvalue['variables']['locations']=$locations;
        $rvalue['variables']['numallowed']=$numallowed['maxstoragelocs'];
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = $this->listbody;
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }



    public function getUpdateFunction($funcnum=false, $action=false, $rvalues=false) {
        $rvalue['type']='html';
        $rvalue['variables']['title']=$this->thing." Management: Edit"; 
        $redisplay = false;
        if (isset($_SESSION['returncode'])) {
            if ($_SESSION['returncode'] == 5) {
                $redisplay = true;
            }
        }
        if ($funcnum && !$redisplay) {
            $vars=$this->model->getStorageLocation($funcnum);
            if ($vars) {
                $rvalue['variables']['location']=$vars;
                $rvalue['variables']['actiontype']=$action;
                $_SESSION['funcnum']=$funcnum;    
            }
            else {
                $rvalue['variables']['error']['funcnum'] = $funcnum;
                $_SESSION['returncode'] = 6;
            }
        }
        elseif ($funcnum && $redisplay) {   
            $vars=$this->model->getStorageLocation($funcnum);
            $vars=$_POST;          
            $rvalue['variables']['function']=$vars;
            $rvalue['variables']['actiontype']=$action;
            $rvalue['variables']['function']['funcnum']=$funcnum;
        }
        if ($rvalues) {
            foreach ($rvalues as $key=>$value) {
                $rvalue['variables'][$key]=$value;
            }
        }
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = $this->dispbody;
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }


     public function doUpdateFunction($actiontype="NEW") {
        // If This is a new entry, we first check for a duplicate, then do the add if none is found
        if ($actiontype == "NEW") {
            if (isset($_SESSION['funcnum'])) {unset($_SESSION['funcnum']);}
            $result = $this->model->updateStorageLocation($actiontype, false, $_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);
                }
                if ($result[1]=="MISSING_DATA") {
                    $_SESSION['returncode'] = 4;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);   
                }
                if ($result[1]=="EXCEEDED_LIMITS") {
                    $_SESSION['returncode'] = 6;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);   
                }
                else {
                    $_SESSION['returncode'] = 7;
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);
                }
            }
            else {
                    $_SESSION['funcnum'] = $result[2];
                    $_SESSION['returncode'] = 2;
                    return $this->getUpdateFunction($result[2], 'EDIT');
            }
        }
        elseif ($actiontype == "UPDATE") {
            $funcnum = $_SESSION['funcnum'];
            $result = $this->model->updateStorageLocation($actiontype, $funcnum, $_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    $rvalues['tempdata']['groupindex'] = $funcnum;
                    return $this->getUpdateFunction($funcnum, 'EDIT', $rvalues);
                }
                elseif ($result[1]=="MISSING_DATA") {
                    $_SESSION['returncode'] = 4;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);   
                }
                elseif ($result[1]=="EXCEEDED_LIMITS") {
                    $_SESSION['returncode'] = 6;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);   
                }
                else {
                    $_SESSION['returncode'] = 7;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'EDIT', $rvalues);
                }
            }
            else {
                    $_SESSION['funcnum'] = $result[2];
                    $_SESSION['returncode'] = 3;
                    return $this->getUpdateFunction($result[2], 'EDIT');
            }
        }
    }

}
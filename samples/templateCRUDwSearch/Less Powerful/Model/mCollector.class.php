<?php

class mCollector extends baseModel {
    
   
    /**************************************************************************
    /* ITEM CONDITIONS MANAGEMENT 
    /**************************************************************************/

    // This method returns ALL data from the DB about ALL Item Conditions
    // if $showall = True, show the main module as well (caution)
    // if $removepermissions = True, only return the index and names, not the allow or deny permissions
    public function getAllConditions($showall=false, $removepermissions=false, $sortbyname=false) {
        $sql = "SELECT * FROM collections_conditions ORDER BY condgrade DESC";
        $result = $this->registry->db->query($sql);
        if ($result) { 
            $conditions = $this->registry->db->getAllRows(); 
            return $conditions;
        }
        else { return false; }
    }

    
    // Pulls one single condition for editing or viewing
    public function getCondition($modnum=false, $fullurl=false) {
        if ($modnum) {
            $sql = "SELECT * FROM collections_conditions WHERE condno = $modnum LIMIT 1";
            $vars=$this->registry->db->oneRowQuery($sql);
            if ($vars) {
                if ($fullurl) {
                    $baseurl = $this->registry->config['url'];
                    $fullurl = $baseurl."/".$vars['condgrade'];
                    return $fullurl;
                }
                else {
                    return $vars;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    // This method deletes an Condition completely  
    // Returns TRUE if successful, FALSE if not
    public function deleteCondition($modulenum) {
        // If that was successful, now let's remove the main function from the table
        $sql = "DELETE FROM collections_conditions WHERE condno = $modulenum LIMIT 1";
        if ($this->registry->db->query($sql)) { return true; } else { return false; } 
    }




    public function newCondition($data) {
        // No need to sanitize, this was already done in the controller
        // $data=$this->registry->security->sanitizeArray($data);

        // Next, extract it all to individual variables
        extract($data);

        // Now, check that all variables needed exist
        if (!isset($condgrade) || !isset($condname) || empty($condname)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        else {
            // CondName needs to be upper case
            $condname = strtoupper($condname);

            // Next, check for duplicate entries   
            $exists = $this->checkConditionDups($condgrade);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }
            // Next, do the insert
            else {
                $sql = "INSERT INTO collections_conditions (condname, condgrade, condshortdescr, condlongdescr, isactive) VALUES 
                ('$condname', $condgrade, '$condshortdescr', '$condlongdescr', $isactive)";      
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $modulenum = $this->registry->db->getInsertId();
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $modulenum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = false;
                }
            }
        }
        return $result;
    }

     
    public function updateCondition($modulenum, $data) {
         // No need to  sanitize the incoming data - done in controller
        // $data=$this->registry->security->sanitizeArray($data);
        
        // Next, extract it all to individual variables
        extract($data);
        
        // Now, check that all variables needed exist
        if (!isset($condgrade) || empty ($condgrade) || !isset($condname) || empty($condname)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }

        // CondName needs to be upper case
        $condname = strtoupper($condname);
        
        // Next, check for duplicate entries             
        $exists = $this->checkConditionDups($condgrade, $modulenum);
        if ($exists) {
            $result[0] = false;
            $result[1] = "DUPLICATE";
            $result[2] = $exists;
        }
        // Next, do the update
        else {
            $sql = "UPDATE collections_conditions SET condname =' $condname', condgrade = $condgrade, condshortdescr = '$condshortdescr',
            condlongdescr = '$condlongdescr', isactive = $isactive WHERE condno = $modulenum LIMIT 1";
            $success = $this->registry->db->query($sql);
            if ($success) {
                $result[0] = true;
                $result[1] = "SUCCESS";
                $result[2] = $modulenum;       
            }
            else {
                $result[0] = false;
                $result[1] = "DATABASE_ERROR";
                $result[2] = $modulenum;
            }
        }
        return $result;
    }

        private function checkConditionDups($condgrade=false, $modnum=false) {
        if (!is_bool($condgrade) && $condgrade != "") {
            if ($modnum) {
                $sql = "SELECT condno FROM collections_conditions WHERE condgrade = '$condgrade' AND condno != $modnum LIMIT 1"; 
            }
            else {
                $sql = "SELECT condno FROM collections_conditions WHERE condgrade = '$condgrade' LIMIT 1";
            }
            $modnum = $this->registry->db->oneRowQuery($sql);
            if ($modnum) {
                return $modnum['condno'];
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }



    /**************************************************************************
    /* COLLECTION MANAGEMENT 
    /**************************************************************************/


    // Pull all collections existing for a user that are not deleted or inactive
    // Each row will also contain the data about the collection type from the collections_Types table
    public function getUserCollections($usernum=false) {
        if ($usernum == false) { return false; }
        $sql = "SELECT collections_users.*, collections_types.* FROM collections_users LEFT JOIN 
                collections_types ON collections_users.colltype = collections_types.typeno WHERE 
                usernum = $usernum AND (isdeleted = 0 AND isactive = 1 AND islocked = 0)";
        $collquery = $this->registry->db->query($sql);
        if ($collquery) {
            $collections = $this->registry->db->getAllRows();
            return $collections;
        }
        else {
            return false;
        }
    }


    // Returns the number of items in a given collection for a user
    public function getCollectionNumbers($dbtable = false, $collno=false) {
        if ($dbtable) {
            $sql = "SELECT itemno FROM $dbtable WHERE collno = $collno AND (isactive = 1 AND issold = 0 AND isdeleted = 0 AND isviewable = 1)";
            $result = $this->registry->db->query($sql);
            if ($result) { 
                $noitems = $result->num_rows;
                return $noitems;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }


    // Pulls all the data about a specific collection for edit or display
    public function getCollection($collno = false) {
        if ($collno) {
            $sql = "SELECT collections_users.*, collections_types.* FROM collections_users LEFT JOIN collections_types ON 
            collections_users.colltype = collections_types.typeno WHERE collno = $collno LIMIT 1";
            $collection = $this->registry->db->oneRowQuery($sql);
            if ($collection) { return $collection; } else { return false; }
        }
        return false;
    }



    public function newCollection($data) {
        // WAIT, can this user even HAVE a new collection?
        $enteredby = $_SESSION['usernum']; 
        $planlimits = $this->getPlanLimits($enteredby);
        $numcollects = $this->getUserCollections($enteredby);
        if (!$numcollects) {$nc = 0;} else {$nc = count($numcollects); }
        if ($nc >= $planlimits['maxcollections'] ) {
            $result[0] = false;
            $result[1] = "EXCEEDED_LIMITS";
            $result[2] = false; 
            return $result;
        }


        // No need to sanitize the incoming data - it was done in the controller
        // $data=$this->registry->security->sanitizeArray($data);

        // Next, extract it all to individual variables
        extract($data);

        // Now, check that all variables needed exist
        if (!isset($collname) || !isset($typeno) || $typeno == "") {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        else {

            // Next, check for duplicate entries  \
            $exists = $this->checkCollectionDuplicates($typeno, $collname, $enteredby);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }

            // Next, do the insert
            else {
                // Set some variables if there were not set
                if ($isactive == "") { $isactive = 1; }
                if ($islocked == "") { $islocked = 0; }
                if ($isshared == "") { $isshared = 0; }
                $sql = "INSERT INTO collections_users (usernum, colltype, collname, isactive, islocked, isshared, collnotes, lastchangedby) 
                VALUES ($enteredby, $typeno, '$collname', $isactive, $islocked, $isshared, '$collnotes', $enteredby)";      
                //echo $sql;
                //exit();
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $modulenum = $this->registry->db->getInsertId();
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $modulenum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = false;
                }
            }
        }
        return $result;
    }


    public function updateCollection($collno, $data) {
        // No need to sanitize, this was already done in the controller
        // $data=$this->registry->security->sanitizeArray($data);
        
        // Next, extract it all to individual variables
        extract($data);
        
        // Now, check that all variables needed exist
        if (!isset($collname) || empty ($collname)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }

        
        // Next, check for duplicate entries         
        $enteredby = $_SESSION['usernum'];     
        $exists = $this->checkCollectionDuplicates($colltype, $collname, $usernum, $collno);
        if ($exists) {
            $result[0] = false;
            $result[1] = "DUPLICATE";
            $result[2] = $exists;
        }
        // Next, do the update
        else {
            $sql = "UPDATE collections_users SET collname='$collname', isdeleted = $isdeleted, isactive = $isactive, islocked = $islocked,
            isshared = $isshared, collnotes = '$collnotes', lastchangedby = $enteredby WHERE collno = $collno LIMIT 1";
            //echo $sql;
            //exit();
            $success = $this->registry->db->query($sql);
            if ($success) {
                $result[0] = true;
                $result[1] = "SUCCESS";
                $result[2] = $collno;       
            }
            else {
                $result[0] = false;
                $result[1] = "DATABASE_ERROR";
                $result[2] = $collno;
            }
        }
        return $result;
    }

    // Simply returns all the collection types available
    public function getCollectionTypes() {
        $sql = "SELECT * FROM collections_types";
        $result = $this->registry->db->query($sql);
        if ($result) {
            $colltypes = $this->registry->db->getAllRows();
            return $colltypes;
        }
        else {
            return false;
        }
    }


    private function checkCollectionDuplicates($colltype, $collname, $usernum, $collno = false) {
        if ($collno) { $extra = "AND collno != $collno"; } else { $extra = ""; }
        $sql = "SELECT collno FROM collections_users WHERE colltype = $colltype AND collname = '$collname' 
        AND usernum = $usernum AND isdeleted = 0 $extra";
        $dup = $this->registry->db->oneRowQuery($sql);
        if ($dup) { return $dup['collno']; } else { return false; }
        return false;
    }


    // Marks a Collection as Deleted. DOES NOT MARK ITEMS OR PHOTOS AS DELETED THOUGH
    // This is so that if needed, we can restore the collection easily later
    public function deleteCollection($collno=false) {
        if ($collno) {
            $sql = "UPDATE collections_users SET isdeleted = 1 WHERE collno = $collno LIMIT 1";
            $delquery = $this->registry->db->query($sql);
            if ($delquery) { return true; } else { return false; }
        }
        else {
            return false;
        }
    }


    /**************************************************************************
    /* PLAN MANAGEMENT 
    /**************************************************************************/



        // Pull User Plan Allowed Information
        public function getPlanLimits($usernum = false) {
            if ($usernum) {
                //************************************************************************
                // CODE HERE ONCE THIS BECOMES AVAILABLE - THIS IS JUST DEFAULT TEST SQL:
                //************************************************************************
                $sql = "SELECT * FROM collections_settings_default WHERE defsetno = 1 LIMIT 1";
            }
            else {
                // Pull Default Limits
                $sql = "SELECT * FROM collections_settings_default WHERE defsetno = 1 LIMIT 1";
            }
            $defaults = $this->registry->db->oneRowQuery($sql);
            return $defaults;
        }

        

    /**************************************************************************
    /* STORAGE LOCATION MANAGEMENT 
    /**************************************************************************/


        // Gets all storage locations that exist for a user as long as they are not deleted
        // We allow users to see inactive locations since they set and unset that value themselves.
        public function getUserStorageLocs($usernum=false) {
            if ($usernum == false) { return false; }
            $sql = "SELECT * FROM collections_users_storage WHERE usernum = $usernum";
            $locquery = $this->registry->db->query($sql);
            if ($locquery) {
                $locations = $this->registry->db->getAllRows();
                return $locations;
            }
            else {
                return false;
            }
        }

        // Pulls all the data about a specific storage location for edit or display
        public function getStorageLocation($storageno = false) {
            if ($storageno) {
                $sql = "SELECT * FROM collections_users_storage WHERE storageno = $storageno LIMIT 1";
                $storage = $this->registry->db->oneRowQuery($sql);
                if ($storage) { return $storage; } else { return false; }
            }
            return false;
        }

        // Adds or updates Storage Locations
        public function updateStorageLocation($action, $storageno=false, $data) {
            // WAIT, can this user even HAVE a new collection?
            if ($action == "NEW") {
                $enteredby = $_SESSION['usernum']; 
                $planlimits = $this->getPlanLimits($enteredby);
                $numstores = $this->getUserStorageLocs($enteredby);
                if (!$numstores) {$ns = 0;} else {$ns = count($numstores); }
                if ($ns >= $planlimits['maxstoragelocs'] ) {
                    $result[0] = false;
                    $result[1] = "EXCEEDED_LIMITS";
                    $result[2] = false; 
                    return $result;
                }
            }

             // The data was already sanitized in the Controller - no need to again

            // Next, extract it all to individual variables
            extract($data);
            
            // Now, check that all variables needed exist
            if (!isset($storename) || empty ($storename)) {
                $result[0] = false;
                $result[1] = "MISSING_DATA";
                $result[2] = false;
            }

            
            // Next, check for duplicate entries         
            if ($storageno) {$extra = "AND storageno != $storageno"; } else { $extra = ""; }
            $sql = "SELECT storageno FROM collections_users_storage WHERE storename = '$storename' $extra";
            $exists = $this->registry->db->oneRowQuery($sql);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists['storageno'];
            }

            // Next, do the update or add
            else {
                $usernum = $_SESSION['usernum'];
                if ($action == "UPDATE") {
                    $sql = "UPDATE collections_users_storage SET storename = '$storename', storelong = '$storelong', isactive = $isactive
                    WHERE storageno = $storageno LIMIT 1";
                }
                else {
                    $sql = "INSERT INTO collections_users_storage (usernum, storename, storelong, isactive) VALUES ($usernum, '$storename', 
                    '$storelong', $isactive)";
                }
                $success = $this->registry->db->query($sql);
                if ($success) {
                    if ($action == "NEW") { $storageno = $this->registry->db->getInsertId(); }
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $storageno;       
                }
                else {
                    if (!isset($storageno) || $storageno = "") { $storageno = false; }
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = $storageno;
                }
            }
            return $result;
        }


        // Deletes a storage location permanently - we don't hold on to these.
        // The database will automatically cascade the delete from the associated items
        public function deleteStorageLocation($storageno) {
            $sql = "DELETE FROM collections_users_storage WHERE storageno = $storageno LIMIT 1";
            $success = $this->registry->db->query($sql);
            if ($success) { return true; } else { return false; }
        }

}

?>
<?php
// OKAY, TO SET THIS UP, YOU NEED TO SET THESE VARIABLES FIRST //

// The Module to submit all data to upon submit (usually the controller )
$mainmodule = "collstoremgmt";

// The singular version of the thing you are managing (Record, Class, Collection, Photo, etc.)
$subjectitem = "Storage Location";

// THAT'S IT - TThis one is going to require the most work.  Start at line 96 and work all the way down//
?>

<form class="form-horizontal" action="<?= BASE_URL ?><?= $mainmodule ?>" method="post" name="<?= $mainmodule ?>" id="<?= $mainmodule ?>">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
  <div class="panel-heading">
   <div class="form-group">
<?php
  //var_dump($_SESSION);
  //var_dump($_POST);
  //exit();
  //var_dump($values);

  $capitem = strtoupper($subjectitem);
  

  // Determine what type of thing we are doing - new, display, or edit?
  if (isset($values['actiontype'])) {
    $actiontype = $values['actiontype'];
    if (isset($values['location'])) { extract($values['location']); }
  }
  else {
    $actiontype = "NEW";
  }

  if ($actiontype == "EDIT") {
      echo '<h3 class="panel-title">Update Existing '.$subjectitem.'</h3>'; 
    }
    else {
      echo '<h3 class="panel-title">New '.$subjectitem.' Entry Form</h3>';
    }
?>
   </div>
  </div>

<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "New $subjectitem Added. Continue editing if desired or click CANCEL to go back.";
                        break;
                    case 3:
                        echo "$subjectitem Updated. Continue editing if desired or click CANCEL to go back.";
                        break;
                    case 4:
                        echo "ERROR: Storage Name must be filled in.  Please check and try again.";
                        extract($values['tempdata']);
                        break;
                    case 5:
                        $dupfunc = $values['funcnum'];
                        $link = '<a href="'.BASE_URL.'collstoremgmt/'.$dupfunc.'">';
                        echo "ERROR: $subjectitem".$link." Already Exists</a> - Names for Each Storage Location Must Be Different";
                        extract($values['tempdata']);
                        break;
                    case 6:
                        echo "ERROR: You have reached the maximum number of storage locations allowed by your Plan. Contact Support if you need assistance.";
                        extract($values['tempdata']);
                        break;
                    case 7:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '</div>';
            }                    
?>        


<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($_SESSION['funcnum']) && $actiontype != "NEW") {
  $funcnum = $_SESSION['funcnum'];
  echo '<input type="hidden" name="storageno" value ='.$storageno.' />';
}
?>
<input type="hidden" name="usernum" value = "<?= $usernum ?>">
<input type="hidden" name="isdeleted" value = "<?= $isdeleted ?>">


<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="storename"><abbr title="The Short Name for this storage location. 50 Character Limit (Required)" ><strong>Location Name</strong></abbr></label>  
    <div class="col-md-4">
      <input id="storename" name="storename" maxlength="50" type="text" placeholder="" class="form-control input-md" value="<?= $storename ?>" required >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="storelong"><abbr title="Additional identifying details about this storage location (optional)">Location Details</abbr></label>  
    <div class="col-md-6">
      <input id="storelong" name="storelong" maxlength="255" type="text" placeholder="" class="form-control input-md" value="<?= $storelong ?>" >
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="isactive"><abbr title="Is this location currently active for use? You can set it to Inactive if you don't want to use any longer - you can always set it back to active.">Location Active?</abbr></label>
    <div class="col-md-2">
      <select id="isactive" name="isactive" class="form-control" >
        <option value="1" <?php if ($isactive==1) { echo "selected"; }?>>YES</option>
        <option value="0" <?php if ($isactive==0 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>NO</option>
      </select>
    </div>
  </div>
</div>


<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8">
      <?php if ($actiontype == "NEW") {
              unset($_SESSION['modulenum']);
              ?><button id="submit" name="submit" value="savenew" class="btn btn-primary">ADD NEW <?= $capitem ?></button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>collstoremgmt" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">UPDATE <?= $capitem ?></button>
              <button id="submit" name="submit" value="funcmgmt" class="btn btn-primary">CANCEL</button>
              <?php
              if ($this->registry->security->checkFunction("strgdelete")) { ?>
                <button id="submit" name="submit" value="delete" class="btn btn-danger" onclick="return confirm('Are you sure? This <?= $capitem ?> will be deleted and everything stored there will be removed to no storage.')">DELETE STORAGE LOCATION</button> 
                <?php 
              } 
            }
            ?>
      
    </div>
  </div>
</div>

</fieldset>
</form>



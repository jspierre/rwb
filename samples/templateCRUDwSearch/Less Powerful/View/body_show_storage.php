<?php
// OKAY, TO SET THIS UP, YOU NEED TO SET THESE VARIABLES FIRST //

// The Module to submit all data to upon submit (usually the controller )
$mainmodule = "collstoremgmt";

// The singular version of the thing you are managing (Record, Class, Collection, Photo, etc.)
$subjectitem = "Storage Location";

// THAT'S IT - Then adjust the HTML below to suit your needs, especially LINES 58-83 //
?>

<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">
      <h3 class="panel-title"><?= $subjectitem ?> Management</h3>
      <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>
  </div>

  <?php
  if (isset($_SESSION['returncode'])) {
    if ($_SESSION['returncode'] < 4) { $color = "alert-success"; } else { $color = "alert-danger"; }
      echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
      echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
      
      switch ($_SESSION['returncode']) {
        case 2:
          echo "SUCCESS: <?= $subjectitem ?> Deleted as requested.";
          Break;
        case 5:
          echo "ERROR: <?= $subjectitem ?> NOT Deleted. Please try again or contact Support."; 
          Break;
        default:
          echo "An unknown error has occurred.  Please contact Support.";
      }

      echo '</div>';
  }                   
?>  

</div>
  <?php $moduleurl = BASE_URL.$mainmodule; ?>
  <form action="<?= $moduleurl ?>" method="post" name="<?= $mainmodule ?>" id="<?= $mainmodule ?>">
    <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <?php 
          $capitem = strtoupper($subjectitem); 
          if (isset($values['numallowed'])) { 
            $numallowed = $values['numallowed']; 
            if (isset($values['locations']) && $values['locations']) {$numhas = count($values['locations']); } else { $numhas = 0; }
            if ($numhas >= $numallowed) {$allowadd = false; } else { $allowadd = true; }
          }
          else { $allowadd = false; }
        if ($allowadd) { ?>
        <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW <?= $capitem ?></button>
        <?php } ?>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>
    <table class="table table-bordered table-hover table-responsive" id="myTable2">
      <thead>
        <tr>
          <th onclick="sortTable(0)"><a href="#">Storage Name</a></th>
          <th onclick="sortTable(1)"><a href="#">Storage Description</a></th>
          <th onclick="sortTable(2)"><a href="#">Status</a></th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['locations'])) {
      $rows = $values['locations'];  
    }
    else {
      $rows = false;
    }
    if ($rows) {
      foreach ($rows as $row) { 
        if ($row['isactive'] == 1) { $ia = "ACTIVE"; } else { $ia = "INACTIVE"; }
        ?>
      <tr>
        <td title = "<?= $row['storelong'] ?>"><?= $row['storename'] ?></td>
        <td title= "<?= $row['storename'] ?>"><?= $row['storelong'] ?></td>
        <td><?= $ia ?></td>
        <td><button id="submit" name="submit" class="btn btn-primary btn-space" value="SNX<?= $row['storageno'] ?>">EDIT <?= $capitem ?></button></td>
      </tr>
      <?php
      }
   } 
  else { ?>
  <p> No Storage Locations Found. Why not Add a New Storage Location Today? </p>
  <?php } ?>
  </table>

<?php if ($rows  && (count($rows) > 10)) { ?>
  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <?php if ($allowadd) { ?>
        <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW <?= $capitem ?></button>
        <?php } ?>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>
<?php } ?>

</form>
</div>             






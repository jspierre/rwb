<?php
/** 
 * MAIN INDEX FILE
 * @author John St. Pierre
 * @ver 0.1
 *
 */

/*** error reporting on ***/
error_reporting(E_ALL);

/*** define the site path constant ***/
$site_path = realpath(dirname(__FILE__));
define ('SITE_PATH', $site_path);


/*** include the init.php file ***/
require(SITE_PATH.'/includes/init.php');

/*** call the router ***/
$registry->router = new router($registry);
$registry->router->doRoute();


?> 



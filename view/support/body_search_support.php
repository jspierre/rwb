
<form class="form-horizontal" action="<?= BASE_URL ?>supportmastersearch" method="post" name="supportmastersearch" id="user_form">
<fieldset>
<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
    <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    // Also, we need to unset some sessio variables if they exist or our functions won't work properly
    // (added 11-8-2020)
    if (isset($_SESSION['fromcontroller'])) { unset($_SESSION['fromcontroller']); echo "DONE";}
    if (isset($_SESSION['loginnum'])) { unset($_SESSION['loginnum']); }

    // And now, let's set our page on-screen header
    $header = "Searching ".$thingname." Records";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
    ?>
    <hr />
</div>
<div class="panel-body">
             
  <?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                $searchterm = $_SESSION['searchterm'];
                $active = $_SESSION['searchactive'];
                $searchall = $_SESSION['searchall'];
                unset ($_SESSION['searchterm'], $_SESSION['searchactive']);
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
            
                switch ($_SESSION['returncode']) {
                    case 5:
                        echo "ERROR: No records were found.  Please try searching again.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }
            else {
              $active = 1;
              $searchall = 1;
            }
            
          
?>        

<?php 
    // This is used during dev and can be commented our in production
    error_reporting(E_ALL & ~E_NOTICE);
?>


<!-- Text input-->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="searchterm">Search Term</label>  
    <div class="col-md-4">
    <input id="searchterm" name="searchterm" type="text" placeholder="Keywords or click SEARCH to list all" class="form-control input-md" value="<?= $searchterm?>" autofocus>
    </div>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="active">Status</label>
    <div class="col-md-4">
      <select id="active" name="active" class="form-select">
        <option value="0" <?php if ($active==0) { echo "selected"; }?>>Show ALL <?= $thingname ?>s</option>
        <option value="1" <?php if ($active==1) { echo "selected"; }?>>Show Only Open <?= $thingname ?>s</option>
        <option value="2" <?php if ($active==2) { echo "selected"; }?>>Show Only Closed <?= $thingname ?>s</option>
      </select>
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="searchall">Limit Search</label>
    <div class="col-md-4">
      <select id="searchall" name="searchall" class="form-select">
        <option value="0" <?php if ($searchall==0) { echo "selected"; }?>>Search Only <?= $thingname ?> Numbers</option>
        <option value="1" <?php if ($searchall==1) { echo "selected"; }?>>Search All Possible Fields</option>
        <option value="2" <?php if ($searchall==2) { echo "selected"; }?>>Search Only Unassociated Tickets</option>
        <option value="3" <?php if ($searchall==3) { echo "selected"; }?>>Search Only Unassigned Tickets</option>
      </select>
    </div>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8">
      <button id="submit" name="submit" value="search" class="btn btn-primary btn-space">SEARCH</button>
      <a class="btn btn-primary btn-space" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      <?php
        if ($this->registry->security->checkFunction("hallsermgmta")) { ?>
          <a class="btn btn-warning btn-space" href="hallmarksermgmt" role="button">ADD NEW SERIES</a>
        <?php 
        }
      ?>
      
    </div>
  </div>
</div>


</fieldset>
</form>



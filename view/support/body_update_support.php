<script src="https://kit.fontawesome.com/01a10139a1.js" crossorigin="anonymous"></script>

<form class="form-horizontal" action="<?= BASE_URL ?>supportmaster" method="post" name="supportmaster" id="supportmaster">
<fieldset>
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  
// Let's get all of our variables extracted so we can work with them
extract($values);
if (isset($returnPayload[4]) && !empty($returnPayload[4])) {
  extract ($returnPayload[4]);
}
if (isset($returnPayload[5]) && !empty($returnPayload[5])) {
  $assignees = $returnPayload[5];
}
if (isset($returnPayload[6]) && !empty($returnPayload[6])) {
  $messages = $returnPayload[6];
}


// Now, let's set some inputs so we'll have them again later
// Replace $coursenum with the index variable in your table here
// We'll also 
if (isset($record)) {
  echo '<input type="hidden" name="record" value="'.$record.'">';
}


// Is this an error situation? If so, edit needs to be re-opened
if (isset($returnPayload[0]) && $returnPayload[0] == false) {
  $action = "EDIT";
  unset($messages);
}
if ($action == "DISPLAY") { $subaction = false; }

// This is the Headline on the page 
if ($action=="ADD") {
  $header = "Add New ".$thingname." Form";
}
elseif ($action=="EDIT") {
  $header = "Editing ".$thingname." #".$record;
}
else {
  $header = "Displaying ".$thingname." #".$record;
}
echo '<h3 class="panel-title spacer">'.$header.'</h3>'; 
//echo '<p><small>NOTE: Items in <strong>bold</strong> are Required.</small></p>';
?>

</div>
<div class="panel-body">
             
<?php
// Now, let's deal with any return messages we need to display.  These are always
// located in the $returnPayload array
if (isset($returnPayload)) {
    if (empty($returnPayload[3])) {$message = false; } else { $message = $returnPayload[3];}
  switch ($returnPayload[1]) {
    case 1:
      $color="alert-success";
      break;
    case 2:
      $color="alert-info";
      break;
    case 3:
      $color="alert-warning";
      break;
    case 4:
      $color="alert-danger";
      break;
    case 5:
      $color="alert-dark";
      break;
    default:
      $color="alert-primary";
      break;
  }
}
if ($message) {
  echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
  echo $message;
  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
  echo '</div>';
}
?>        



<?php 
// This is for testing only, comment out for production
error_reporting(E_ALL & ~E_NOTICE);
?>

<?php
// This is the button section.  It will display certain additional buttons
// based upon access level and current task.  
if ($action=="DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" title="Go back to your search results" class="btn btn-success btn-sm btn-space" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("supportedit")) { ?>
          <button id="submit" name="submit" value="edit" title="Edit details about the ticket itself, including whether it is opened or closed" class="btn btn-sm btn-warning btn-space">Edit Ticket</button>
        <?php 
        }
      ?>
      <?php
        if (($isescalated == 0 && $this->registry->security->checkFunction("respondsupport")) || ($isescalated == 1 && $this->registry->security->checkFunction("respondescalated"))) { ?>
          <button id="submit" name="submit" value="response" title="Send a response email or text back to the user in response to their ticket" class="btn btn-sm btn-warning btn-space">Respond to User</button>
        <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("notatesupport")) { ?>
          <button id="submit" name="submit" value="notate" title="Add an internal note to this ticket that only staff can see" class="btn btn-sm btn-warning btn-space">Add a Note</button>
        <?php 
        }
      ?>
      <?php
        if ((!isset($_SESSION['fromcontroller']) && !isset($_SESSION['loginnum'])) && (($userstart == "" && $this->registry->security->checkFunction("supportassoc")) || ($userstart > 0 && $this->registry->security->checkFunction("supportreassoc")))) { ?>
          <button id="submit" name="submit" value="associate" title="Associate a User Account with this ticket in case the system did not or did not associate correctly" class="btn btn-sm btn-warning btn-space">Associate with User</button>
        <?php 
        }
      ?>
      <?php
        if ((!isset($_SESSION['fromcontroller']) && !isset($_SESSION['loginnum'])) && ($userstart >0 && $this->registry->security->checkFunction("usermgmt"))) { ?>
          <button id="submit" name="submit" value="gotouser" title="Go to the User's account screen" class="btn btn-sm btn-primary btn-space">Go to User Acct</button>
        <?php 
        }
      ?>
      <?php
        if ((!$messages || $messages & count($messages) < 2) && $this->registry->security->checkFunction("supportdelete")) { ?>
          <button id="submit" name="submit" value="delete" title="Permanently delete this ticket" class="btn btn-sm btn-danger btn-space" onclick="return confirm('Are you sure? This Ticket and all associated messages will be deleted permanently. This cannot be undone!')">Delete Ticket</button>
        <?php 
        }
      ?>
</div>
<?php
}
?>

<!-- Select Basic -->
<?php if (isset($subaction)) { ?> 
<input type="hidden" name="subaction" value="<?= $subaction ?>"> 
<?php } ?>
<h4 class="spacer-half underline">Ticket Information</h4>
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="convno"><abbr title="This is the assigned Ticket Number - it cannot be changed.">Ticket No.</abbr></label>  
    <div class="col-md-2">
      <input id="convno" name="seriesno" type="text" placeholder="" class="form-control input-md" value="<?= $convno ?>" disabled>
      <input id="convno" name="convno" type="hidden" value="<?= $convno ?>">
    </div>
  </div>

  <div class="row">
      <label class="col-md-2 control-label" for="convtype"><abbr title="What type of conversation is this?">Conv. Type</abbr></label>
      <div class="col-md-2">
      <input type="hidden" name="convtype" value="<?= $convtype ?>">
       <select id="convtype" name="convtype" class="form-select" disabled>
         <option value="1" <?php if ($convtype==1) { echo "selected"; }?>>EMAIL</option>
         <option value="0" <?php if ($convtype==0 && ($action !== "ADD")) { echo "selected"; }?>>TEXT MSG</option>
        </select>
      </div>
      <label class="col-md-2 control-label" for="convstart"><abbr title="The date and time this this ticket was started.">Ticket Started On</abbr></label>  
      <div class="col-md-2">
        <input id="convstart" name="convstart" type="text" maxlength="255" placeholder="" class="form-control input-md small-field" value="<?= $convstart ?>" disabled >
      </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="convsubject"><abbr title="The subject of the conversation">Subject</abbr></label>  
    <div class="col-md-6">
      <input type="hidden" name="wassubject" value="<?= $convsubject ?>">
      <input id="convsubject" name="convsubject" type="text" maxlength="255" placeholder="--No Subject Set--" class="form-control input-md" value="<?= $convsubject ?>" <?php if ($action == "DISPLAY" || $subaction) { echo "disabled"; } ?> >
    </div>
  </div>

  <?php
    if (!isset($firstname) || $firstname == "") { 
      if ($userphone != "") { $pphone = $this->registry->template->prettyPhone($userphone, "DASHES"); }
      $uname = "UNKNOWN (From: ".$pphone.$useremail.")"; 
      $un=false; 
    }
    else { $uname = $firstname." ".$lastname." (USER # ".$usernum.")"; $un=true;}
    if ($un == false) { $userstart = "NULL"; }
  ?>
  <div class="row">
    <label class="col-md-2 control-label" for="uname"><abbr title="The user who opened this ticket.">User Name</abbr></label>  
    <div class="col-md-6">
      <input type="hidden" name="userstart" value="<?= $userstart ?>">
      <input id="uname" name="uname" type="text" maxlength="255" placeholder="" class="form-control input-md" value="<?= $uname ?>" disabled>
    </div>
  </div>

  <?php if (($assignedto == "" && $this->registry->security->checkFunction("supportassign")) || ($assignedto > 0 && $this->registry->security->checkFunction("supportreassign"))) { $disassign = false; } else { $disassign = true; } ?>
  <div class="row">
    <label class="col-md-2 control-label" for="assignedto"><abbr title="The staff member this ticket was assigned to">Assigned To</abbr></label>  
    <div class="col-md-6">
       <input type="hidden" name="wasassigned" value="<?= $assignedto ?>">
       <select id="assignedto" name="assignedto" class="form-select" <?php if ($action == "DISPLAY" || $subaction || $disassign) { echo "disabled"; } ?>>
          <option value="NULL" <?php if ($assignedto=="" && ($action !== "ADD")) { echo "selected"; }?>>-- Unassigned --</option>
        <?php
        foreach ($assignees as $assignee) {
          ?>
          <option value = "<?= $assignee['usernum'] ?>" <?php if ($assignedto == $assignee['usernum']) { echo "selected"; }?>><?= $assignee['firstname'] ?> <?= $assignee['lastname'] ?> </option>
          <?php
        } ?>
      </select>
    </div>
  </div>  

  <div class="row">
    <label class="col-md-2 control-label" for="isescalated"><abbr title="Has this ticket been escalated to management?">Escalation Status</abbr></label>
    <div class="col-md-2">
      <input type="hidden" name="wasescalated" value="<?= $isescalated ?>">
      <select id="isescalated" name="isescalated" class="form-select" <?php if ($action == "DISPLAY" || $subaction) { echo "disabled"; } ?>>
        <option value="0" <?php if ($isescalated==0) { echo "selected"; }?>>Not Escalated</option>
        <option value="1" <?php if ($isescalated==1 && ($action !== "ADD")) { echo "selected"; }?>>ESCALATED</option>
      </select>
    </div>
    <label class="col-md-2 control-label" for="isclosed"><abbr title="What type of conversation is this?">Ticket Status</abbr></label>
    <div class="col-md-2">
      <input type="hidden" name="wasclosed" value="<?= $isclosed ?>">
     <select id="isclosed" name="isclosed" class="form-select" <?php if ($action == "DISPLAY" || $subaction) { echo "disabled"; } ?>>
       <option value="0" <?php if ($isclosed==0) { echo "selected"; }?>>OPEN</option>
       <option value="1" <?php if ($isclosed==1 && ($action !== "ADD")) { echo "selected"; }?>>CLOSED</option>
      </select>
    </div>
  </div>

  <div class="row">
      <label class="col-md-2 control-label" for="lastaction"><abbr title="The date and time the last action was taken on this ticket.">Last Action On</abbr></label>  
      <div class="col-md-2">
        <input id="lastaction" name="lastaction" type="text" maxlength="255" placeholder="" class="form-control input-md small-field" value="<?= $lastaction ?>" disabled >
      </div>
      <label class="col-md-2 control-label" for="lastactionparty"><abbr title="What party took the last action on this ticket?">Last Action By</abbr></label>  
      <div class="col-md-2">
        <select id="lastactionparty" name="convtype" class="form-select" disabled>
          <option value="0" <?php if ($lastactionparty==0) { echo "selected"; }?>>USER</option>
          <option value="1" <?php if ($lastactionparty==1 && ($action !== "ADD")) { echo "selected"; }?>>STAFF</option>
        </select>
      </div>
  </div>

</div> <!--form-group-->
<div class="spacer"></div>

<!-- ******************************************************* -->
<!--                  MESSAGES START HERE                    -->
<!-- ******************************************************* -->

<?php
$x=0;
if ($messages) { 
  foreach ($messages as $message) { 
    if ($message['messagetype'] ==2 && ($this->registry->security->checkFunction("notatesupport") == false)) {
      continue;
    }
    $x++;
    if ($x == 1) {$header = "Initial Request ".$incoming; $rname = "Request From"; }
    elseif ($message['messagetype'] == 2) {$header = "Internal Note"; $rname = "Note From";}
    elseif ($message['isincoming']==0) {$header = "Staff Response (Outgoing)".$incoming; $rname = "Response From";}
    elseif ($message['isincoming']==1) {$header = "User Response (Incoming)".$incoming; $rname = "Response From";}
    ?>

    <h4 class="spacer-half underline"><?= $header ?></h4>

    <div class="form-group"> 
      <div class="row">
        <label class="col-md-2 control-label" for="convno"><abbr title="Date and time of this interaction">Date &amp; Time</abbr></label>  
        <div class="col-md-2">
          <input id="messagetime" name="messagetime" type="text" placeholder="" class="form-control input-md smaller-field bold-input" value="<?= $message['messagetime'] ?>" disabled>
        </div>
      </div>

      <?php
      $iname = $this->registry->security->getUserName($message['sendernum']);
      if ($iname) {$fullname = $iname['firstname']." ".$iname['lastname']; } else {$fullname = "UNKNOWN"; }
      ?>
      <div class="row">
      <label class="col-md-2 control-label" for="fullname"><abbr title="The user who created this interaction"></abbr><?= $rname ?></label>  
        <div class="col-md-6">
          <input id="fullname" name="fullname" type="text" maxlength="255" placeholder="" class="form-control input-md" value="<?= $fullname ?>" disabled>
        </div>
      </div>

      <?php
      if ($message['messagetype']==2) {$body = $message['internalnotes']; } else { $body = $message['messagebody']; }
      ?>
      <div class="row">
        <label class="col-md-2 control-label" for="message"><abbr title="The message received or sent in this particular interaction.">Message</abbr></label>  
        <div class="col-md-6">
          <textarea id="message" name="message" rows="5" placeholder="--No Message Detected--" class="form-control input-md" disabled><?= $body ?></textarea>
        </div>
      </div>


  <div class="spacer-half"></div>


  <?php } ?>
<?php } ?>


<!-- ******************************************************* -->
<!--                   RESPONSE STARTS HERE                  -->
<!-- ******************************************************* -->

<?php
if ($isescalated == 1) { $funcneed = "respondescalated"; } else { $funcneed = "respondsupport"; }
if ($subaction == "RESPOND" && $this->registry->security->checkFunction($funcneed)) { ?>
  <h4 class="underline bump-up">Your Response</h4>
  <div class="form-group"> 
<?php
  if ($convtype == 0) { ?>
    <div class="row">
      <label class="col-md-2 control-label" for="messagebody"><abbr title="Your return text for the user."></abbr>Message (SMS)</label>  
      <div class="col-md-5">
        <textarea id="messagebody" name="messagebody" rows="4" maxlength="160" placeholder="Enter Your Reply - Max 160 Chars" class="form-control input-md" onkeyup="textCounter(this, 'counter', 160);" required></textarea>
      </div>
      <div class="col-md-1">
        <input disabled maxlength="3" size="3" value="160" id="counter">
    </div>
  </div>
  <?php } 
  else { ?>
    <div class="row">
      <label class="col-md-2 control-label" for="messagebody"><abbr title="Your return text for the user."></abbr>Message (Email)</label>  
      <div class="col-md-6">
        <textarea id="messagebody" name="messagebody" rows="5" placeholder="Enter Your Reply Email Message" class="form-control input-md" required></textarea>
      </div>
  </div>
<?php }
} ?>

<!-- ******************************************************* -->
<!--                   NOTATE STARTS HERE                  -->
<!-- ******************************************************* -->
<?php
if ($subaction == "NOTATE" && $this->registry->security->checkFunction("notatesupport")) { ?>
  <h4 class="underline bump-up">Internal Note</h4>
  <div class="form-group"> 
    <div class="row">
      <label class="col-md-2 control-label" for="messagebody"><abbr title="Your internal message for staff eyes only"></abbr>Your Notes</label>  
      <div class="col-md-6">
        <textarea id="messagebody" name="messagebody" rows="5" placeholder="Enter Your Internal Note (can only be seen by staff)" class="form-control input-md" required></textarea>
      </div>
  </div>
<?php } ?>


<!-- Bottom Buttons -->
<?php if ($action !== "DISPLAY") {
  ?>
<div class="form-group">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-4">
      <?php if ($action == "ADD") {
              ?><button id="submit" name="submit" value="add" class="btn btn-primary btn-space">Add New <?= $thingname ?></button>
                <a class="btn btn-primary btn-space" href="<?= BASE_URL ?>" role="button">CANCEL</a>
              <?php 
            }
            elseif ($subaction == false) {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning btn-space" onClick="showWait()">Update <?= $thingname ?></button>
              <button id="submit" name="submit" value="display" class="btn btn-primary btn-space">CANCEL</button>
              <?php
            }
            elseif ($subaction == "RESPOND") {
              ?><button id="submit" name="submit" value="sendresponse" class="btn btn-warning btn-space" onClick="showWait()">Submit Message</button>
              <button id="submit" name="submit" value="display" class="btn btn-primary btn-space" onClick="unlockCancel()">CANCEL</button>
              <?php
            }
            elseif ($subaction == "NOTATE") {
              ?><button id="submit" name="submit" value="sendnote" class="btn btn-warning btn-space">Submit Notes</button>
              <button id="submit" name="submit" value="display" class="btn btn-primary btn-space" onClick="unlockCancel()">CANCEL</button>
              <?php
            }?>
    </div>
</div>

<?php 
}
?>
<?php

if ($action=="DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" title="Go back to your search results" class="btn btn-success btn-sm btn-space" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("supportedit")) { ?>
          <button id="submit" name="submit" value="edit" title="Edit details about the ticket itself, including whether it is opened or closed" class="btn btn-sm btn-warning btn-space">Edit Ticket</button>
        <?php 
        }
      ?>
      <?php
        if (($isescalated == 0 && $this->registry->security->checkFunction("respondsupport")) || ($isescalated == 1 && $this->registry->security->checkFunction("respondescalated"))) { ?>
          <button id="submit" name="submit" value="response" title="Send a response email or text back to the user in response to their ticket" class="btn btn-sm btn-warning btn-space">Respond to User</button>
        <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("notatesupport")) { ?>
          <button id="submit" name="submit" value="notate" title="Add an internal note to this ticket that only staff can see" class="btn btn-sm btn-warning btn-space">Add a Note</button>
        <?php 
        }
      ?>
      <?php
        if ((!isset($_SESSION['fromcontroller']) && !isset($_SESSION['loginnum'])) && (($userstart == "" && $this->registry->security->checkFunction("supportassoc")) || ($userstart > 0 && $this->registry->security->checkFunction("supportreassoc")))) { ?>
          <button id="submit" name="submit" value="associate" title="Associate a User Account with this ticket in case the system did not or did not associate correctly" class="btn btn-sm btn-warning btn-space">Associate with User</button>
        <?php 
        }
      ?>
      <?php
        if ((!isset($_SESSION['fromcontroller']) && !isset($_SESSION['loginnum'])) && ($userstart >0 && $this->registry->security->checkFunction("usermgmt"))) { ?>
          <button id="submit" name="submit" value="gotouser" title="Go to the User's account screen" class="btn btn-sm btn-primary btn-space">Go to User Acct</button>
        <?php 
        }
      ?>
      <?php
        if ((!$messages || $messages & count($messages) < 2) && $this->registry->security->checkFunction("supportdelete")) { ?>
          <button id="submit" name="submit" value="delete" title="Permanently delete this ticket" class="btn btn-sm btn-danger btn-space" onclick="return confirm('Are you sure? This Ticket and all associated messages will be deleted permanently. This cannot be undone!')">Delete Ticket</button>
        <?php 
        }
      ?>
</div>
<?php
}
?>
<!-- End Bottom Buttons -->
</fieldset>
</form>
<a id="back-to-bottom" href="#" class="btn btn-secondary btn-lg back-to-bottom" role="button"><i class="fas fa-chevron-down"></i></a>
<a id="back-to-top" href="#" class="btn btn-secondary btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>


<script>
function showWait() {
  $("#supportmaster").validate({
     submitHandler: function(form){
      myApp.showPleaseWait();
      form.submit();
     }
  });
}
</script>


<script>
var myApp;
myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Please Wait - Updating...</h5></div><div class="modal-body" align="center"><img src="css/loading.gif" width="75%"></div><div class="modal-footer"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };
})();
</script>


<script>
function unlockCancel() {
  document.getElementById("messagebody").required = false;
  return true;
}
</script>


<script>
function textCounter(field,field2,maxlimit)
{
 var countfield = document.getElementById(field2);
 if ( field.value.length > maxlimit ) {
  field.value = field.value.substring( 0, maxlimit );
  return false;
 } else {
  countfield.value = maxlimit - field.value.length;
 }
}
</script>

<script>
$(document).ready(function(){
  $(window).scroll(function () {
      if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 400);
      return false;
    });
});
</script>


<script>
$(document).ready(function(){
    $('#back-to-bottom').fadeIn();
  $(window).scroll(function () {
      if ($(this).scrollTop() == $(document).height()-$(window).height()) {
        $('#back-to-bottom').fadeOut();
      } else {
        $('#back-to-bottom').fadeIn();
      }
    });
    // scroll body to 0px on click
    $('#back-to-bottom').click(function () {
      $('body,html').animate({
        scrollTop: $(document).height()-$(window).height()}, 
        1400, 
        "easeOutQuint");
      return false;
    });
});
</script>




<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>
<div class="panel panel-primary">

  <div class="panel-heading pb-4">

      <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    /* LOGIC FOR RETURN MESSAGES */
    if (isset($_SESSION['returncode'])) {
        if ($_SESSION['returncode'] < 4) { 
            $color = "alert-success"; 
        } 
        elseif ( $_SESSION['returncode'] == 17 )  { 
            $color = "alert-warning"; 
        } 
        else { 
            $color = "alert-danger"; 
        }
        echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
        
        switch ($_SESSION['returncode']) {
            case 3:
                echo "SUCCESS: Record Deleted Successfully.";
                break;
            case 5:
                echo "ERROR: Record Delete Failed. Try again, or contact your administrator.";
                break;
            default:
                echo "An unknown error has occurred.  Please contact your administrator.";
        }
        echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
        echo '</div>';
    }
    else {
      $active = 1;
      $searchall = 1;
    }
    unset($_SESSION['returncode']);
                   

    // And now, let's set our page on-screen header
    $header = $thingname." Record Search Results";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
    ?>
    <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>

  </div>

</div>

  <form action="<?= BASE_URL ?>supportmaster" method="post" name="supportmaster" id="client_form">
    <input type="hidden" name="searchform" value="searchform" />
    <table class="table table-bordered table-hover table-responsive-lg" id="myTable2">
      <thead>
        <tr>
            <th onclick="sortTable(0)"><a href="#">Ticket #</a></th>
            <th onclick="sortTable(1)"><a href="#">Type</a></th>
            <th onclick="sortTable(3)"><a href="#">Subject</a></th>
            <th onclick="sortTable(4)"><a href="#">Last Active</th>
            <th onclick="sortTable(5)"><a href="#">User Being Helped</th>
            <th onclick="sortTable(6)"><a href="#">Assigned To</th>
            <th onclick="sortTable(7)"><a href="#">Status</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['results'])) {
      $rows = $values['results'];  
    }
    foreach ($rows as $row) {
      if ($row['isclosed'] == 1) {$astatus = "Closed"; } else {$astatus = "Open"; }
      if ($row['convtype'] == 0) { $ctype = "Text Msg"; } else { $ctype = "Email"; }
      if ($row['userstart'] == "") { $uname = "-- Unmatched --"; } else { $uname = $row['firstname']." ".$row['lastname']; }
      if ($row['assignedto'] == "") {
        $aname = "Unassigned";
      }
      else {
        $auser = $this->registry->security->getUserName($row['assignedto']);
        if ($auser) {
          $aname = $auser['firstname']." ".$auser['lastname'];
        }
        else {
          $aname = "Unassigned";
        }
      }
      ?>
      <tr>
            <td><?= $row['convno'] ?></td>
            <td><?= $ctype ?></td>
            <td><?= $row['convsubject'] ?></td>
            <td><?= $row ['lastaction'] ?></td>
            <td><?= $uname ?></td>
            <td><?= $aname ?></td>
            <td><?= $astatus ?></td>
            <td><button id="submit" name="submit" class="btn btn-primary" value="select-<?= $row['convno'] ?>">SELECT</button></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <?php 
          if (isset($_SESSION['returnto']) && isset($_SESSION['loginnum'])) { ?>
            <button id="submit" name="submit" value="backtouser" class="btn btn-primary btn-space">BACK TO USER</button>
          <?php }
          else { ?>
            <button id="submit" name="submit" value="research" class="btn btn-primary btn-space">BACK TO SEARCH</button>
         <?php } ?>
        <a class="btn btn-primary btn-space" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             






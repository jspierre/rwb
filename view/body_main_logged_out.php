<link href="<?= BASE_URL ?>css/login.css" rel="stylesheet">
 
  <div class = "row">
    <div class="col-md-6 well">
      <h1>Welcome!</h1>
      <p>Welcome to <em><?= $sitename ?></em>!  We're so happy you're here!</p>
      <p>Log in with your credentials to the right,
    <?php 
        if ($this->registry->config['createself']) { ?> or create a free account by clicking the Create an Account Link beneath the login area.</p> <?php } else { ?> please. <?php }
        if ($this->registry->config['resetpw']) { ?> <p>Forgot your password?  No problem, just click the link below the login box that says "I forgot my password"!</p> <?php }       
    ?>
    </div>
    <div class="col-md-1 col-md-1"></div>
    <div class = "col-md-4">
      <div class = "row">
        <div class="list-group">
          <form class="form-signin" action="<?= BASE_URL ?>login" onsubmit="return validateForm()" method="post" name="login" id="login_form">
            <div class="text-center mb-4">
              <h1 class="h3 mb-3 font-weight-normal">Log In To Your Account</h1>
              <p>Enter your credentials to log in.</p>
            <?php
              if ($this->registry->config['createself']) { ?> <p>Don't have an account? <a href="signup">Create one now!</a></p> <?php }
            ?>
              <hr />
            </div>
            <div class="form-label-group">
<?php
              if($this->registry->config['loginwithemail']) { $prompt="Enter your email address"; $type="email"; $label="Email Address";} else  { $prompt="Enter your user name"; $type="text"; $label="Username";}
?>
              <input type="<?= $type ?>" id="inputEmail" name="user" class="form-control" placeholder="<?= $label ?>" required autofocus>
              <label for="inputEmail">&nbsp;<?= $label ?></label>
            </div>

            <div class="form-label-group">
              <input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Password" required>
              <label for="inputPassword">&nbsp;Password</label>
            </div>       
<?php     
        if ($this->registry->config['resetpw']) {
        ?>
            <div class="form-label-group">
              <p class="text-center"><a href ="pwrst">I forgot my password.</a></p>
            </div>
<?php
        }
?>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <p></p>
            <p><small>IMPORTANT NOTE: By logging in to this system, you agree to be bound by our <a href="tandc">terms of use</a>.</small></p>
          </form>
        </div>
     </div>
   </div>
  </div>
 
</div>             






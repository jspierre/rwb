 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script>
  $(document).ready(function(){
    // Make sure mobile number is only numbers
    $(".numbersonly").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl+A, Command+A
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
           // Allow: home, end, left, right, down, up
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
    });
    });
</script>

<form class="form-horizontal" action="<?= BASE_URL ?>bgtaskmgmt" method="post" name="edittask" id="edittask">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  //var_dump($_SESSION);
  //var_dump($_POST);
  //exit();
  //var_dump($values);
  
  

  // Determine what type of thing we are doing - new, display, or edit?
  if (isset($values['actiontype'])) {
    $actiontype = $values['actiontype'];
    extract($values['task']);
  }
  else {
    $actiontype = "NEW";
  }
 

  if ($actiontype == "EDIT") {
      echo '<h3 class="panel-title">Update Existing Task</h3>'; 
    }
    else {
      echo '<h3 class="panel-title">New Background Task Entry</h3>';
    }
?>
</div>
<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "New Task Added. Please check settings to make sure all is correct!";
                        break;
                    case 3:
                        echo "Background Task Updated.";
                        break;
                    case 5:
                        $tasknum = $values['bgtasknum'];
                        $link = '<a href="'.BASE_URL.'bgtaskmgmt/'.$tasknum.'">';
                        echo "ERROR: Task Already Exists - See ".$link."Task # $tasknum</a>";
                        extract($values['tempdata']);
                        break;
                    case 6:
                        $bgtasknum = $values['error']['bgtasknum'];
                        echo "ERROR: Invalid Module Number: $bgtasknum.  Add new user or try again.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }                    
?>        


<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($_SESSION['bgtasknum'])) {
  $bgtasknum = $_SESSION['bgtasknum'];
  $taskindex = $bgtasknum;
  echo '<input type="hidden" name="bgtasknum" value ='.$bgtasknum.' />';
}
?>


<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="moduleindex">Task #</label>  
    <div class="col-md-2">
      <input id="bgtasknum" name="bgtasknum" type="text" placeholder="" class="form-control input-md" value="<?= $bgtasknum?>" disabled >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="modulename">Module Name</label>  
    <div class="col-md-2">
      <input id="modulename" name="modulename" type="text" placeholder="" class="form-control input-md" value="<?= $modulename?>" autofocus required >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="friendlyname">Task Description</label>  
    <div class="col-md-5">
      <input id="friendlyname" name="friendlyname" type="text" placeholder="" class="form-control input-md" value="<?= $friendlyname ?>" required >
    </div>
  </div>

    <div class="row">
    <label class="col-md-2 control-label" for="minutes">Run Every (Minutes)</label>  
    <div class="col-md-2">
      <input id="minutes" type="text" maxlength="5" name="minutes" class="form-control input-md numbersonly" <?php if ($actiontype == "DISPLAY") { echo 'value="'.$minutes.'" disabled'; } else  { echo 'value="'.$minutes.'" required'; } ?>>
    </div>
    <label class="col-md-1 control-label" for="isactive">Task Active?</label>
    <div class="col-md-2">
      <select id="isactive" name="isactive" class="form-select" >
       <option value="0" <?php if ($isactive==0  && $actiontype != "NEW" ) { echo "selected"; }?>>NO</option>
       <option value="1" <?php if ($isactive==1 || $actiontype == "NEW") { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>
</div>


<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8 btn-space">
      <?php if ($actiontype == "NEW") {
              unset($_SESSION['bgtasknum']);
              ?><button id="submit" name="submit" value="savenew" class="btn btn-primary">ADD NEW TASK</button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>bgtaskmgmt" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">UPDATE TASK</button>
              <button id="submit" name="submit" value="bgtaskmgmt" class="btn btn-primary">CANCEL</button>
              <?php
              if ($this->registry->security->checkFunction("deletetask")) { ?>
                <button id="submit" name="submit" value="delete" class="btn btn-danger" onclick="return confirm('Are you sure? This task will be deleted and will NEVER run again.')">DELETE TASK</button> 
                <?php 
              } 
            }
            ?>
      
    </div>
  </div>
</div>

</fieldset>
</form>



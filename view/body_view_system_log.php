
<form class="form-horizontal" action="<?= BASE_URL ?>viewsyslog" method="post" name="viewsyslog" id="view_system_log">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">SYSTEM LOG</h3>
  </div>

<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible col-md-10" role="alert">';
                       
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "Log has been archived successfully.";
                        break;
                    default:
                        echo "Log archive FAILED.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }
            
          
?>        

<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>


<!-- Text input-->
<div class="form-group">
  <div class="row">
    <div class="col-md-12">
      <?php extract($values); ?>
      <textarea rows="25" cols = "150" name="log" id="log" style="font-size:12px;" disabled><?= $systemlog ?></textarea>    
    </div>
  </div>
  <div class="row"><p></p></div>
  <div class="row">
    <div class="col-md-4">
      <?php
        if ($values['action'] == "DISPLAY") { ?>
        <button id="submit" name="submit" value="cancel" class="btn btn-primary">GO BACK</button>
        <button value="reload" class="btn btn-primary" onClick="window.location.reload();">REFRESH</button>
      <?php }
        if ($this->registry->security->checkfunction("archivelog")) { ?>
        <button id="submit" name="submit" value="archive" class="btn btn-danger" onclick="return confirm('Are you sure? This will move the active log into an archive file that can only be read through the file system.')">ARCHIVE LOG</button>
      <?php }
      ?>
    </div>
  </div>
</div>
</fieldset>
</form>

<script>
  var textarea = document.getElementById('log');
  textarea.scrollTop = textarea.scrollHeight;
</script>
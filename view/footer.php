 

			</section>  <!-- Closed from header -->
    	</main> <!-- Closed from header -->

 	</body>

	<!-- Script to stop the back button from being used -->
	<script type = "text/javascript" > 
		history.pushState(null, null, location.href); 
		history.back(); 
		history.forward(); 
		window.onpopstate = function () { history.go(1); }; 
	</script>

	<!-- Script and code to force logout on timeout -->
  	<script src="<?= BASE_URL ?>js/timeout-dialog/js/timeout-dialog.js"></script>
	<?php 
		if ($this->registry->isLoggedIn()) {
	  	$logouturl = $this->registry->config['url'];
	  	$keepalive = $this->registry->config['url']."/keepalive";
	  	$timeout = ($this->registry->config['sessiontime'] * 60) + 1;
  	?>
	<script>
		$.timeoutDialog({timeout: <?= $timeout ?>, countdown: 90, keep_alive_url: '<?= $keepalive ?>', logout_redirect_url: '<?= $logouturl ?>', restart_on_yes: true});
	</script>

<!-- Close everything up and end the file -->
<?php
}
?>

</html>
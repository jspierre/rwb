<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">
      <h3 class="panel-title">Background Tasks: Add or Edit a Task</h3>
      <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>
  </div>

  <?php
  if (isset($_SESSION['returncode'])) {
    if ($_SESSION['returncode'] < 4) { $color = "alert-success"; } else { $color = "alert-danger"; }
      echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
            
      switch ($_SESSION['returncode']) {
        case 2:
          echo "SUCCESS: Task Deleted as requested.";
          Break;
        case 5:
          echo "ERROR: Task NOT Deleted. Please try again or contact administrator."; 
          Break;
        default:
          echo "An unknown error has occurred.  Please contact your administrator.";
      }
      echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      echo '</div>';
  }                   
?>  

</div>

  <form action="<?= BASE_URL ?>bgtaskmgmt" method="post" name="editmodule" id="editmodule">
    <div class="form-group">
      <div class="row">
        <div class="col-md-8 btn-space">
          <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW TASK</button>
          <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
        </div>
      </div>
    </div>
    <table class="table table-bordered table-hover table-responsive" id="myTable2">
      <thead>
        <tr>
          <th onclick="sortTable(0)"><a href="#">Task #</a></th>
          <th onclick="sortTable(1)"><a href="#">Module Name</a></th>
          <th onclick="sortTable(2)"><a href="#">Description</a></th>
          <th onclick="sortTable(3)"><a href="#">Run Every</a></th>
          <th oncilck="sortTable(4)"><a href="#">Last Run Time</a></th>
          <th onclick="sortTable(5)"><a href="#">Active</a></th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['tasks'])) {
      $rows = $values['tasks'];  
    }
    foreach ($rows as $row) {
      if ($row['isactive'] == 1) {$astatus = "YES";} else {$astatus="NO";}
      $runevery = $row['minutes']." Min.";
      ?>
      <tr>
        <td><?= $row['bgtasknum'] ?></td>
        <td><?= $row['modulename'] ?></td>
        <td><?= $row['friendlyname'] ?></td>
        <td><?= $runevery ?></td>
        <td><?= $row['lastrun'] ?></td>
        <td><?= $astatus ?></td>
        <td><button id="submit" name="submit" class="btn btn-primary" value="BGX<?= $row['bgtasknum'] ?>">EDIT</button></td>
      </tr>
    <?php
    }
  ?>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW TASK</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             






<link href="<?= BASE_URL ?>css/signup.css" rel="stylesheet">
<form class="form-signin" action="<?= BASE_URL ?>login" onsubmit="return validateForm()" method="post" name="login" id="login_form">

<div class="text-center mb-4">
	
	<h1 class="h3 mb-3 font-weight-normal">Log In to Your Account</h1>
	<p>Enter Your Credentials to Log In</p>
<?php			
	if ($this->registry->config['createself']) {
	?>	
	<p>Don't have an account yet?  <a href="signup">Create one here!</a></p>
	<?php
	}
?>
	<hr />
</div>

		
	<?php
				/* LOGIC FOR RETURN MESSAGES */
				if (isset($_SESSION['returncode'])) {
					if ($_SESSION['returncode'] < 3) { 
						$color = "alert-success"; 
					} 
					elseif ( $_SESSION['returncode'] == 17 )  { 
						$color = "alert-warning"; 
					} 
					else { 
						$color = "alert-danger"; 
					}
					
	 				echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
	                
					switch ($_SESSION['returncode']) {
						case 2:
							echo "Logout Successful.  Goodbye!";
							break;
						case 3:
							echo "ERROR: Invalid Username or Password.";
							break;
						case 4:
							echo "ERROR: Invalid Authentication Code.";
							break;
						case 5:
							echo "ERROR: Your username must be a valid email address.";
							break;
						case 7:
							echo "Your account is blocked.  Please contact your administrator.";
							break;
						case 6:
							echo "Your account has been locked.  Please contact your administrator.";
							break;
						case 8:
							echo "Your Department has been deactivated.  Access Denied.  Contact your administrator.";
							break;
						case 9:
							echo "Your Access Group has been deactivated.  Access Denied.  Contact administration.";
							break;
						case 17:
							echo "Your session has expired.  Please log in again to continue.";
							break;
						default:
							echo "An unknown error has occurred.  Please contact your administrator.";
					}
					echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
					echo '</div>';
				}
				
				/* GET LOGIN TYPE */
				if($this->registry->config['loginwithemail']) { $prompt="Enter your email address"; $type="email"; $label="Email Address";} else  { $prompt="Enter your user name"; $type="text"; $label="Username";}
	?>			
				<div class="form-label-group">
					<input type="<?= $type ?>" id="user" name="user" class="form-control" placeholder="<?= $prompt ?>" required autofocus>
					<label for="user"><?= $label ?></label>
				</div>

				<div class="form-label-group">
					<input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Password" required>
					<label for="inputPassword">&nbsp;Password</label>
				</div>
<?php			
				if ($this->registry->config['resetpw']) {
				?>
				<div class="form-label-group">
					<p class="text-center"><a href ="pwrst">I forgot my password.</a></p>
				</div> 
				<?php
				}
?>
				
      			<button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Sign In</button>
      			<p></p>
      			<p><small>IMPORTANT NOTE: By signing in or using this system, you agree to be bound by our <a href="tandc">terms of use</a>. Do not log in if you do not agree to these terms.</small></p>

    		</form>


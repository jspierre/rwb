
<form class="form-horizontal" action="<?= BASE_URL ?>fncmgmt" method="post" name="editfunction" id="editfunction">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  //var_dump($_SESSION);
  //var_dump($_POST);
  //exit();
  //var_dump($values);
  
  if (isset($values['modules'])) {
    $modules = $values['modules'];
  }
 
  // Determine what type of thing we are doing - new, display, or edit?
  if (isset($values['actiontype'])) {
    $actiontype = $values['actiontype'];
    extract($values['function']);
  }
  else {
    $actiontype = "NEW";
  }

  if ($actiontype == "EDIT") {
      echo '<h3 class="panel-title">Update Existing Function</h3>'; 
    }
    else {
      echo '<h3 class="panel-title">New Function Entry Form</h3>';
    }
?>
</div>
<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "New Function Added. Please check settings to make sure all is correct!";
                        break;
                    case 3:
                        echo "Function Updated.";
                        break;
                    case 5:
                        $dupfunc = $values['funcnum'];
                        $link = '<a href="'.BASE_URL.'fncmgmt/'.$dupfunc.'">';
                        echo "ERROR: Function Already Exists - See ".$link."Function # $dupfunc</a>";
                        extract($values['tempdata']);
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }                    
?>        


<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($_SESSION['funcnum']) && $actiontype != "NEW") {
  $funcnum = $_SESSION['funcnum'];
  echo '<input type="hidden" name="funcnum" value ='.$funcnum.' />';
}
?>


<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="funcnum">Function #</label>  
    <div class="col-md-2">
      <input id="funcnum" name="funcnum" type="text" placeholder="" class="form-control input-md" value="<?= $funcnum?>" disabled >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="modulenum">For Module</label>
    <div class="col-md-2">
      <select id="modulenum" name="modulenum" class="form-select" >
          <?php
            foreach ($modules as $module) {
              if ($module['moduleindex'] == $modulenum) {
                echo '<option value="'.$module['moduleindex'].'" title = "'.$module['modulename'].'" selected>'.$module['controller'].'</option>';
              }
              else {
                echo '<option value="'.$module['moduleindex'].'" title = "'.$module['modulename'].'">'.$module['controller'].'</option>';
              }
            }
          ?>
      </select>
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="funcname">Function Name</label>  
    <div class="col-md-2">
      <input id="funcname" name="funcname" type="text" placeholder="" class="form-control input-md" value="<?= $funcname?>" required >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="funcdescr">Function Allows</label>  
    <div class="col-md-5">
      <input id="funcdescr" name="funcdescr" type="text" placeholder="" class="form-control input-md" value="<?= $funcdescr ?>"  required>
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="functionactive">Function Active?</label>
    <div class="col-md-2">
      <select id="functionactive" name="functionactive" class="form-select" >
        <option value="1" <?php if ($functionactive==1) { echo "selected"; }?>>YES</option>
        <option value="0" <?php if ($functionactive==0 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>NO</option>
      </select>
    </div>
  </div>

   <div class="row">
    <label class="col-md-2 control-label" for="allowall">Allow Everyone?</label>
    <div class="col-md-2">
      <select id="allowall" name="allowall" class="form-select" >
        <option value="0" <?php if ($allowall==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($allowall==1 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>
</div>




<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8 btn-space">
      <?php if ($actiontype == "NEW") {
              unset($_SESSION['modulenum']);
              ?><button id="submit" name="submit" value="savenew" class="btn btn-primary">ADD NEW FUNCTION</button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>fncmgmt" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">UPDATE FUNCTION</button>
              <button id="submit" name="submit" value="funcmgmt" class="btn btn-primary">CANCEL</button>
              <?php
              if ($this->registry->security->checkFunction("deletefunction")) { ?>
                <button id="submit" name="submit" value="delete" class="btn btn-danger" onclick="return confirm('Are you sure? This function will be deleted and all access allowed by this funtion will IMMEDIATELY CEASE.')">DELETE FUNCTION</button> 
                <?php 
              } 
            }
            ?>
      
    </div>
  </div>
</div>

</fieldset>
</form>



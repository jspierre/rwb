<form class="form-signin" action="<?= BASE_URL ?>pwchg" onsubmit="return validateForm()" method="post" name="pwchg" id="pwchg_form">

<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
	<div class="panel-heading">
	    <h3 class="panel-title">Change Your Password</h3>
	</div>
<div class="panel-body">
<?php
			/* LOGIC FOR RETURN MESSAGES */
			if (isset($_SESSION['returncode'])) {
				if ($_SESSION['returncode'] == 15) { $color = "alert-success"; } else { $color = "alert-danger"; }
				echo '<div class="alert '.$color.' alert-dismissible" role="alert">';

				switch ($_SESSION['returncode']) {
					case 10:
						echo "Your password has expired.  You must change it now.";
						break;
					case 11:
						echo "ERROR: Old Password is incorrect.";
						break;
					case 12:
						echo "ERROR: Password may not be blank. All fields must be filled in.";
						break;
					case 13:
						echo "ERROR: Password must be at least ".$this->input['pwlength']." characters in length.";
						break;
					case 14:
						echo "ERROR: Your new passwords do not match.  Try again.";
						break;
					case 15:
						echo "Password change successful!";
						break;
					case 16:
						echo "ERROR: New password must be different from old password.";
						break;
					case 17:
						echo "ERROR: You must agree to the Terms of Use to continue. See checkbox below.";
						break;
					default:
						echo "An unknown error has occurred.  Please contact your administrator.";
				}
				echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
				echo '</div>';
			}
	if ((isset($_SESSION['returncode']) && $_SESSION['returncode']<>15) || (!isset($_SESSION['returncode']))) {
?>			
				<div class="form-group">
					<div class="row">
						<label class="col-md-2 control-label" for="oldpass">Old Password</label>
						<div class="col-md-4">
							<input type="password" name="oldpass" class="form-control" placeholder="Enter your old Password" required="" autofocus>
						</div>
					</div>
					<div class="row">
						<label class="col-md-2 control-label" for="oldpass">New Password</label>
						<div class="col-md-4">
							<input type="password" name="newpass1" class="form-control" placeholder="Enter your new Password" required="">
						</div>
					</div>
					<div class="row">
						<label class="col-md-2 control-label" for="oldpass">Repeat Password</label>
						<div class="col-md-4">
							<input type="password" name="newpass2" class="form-control" placeholder="Enter your new Password again" required="">
						</div>
					</div>
				</div>
				<div class="checkbox mb-3">         
			      <label>
			        <input type="hidden" name="tcagree" value="0" />           
			        <input type="checkbox" name="tcagree" value="1" required> I have read and agree to the <a href="tandc" target="_blank">terms of use</a>.
			      </label>       
			    </div>
			<button class="col-md-2 btn btn-primary" type="submit">Change Password</button>
<?php
	}
	else {
?>
	</form>
	<form action="<?= $this->input['url'] ?>" method="POST">
		<button class="col-md-2 btn btn-primary" type="submit">Click here to continue</button>
		
<?php			
		}
?>
		</div>
	</form>
</div> <!-- /container -->

<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">

      <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    // And now, let's set our page on-screen header
    $header = $thingname." Record Search Results";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
    ?>
    <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>

  </div>

</div>

  <form action="<?= BASE_URL ?>bans" method="post" name="bans" id="client_form">
    <input type="hidden" name="searchform" value="searchform" />
    <table class="table table-bordered table-hover table-responsive" id="myTable2">
      <thead>
        <tr>
            <th onclick="sortTable(0)"><a href="#">Login Time</a></th>
            <th onclick="sortTable(1)"><a href="#">User Name</a></th>
            <th onclick="sortTable(2)"><a href="#">First Name</a></th>
            <th onclick="sortTable(3)"><a href="#">Last Name</a></th>
            <th onclick="sortTable(4)"><a href="#">IP Address</a></th>
            <!-- <th>Actions</th> -->
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['results'])) {
      $rows = $values['results']; 
    }
    if ($rows) { foreach ($rows as $row) {
      if ($row['ipaddress'] == "::1" || $row['ipaddress'] == "127.0.0.1") { $ipaddress = "Local Computer"; } else {$ipaddress = $row['ipaddress']; }
      //else { 
      //  $rdns = gethostbyaddr($row['ipaddress']); 
      //  if (!$rdns) { $rdns = "Not Available"; }
      //}
      ?>
      <tr>
            <td><?= $row['loggedintime'] ?></td>
            <td><?= $row['username'] ?></td>
            <td><?= $row['firstname'] ?></td>
            <td><?= $row['lastname'] ?></td>
            <td><?= $ipaddress ?></td>
            <!-- <td><button id="submit" name="submit" class="btn btn-primary" value="select-<?= $row['bannumber'] ?>">UNBAN</button></td> -->
      </tr>
      <?php
        }
      }
      else {
      ?>
        <tr>
            <td>No Records Found.</td>
            <td></td>
            <td></td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>

      </div>
    </div>
  </div>

</form>
</div>             






<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">

      <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    // And now, let's set our page on-screen header
    $header = $thingname." Record Search Results";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
    ?>
    <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>

  </div>

</div>

  <form action="<?= BASE_URL ?>bans" method="post" name="bans" id="client_form">
    <input type="hidden" name="searchform" value="searchform" />
    <table class="table table-bordered table-hover table-responsive" id="myTable2">
      <thead>
        <tr>
            <th onclick="sortTable(0)"><a href="#">IP Address</a></th>
            <th onclick="sortTable(1)"><a href="#">Reverse DNS</a></th>
            <th onclick="sortTable(2)"><a href="#">Banned On</a></th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['results'])) {
      $rows = $values['results']; 
    }
    if ($rows) { foreach ($rows as $row) {
      if ($row['ipaddress'] == "::1" || $row['ipaddress'] == "127.0.0.1") { $rdns = "Local Computer"; }
      else { 
        $rdns = gethostbyaddr($row['ipaddress']); 
        if (!$rdns) { $rdns = "Not Available"; }
      }
      ?>
      <tr>
            <td><?= $row['ipaddress'] ?></td>
            <td><?= $row['bandate'] ?></td>
            <td><?= $rdns ?></td>
            <td><button id="submit" name="submit" class="btn btn-primary" value="select-<?= $row['bannumber'] ?>">UNBAN</button></td>
      </tr>
      <?php
        }
      }
      else {
      ?>
        <tr>
            <td>No Records Found.</td>
            <td></td>
            <td></td>
        </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <?php
        if ($rows) {
          ?>
          <button id="submit" name="submit" value="removeall" class="btn btn-danger" onclick="return confirm('Are you sure? This will remove ALL Banned IPs and ALL Attack IPs.')">REMOVE ALL BANS</button>
          <?php
        }
        ?>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>

      </div>
    </div>
  </div>

</form>
</div>             






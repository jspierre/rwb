<?php
/* TO USE THIS TEMPLATE:
  1. change Line 27 from crsmgmt to the CRUD controller you are using (not the search controller)
  2. Lines 28-66 - Change as necessary to display the fields and other data you want from the results.  
  3. Remove this whole php section and move the <div... to the top.
*/
?>
<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>
<div class="panel panel-primary">

  <div class="panel-heading pb-4">

      <?php
    // Let's get all of our variables extracted so we can work with them
    extract($values);

    // And now, let's set our page on-screen header
    $header = $thingname." Record Search Results";
    echo '<h3 class="panel-title">'.$header.'</h3>'; 
    ?>
    <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>

  </div>

</div>

  <form action="<?= BASE_URL ?>emailmgmt" method="post" name="emailmgmt" id="client_form">
    <input type="hidden" name="searchform" value="searchform" />
    <table class="table table-bordered table-hover table-responsive-lg" id="myTable2">
      <thead>
        <tr>
            <th onclick="sortTable(0)"><a href="#">Email #</a></th>
            <th onclick="sortTable(1)"><a href="#">Sending To</a></th>
            <th onclick="sortTable(2)"><a href="#">Subject Line</a></th>
            <th onclick="sortTable(3)"><a href="#">Send Time</a></th>
            <th onclick="sortTable(4)"><a href="#">Status</a></th>
            <th onclick="sortTable(5)"><a href="#">Expected</th>
            <th onclick="sortTable(6)"><a href="#">Sent</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['results'])) {
      $rows = $values['results'];  
    }
    foreach ($rows as $row) {
      if ($row['emailstatus'] == 0) {$estatus = "COMPLETE"; } elseif ($row['emailstatus'] == 1) {$estatus = "READY"; } elseif ($row['emailstatus'] == 2) {$estatus = "SENDING"; } elseif ($row['emailstatus'] == 3) {$estatus = "TERMINATED"; }
      if ($row['groupnum'] == 0 && $row['usernum'] == 0) { $sendto = "ALL USERS"; } elseif ( $row['groupnum'] == 0 ) { $sendto = $row['firstname']." ".$row['lastname']; }
      if ($row['groupnum'] > 0 ) { $sendto = $row['groupname']; }
      if ($row['numberexpected'] == 0) {
        if ($row['emailstatus'] == 3) { $ne = 0; }
        else { $ne = "PENDING"; } 
      } 
      else { $ne = $row['numberexpected']; }
      if ($row['numbersent'] == 0) { 
        if ($row['emailstatus'] == 3) { $ns = 0; }
        else { $ns = "PENDING"; } 
        }
      else { $ns = $row['numbersent']; }
      ?>
      <tr>
            <td><?= $row['mailnum'] ?></td>
            <td><?= $sendto ?></td>
            <td><?= $row['subjectline'] ?></td>
            <td><?= $row['sendtime'] ?></td>
            <td><?= $estatus ?></td>
            <td><?= $ne ?></td>
            <td><?= $ns ?></td>
            <td><button id="submit" name="submit" class="btn btn-primary" value="select-<?= $row['mailnum'] ?>">SELECT</button></td>
      </tr>
      <?php
    }
    ?>
    </tbody>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="research" class="btn btn-primary">BACK TO SEARCH</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             






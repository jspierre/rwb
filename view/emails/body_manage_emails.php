<!-- datetime picker -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script>
  $( function() {
    $("#datepicker" ).datepicker({ minDate: 1 });
    if ($('input[type=checkbox]').prop('checked')) {
      $("#datepicker").datepicker().datepicker('setDate', new Date());
    }
  } );
</script>
<script>
  function toggle(className, obj) {
    $(className).toggle( !obj.checked )
    $(className).removeClass('hidden');
  }
</script>
<form class="form-horizontal" action="<?= BASE_URL ?>emailmgmt" method="post" name="emailmgmt" id="course_form">
<fieldset>
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  
// Let's get all of our variables extracted so we can work with them
extract($values);
if (isset($returnPayload[4]) && !empty($returnPayload[4])) {
  extract ($returnPayload[4]);
}

// Now, let's set some inputs so we'll have them again later
// Replace $mailnum with the index variable in your table here
// We'll also 
if (isset($record) && empty($mailnum)) {   $mailnum = $record; }
if (!empty ($mailnum) && empty($datecreated)) { $datecreated = "JUST NOW"; }
if (!empty ($mailnum) && !isset($emailstatus)) { $emailstatus = 1; }
if (!empty ($mailnum) && empty($numberexpected)) { $numberexpected = "PENDING"; }
if (!empty ($mailnum) && empty($numbersent)) { $numbersent = 0; }

if (isset($record) && $action != "ADD") {
  echo '<input type="hidden" name="record" value="'.$record.'">';
  echo '<input type="hidden" name="emailstatus" value="'.$emailstatus.'">';
  echo '<input type="hidden" name="datecreated" value="'.$datecreated.'">';
  echo '<input type="hidden" name="numberexpected" value="'.$numberexpected.'">';
  echo '<input type="hidden" name="numbersent" value="'.$numbersent.'">';
}
if (!empty ($mailnum) && empty($datecreated)) { $datecreated = "JUST NOW"; }
// if (!empty ($mailnum) && empty($emailstatus)) { $emailstatus = 1; }

if ($action == "DISPLAY" || $action == "EDIT") {
  if (isset($usernum) && $usernum > 0) {
    $groupnum = 99;
  }
  if (isset($sendtime) && strlen($sendtime) > 10) {
    $y = substr($sendtime, 0, 4);
    $m = substr($sendtime, 5, 2);
    $d = substr($sendtime, 8, 2);
    $st = $m."-".$d."-".$y;
  }
  elseif (!isset($sendtime) || $sendtime == "") {
    $st = "ASAP";
  }
  else {
    $st = $sendtime;
  }
}

// This is the Headline on the page 
if ($action=="ADD") {
  $header = "Add New ".$thingname." Form";
}
elseif ($action=="EDIT") {
  $header = "Editing ".$thingname." #".$record;
}
else {
  $header = "Displaying ".$thingname." #".$record;
}
echo '<h3 class="panel-title">'.$header.'</h3>'; 
echo '<p><small>NOTE: Items in <strong>bold</strong> are Required.</small></p>';
?>

</div>
<div class="panel-body">
             
<?php
// Now, let's deal with any return messages we need to display.  These are always
// located in the $returnPayload array
if (isset($returnPayload)) {
    if (empty($returnPayload[3])) {$message = false; } else { $message = $returnPayload[3];}
  switch ($returnPayload[1]) {
    case 1:
      $color="alert-success";
      break;
    case 2:
      $color="alert-info";
      break;
    case 3:
      $color="alert-warning";
    case 4:
      $color="alert-danger";
    case 5:
      $color="alert-dark";
    default:
      $color="alert-primary";
      break;
  }
}
if ($message) {
  echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
  echo $message;
  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
  echo '</div>';
}
?>        



<?php 
// This is for testing only, comment out for production
error_reporting(E_ALL & ~E_NOTICE);
?>

<?php
// This is the button section.  It will display certain additional buttons
// based upon access level and current task.  
if ($action=="DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("emgmtedit")) { 
          if ((isset($groupnum) && $groupnum !=99) && (isset($emailstatus) && $emailstatus == 1)) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning">Edit <?= $thingname ?></button>
          <?php 
          }
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("emgmtedit")) { 
          if ((isset($groupnum) && $groupnum !=99) && (isset($emailstatus) && $emailstatus != 0 && $emailstatus != 3)) { ?>
          <button id="submit" name="submit" value="terminate" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure? This will terminate the email and any pending emails not yet sent. THIS ACTION CANNOT BE UNDONE.')">Terminate <?= $thingname ?></button>
          <?php 
          }
        }
      ?>
</div>
<?php
}
?>

<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="mailnum"><abbr title="This is the Mail Index System Number - it is assigned automatically and cannot be changed.">Mail Index No.</abbr></label>  
    <div class="col-md-2">
      <input id="mailnum" name="mailnum" type="text" placeholder="TBA" class="form-control input-md" value="<?= $mailnum ?>" disabled>
      <input id="mailnum" name="mailnum" type="hidden" value="<?= $mailnum ?>">
    </div>
  </div>
  <div class="row">
  <label class="col-md-2 control-label" for="groupnum">Group To Send To</label>
    <div class="col-md-2">
      <select id="groupnum" name="groupnum" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?> >
        <?php if ($groupnum == 99) { ?> <option value ="99" title="CERTAIN USER">CERTAIN USER</option> <?php } ?>
        <option value ="0" title="ALL USERS">ALL USERS</option>
       <?php
            foreach ($groupnumbers as $groupnumber) {
              if ($groupnumber['groupindex'] == $groupnum) {
                echo '<option value="'.$groupnumber['groupindex'].'" title = "'.$groupnumber['groupname'].'" selected>'.$groupnumber['groupname'].'</option>';
              }
              else {
                echo '<option value="'.$groupnumber['groupindex'].'" title = "'.$groupnumber['groupname'].'">'.$groupnumber['groupname'].'</option>';
              }
            }
          ?>
        </select>
    </div>
    <label class="col-md-2 control-label" for="emailstatus">Current Email Status</label>  
    <div class="col-md-2">
      <?php
        if (isset($emailstatus)) {
          if ($emailstatus == 0) { $ces = "COMPLETE"; }
          if ($emailstatus == 1) { $ces = "SCHEDULED"; }
          if ($emailstatus == 2) { $ces = "IN PROGRESS"; }
          if ($emailstatus == 3) { $ces = "TERMINATED"; }
        }
      ?>
      
      <input id="emailstatus" name="emailstatus" type="text" placeholder="NEW" class="form-control input-md" value="<?= $ces ?>" disabled>
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="sendnow"><abbr title="Send the Email as soon as possible?">Send Email ASAP</abbr></label>  
    <div class="col-md-2">
      <?php 
        if ($action == "DISPLAY") {
          $disabled="disabled";
          $hidden="";
          ?>
          <input name="sendnow" type="text" id="sendnow" class="form-control input-md" value="SEE DATE -->" disabled />
          <?php
        } 
        elseif ($action == "EDIT") {
          $disabled="";
          $hidden="";
          ?>
          <input name="sendnow" type="checkbox" id="sendnow" value="true" class="big-checkbox" onclick="toggle('.datepicker', this)" />
          <?php                    
        }
        else {
            $hidden="hidden";
            $disabled="";
            ?>
           <input name="sendnow" type="checkbox" id="sendnow" value="true" class="big-checkbox" onclick="toggle('.datepicker', this)" checked />          
           <?php
        }
      ?>
    </div>
    <span class="datepicker <?= $hidden ?> col-md-2"><label class="control-label" for="sendtime"><abbr title="The EARLIEST date that the email should be sent"><strong>Send Date</strong></abbr></label></span>  
    <div class="col-md-2">
           <input name="sendtime" id="datepicker" type="text" value="<?= $st ?>" class="form-control datepicker <?= $hidden ?>" required <?= $disabled ?> />
    </div>
  </div>


  <div class="row">
    <label class="col-md-2 control-label" for="ccadmins"><abbr title="Send all admins a copy of this Email?">Copy to Admins</abbr></label>
    <div class="col-md-2">
      <select id="ccadmins" name="ccadmins" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($ccadmins==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($ccadmins==1 && ($action !== "ADD")) { echo "selected"; }?>>YES</option>
      </select>
    </div>
    <label class="col-md-2 control-label" for="optinonly"><abbr title="Send this email only to users who opted-in to receiving non-system emails?">Send To Which Users</abbr></label>
    <div class="col-md-2">
      <select id="optinonly" name="optinonly" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="1" <?php if ($optinonly==1) { echo "selected"; }?>>OPT-IN ONLY</option>
        <option value="0" <?php if ($optinonly==0 && ($action !== "ADD")) { echo "selected"; }?>>EVERYONE</option>
      </select>
    </div>
  </div>
  <?php
    $systememail = $this->registry->config['systememail'];
    $siteemail = $this->registry->config['siteemail'];
  ?>
  <div class="row">
    <label class="col-md-2 control-label" for="sendfrom"><abbr title="Choose which email from the config file to use as the FROM on the email">Send Email From</abbr></label>
    <div class="col-md-6">
      <select id="sendfrom" name="sendfrom" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($sendfrom==0) { echo "selected"; }?>>SYSTEM EMAIL - <?= $systememail ?></option>
        <option value="1" <?php if ($sendfrom==1 && ($action !== "ADD")) { echo "selected"; }?>>SUPPORT EMAIL - <?= $siteemail ?></option>
        <?php 
        if ($sendfrom == 2) {
          ?>
          <option value="2" selected>SENDING USER'S EMAIL: USER# <?= $createdby ?></option>
          <?php
        }
       ?>
      </select>
    </div>
  </div>
  <?php
  if ($groupnum == 99) {
    $sendto = $firstname." ".$lastname." ( ".$emailaddress." )"; ?>
  <div class="row">
    <label class="col-md-2 control-label" for="subjectline"><abbr title="The use the email was sent to"><strong>Send Email To</strong></abbr></label>  
    <div class="col-md-6">
      <input id="sendto" name="sendto" type="text" maxlength="255" placeholder="" class="form-control input-md" required value="<?= $sendto ?>" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  >
    </div>
  </div> <?php
  } ?>

  <div class="row">
    <label class="col-md-2 control-label" for="datecreated">Date Created</label>  
    <div class="col-md-2">
      <input id="datecreated" name="datecreated" type="text" placeholder="TODAY" class="form-control input-md" style="font-size:14px;" value="<?= $datecreated ?>" disabled>
    </div>
    <label class="col-md-2 control-label" for="lastchange">Date Last Changed</label>  
    <div class="col-md-2">
      <input id="lastchange" name="lastchange" type="text" placeholder="TODAY" class="form-control input-md" style="font-size:14px;" value="<?= $lastchange ?>" disabled>
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="numberexpected">Emails Expected</label>  
    <div class="col-md-2">
      <input id="numberexpected" name="numberexpected" type="text" placeholder="PENDING" class="form-control input-md" value="<?= $numberexpected ?>" disabled>
    </div>
    <label class="col-md-2 control-label" for="numbersent">Total Emails Sent</label>  
    <div class="col-md-2">
      <input id="numbersent" name="numbersent" type="text" placeholder="PENDING" class="form-control input-md" value="<?= $numbersent ?>" disabled>
    </div>
  </div>
</div>

<div class="form-group">
  <?php 
  if (isset($subjectline)) { $subjectline = $this->registry->security->unSanitize($subjectline); }
  ?>
  <div class="row">
    <label class="col-md-2 control-label" for="subjectline"><abbr title="The subject line of the email to be sent. You can use %%FULLNAME%% or %%FIRSTNAME%% in your text to have the system automatically personalize each outgoing message."><strong>Subject Line</strong></abbr></label>  
    <div class="col-md-6">
      <input id="subjectline" name="subjectline" type="text" maxlength="255" placeholder="" class="form-control input-md" required value="<?= $subjectline ?>" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  >
    </div>
  </div>
  <?php
  if (!isset($emailbody)) {
    $emailbody = "\n\n\nThank you,\n\n".$this->registry->config['sitename']."\nPhone: ".$this->registry->config['sitephone']."\nEmail: ".$this->registry->config['siteemail']."\n";
    if ($sendfrom == 0) { $emailbody .= "\n\n**********\nNote: this email was sent from a send-only address; please do not reply to this email as it will fail.  If you need to contact us, please contact ".$this->registry->config['siteemail']."."; }
  }
  else {
    $emailbody = $this->registry->security->unSanitize($emailbody);
  }
  ?>
  <div class="row">
    <label class="col-md-2 control-label" for="emailbody"><abbr title="The body of the email to be sent. You can use %%FULLNAME%% or %%FIRSTNAME%% in your text to have the system automatically personalize each outgoing message."><strong>Email Body</strong></abbr></label>  
    <div class="col-md-6">
    <textarea rows="15" id="emailbody" name="emailbody" placeholder="" class="form-control input-md" required" <?php if ($action == "DISPLAY") { echo "disabled"; } ?> ><?= $emailbody ?></textarea>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="internalnotes"><abbr title="Notes that can only be seen on this screen.">Internal Notes</abbr></label>  
    <div class="col-md-6">
      <textarea id="internalnotes" name="internalnotes" rows="4" placeholder="" class="form-control input-md" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  ><?= $internalnotes ?></textarea>
    </div>
  </div>
</div>



<?php
// AND NOW HERE ARE OUR BOTTOM BUTTONS....
if ($action == "DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("crsedit")) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning">Edit <?= $thingname ?></button>
        <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("usernotes")) { ?>
          <button id="submit" name="submit" value="notes" class="btn btn-sm btn-primary"><?= $thingname ?> Notes</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("userlog")) { ?>
          <button id="submit" name="submit" value="log" class="btn btn-sm btn-primary"><?= $thingname ?> Log</button>
      <?php 
        }
      ?>
</div>
<?php
}
?>

<!-- Button -->
<?php if ($action !== "DISPLAY") {
  ?>
<div class="form-group">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-4">
      <?php if ($action == "ADD") {
              ?><button id="submit" name="submit" value="add" class="btn btn-danger">Schedule New <?= $thingname ?></button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>searchsysemails" role="button" onclick="return confirm('Are you sure you want to cancel? You will lose all data entered above.')">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">Update <?= $thingname ?></button>
              <button id="submit" name="submit" value="display" class="btn btn-primary">CANCEL</button>
              <?php
            }?>
    </div>
</div>
<?php 
}

?>

</fieldset>
</form>




<form class="form-horizontal" action="<?= BASE_URL ?>sysadmin" method="post" name="sysadmin" id="course_form">
<fieldset>
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  
// Let's get all of our variables extracted so we can work with them
extract($values);
if (isset($returnPayload[4]) && !empty($returnPayload[4])) {
  extract ($returnPayload[4]);
}
?>


<h3 class="panel-title">Editing System Administration Settings</h3>
<h5>CAUTION: These functions affect the entire system and take effect immediately!</h5>

</div>
<div class="panel-body">
             
<?php
// Now, let's deal with any return messages we need to display.  These are always
// located in the $returnPayload array
if (isset($returnPayload)) {
    if (empty($returnPayload[3])) {$message = false; } else { $message = $returnPayload[3];}
  switch ($returnPayload[1]) {
    case 1:
      $color="alert-success";
      break;
    case 2:
      $color="alert-info";
      break;
    case 3:
      $color="alert-warning";
    case 4:
      $color="alert-danger";
    case 5:
      $color="alert-dark";
    default:
      $color="alert-primary";
      break;
  }
}
if ($message) {
  echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
  echo $message;
  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
  echo '</div>';
}
?>        



<?php 
// This is for testing only, comment out for production
error_reporting(E_ALL & ~E_NOTICE);
?>

<?php
// This is the button section.  It will display certain additional buttons
// based upon access level and current task.  
if ($action=="DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("crsedit")) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning">Edit <?= $thingname ?></button>
        <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("usernotes")) { ?>
          <button id="submit" name="submit" value="notes" class="btn btn-sm btn-primary"><?= $thingname ?> NOTES</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("userlog")) { ?>
          <button id="submit" name="submit" value="log" class="btn btn-sm btn-primary"><?= $thingname ?> LOG</button>
      <?php 
        }
      ?>
</div>
<?php
}
?>

<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <div class="col-md-2">
      <p></p>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label" for="forcepw"><abbr title="Force immediate Password Change for ALL USERS?">Force Password Change for ALL USERS:</abbr></label>
    <div class="col-md-2">
      <select id="forcepw" name="forcepw" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($forcepw==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($forcepw==1 && ($action !== "ADD")) { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label" for="lastpwchange"><abbr title="When was the last time a password change was forced for all users through the above option?">Last Date/Time of Forced Password Change:</abbr></label>
    <div class="col-md-2">
      <input id="lastpwchange" name="lastpwchange" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $lastpwchange ?>" disabled>
    </div>
  </div>
</div>
<div class="form-group">
<div class="row">
    <label class="col-md-4 control-label" for="forcelogout"><abbr title="Force immediate logout of all logged-in users? Careful: loss of user data could occur!">Force Immediate Logout of ALL USERS:</abbr></label>
    <div class="col-md-2">
      <select id="forcelogout" name="forcelogout" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($forcelogout==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($forcelogout==1 && ($action !== "ADD")) { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label" for="lastforcelogout"><abbr title="When was the last time all users were forcefully logged out through the above option?">Last Date/Time of System-Wide User Logout:</abbr></label>
    <div class="col-md-2">
      <input id="lastforcelogout" name="lastforcelogout" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $lastforcelogout ?>" disabled>
    </div>
  </div>
</div>
<div class="form-group">
<div class="row">
    <label class="col-md-4 control-label" for="force2fa"><abbr title="Force revocation of all 2FA cookies? This will cause all users with 2FA active to have to revalidate on next login.">Revoke ALL 2FA Cookies:</abbr></label>
    <div class="col-md-2">
      <select id="force2fa" name="force2fa" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($force2fa==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($force2fa==1 && ($action !== "ADD")) { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label" for="lastforce2fa"><abbr title="When was the last time all users were forced to revalidate with 2FA using the option above?">Last Date/Time of Revoke ALL 2FA Cookies:</abbr></label>
    <div class="col-md-2">
      <input id="lastforce2fa" name="lastforce2fa" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $lastforce2FA ?>" disabled>
    </div>
  </div>
</div>
<div class="form-group">
<div class="row">
    <label class="col-md-4 control-label" for="showsysbanner"><abbr title="Force every user to immediate see the below message as a dismissable banner on their page? Message will keep displaying until everyone has seen it unless you select TURN OFF - then all users who haven't seen it will not.">Turn System Banner ON:</abbr></label>
    <div class="col-md-2">
      <select id="showsysbanner" name="showsysbanner" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($showsysbanner==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($showsysbanner==1 && ($action !== "ADD")) { echo "selected"; }?>>YES</option>
        <option value="2" <?php if ($showsysbanner==2 && ($action !== "ADD")) { echo "selected"; }?>>TURN OFF</option>
      </select>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label" for="systembanner"><abbr title="What message to display if System Banner is ON?">System Banner Message:</abbr></label>
    <div class="col-md-4">
      <input id="systembanner" name="systembanner" type="text" placeholder="" class="form-control input-md" style="font-size:12px;" value="<?= $systembanner ?>">
    </div>
  </div>
</div>


<div class="form-group">
<div class="row">
    <label class="col-md-4 control-label" for="runstatus"><abbr title="Change the RUN STATUS of the entire system? This can be used for maintenance or other system downtime needs. Careful: loss of user data could occur! NOTE: MAINT MODE-DENY ALL & SYSTEM DOWN COMPLETLY require a database table edit to resume operations!">Change System-Wide Run/Maintenance Status:</abbr></label>
    <div class="col-md-4">
      <select id="runstatus" name="runstatus" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
        <option value="0" <?php if ($runstatus==0) { echo "selected"; }?>>RUN NORMAL</option>
        <option value="1" <?php if ($runstatus==1 && ($action !== "ADD")) { echo "selected"; }?>>MAINT MODE - ALLOW ADMINS</option>
        <option value="2" <?php if ($runstatus==2 && ($action !== "ADD")) { echo "selected"; }?>>MAINT MODE - DENY ALL</option>
        <option value="3" <?php if ($runstatus==3 && ($action !== "ADD")) { echo "selected"; }?>>SYSTEM DOWN COMPLETELY</option>
      </select>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label" for="laststatuschange"><abbr title="When was the last time the System-Wide Run Status was changed through the above option?">Last Date/Time of Run Status Change:</abbr></label>
    <div class="col-md-2">
      <input id="laststatuschange" name="laststatuschange" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $laststatuschange ?>" disabled>
    </div>
  </div>
</div>


<?php
// AND NOW HERE ARE OUR BOTTOM BUTTONS....
if ($action == "DISPLAY") {
  ?>
<div class="form-group">
        <button id="submit" name="submit" class="btn btn-success btn-sm" value="search">Back to Results</button>
      <?php
        if ($this->registry->security->checkFunction("crsedit")) { ?>
          <button id="submit" name="submit" value="edit" class="btn btn-sm btn-warning">Edit <?= $thingname ?></button>
        <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("usernotes")) { ?>
          <button id="submit" name="submit" value="notes" class="btn btn-sm btn-primary"><?= $thingname ?> Notes</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("userlog")) { ?>
          <button id="submit" name="submit" value="log" class="btn btn-sm btn-primary"><?= $thingname ?> Log</button>
      <?php 
        }
      ?>
</div>
<?php
}
?>

<!-- Button -->
<?php if ($action !== "DISPLAY") {
  ?>
<div class="form-group">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-4">
     <button id="submit" name="submit" value="update" class="btn btn-warning">Update <?= $thingname ?></button>
     <button id="submit" name="submit" value="display" class="btn btn-primary">CANCEL</button>
    </div>
</div>
<?php 
}

?>

</fieldset>
</form>



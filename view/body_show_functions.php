<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">
      <h3 class="panel-title">ACL Group Function Granular Control</h3>
      <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>
  </div>

  <?php
  if (isset($_SESSION['returncode'])) {
    if ($_SESSION['returncode'] < 4) { $color = "alert-success"; } else { $color = "alert-danger"; }
      echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
     
      switch ($_SESSION['returncode']) {
        case 2:
          echo "SUCCESS: Function Deleted as requested.";
          Break;
        case 5:
          echo "ERROR: Function NOT Deleted. Please try again or contact administrator."; 
          Break;
        default:
          echo "An unknown error has occurred.  Please contact your administrator.";
      }
      echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      echo '</div>';
  }                   
?>  

</div>

  <form action="<?= BASE_URL ?>fncmgmt" method="post" name="showfunctions" id="showfunctions">
    <div class="form-group">
    <div class="row">
      <div class="col-md-8 btn-space">
        <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW FUNCTION</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>
    <table class="table table-bordered table-hover table-responsive" id="myTable2">
      <thead>
        <tr>
          <th onclick="sortTable(0)"><a href="#">Function #</a></th>
          <th onclick="sortTable(1)"><a href="#">Function Of</a></th>
          <th onclick="sortTable(2)"><a href="#">Function Name</a></th>
          <th onclick="sortTable(3)"><a href="#">Function Allows</a></th>
          <th onclick="sortTable(4)"><a href="#">Active?</a></th>
          <th onclick="sortTable(5)"><a href="#">Allow All?</a></th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['functions'])) {
      $rows = $values['functions'];  
    }
    foreach ($rows as $row) {
      if ($row['functionactive'] == 1) {$astatus = "YES";} else {$astatus="NO";}
      if ($row['allowall'] == 1) {$bstatus = "YES";} else {$bstatus="NO";}
      ?>
      <tr>
        <td><?= $row['funcnum'] ?></td>
        <td><?= $row['controller'] ?></td>
        <td><?= $row['funcname'] ?></td>
        <td><?= $row['funcdescr'] ?></td>
        <td><?= $astatus ?></td>
        <td><?= $bstatus ?></td>
        <td><button id="submit" name="submit" class="btn btn-primary" value="FNX<?= $row['funcnum'] ?>">EDIT</button></td>
      </tr>
    <?php
    }
  ?>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW FUNCTION</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             






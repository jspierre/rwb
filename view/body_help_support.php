
<form class="form-horizontal" action="<?= BASE_URL ?>support" method="post" name="createticket" id="createticket">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title spacer">Help and Support</h3>
    <p>We are here to help! If you need assistance, or just have a suggestion, praise, or complaint, feel free to reach out to us day or night!</p>
    <?php 
    if ($this->registry->security->checkFunction("faq")) { ?>
      <p>You are also welcome to view our list of <a href="faq">Frequently Asked Questions (FAQ)</a> for answers to common questions, or contact us at:
    <?php } ?>
    <p><strong>Email:</strong> <a href="mailto:<?= $this->registry->config['siteemail'] ?>"><?= $this->registry->config['siteemail'] ?></a><br /><strong>Phone:</strong> <?= $this->registry->config['sitephone'] ?> 
      <br /><strong>Text&#47;SMS:</strong> <?= $this->registry->config['sitephone'] ?></p>
    <?php
      if ($this->registry->security->checkFunction("submitticketself")) { ?>
      <p>Or, simply send us a message below - we'll get back to you as soon as we can!</p>
  </div>

<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "SUCCESS: Your request was submitted!  We'll be in touch soon!";
                        break;
                    case 5:
                      echo "FAILED: Oh, no! Your request did not go through. Try again later or contact support for additional help.";
                      break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }
            
          
?>        

<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($values['values'])) {
  extract ($values['values']);
}

?>

<!-- Text input-->
<?php
if ($this->registry->security->checkFunction("submitticketself")) { ?>
<input type="hidden" name="loginnum" value="<?= $_SESSION['usernum'] ?>">
<div class="form-group">
  <div class="row">
  <label class="col-md-2 control-label" for="convsubject"><abbr title="The subject of the conversation">Subject</abbr></label>  
    <div class="col-md-6">
      <select id="convsubject" name="convsubject" class="form-control" <?php if ($values['action'] == "DISPLAY") { echo "disabled"; } ?>>
       <option value="General Inquiry" <?php if ($convsubject=="" && ($action !== "ADD")) { echo "selected"; }?>>General Inquiry</option>
       <option value="Billing and Sales" <?php if ($convsubject=="Billing and Sales") { echo "selected"; }?>>Billing and Sales Questions</option>
       <option value="Complaint" <?php if ($convsubject=="Billing and Sales") { echo "selected"; }?>>Complaint or Bug Report</option>
       <option value="Feature Request" <?php if ($convsubject=="Feature Request") { echo "selected"; }?>>Feature Request or Suggestion</option>
       <option value="Praise" <?php if ($convsubject=="Technical Support") { echo "selected"; }?>>Praise or Compliment</option>
       <option value="Technical Support" <?php if ($convsubject=="Technical Support") { echo "selected"; }?>>Technical Support</option>
      </select>
    </div>
  </div>
  <div class="row">
  <label class="col-md-2 control-label" for="messagebody"><abbr title="The message received or sent in this particular interaction.">Message</abbr></label>  
    <div class="col-md-6">
      <textarea id="messagebody" name="messagebody" rows="5" placeholder="Enter the details of your request here" class="form-control input-md" required <?php if ($values['action'] == "DISPLAY" ) { echo "disabled"; } ?>><?= $messagebody ?></textarea>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-md-2"></div>
    <div class = "col-md-6">
    <?php 
    if ($values['action'] == "ADD") { ?>
      <button id="submit" name="submit" value="submitticket" class="btn btn-warning btn-sm btn-space" onClick="showWait()">SUBMIT SUPPORT REQUEST</button>
    <?php }
}
?>
        <button id="submit" name="submit" value="gohome" class="btn btn-primary btn-sm btn-space" onClick="unlockCancel()">BACK TO HOME PAGE</button>
    </div>
  </div>
</div>


</fieldset>
</form>
<?php } ?>

<script>
function unlockCancel() {
  document.getElementById("messagebody").required = false;
  document.getElementById("convsubject").required = false;
  return true;
}
</script>


<script>
function showWait() {
  $("#createticket").validate({
     submitHandler: function(form){
      myApp.showPleaseWait();
      form.submit();
     }
  });
}
</script>


<script>
var myApp;
myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Sending Message- Please Wait...</h5></div><div class="modal-body" align="center"><img src="css/loading.gif" width="75%"></div><div class="modal-footer"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };
})();
</script>


<h1 id="begin">FREQUENTLY ASKED QUESTIONS (FAQ)</h1>
<hr />

<div class="list-group">
  <a href="#" class="list-group-item list-group-item-action active">
    General Questions
  </a>
  <a href="#1" class="list-group-item list-group-item-action">1. Why does...?</a>
  <a href="#2" class="list-group-item list-group-item-action">2. How do I...?</a>
  <a href="#3" class="list-group-item list-group-item-action">3. Where can I...?</a>
</div>

<div class = "spacer"></div>
<hr />



<div class = "spacer"></div>
<div class="list-group" id="1">
  <a href="#" class="list-group-item list-group-item-action active">
    1. Why does...?
  </a>
  <p class="list-group-item">
  	The reason it does is because...
	<br /><br /><a href="#begin" class="btn btn-primary btn-sm btn-space">BACK TO TOP</a>
  </p>
</div>



<div class = "spacer"></div>
<div class="list-group" id="2">
  <a href="#" class="list-group-item list-group-item-action active">
    2. How do I...?
  </a>
  <p class="list-group-item">
  	The way you do that is by doing this...
	<br /><br /><a href="#begin" class="btn btn-primary btn-sm btn-space">BACK TO TOP</a>
  </p>
</div>

<div class = "spacer"></div>
<div class="list-group" id="3">
  <a href="#" class="list-group-item list-group-item-action active">
    3. Where can I...?
  </a>
  <p class="list-group-item">
  	You can find that by going to...
	<br /><br /><a href="#begin" class="btn btn-primary btn-sm btn-space">BACK TO TOP</a>
  </p>
</div>
<form class="form-signin" action="<?= BASE_URL ?>pwrst" onsubmit="return validateForm()" method="post" name="pwrst" id="pwrst_form">

<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
	<div class="panel-heading">
	    <h3 class="panel-title">Reset Your Forgotten Password</h3>
	</div>
<div class="panel-body">
<?php
			/* LOGIC FOR RETURN MESSAGES */
			if (isset($_SESSION['returncode'])) {
				if ($_SESSION['returncode'] == 15) { $color = "alert-success"; } else { $color = "alert-danger"; }
				?>
					<div class="alert alert-success col-md-6" role="alert">
        		<?php
   
				switch ($_SESSION['returncode']) {
					case 15:
						echo "<p>Your request was received.</p><p>If that email address you entered exists in our system, you will receive an email shortly with instructions on how to reset your password.</p>";
						break;
					default:
						echo "An unknown error has occurred.  Please contact your administrator.";
				}
				echo '</div>';
			}
	if($this->registry->config['loginwithemail']) { $prompt="Email Address"; } else  { $prompt="User Name"; }		
	if ((isset($_SESSION['returncode']) && $_SESSION['returncode']<>15) || (!isset($_SESSION['returncode']))) {
?>			
				<div class="form-group">
					<div class="row">
						<div class="col-md-4">
							<p>Please enter the <?= $prompt ?> associated with your account to reset your password.</p>
							<hr />
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<input type="email" id="inputEmail" name="user" class="form-control" placeholder="<?= $prompt ?>" required autofocus>
							<label for="inputEmail">&nbsp;Your <?= $prompt ?></label>
						</div>
					</div>
				</div>
			<button class="col-md-2 btn btn-primary" type="submit">Request Reset</button>
<?php
	}
	else {
?>
	</form>
	<form action="<?= $this->input['url'] ?>" method="POST">
		<button class="col-md-2 btn btn-primary" type="submit">Click here to continue</button>
		
<?php			
		}
?>
		</div>
	</form>
</div> <!-- /container -->


<form class="form-horizontal" action="<?= BASE_URL ?>modmgmt" method="post" name="editmodule" id="editmodule">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  //var_dump($_SESSION);
  //var_dump($_POST);
  //exit();
  //var_dump($values);
  
  

  // Determine what type of thing we are doing - new, display, or edit?
  if (isset($values['actiontype'])) {
    $actiontype = $values['actiontype'];
    extract($values['module']);
  }
  else {
    $actiontype = "NEW";
  }
 

  if ($actiontype == "EDIT") {
      echo '<h3 class="panel-title">Update Existing Module</h3>'; 
    }
    else {
      echo '<h3 class="panel-title">New Module Entry Form</h3>';
    }
?>
</div>
<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "New Module Added. Please check settings to make sure all is correct!";
                        break;
                    case 3:
                        echo "Module Updated.";
                        break;
                    case 5:
                        $modulenum = $values['modulenum'];
                        $link = '<a href="'.BASE_URL.'modmgmt/'.$modulenum.'">';
                        echo "ERROR: Module Already Exists - See ".$link."Module # $modulenum</a>";
                        extract($values['tempdata']);
                        break;
                    case 6:
                        $loginnum = $values['error']['modulenum'];
                        echo "ERROR: Invalid Module Number: $modulenum.  Add new user or try again.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }                    
?>        


<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($_SESSION['modulenum'])) {
  $modulenum = $_SESSION['modulenum'];
  $moduleindex = $modulenum;
  echo '<input type="hidden" name="modulenum" value ='.$modulenum.' />';
}
?>


<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="moduleindex">Module #</label>  
    <div class="col-md-2">
      <input id="moduleindex" name="moduleindex" type="text" placeholder="" class="form-control input-md" value="<?= $moduleindex?>" disabled >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="controller">Controller</label>  
    <div class="col-md-2">
      <input id="controller" name="controller" type="text" placeholder="" class="form-control input-md" value="<?= $controller?>" autofocus >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="modulename">Module Description</label>  
    <div class="col-md-5">
      <input id="modulename" name="modulename" type="text" placeholder="" class="form-control input-md" value="<?= $modulename ?>"  >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="allowall">Allow All?</label>
    <div class="col-md-2">
      <select id="allowall" name="allowall" class="form-select" >
        <option value="0" <?php if ($allowall==0 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($allowall==1) { echo "selected"; }?>>YES</option>
      </select>
    </div>
    <label class="col-md-1 control-label" for="denyall">Deny All?</label>
    <div class="col-md-2">
      <select id="denyall" name="denyall" class="form-select" >
       <option value="0" <?php if ($denyall==0 ) { echo "selected"; }?>>NO</option>
       <option value="1" <?php if ($denyall==1 ) { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>
</div>


<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8 btn-space">
      <?php if ($actiontype == "NEW") {
              unset($_SESSION['modulenum']);
              ?><button id="submit" name="submit" value="savenew" class="btn btn-primary">ADD NEW MODULE</button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>modmgmt" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">UPDATE MODULE</button>
              <button id="submit" name="submit" value="modmgmt" class="btn btn-primary">CANCEL</button>
              <?php
              if ($this->registry->security->checkFunction("deletemodule")) { ?>
                <button id="submit" name="submit" value="delete" class="btn btn-danger" onclick="return confirm('Are you sure? This module AND ALL ASSOCIATED FUNCTIONS will be deleted and NO ONE will have access to that controller or page or functions.')">DELETE MODULE</button> 
                <?php 
              } 
            }
            ?>
      
    </div>
  </div>
</div>

</fieldset>
</form>



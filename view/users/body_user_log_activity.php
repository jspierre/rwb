
<div class="panel panel-primary">

  <div class="panel-heading">
      <h3 class="panel-title">User Activity Log</h3>
  </div>

  <div class="panel-body">
  </div>

  <form action="<?= BASE_URL ?>usmgmt" method="post" name="userlog" id="client_form">
          <label for="submit"></label>
          <button id="submit" name="submit" class="btn btn-primary" value="usmgmt">RETURN TO USER VIEW</button>
          <hr />
    <table class="table">
      <thead>
        <tr>
          <th>Date &amp; Time</th>
          <th>User #</th>
          <th>Username</th>
          <th>Log Activity</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['results'])) {
      $rows = $values['results'];  
    }
    foreach ($rows as $row) {
      if ($row['isprivate'] == 1) {
        $style='style="color:red; font-weight:bold; font-style:italic;"';
      }
      else {
       $style=""; 
      }
        if ($row['isprivate'] == 0 || $this->registry->security->checkFunction("userhrview")) {
      ?>
      <tr <?= $style?>>
        <td><?= $row['logtime'] ?></td>
        <td><?= $row['creatornum'] ?></td>
        <td><?= $row['username'] ?></td>
        <td><?= $row['logmessage'] ?></td>
      </tr>
    <?php
      }
    }
  ?>
  </table>
  <hr />
  <label for="submit"></label>
  <button id="submit" name="submit" class="btn btn-primary" value="usmgmt">RETURN TO USER VIEW</button>
  <hr />
</form>
</div>             






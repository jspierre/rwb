
<form class="form-horizontal" action="<?= BASE_URL ?>usmgmt" method="post" name="usernotes" id="user_notes">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">USER NOTES</h3>
  </div>

<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                eecho '<div class="alert '.$color.' alert-dismissible" role="alert">';
        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "Notes Saved.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }
            
          
?>        

<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($_SESSION['loginnum'])) {
  $usernum = $_SESSION['loginnum'];
  echo '<input type="hidden" name="loginnum" value ='.$usernum.' />';
}

if ($_SESSION['usernum'] == $_SESSION['loginnum']) { $ownacct = true; } else { $ownacct = false; }
?>

<!-- Text input-->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="notes">User Notes</label>  
    <div class="col-md-8">
      
      <?php if ($values['notes'] == false && $values['action'] == "DISPLAY") { 
              $notes = "NO NOTES ENTERED YET."; 
            } 
            elseif ($values['notes'] == false) {
              $notes = "";
            }
            else {
              $notes = $values['notes'];
            }
      ?>

      <?php if ($values['action'] == "DISPLAY") { ?>
        <textarea rows="20" cols = "100" name="notes" id="notes" disabled><?= $notes ?></textarea>    
      <?php }
      else { 
        $usernum = $_SESSION['usernum'];
        $username = $_SESSION['username'];
        $datetime = $this->registry->getDateTime();
        if ($notes == "") { $startnote = ""; } else { $startnote = chr(13); }
        $notelog = $startnote."##### BELOW NOTE BY USER ".$usernum." (".$username.") AT ".$datetime." #####".chr(13);
        $notes .= $notelog;
        ?>
        <textarea rows="20" cols = "100" name="notes" id="notes"><?= $notes ?></textarea>    
      <?php } ?>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="form-group btn-space">
      <label class="control-label" for="submit"> </label>
      <?php
        if ($this->registry->security->checkFunction("usernotesedit")) { 

          if (($ownacct==true && $this->registry->security->checkFunction("editownacct")) || $ownacct == false) {
            if ($values['action'] == "DISPLAY" ) { ?>
            <button id="submit" name="submit" value="editnotes" class="btn btn-warning">EDIT NOTES</button>
          <?php }
          elseif ($values['action'] == "EDIT") { ?>
            <button id="submit" name="submit" value="savenotes" class="btn btn-warning">SAVE CHANGES</button>
          <?php }
          }
        } 
      ?>

      <?php
        if ($values['action'] == "DISPLAY") { ?>
        <button id="submit" name="submit" value="usmgmt" class="btn btn-primary">RETURN TO USER</button>
      <?php }
      else { ?>
        <button id="submit" name="submit" value="notes" class="btn btn-primary">DISCARD CHANGES</button>
      <?php }
      ?>
  </div>
</div>


</fieldset>
</form>

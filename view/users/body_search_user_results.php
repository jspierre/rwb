<script src="https://kit.fontawesome.com/01a10139a1.js" crossorigin="anonymous"></script>
<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">
      <?php if (isset($values['results']) && $values['results'])  { $numresults = count($values['results']); } ?>
      <h3 class="panel-title">User Mgmt Search Results (<?= $numresults ?> Records)</h3>
      <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>
      <?php 
        if (isset($_SESSION['ticketno'])) {
          $_SESSION['fromsearch'] = "searchuser";
          ?> <h6 class="btn btn-sm btn-outline-info">SUPPORT TICKET <?= $_SESSION['ticketno'] ?> ACTIVE</h6> <?php 
        }
      ?>

  </div>

</div>
  <?php $searchactive =  $values['searchactive']; ?>
  <form action="<?= BASE_URL ?>usmgmt" method="post" name="viewuser" id="client_form">

  <?php 
    if (isset($values['results'])) {
      $rows = $values['results'];  
    }
    if ($rows && count($rows)>9) { ?>
     <div class="form-group">
        <div class="row">
          <div class="col-md-8">
            <button id="submit" name="submit" value="research" class="btn btn-primary btn-space">BACK TO SEARCH</button>
            <a class="btn btn-primary btn-space" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
          </div>
        </div>
      </div>
  <?php } ?>


    <input type="hidden" name="searchform" value="searchform" />
    <table class="table table-bordered table-hover table-responsive-sm" id="myTable2">
      <thead>
        <tr>
            <th onclick="sortTable(0)"><a href="#">User #</a></th>
            <th onclick="sortTable(1)"><a href="#">Username</th>
            <th onclick="sortTable(2)"><a href="#">First Name</th>
            <th onclick="sortTable(3)"><a href="#">Last Name</th>
            <th onclick="sortTable(4)"><a href="#">Billing City</th>
            <th onclick="sortTable(5)"><a href="#">Billing State</th>
            <th onclick="sortTable(6)"><a href="#">Status</th>
            <th onclick="sortTable(7)"><a href="#">Locked</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php

    foreach ($rows as $row) {
      if ($searchactive == 2) { if ($row['isalive'] ==1) { $astatus = "ONLINE"; } else { $astatus = "OFFLINE"; } }
      else { if ($row['useractive'] == 1) {$astatus = "ACTIVE"; } else {$astatus = "DISABLED"; } }
      if ($row['userlocked'] == 1) {$lstatus = "YES";} else {$lstatus="NO";}
      ?>
      <tr>
            <td><?= $row['usernum'] ?></td>
            <td><?= $row['username'] ?></td>
            <td><?= $row['firstname'] ?></td>
            <td><?= $row['lastname'] ?></td>
            <td><?= $row['billingcity'] ?></td>
            <td><?= $row['billingstate'] ?></td>
            <td><?= $astatus ?></td>
            <td><?= $lstatus ?></td>
            <td><button id="submit" name="submit" class="btn btn-primary" value="UNX<?= $row['usernum'] ?>">SELECT</button></td>
      </tr>
      <?php
      }
      ?>
    </tbody>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="research" class="btn btn-primary btn-space">BACK TO SEARCH</button>
        <a class="btn btn-primary btn-space" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             

<?php
if ($rows && count($rows)>9) { ?>
<a id="back-to-bottom" href="#" class="btn btn-secondary btn-lg back-to-bottom" role="button"><i class="fas fa-chevron-down"></i></a>
<a id="back-to-top" href="#" class="btn btn-secondary btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>

<script>
$(document).ready(function(){
  $(window).scroll(function () {
      if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 400);
      return false;
    });
});
</script>


<script>
$(document).ready(function(){
    $('#back-to-bottom').fadeIn();
  $(window).scroll(function () {
      if ($(this).scrollTop() == $(document).height()-$(window).height()) {
        $('#back-to-bottom').fadeOut();
      } else {
        $('#back-to-bottom').fadeIn();
      }
    });
    // scroll body to 0px on click
    $('#back-to-bottom').click(function () {
      $('body,html').animate({
        scrollTop: $(document).height()-$(window).height()}, 
        1400, 
        "easeOutQuint");
      return false;
    });
});
</script>

<?php } ?>



<link href="<?= BASE_URL ?>css/signup.css" rel="stylesheet">
      <div class="text-center mb-4">
        <h1 class="h3 mb-3 font-weight-normal">Now, Check Your Email</h1>
        <div class="alert alert-success" role="alert">
          <p>Congrats! You successfully signed up!  We told you it was easy!
          What next? Well, we just emailed your <strong><em>temporary password</em></strong>
          to your email address.  Once you receive it, you can sign in on our home page,
          or by <a href="login">clicking here</a> to go to the login page! Welcome!</p>
          <p>Didn't receive your email from us?  Check your SPAM or JUNK folders in your
          	email client to make sure it didn't accidentally end up there!</p>
        </div>
      </div>

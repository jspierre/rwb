
<form class="form-horizontal" action="<?= BASE_URL ?>usmgmt" method="post" name="userlog" id="client_form">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">USER-FILTERED SYSTEM LOG</h3>
  </div>

<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible col-md-10" role="alert">';
                echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "Log has been archived successfully.";
                        break;
                    default:
                        echo "Log archive FAILED.  Please contact your administrator.";
                }
                echo '</div>';
            }
            
          
?>        

<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>


<!-- Text input-->
<div class="form-group">
  <div class="row">
    <div class="col-md-12">
      <?php extract($values); ?>
      <textarea rows="25" cols = "150" name="log" id="log" style="font-size:12px;" disabled><?= $results ?></textarea>    
    </div>
  </div>
  <div class="row"><p></p></div>
  <div class="row">
    <div class="col-md-4">
        <button id="submit" name="submit" value="usmgmt" class="btn btn-primary">GO BACK</button>
    </div>
  </div>
</div>
</fieldset>
</form>

<script>
  var textarea = document.getElementById('log');
  textarea.scrollTop = textarea.scrollHeight;
</script>
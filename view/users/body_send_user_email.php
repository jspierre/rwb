
<form class="form-horizontal" action="<?= BASE_URL ?>usmgmt" method="post" name="useremail" id="user_email">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">USER EMAIL</h3>
  </div>

<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "SUCCESS: Email sent and logged in system email logs.";
                        break;
                    default:
                        echo "FAILED: An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }
            
          
?>        

<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($_SESSION['loginnum'])) {
  $usernum = $_SESSION['loginnum'];
  echo '<input type="hidden" name="loginnum" value ='.$usernum.' />';
}

if ($_SESSION['usernum'] == $_SESSION['loginnum']) { $ownacct = true; } else { $ownacct = false; }
?>

<!-- Text input-->
  <?php
    $systememail = $this->registry->config['systememail'];
    $siteemail = $this->registry->config['siteemail'];
    extract($values);
  ?>
<div class="form-group">  
    <div class="row">
      <label class="col-md-2 control-label" for="sendfrom"><abbr title="Choose which email to use as the FROM on the email">Send Email From</abbr></label>
      <div class="col-md-6">
        <select id="sendfrom" name="sendfrom" class="form-select" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>>
          <option value="0" <?php if ($sendfrom==0) { echo "selected"; }?>>SYSTEM EMAIL - <?= $systememail ?></option>
          <option value="1" <?php if ($sendfrom==1 && ($action !== "ADD")) { echo "selected"; }?>>SUPPORT EMAIL - <?= $siteemail ?></option>
          <option value="2" <?php if ($sendfrom==2) { echo "selected"; }?>>YOUR ACCOUNT EMAIL - <?= $youremail ?></option>
        </select>
      </div>
    </div>
  </div>
  <?php
  if (!isset($emailbody) || empty($emailbody)) {
    $emailbody = "\n\n\nThank you,\n\n".$yourname."\n".$this->registry->config['sitename']."\nPhone: ".$this->registry->config['sitephone']."\nEmail: ".$this->registry->config['siteemail']."\n";
  }
  ?>
  <div class="form-group">
    <div class="row">
      <label class="col-md-2 control-label" for="subjectline"><abbr title="The subject line of the email to be sent.">Subject Line</abbr></label>  
      <div class="col-md-6">
        <input id="subjectline" name="subjectline" type="text" maxlength="255" placeholder="" class="form-control input-md" value="<?= $subjectline ?>" <?php if ($action == "DISPLAY") { echo "disabled"; } ?>  >
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="row">
      <label class="col-md-2 control-label" for="emailbody"><abbr title="The email you wish to send to the user.">Email Body:</abbr></label>
        <div class="col-md-6">
        <?php if ($values['action'] == "DISPLAY") { ?>
          <textarea rows="15" cols = "80" name="emailbody" id="emailbody" disabled><?= $emailbody ?></textarea>    
        <?php }
        else { ?>
          <textarea rows="15" cols = "80" name="emailbody" id="emailbody"><?= $emailbody ?></textarea>    
        <?php } ?>
      </div>
    </div>
  </div>

<div class="form-group col-md-8">
  <label class="col-md-3"></label>
      <?php
        if ($this->registry->security->checkFunction("useremailsend")) { 
          if (($ownacct==true && $this->registry->security->checkFunction("editownacct")) || $ownacct == false) {
            if ($values['action'] == "EDIT") { ?>
            <button id="submit" name="submit" value="sendemail" class="btn btn-warning btn-space">SEND EMAIL</button>
          <?php }
          }
        } 
      ?>

      <?php
        if ($values['action'] == "DISPLAY") { ?>
        <button id="submit" name="submit" value="usmgmt" class="btn btn-primary">RETURN TO USER</button>
      <?php }
      else { ?>
        <button id="submit" name="submit" value="usmgmt" class="btn btn-primary btn-space">DISCARD EMAIL</button>
      <?php }
      ?>
</div>


</fieldset>
</form>

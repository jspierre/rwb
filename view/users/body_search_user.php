
<form class="form-horizontal" action="<?= BASE_URL ?>searchuser" method="post" name="searchuser" id="user_form">
<fieldset>
<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
    <h3 class="panel-title">User Management Search</h3>
    <?php 
      if (isset($_SESSION['ticketno'])) { 
        ?> <h6 class="btn btn-sm btn-outline-info">SUPPORT TICKET <?= $_SESSION['ticketno'] ?> ACTIVE</h6> <?php 
      }
    ?>
    <hr />
</div>
<div class="panel-body">
             
  <?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                $searchterm = $_SESSION['searchterm'];
                $active = $_SESSION['searchactive'];
                $searchall = $_SESSION['searchall'];
                unset ($_SESSION['searchterm'], $_SESSION['searchactive']);
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                        ;
                switch ($_SESSION['returncode']) {
                    case 3:
                        echo "SUCCESS: User Data Cleared.";
                        break;
                    case 5:
                        echo "ERROR: No records were found.  Please try searching again.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }
            else {
              $active = 1;
              $searchall = 1;
            }
            
          
?>        

<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>


<!-- Text input-->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="searchterm">Search Term</label>  
    <div class="col-md-4">
    <input id="searchterm" name="searchterm" type="text" placeholder="Keywords or click SEARCH to list all" class="form-control input-md" value="<?= $searchterm?>" autofocus>
    </div>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="active">Status</label>
    <div class="col-md-4">
      <select id="active" name="active" class="form-select">
        <option value="1" <?php if ($active==1) { echo "selected"; }?>>Show Only Active Users</option>
        <option value="4" <?php if ($active==4) { echo "selected"; }?>>Show Only Disabled Users</option>
        <option value="3" <?php if ($active==3) { echo "selected"; }?>>Show Only Locked Out Active Users</option>
        <?php if ($this->registry->security->checkFunction("searchonline")) { ?> 
        <option value="2" <?php if ($active==2) { echo "selected"; }?>>Show Only Logged In Users</option>
        <?php } ?>
        <option value="0" <?php if ($active==0) { echo "selected"; }?>>Show ALL Users</option>
      </select>
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="searchall">Limit Search</label>
    <div class="col-md-4">
      <select id="searchall" name="searchall" class="form-select">
        <option value="1" <?php if ($searchall==1) { echo "selected"; }?>>Search All Possible Fields</option>
        <option value="0" <?php if ($searchall==0) { echo "selected"; }?>>Search Only Names</option>
        <option value="2" <?php if ($searchall==2) { echo "selected"; }?>>Search Only ACL Groups</option>
      </select>
    </div>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-8 btn-space">
      <button id="submit" name="submit" value="search" class="btn btn-primary">SEARCH</button>
      <?php if (isset($_SESSION['ticketno'])) { 
        $_SESSION['fromcontroller'] = "searchuser";
        ?>
        <a class="btn btn-primary btn-space" href="<?= BASE_URL ?>supportmaster" role="button">RETURN TO TICKET</a>  
      <?php }
      else { ?>
        <a class="btn btn-primary btn-space" href="<?= BASE_URL ?>" role="button">RETURN TO HOME</a>
      <?php } ?>
      <?php if ($this->registry->security->checkFunction("usradd") && (!isset($_SESSION['ticketno']))) { ?> <a class="btn btn-space btn-warning" href="<?= BASE_URL ?>usmgmt" role="button">ADD NEW USER</a> <?php } ?>
      <?php if (isset($_SESSION['loginnum'])) { ?> <button id="submit" name="submit" value="clear" class="btn btn-warning btn-space">CLEAR USER DATA</button> <?php } ?>
    </div>
  </div>
</div>


</fieldset>
</form>



<link href="<?= BASE_URL ?>css/signup.css" rel="stylesheet">
<form class="form-signin" action="<?= BASE_URL ?>signup" method="post" name="user_form" id="user_form">
      <div class="text-center mb-4">
        
        <?php $sitename = $this->registry->config['sitename']; ?>
        <h1 class="h3 mb-3 font-weight-normal">Sign Up for <?= $sitename ?></h1>
        
        <?php
          if ($this->registry->config['loginwithemail']) { $untype = "email"; } else { $untype = "username"; }
        	// If there's a return code, display message and deal with it.
        	if ($values['returncode']) {
        		if ($values['returncode'] == 5) {
        			    echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                	echo '  <p>Oops! That '.$untype.' is already signed up!  Want to Log In instead?  <a href="login">Just click here</a>!</p>';
                	echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                  echo '</div>';
                	extract($values['result'][2]['tempdata']);
                }
                if ($values['returncode'] == 13) {
                  echo '<div class="alert alert-danger alert-dismissible" role="alert">';
                  echo '  <p>Oops! That email address is already signed up!  Want to Log In instead?  <a href="login">Just click here</a>!</p>';
                  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                  echo '</div>';
                  extract($values['result'][2]['tempdata']);
                }
                if ($values['returncode'] == 7) {
        			   echo '<div class="alert alert-danger alert-dismissible" role="alert">';
                	echo '  <p>Oops! That email does not appear to be a valid email address.  Try again, please.</p>';
                  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                	echo '</div>';
                	extract($values['result'][2]['tempdata']);	
                }
                if ($values['returncode'] == 8) {
                  echo '<div class="alert alert-danger alert-dismissible" role="alert">';
                  echo '  <p>Oops! That Offer Code is not valid.<br/> Try again, please.</p>';
                  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                  echo '</div>';
                  extract($values['result'][2]['tempdata']);  
                }
                if ($values['returncode'] == 9) {
                  echo '<div class="alert alert-danger alert-dismissible" role="alert">';
                  echo '  <p>Oops! Please enter your <em>full</em> first and last names. Try again, please.</p>';
                  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                  echo '</div>';
                  extract($values['result'][2]['tempdata']);  
                }
                if ($values['returncode'] == 11) {
        			   echo '<div class="alert alert-danger alert-dismissible" role="alert">';
                	echo '  <p>Oops! We require you to agree to our terms and conditions before signing up.  Please see the checkbox below.</p>';
                  echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                	echo '</div>';
                	extract($values['result'][2]['tempdata']);	
                }
        	}
        	// If there's no return message, initialize the variables and prepare for a new form.
        	else {
        		?>
        		<p>Never fear! Signup is quick and painless... plus there's NO credit card required to sign up!</p>
        		<p>Already have an account?  <a href="login">Sign in here!</a></p>
        		<?php		
        		$firstname =""; $lastname=""; $username=""; $emailaddress=""; $tcagree=0; $offercode = ""; $check="";
        	}
        ?>
        
        
      </div>

      <div class="form-label-group">
        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="First Name" value="<?= $firstname ?>" required autofocus>
        <label for="firstname">First Name</label>
      </div>

      <div class="form-label-group">
        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last Name" value="<?= $lastname ?>" required autofocus>
        <label for="lastname">Last Name</label>
      </div>
<?php
      if($this->registry->config['loginwithemail']) { $type="email"; $label="Email Address";} else  { $type="text"; $label="Username";}
?>
      <div class="form-label-group">
        <input type="<?= $type?>" id="inputUser" name="username" class="form-control" placeholder="<?= $label ?>" value="<?= $username ?>" required autofocus>
        <label for="inputUser"><?= $label ?></label>
      </div>
<?php
      if($this->registry->config['loginwithemail'] == false) { ?>
      <div class="form-label-group">
        <input type="email" id="inputEmail" name="emailaddress" class="form-control" placeholder="Email Address" value="<?= $emailaddress ?>" required autofocus>
        <label for="inputEmail">Email Address</label>
      </div>
      <?php
    }
?>
<?php
      if($this->registry->config['offercodeon']) { ?>
      <div class="form-label-group">
        <input type="text" id="offercode" name="offercode" class="form-control" placeholder="Offer Code" value="<?= $offercode ?>" autofocus>
        <label for="offercode">Offer Code (Optional)</label>
      </div>
      <?php
    }
?>




      <div class="checkbox mb-3">         
        <label>
         <input type="hidden" name="tcagree" value="0" />           
         <input type="checkbox" name="tcagree" value="1" required> I have read and agree to the <a href="tandc" target="_blank">terms of use</a>.
       </label>       
     </div>

      <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="new" onClick="showWait()">Sign Up</button>
      <div class = "text mb-4">
      	<p></p>
      	<p><small>IMPORTANT NOTE: By creating an account on this system, you agree to be bound by our <a href="tandc">terms of use</a>. Do not create an account if you do not agree to these terms.</small></p>
      </div>

    </form>


<script>
function showWait() {
  $("#user_form").validate({
     submitHandler: function(form){
      myApp.showPleaseWait();
      form.submit();
     }
  });
}
</script>


<script>
var myApp;
myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Creating Your Account - Please Wait...</h5></div><div class="modal-body" align="center"><img src="css/loading.gif" width="75%"></div><div class="modal-footer"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };
})();
</script>


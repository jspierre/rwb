 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script>
  $(document).ready(function(){
    // Make sure mobile number is only numbers
    $(".phonenumber").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl+A, Command+A
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
           // Allow: home, end, left, right, down, up
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
    });
    });
</script>


<form class="form-horizontal" action="<?= BASE_URL ?>usmgmt" method="post" name="newuser" id="user_form">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
<?php
//var_dump($_SESSION);
  //var_dump($_POST);
  //exit();
 //var_dump($values['user']);
  
  if (isset($values['aclgroups'])) {
    $aclgroups = $values['aclgroups'];
  }

  if(isset($values['randompw'])) {
    $randompw = $values['randompw'];
  }

  // Determine what type of thing we are doing - new, display, or edit?
  if (isset($values['actiontype'])) {
    $actiontype = $values['actiontype'];
    extract($values['user']);
  }
  else {
    $actiontype = "NEW";
  }

  if (isset($values['returncode']) && ($values['returncode'] == 5 || $values['returncode'] == 7 || $values['returncode'] == 8 || $values['returncode'] == 11 || $values['returncode'] == 13)) { $displaycompany = false; } else { $displaycompany = true; }


  if (isset($values['user']) && $displaycompany) {
    if (isset($values['user']['companyname']) && !empty($values['user']['companyname'])) {
      $displayname = $values['user']['firstname']." ".$values['user']['lastname']."  (".$values['user']['companyname'].")";
    }
    else {
      $displayname = $values['user']['firstname']." ".$values['user']['lastname'];   
    }
    if ($values['returncode'] == 12 || $values['returncode'] == 15) {
      echo '<h3 class="panel-title">MEMBER #  '.$values['user']['loginnum'].' - '.$displayname;
      $values['user']['usernum'] = $values['user']['loginnum'];
    }
    else {
     echo '<h3 class="panel-title">MEMBER #  '.$values['user']['usernum'].' - '.$displayname; 
    }
  }
  else {
    if (isset($values['user']['actiontype']) && $values['user']['actiontype'] == "UPDATE") {
      echo '<h3 class="panel-title">Update User</h3>'; 
    }
    else {
      echo '<h3 class="panel-title">New User Entry Form</h3>';
    }
  }

  if (isset($_SESSION['ticketno'])) { 
        ?><div class="spacer-half"><h6 class="btn btn-sm btn-outline-info">SUPPORT TICKET <?= $_SESSION['ticketno'] ?> ACTIVE</h6></div> <?php 
    }


if (isset($values['user']['usernum'])) {  
  if ($_SESSION['usernum'] == $values['user']['usernum']) { $ownacct = true; } else { $ownacct = false; }
  $isanadmin=false; 
  foreach ($aclgroups as $acl) {
    if ($acl['groupindex'] == $values['user']['aclgroup']) {
      if ($acl['isstaff'] == 1) { $isastaff = true; } else { $isastaff = false; }
      if ($acl['isadmin'] == 1) { $isanadmin = true; } else { $isanadmin = false; }
    }
  }
}


?>
</div>
<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($values['returncode']) && $values['returncode'] > 0) {
                if ($values['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $values['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                        
                switch ($values['returncode']) {
                    case 1:
                        echo "SUCCESS: User associated with Support Ticket # ".$_SESSION['ticketno'];
                        break;
                    case 2:
                        echo "User created with PASSWORD: ".$values['returnpass'].". Please check settings to make sure all is correct!";
                        break;
                    case 3:
                        echo "User Updated.";
                        break;
                    case 4:
                        echo "User Updated. NOTE: COULD NOT REACTIVATE USER DUE TO HR RESTRICTIONS.";
                        break;
                    case 5:
                        $loginnum = $values['user']['loginnum'];
                        $link = '<a href="'.BASE_URL.'usmgmt/'.$loginnum.'">';
                        echo "ERROR: Username Already Exists - See ".$link."User # $loginnum</a>";
                        extract($values['user']);
                        break;
                    case 6:
                        $loginnum = $values['error']['loginnum'];
                        echo "ERROR: Invalid User Number: $loginnum.  Add new user or try again.";
                        break;
                    case 7:
                        echo "ERROR: Primary Email Address is not valid. Correct and try again.";
                        extract($values['user']);
                        break;
                    case 8:
                        echo "ERROR: Email Address is not valid. Correct and try again.";
                        extract($values['user']);
                        break;
                    case 9:
                        echo "ERROR: Primary Email Address is not valid. Correct and try again.";
                        extract($values['user']);
                        break;
                    case 10:
                        echo "ERROR: Secondary Email Address is not valid. Correct and try again.";
                        extract($values['user']);
                        break;
                    case 11: 
                        echo "ERROR: Unable to associate user with Support Ticket # ".$_SESSION['ticketno'];
                        break;
                     case 12:
                      //var_dump($values);
                      $dupnum = $values['user']['dupnum'];
                      $link = '<a href="'.BASE_URL.'usmgmt/'.$dupnum.'">';
                      echo "ERROR: Username Already Exists - See ".$link."User # $dupnum</a>";
                      extract($values['user']);
                      break;
                    case 13:
                      $loginnum = $values['user']['loginnum'];
                      $link = '<a href="'.BASE_URL.'usmgmt/'.$loginnum.'">';
                      echo "ERROR: Email Address Already Exists - See ".$link."User # $loginnum</a>";
                      extract($values['user']);
                      break;
                    case 14: 
                        echo "ERROR: Unable to process request. Try again or contact administrator.";
                        break;
                    case 15:
                        echo "CRITICAL ERROR: Database Error. Please contact your administrator. Data not saved.";
                        extract($values['user']);
                        break;
                    case 17:
                        $code = $values['code'];
                        echo "CODE SENT: Ask the user for the code now. The CODE IS: <strong>".$code."</strong>";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }                    
?>        


<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
//var_dump($values['user']);
if (isset($values['loginnum'])) {
  $loginnum = $values['loginnum'];
  echo '<input type="hidden" name="loginnum" value ='.$loginnum.' />';
}
if (isset($values['user']['lastloginip'])) {
  $lastloginip = $values['user']['lastloginip'];
  echo '<input type="hidden" name="lastloginip" value ='.$lastloginip.' />';
}


?>


<!-- Button Group for Display Only -->
<?php
if ($actiontype == "DISPLAY") {
  if (isset($_SESSION['ticketno'])) { $back = "BACK TO TICKET"; } else { $back = "BACK TO RESULTS"; }
  ?>
<div class="form-group col-md-8">
  <?php
  if (isset($_SESSION['ticketno'])) { ?>
        <button type="submit" id="submit" name="submit" class="btn btn-success btn-sm btn-space" value="ticket">BACK TO TICKET</button>
  <?php }
  if (isset($_SESSION['ticketno']) && isset($_SESSION['tocontroller']) || !isset($_SESSION['ticketno'])) { ?>
        <button type="submit" id="submit" name="submit" class="btn btn-success btn-sm btn-space" value="search">BACK TO SEARCH</button>
  <?php } ?>
        <?php
        if ($this->registry->security->checkFunction("associate") && isset($_SESSION['ticketno']) && isset($_SESSION['tocontroller']) && $_SESSION['tocontroller'] == "searchuser") { ?>
          <button type="submit" id="submit" name="submit" title="Associate the current support ticket with this user" value="associate" class="btn btn-sm btn-warning btn-space" onclick="return confirm('Are you sure you want to associate ticket <?= $_SESSION['ticketno'] ?> with this user?')">ASSOCIATE WITH TICKET</button>
        <?php 
        }
        ?>
</div>
<div class="form-group col-md-8 btn-space">
      <?php
        if ($this->registry->security->checkFunction("useredit")) {
          if (($isanadmin==true && $this->registry->security->checkFunction("adminedit")) || $isanadmin==false ) {   
            if (($ownacct==true && $this->registry->security->checkFunction("editownacct")) || $ownacct == false ) {
              ?>
              <button type="submit" id="submit" title="Edit the information below about this user" name="submit" value="edit" class="btn btn-sm btn-warning btn-space">EDIT USER</button>
      <?php 
          }
        }
      }
      ?>
      <?php
        if ($this->registry->security->checkFunction("usernotes")) { ?>
          <button type="submit" id="submit" title="View or Edit the internal notes about this user (only seen internally)" name="submit" value="notes" class="btn btn-sm btn-primary btn-space">USER NOTES</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("userlog")) { ?>
          <button type="submit" id="submit" title="View the change and event log for this user" name="submit" value="log" class="btn btn-sm btn-primary btn-space">USER LOG</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("usersystemlog")) { ?>
          <button type="submit" id="submit" title="View the system log filtered just for this user" name="submit" value="syslog" class="btn btn-sm btn-primary btn-space">SYSTEM LOG</button>
      <?php 
        }
      ?>
      <?php
        if ($ownacct==false && $this->registry->security->checkFunction("impersonate")) { ?>
          <button type="submit" id="submit" title="Log In as this user - NOTE: You will be LOGGED OUT when this happens and will become this user instead. USE WITH CARE." name="submit" value="impersonate" class="btn btn-sm btn-warning btn-space" onclick="return confirm('Are you sure? You will be logged out and will become this user.')">IMPERSONATE</button>
      <?php 
        }
      ?>
      <?php 
        if ($ownacct==false && $this->registry->security->checkFunction("forceuserpw")) { ?>
          <button type="submit" id="submit" title="Force this user to IMMEDIATELY change their password if logged in, or on next log on if not logged in" name="submit" value="forceuserpw" class="btn btn-sm btn-warning btn-space" onclick="return confirm('Are you sure? This user will be forced to change their password immediately.')">FORCE PW CHANGE</button>
      <?php 
        }
      ?>
      <?php 
        if ($isalive==1 && $ownacct==false && $this->registry->security->checkFunction("forcelogoff")) { ?>
          <button type="submit" id="submit" title="Force this user to IMMEDIATELY be logged out" name="submit" value="forcelogoff" class="btn btn-sm btn-warning btn-space" onclick="return confirm('Are you sure? This user will be logged off immediately and could lose work in progress.')">FORCE LOGOFF</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("forceuser2fa") && $tfalogin == 1) { ?>
          <button type="submit" id="submit" title="Force this user to reverify with Two-Factor Authorization the next time they log in (forget all saved devices)" name="submit" value="forceuser2fa" class="btn btn-sm btn-warning btn-space" onclick="return confirm('Are you sure? This user will lose all saved locations and be forced to re-verify through 2FA at next login.')">FORCE 2FA</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("useremailsend")) { ?>
          <button type="submit" id="submit" title="Compose and Send an Email to this user" name="submit" value="emailuser" class="btn btn-sm btn-primary btn-space">EMAIL USER</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("vvsms") && !empty($mobilephone) && $oktotext == 1 && $ownacct == false) { ?>
          <button type="submit" id="submit" title="Send a text to the user with a code to verify that you are talking to the actual user" name="submit" value="vvsms" class="btn btn-sm btn-primary btn-space" onclick="return confirm('Are you sure? TELL THE USER: I do have to advise you that your normal text messaging rates will apply to this message - are you okay with that? CONFIRM LAST FOUR DIGITS OF PHONE NUMBER: <?= $mobilephone ?>.')">VOICE VERIFY - SMS</button>
      <?php 
        }
      ?>
      <?php
        if ($this->registry->security->checkFunction("vvemail") && !empty($emailaddress) && $ownacct == false) { ?>
          <button type="submit" id="submit" name="submit" title="Send an email to the user with a code to verify that you are talking to the actual user" value="vvemail" class="btn btn-sm btn-primary btn-space" onclick="return confirm('Are you sure? ASK THE USER TO CONFIRM THEIR EMAIL ADDRESS: <?= $emailaddress ?>.')">VOICE VERIFY - EMAIL</button>
      <?php 
        }
      ?>
      <?php
        if ($values['numtickets'] == "") { $totaltickets = 0; } else { 
          $numtickets = $values['numtickets']; $opentickets = $numtickets[0]; $closedtickets = $numtickets[1]; $totaltickets = $numtickets[2];
        }
        if ($this->registry->security->checkFunction("listtickets") && !isset($_SESSION['ticketno']) && $totaltickets > 0) { ?>
          <button type="submit" id="submit" title="See all support tickets for this user - <?= $opentickets ?> OPEN TICKETS - <?= $closedtickets ?> CLOSED TICKETS." name="submit" value="listtickets" class="btn btn-sm btn-primary btn-space" >SUPPORT TICKETS (<?= $opentickets ?>)</button>
      <?php 
        }
      ?>
      <?php 
        if ($this->registry->security->checkFunction("createticket") && !isset($_SESSION['ticketno'])) { ?>
          <button id="submit" name="submit" value="createticket" title="Create a new Support Ticket for this user if they have an issue that you cannot resolve in one call" class="btn btn-sm btn-warning btn-space" onclick="return confirm('Are you sure you want to create a new Support Ticket for this user? They will be notified by email!')">CREATE USER TICKET</button>
       <?php 
        } 
      ?>
      <?php 
        if ($this->registry->security->checkFunction("unbanuserip") && $isbanned) { ?> 
          <button id="submit" name="submit" value="unbanuserip" title="Remove ban from user's last login IP (<?= $lastloginip ?>)" class="btn btn-sm btn-warning btn-space" onclick="return confirm('Are you sure you want to unban IP Address <?= $lastloginip ?>?')">UNBAN LAST IP</button>
       <?php 
        } 
      ?>
      <?php
        if ($ownacct==false && $this->registry->security->checkFunction("deleteuser")) { ?>
          <button type="submit" id="submit" name="submit" title="Delete this user completely from the system - DANGER - Change User System Status to INACTIVE instead if possible." value="delete" class="btn btn-sm btn-danger btn-space" onclick="return confirm('Are you sure? THIS ACTION IS NOT REVERSIBLE!')">DELETE USER</button>
      <?php 
        }
      ?>
      <?php
        if (isset($this->registry->config['usr_plugins']) && $this->registry->config['usr_plugins']) {
          $plugins = $this->registry->config['usr_plugins'];
          foreach ($plugins as $plugin) {
            $chkfunc = $plugin['button_function'];
            $butdesc = $plugin['button_description'];
            $butval = $plugin['button_submit_value'];
            $butclass = $plugin['button_color_class'];
            $butname = strtoupper($plugin['button_name']);
            if ($this->registry->security->checkFunction($chkfunc)) { ?>
              <button type="submit" id="submit" name="submit" title="<?= $butdesc ?>" value="<?= $butval ?>" class="btn btn-sm btn-space <?= $butclass ?>"><?= $butname ?></button> 
            <?php  } 
          } 
        } ?>
</div>
<?php
}
?>

<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <input id="loginnum" name="loginnum" type="hidden" value=<?= $values['user']['usernum'] ?>>
    <?php if ($this->registry->config['loginwithemail']) { ?>
    <label class="col-md-2 control-label" for="username"><abbr title="This will be the Email address the user logs in with">User Email</abbr></label>  
    <div class="col-md-6">
      <input id="username" name="username" type="email" placeholder="User's Email Address" class="form-control input-md" value="<?= $username ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> required autofocus >
    <?php  }
    else { ?>
    <label class="col-md-2 control-label" for="username"><abbr title="This will be the username the user logs in with">Username</abbr></label>  
    <div class="col-md-6">
      <input id="username" name="username" type="text" placeholder="Username" class="form-control input-md" value="<?= $username ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> required autofocus >
    <?php  } ?> 
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="aclgroup"><abbr title="What type of access should this user have on this system?">Access Level</abbr></label>
    <div class="col-md-6">
      <select id="aclgroup" name="aclgroup" class="form-select" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
          <?php
            foreach ($aclgroups as $title) {
              if (($actiontype == "NEW" && $title['isadmin'] == 1 && $this->registry->security->checkFunction("createadmins") == false) || ($actiontype == "NEW" && $title['isactive'] == 0)) {
                echo '';
              }
              else {
                if ($title['isactive'] == 0) { $title['groupname'] = $title['groupname']." [INACTIVE]"; }
                if ($title['groupindex'] == $aclgroup || ($title['groupindex']==3 && $actiontype=="NEW")) {
                  echo '<option value="'.$title['groupindex'].'" title = "'.$title['groupname'].'" selected>'.$title['groupname'].'</option>';
                }
                else {
                  echo '<option value="'.$title['groupindex'].'" title = "'.$title['groupname'].'">'.$title['groupname'].'</option>';
                }
            }
          }
          ?>
      </select>
    </div>
  </div>
    <div class="row">
      <label class="col-md-2 control-label" for="isanadmin"><abbr title="Is the User an Administrator in this System?">Administrator</abbr></label>  
      <?php if ($isanadmin==1) { $ia = "YES"; } else { $ia = "NO"; } ?>
      <div class="col-md-2">
        <input id="isanadmin" name="isanadmin" type="text" placeholder="" class="form-control input-md" value="<?= $ia ?>" disabled >
      </div>
      <label class="col-md-2 control-label" for="isastaff"><abbr title="Is the User an Employee/Staff Member?">Staff Member</abbr></label>  
      <?php if ($isastaff==1) { $is = "YES"; } else { $is = "NO"; } ?>
      <div class="col-md-2">
        <input id="isastaff" name="isastaff" type="text" placeholder="" class="form-control input-md" value="<?= $is ?>" disabled >
      </div>
  

</div>

  <div class="row">
    <label class="col-md-2 control-label" for="active"><abbr title="Is the member allowed to log in and use this system? This is usually set manually and is used to ban abusive users, etc.">User System Status</abbr></label>
    <div class="col-md-2">
      <select id="useractive" name="useractive" class="form-select" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>>
        <option value="1" <?php if ($useractive==1) { echo "selected"; }?>>Active</option>
        <option value="0" <?php if ($useractive==0 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>Disabled</option>
      </select>
    </div>
    <label class="col-md-2 control-label" for="locked"><abbr title="Locks are meant to be temporary and can be triggered by the system. When locked, User cannot log in to the system, but their account remains active">User Locked Status</abbr></label>
    <div class="col-md-2">
      <select id="userlocked" name="userlocked" class="form-select" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>>
       <option value="0" <?php if ($userlocked==0 ) { echo "selected"; }?>>Unlocked</option>
       <option value="1" <?php if ($userlocked==1 ) { echo "selected"; }?>>Locked</option>
      </select>
    </div>
  </div>

    <div class="row">
    <label class="col-md-2 control-label" for="datecreated"><abbr title="The date this user was first created">Date User Created</abbr></label>  
    <div class="col-md-2">
      <input id="datecreated" name="datecreated" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $datecreated ?>" disabled >
    </div>
    <label class="col-md-2 control-label" for="selfcreated"><abbr title="Did the user create their own account (as opposed to an admin)?">Self-Created User</abbr></label>  
    <?php if ($selfcreated==1) { $sc = "YES"; } else { $sc = "NO"; } ?>
    <div class="col-md-2">
      <input id="selfcreated" name="selfcreated" type="text" placeholder="" class="form-control input-md" value="<?= $sc ?>" disabled >
    </div>
  </div>

    <div class="row">
    <label class="col-md-2 control-label" for="tcagree"><abbr title="Has the User agreed to our T&Cs?">T&C Agreed To</abbr></label>  
    <?php if ($tcagree==1) { $tca = "YES"; } else { $tca = "NO"; } ?>
    <div class="col-md-2">
      <input id="tcagree" name="tcagree" type="text" placeholder="" class="form-control input-md" value="<?= $tca ?>" disabled >
    </div>
    <label class="col-md-2 control-label" for="lasttcagree"><abbr title="The last date and time the user explicitly agreed to our T&Cs">Date Last Agreed<abbr></label>  
    <div class="col-md-2">
      <input id="lasttcagree" name="lasttcagree" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $lasttcagree ?>" disabled >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="tfalogin"><abbr title="Has the User enabled 2FA on their account?">Two-Factor Enabled</abbr></label>  
    <?php if ($tfalogin==1) { $tfa = "YES"; } else { $tfa = "NO"; } ?>
    <div class="col-md-2">
      <input id="tfalogin" name="tfalogin" type="text" placeholder="" class="form-control input-md" value="<?= $tfa ?>" disabled >
    </div>
    <label class="col-md-2 control-label" for="tfaverified"><abbr title="The last date and time the user set up 2FA on their account.">Date 2FA Setup<abbr></label>  
    <div class="col-md-2">
      <input id="tfaverified" name="tfaverified" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $tfaverified ?>" disabled >
    </div>
  </div>

  
  <div class="row">
    <label class="col-md-2 control-label" for="passexpires"><abbr title="The date this user's password will expire, forcing them to create a new password for security.">Password Expires</abbr></label>  
    <div class="col-md-2">
      <input id="passexpires" name="passexpires" type="text" placeholder="" class="form-control input-md" value="<?= $passexpires ?>" <?php if ($actiontype == "DISPLAY" || $actiontype == "NEW") { echo "disabled"; } ?> >
    </div>
    <label class="col-md-2 control-label" for="firstname"><abbr title="The number of times someone tried to log in to this account with the wrong password since last successful login">Wrong PW Attempts</abbr></label>  
    <div class="col-md-2">
      <input id="pwattempts" name="pwattempts" type="text" placeholder="" class="form-control input-md" value="<?= $pwattempts ?>" <?php if ($actiontype == "DISPLAY" || $actiontype == "NEW") { echo "disabled"; } ?> >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="lastlogin"><abbr title="The last date and time this user logged in to the system.">Last Login Date</abbr></label>  
    <div class="col-md-2">
      <input id="lastlogin" name="lastlogin" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $lastlogin ?>" disabled >
    </div>
    <?php if ($isbanned) {$iplabel = "<strong>Last IP **BANNED**</strong>"; } else {$iplabel = "Last Login IP Address";} ?>
    <label class="col-md-2 control-label" for="lastloginip"><abbr title="The last IP address this user logged in from"><?= $iplabel ?><abbr></label>  
    <div class="col-md-2">
      <input id="lastloginip" name="lastloginip" type="text" placeholder="" class="form-control input-md" value="<?= $lastloginip ?>" disabled >
    </div>
  </div>
    <div class="row">
    <label class="col-md-2 control-label" for="isalive"><abbr title="Is the User currently logged in to the system?">Logged In Now</abbr></label>  
    <div class="col-md-2">
      <?php if ($isalive == 1) { $loggedin = "YES"; } else { $loggedin = "NO"; } ?>
      <input id="loggedin" name="loggedin" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $loggedin ?>" disabled >
    </div>
    <label class="col-md-2 control-label" for="module"><abbr title="The last module this user accessed">Last Module Used<abbr></label>  
    <div class="col-md-2">
      <?php if ($module=="") { $module = "-none-"; } ?>
      <input id="module" name="module" type="text" placeholder="" class="form-control input-md" style="font-size:14px;" value="<?= $module ?>" disabled >
    </div>
  </div>
</div>


<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="password"><?php if ($actiontype =="NEW") {
      echo "<abbr title='The initial password user will use to log in the first time.\nOne has been randomly generated for you, but you can overwrite this if desired.\nThey will be fored to immediately change their password for security on first login.'>Initial ";} 
      else {
        echo "<abbr title='Only fill this in if you are RESETTING the user password- otherwise, leave BLANK'>";
      } ?>New Password:</abbr></label>  
    <div class="col-md-6">
    <?php if ($actiontype == "NEW") { 
      $password = $randompw;
      ?>
      <input id="password" name="password" type="text" placeholder="" class="form-control input-md" value="<?= $password ?>" required >
     <?php } 
          elseif ($actiontype == "DISPLAY" || ($this->registry->security->checkFunction("userresetpw") == false)) { ?>
      <input id="password" name="password" type="text" placeholder="" class="form-control input-md" value="********************"  disabled >
    <?php }
          else { ?>
      <input id="password" name="password" type="text" class="form-control input-md" value="" placeholder="Leave this BLANK unless resetting password" >   
     <?php }
     ?>   
    </div>
  </div>
<?php if ($actiontype == "NEW") { ?>
  <div class="row">
    <label class="col-md-2 control-label" for="sendcredentials"><abbr title="Should the system email the user with a welcome message and their new credentials? This is ONLY available when setting up a new user for the first time.">Send Credentials</abbr></label>
    <div class="col-md-2">
      <select id="sendcredentials" name="sendcredentials" class="form-select" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>>
        <option value="1" "selected">YES</option>
        <option value="0">NO</option>
      </select>
    </div>
  </div>
<?php } ?>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="firstname">First Name</label>  
    <div class="col-md-6">
      <input id="firstname" name="firstname" type="text" placeholder="First Name of the User" class="form-control input-md" required value="<?= $firstname ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>  >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="lastname">Last Name</label>  
    <div class="col-md-6">
    <input id="lastname" name="lastname" type="text" placeholder="Last Name of the User" class="form-control input-md" required value="<?= $lastname ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>
<?php
  if ($this->registry->config['loginwithemail'] == false) { ?>
  <div class="row">
    <label class="col-md-2 control-label" for="username"><abbr title="This is the user's email address">Email Address</abbr></label>  
    <div class="col-md-6">
        <input id="emailaddress" name="emailaddress" type="email" placeholder="User's Email Address" class="form-control input-md" value="<?= $emailaddress ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> required autofocus >
    </div>
  </div>
<?php } ?>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="billingaddress1"><abbr title='The Primary and/or address of the user that (for billing, must match their credit card)'>Primary (Bill) Address</abbr></label>  
    <div class="col-md-6">
      <input id="billingaddress1" name="billingaddress1" type="text" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional - Only needed upon Credit Card Charge"'; } ?> class="form-control input-md"  value="<?= $billingaddress1 ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>  >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="billingaddress2">Address Line 2</label>  
    <div class="col-md-6">
    <input id="billingaddress2" name="billingaddress2" type="text" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $billingaddress2 ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="billingcity">City</label>
    <div class="col-md-6">
      <input id="billingcity" name="billingcity" type="text" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $billingcity ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="billingstate">State</label>
    <div class="col-md-2">
      <input id="billingstate" name="billingstate" type="text" maxlength="2" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $billingstate ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
    <label class="col-md-1 control-label" for="billingzip">Zipcode</label>
    <div class="col-md-2">
      <input id="billingzip" name="billingzip" type="text" maxlength="10" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $billingzip ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>      
    <div class="row">
    <label class="col-md-2 control-label" for="billingphone"><abbr title="This should be the primary phone number to use to contact the user by VOICE (not text)">Primary Phone</abbr></label>  
    <div class="col-md-6">
      <input id="billingphone" type="text" maxlength="10" name="billingphone"<?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md phonenumber"  <?php if ($actiontype == "DISPLAY") { echo 'value="'.$this->prettyPhone($billingphone).'" disabled'; } else  { echo 'value="'.$billingphone.'"'; } ?>>
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="mobilephone"><abbr title="This should be the mobile phone number that can be used for TEXT and VOICE to the user - this is the number that WILL be used when sending texts. It can be the same as the Primary Phone">Mobile Phone</abbr></label>  
    <div class="col-md-6">
      <input id="mobilephone" type="text" maxlength="10" name="mobilephone"<?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md phonenumber"  <?php if ($actiontype == "DISPLAY") { echo 'value="'.$this->prettyPhone($mobilephone).'" disabled'; } else  { echo 'value="'.$mobilephone.'"'; } ?>>
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="okaytoemail"><abbr title="Do you have permission to send MARKETING email this member? NOTE: Even if 'no', you can still send system/account emails to them.">Emails?</abbr></label>
    <div class="col-md-2">
      <select id="okaytoemail" name="okaytoemail" class="form-select" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>>
       <option value="1" <?php if ($okaytoemail==1 ) { echo "selected"; }?>>Okay to Email</option>
       <option value="0" <?php if ($okaytoemail==0 ) { echo "selected"; }?>>Do Not Email</option>
      </select>
    </div>
    <label class="col-md-1 control-label" for="oktotext"><abbr title="Do you have permission to text this member? WARNING: If you set this to 'Do Not Text', 2FA will be turned OFF for this user">Texting?</abbr></label>
    <div class="col-md-2">
      <select id="oktotext" name="oktotext" class="form-select" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>>
       <option value="1" <?php if ($oktotext==1 ) { echo "selected"; }?>>Okay to Text</option>
       <option value="0" <?php if ($oktotext==0 ) { echo "selected"; }?>>Do Not Text</option>
      </select> 
    </div>
  </div>
</div>

<div class = "form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="mailingaddr1"><abbr title='The mailing address of the user ONLY if it is different from the billing address above'>Mailing Address</abbr></label>  
    <div class="col-md-6">
      <input id="mailingaddr1" name="mailingaddr1" type="text" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional - ONLY if different from BILLING Address"'; } ?> class="form-control input-md"  value="<?= $mailingaddr1 ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?>  >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="mailingaddr2">Mailing Addr. 2</label>  
    <div class="col-md-6">
    <input id="mailingaddr2" name="mailingaddr2" type="text" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $mailingaddr2 ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="mailingcity">City</label>
    <div class="col-md-6">
      <input id="mailingcity" name="mailingcity" type="text" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $mailingcity ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>
  <div class="row">
    <label class="col-md-2 control-label" for="mailingstate">State</label>
    <div class="col-md-2">
      <input id="mailingstate" name="mailingstate" type="text" maxlength="2" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $mailingstate ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
    <label class="col-md-1 control-label" for="mailingzip">Zipcode</label>
    <div class="col-md-2">
      <input id="mailingzip" name="mailingzip" type="text" maxlength="10" <?php if ($actiontype !== "DISPLAY") { echo 'placeholder="Optional"'; } ?> class="form-control input-md"  value="<?= $mailingzip ?>" <?php if ($actiontype == "DISPLAY") { echo "disabled"; } ?> >
    </div>
  </div>      


<div class = "form-group">

  
</div>


<!-- Button Group for Display Only -->
<?php
if ($actiontype == "DISPLAY") {
  ?>
<div class="form-group btn-space">
        <button type="submit" id="submit" name="submit" class="btn btn-success btn-sm" value="search"><?= $back ?></button>
</div>
<?php
}
?>

<!-- Button -->
<?php if ($actiontype !== "DISPLAY") {
  ?>
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="submit"></label>
    <div class="col-md-4 btn-space">
      <?php if ($actiontype == "NEW" || $values['returncode'] == 5) {
              unset($_SESSION['loginnum']);
              ?><button id="submit" name="submit" value="new" class="btn btn-warning">ADD NEW USER</button>
                <a class="btn btn-primary" href="<?= BASE_URL ?>searchuser" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-warning">UPDATE USER</button>
              <button id="submit" name="submit" value="usmgmt" class="btn btn-primary">CANCEL</button>
              <?php
            }?>
      
    </div>
  </div>
</div>
<?php 
}

?>

</fieldset>
</form>



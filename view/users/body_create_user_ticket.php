
<form class="form-horizontal" action="<?= BASE_URL ?>usmgmt" method="post" name="createticket" id="createticket">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title spacer">NEW SUPPORT TICKET</h3>
  </div>

<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                        
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "Ticket Created. See SupportMaster for further action on this ticket";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }
            
          
?>        

<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($values['loginnum'])) {
  $usernum = $values['loginnum'];
  echo '<input type="hidden" name="loginnum" value ='.$usernum.' />';
}

if (isset($values['values'])) {
  extract ($values['values']);
}

?>

<!-- Text input-->
<div class="form-group">
  <div class="row">
  <label class="col-md-2 control-label" for="convsubject"><abbr title="The subject of the conversation">Subject</abbr></label>  
    <div class="col-md-6">
      <input id="convsubject" name="convsubject" type="text" maxlength="255" placeholder="Enter a Subject Here" class="form-control input-md" value="<?= $convsubject ?>" required <?php if ($values['action'] == "DISPLAY" ) { echo "disabled"; } ?> >
    </div>
  </div>
  <div class="row">
  <label class="col-md-2 control-label" for="messagebody"><abbr title="The message received or sent in this particular interaction.">Message</abbr></label>  
    <div class="col-md-6">
      <textarea id="messagebody" name="messagebody" rows="5" placeholder="Enter the details of the support ticket here" class="form-control input-md" required <?php if ($values['action'] == "DISPLAY" ) { echo "disabled"; } ?>><?= $messagebody ?></textarea>
    </div>
  </div>
</div>

<div class="form-group">
    <div class="row">
      <div class="col-md-2"></div>
      <div class = "col-md-6">
      <?php
        if ($this->registry->security->checkFunction("createticket")) { 
            if ($values['action'] == "ADD") { ?>
              <button id="submit" name="submit" value="submitticket" class="btn btn-warning btn-sm btn-space" onClick="showWait()">SUBMIT NEW TICKET</button>
          <?php }
          }
      ?>

      <?php
        if ($values['action'] == "DISPLAY") { 
        $_SESSION['loginnum'] = $values['loginnum']; 
        ?>
        <button id="submit" name="submit" value="usmgmt" class="btn btn-primary btn-sm btn-space">RETURN TO USER</button>
      <?php }
      else { ?>
        <button id="submit" name="submit" value="usmgmt" class="btn btn-primary btn-sm btn-space" onClick="unlockCancel()">DISCARD TICKET</button>
      <?php }
      ?>
    </div>
  </div>
</div>


</fieldset>
</form>


<script>
function unlockCancel() {
  document.getElementById("messagebody").required = false;
  document.getElementById("convsubject").required = false;
  return true;
}
</script>


<script>
function showWait() {
  $("#createticket").validate({
     submitHandler: function(form){
      myApp.showPleaseWait();
      form.submit();
     }
  });
}
</script>


<script>
var myApp;
myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Creating Ticket - Please Wait..</h5></div><div class="modal-body" align="center"><img src="css/loading.gif" width="75%"></div><div class="modal-footer"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };
})();
</script>


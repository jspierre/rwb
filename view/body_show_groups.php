<?php

$owngroup = $values['owngroup'];

if ($this->registry->security->checkFunction("editownacct")) {
  $editown = true;
}
else {
  $editown = false;
}

?>

<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">
      <h3 class="panel-title">Group Management: Add or Edit a Group</h3>
      <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>
  </div>
  
<?php
  if (isset($_SESSION['returncode'])) {
    if ($_SESSION['returncode'] < 4) { $color = "alert-success"; } else { $color = "alert-danger"; }
      echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
      
      switch ($_SESSION['returncode']) {
        case 2:
          echo "SUCCESS: Group Deleted as requested.";
          Break;
        case 5:
          echo "ERROR: Group NOT Deleted. Please try again or contact administrator."; 
          Break;
        default:
          echo "An unknown error has occurred.  Please contact your administrator.";
      }
      echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      echo '</div>';
  }                   
?>  


</div>

  <form action="<?= BASE_URL ?>grpmgmt" method="post" name="editgroup" id="editgroup">
    <div class="form-group">
      <div class="row">
        <div class="col-md-8 btn-space">
          <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW GROUP</button>
          <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
        </div>
      </div>
    </div>
    <table class="table table-bordered table-hover table-responsive" id="myTable2">
      <thead>
        <tr>
          <th onclick="sortTable(0)"><a href="#">Group #</a></th>
          <th onclick="sortTable(1)"><a href="#">Group Name</a></th>
          <th onclick="sortTable(2)"><a href="#">Active</a></th>
          <th onclick="sortTable(3)"><a href="#">Admin</a></th>
          <th onclick="sortTable(4)"><a href="#">Staff</a></th>
          <th onclick="sortTable(5)"><a href="#">Members</a></th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['groups'])) {
      $rows = $values['groups'];  
    }
    foreach ($rows as $row) {
      if ($row['isactive'] == 1) {$astatus = "YES";} else {$astatus="NO";}
      if ($row['isadmin'] == 1) {$adstatus = "YES";} else {$adstatus="NO";}
      if ($row['isstaff'] == 1) {$ststatus = "YES";} else {$ststatus="NO";}
      ?>
      <tr>
        <td><?= $row['groupindex'] ?></td>
        <td><?= $row['groupname'] ?></td>
        <td><?= $astatus ?></td>
        <td><?= $adstatus ?></td>
        <td><?= $ststatus ?></td>
        <td><?= $row['members'] ?></td>
        <?php 
        if ($row['groupindex'] == $owngroup && $editown==false && $this->registry->isgod==false) {
          ?> <td><button type="button" class="btn btn-primary" disabled="disabled">EDIT</button></td><?php
        }
        else {
          ?> <td><button id="submit" name="submit" class="btn btn-primary" value="GNX<?= $row['groupindex'] ?>">EDIT</button></td><?php
        }
        ?>
      </tr>
    <?php
    }
  ?>
  </table>

  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW GROUP</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             






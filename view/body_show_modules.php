<script src="<?= BASE_URL ?>js/sort-table-columns.js"></script>

<div class="panel panel-primary">

  <div class="panel-heading pb-4">
      <h3 class="panel-title">Module Management: Add or Edit a Module</h3>
      <h6><em style="color:gray">Click a Column Heading to Sort by that Column</em></h6>
  </div>

  <?php
  if (isset($_SESSION['returncode'])) {
    if ($_SESSION['returncode'] < 4) { $color = "alert-success"; } else { $color = "alert-danger"; }
      echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
            
      switch ($_SESSION['returncode']) {
        case 2:
          echo "SUCCESS: Module Deleted as requested.";
          Break;
        case 5:
          echo "ERROR: Module NOT Deleted. Please try again or contact administrator."; 
          Break;
        default:
          echo "An unknown error has occurred.  Please contact your administrator.";
      }
      echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
      echo '</div>';
  }                   
?>  

</div>

  <form action="<?= BASE_URL ?>modmgmt" method="post" name="editmodule" id="editmodule">
      <div class="row">
        <div class="col-md-8 btn-space">
          <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW MODULE</button>
          <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
        </div>
      </div>
    <table class="table table-bordered table-hover table-responsive" id="myTable2">
      <thead>
        <tr>
          <th onclick="sortTable(0)"><a href="#">Module #</a></th>
          <th onclick="sortTable(1)"><a href="#">Controller</a></th>
          <th onclick="sortTable(2)"><a href="#">Module Description</a></th>
          <th onclick="sortTable(3)"><a href="#">Allow All</a></th>
          <th onclick="sortTable(4)"><a href="#">Deny All</a></th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
  <?php
    if (isset($values['modules'])) {
      $rows = $values['modules'];  
    }
    foreach ($rows as $row) {
      if ($row['allowall'] == 1) {$astatus = "YES";} else {$astatus="NO";}
      if ($row['denyall'] == 1) {$dstatus = "YES";} else {$dstatus="NO";}
      ?>
      <tr>
        <td><?= $row['moduleindex'] ?></td>
        <td><?= $row['controller'] ?></td>
        <td><?= $row['modulename'] ?></td>
        <td><?= $astatus ?></td>
        <td><?= $dstatus ?></td>
        <td><button id="submit" name="submit" class="btn btn-primary" value="MNX<?= $row['moduleindex'] ?>">EDIT</button></td>
      </tr>
    <?php
    }
  ?>
  </table>


  <div class="form-group">
    <div class="row">
      <div class="col-md-8">
        <button id="submit" name="submit" value="new" class="btn btn-warning">ADD A NEW MODULE</button>
        <a class="btn btn-primary" href="<?= BASE_URL ?>" role="button">RETURN TO HOME SCREEN</a>
      </div>
    </div>
  </div>

</form>
</div>             






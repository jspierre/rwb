<h1>TERMS OF USE</h1>

<p>This web page represents our Terms of Use ("Agreement") regarding our website, <?php echo $this->registry->config['sitename']; ?> and its related 
servers, services, databases, and functions (hereinafter, "Website"). It was last posted on October 30, 2020. The terms, "We", "Us", and "Our" as 
used in this Agreement refer to <?php echo $this->registry->config['sitename']; ?> and its owners and members as well as any owning, parent, 
or controlling company(ies). Any references to <?php echo $this->registry->config['sitename']; ?> likewise also refers to this site and 
its owners and members as well as any owning, parent, or controlling company(ies) and/or individuals. The following rules of construction shall 
apply to this Agreement, except in cases where context clearly requires otherwise:  (i) words in the singular shall include the plural, and 
vice versa; (ii) the words “include”, “includes” and “including” are not limiting; (iii) references herein to a “Section” shall mean the 
corresponding Section of this Agreement, (iv) the words “hereof,” “herein” and “hereunder” and words of similar import shall refer to the 
Agreement as a whole, and not to any particular provision of this Agreement; and (v) references to any document, instrument or agreement shall 
include all exhibits, linked pages or documents, schedules and other attachments thereto.</p>

<p>We may amend this Agreement at any time by posting the amended terms on Our Website. We may or may not post notices on the 
	homepage of Our Website when such changes occur and we may or may not notify you by email or otherwise.</p>

<p>We refer to this Agreement, Our Privacy Policy (available at <a href="privacypolicy"><?php echo $this->registry->config['url']; ?>/privacypolicy</a>), 
	and any other terms, rules, or guidelines on Our Website collectively as our "Legal Terms." You explicitly and implicitly agree 
	to be bound by our Legal Terms each time you access Our Website. If you do not wish to be so bound, please do not use or 
	access Our Website.</p>

<p>We offer this site for use by Residents of the United States only. We specifically prohibit all access of any kind to any person(s) who are 
residents or legal citizens of the United Kingdom or any other country which follows, enforces, or has a similar law to the GDPR.  
TO BE CLEAR: WE INTEND, BY ALL LEGAL MEANS NECESSARY, TO NOT BE SUBJECT IN ANY WAY TO THE GDPR OR ANY SIMILAR LAW OR RESTRICTIONS. 
IF YOUR USE OF OUR WEBSITE WOULD MAKE US SUBJECT TO THE GDPR OR ANY SIMILAR LEGAL POLICIES, LAWS, OR REGULATIONS, YOU ARE SPECIFICALLY 
AND EXPLICITLY PROHIBITED FROM ACCESSING THIS WEBSITE AND YOUR CONTINUED USE SHALL CONSTITUTE A BREACH OF OUR LEGAL TERMS AS WELL AS AN INSTANCE OF
ILLEGAL AND UNAUTHORIZED USE OF A COMPUTER SYSTEM AND ANY OTHER LAWS FOR WHICH WE ARE LEGALLY ALLOWED TO SEEK PROSECUTION HERE IN THE UNITED STATES
OR IN YOUR HOME COUNTRY OR ANY OTHER COUNTRY. </p>

<h2>Limited License</h2>

<p>We grant you a non-exclusive, non-transferable, revocable license to access and use Our Website in order for you to 
	access details, files, data, and other information regarding <?php echo $this->registry->config['sitename']; ?> and related services through 
	Our Website, strictly in accordance with the Legal Terms.</p>


<h2>Fees and Payments</h2>
<p>We may charge a fee to use Our Website and may base that fee on a variety of factors at our option, including amount of resources available to 
or used by You, time spent or available to spend on the Website, or any other metric whatsoever. If We charge such a fee, you agree to pay this 
fee to Us to continue using our site and agree to make payment in advance and before the period of usage being paid for. You agree that 
should you fail to make this payment by the date required in any notice or invoice We may make to you that We may take actions to prevent 
your further usage of our Website until payment is made, including inactivating or terminating your account, at our sole discretion. You agree 
to hold us harmless against any claims which may arise over our enforcement of this section, including loss of data due to termination of 
your account or any other factor whatsoever. You also agree that should we inactivate or terminate your account, that you waive any and all 
rights to recovery of any data you may have entered into our Website for any reason and that, upon termination, you explicitly and irrevocably 
transfer any and all copyrights, interests, and moral rights to all data and images you may have entered for any reason into our Website to Us 
forever and agree that We will not compensate nor remunerate you for this transfer of intellectual property.  Additionally, should we inactivate
or terminate your account, you agree that all intellectual property you transfer to us was, at the time of transfer, your sole property and 
that you have all rights and power necessary to	make effective such transfer of intellectual property; specifically, YOU AGREE TO INDEMNIFY 
AND HOLD US HARMLESS AGAINST ANY AND ALL CLAIMS WHICH MAY ARISE OVER OUR CONTINUED USE OF TRANSFERRED INTELLECTUAL PROPERTY ASSETS. </p>
<p>You agree to pay us, when payment is due, in United States Dollars only and only by the payment methods that we offer on our Website. 
All other payment types and kinds shall be invalid and unaccepted. You also agree that all payments made are non-refundable. You may cancel 
your paid subscription or services at any time, but all payments already made shall be non-refundable and We will therefore cancel your paid 
service at your request only at the end of the current billing cycle for which you have already paid. Upon confirmation of your request 
for termination, therefore, your account shall remain active and you shall keep all paid services until the end of such cycle for which you 
have already paid, regardless of whether such cycle is for one week, one month, one year, or multiple years.</p>

<h2>Copyrights and Trademarks</h2>

<p>Unless otherwise noted, all materials including without limitation, logos, brand names, images, designs, photographs, 
	video clips and written and other materials that appear as part of our Website are copyrights, trademarks, service marks, 
	trade dress and/or other intellectual property whether registered or unregistered ("Intellectual Property") owned, 
	controlled or licensed by <?php echo $this->registry->config['sitename']; ?>. Our Website as a whole is protected by copyright 
	and trade dress. Nothing on our Website should be construed as granting, by implication, estoppel or otherwise, any 
	license or right to use any Intellectual Property displayed or used on our Website, without the prior written permission 
	of the Intellectual Property owner. <?php echo $this->registry->config['sitename']; ?> aggressively enforces its 
	intellectual property rights to the fullest extent of the law. The names and logos of <?php echo $this->registry->config['sitename']; ?>, 
	may not be used in any way, including in advertising or publicity pertaining to distribution of materials on our Website, without prior, 
	written permission from <?php echo $this->registry->config['sitename']; ?>. <?php echo $this->registry->config['sitename']; ?> prohibits 
	use of any logo of <?php echo $this->registry->config['sitename']; ?> or any of its affiliates as part of a link to or from any 
	Website unless <?php echo $this->registry->config['sitename']; ?> approves such link in advance and in writing. Fair use 
	of <?php echo $this->registry->config['sitename']; ?>’s Intellectual Property requires proper acknowledgment. Other product and 
	company names mentioned in our Website may be the Intellectual Property of their respective owners.</p>

<p>We may give you the option to upload photographs, digital images, or other types of files on our Website for use in various places and 
for various reasons. By uploading a file of any type or kind for any reason, you are agreeing that the contents of such files (including,
without limitation, file containing images, drawings, photographs, or other graphic content) belongs exclusively to you and the copyright 
of such material belongs to you exclusively.  Further, you explicitly and without limitation agree and represent that, when you upload a 
file of any type or kind for any reason our our Website (hereinafter, an 'Upload'), that you are granting <?php echo $this->registry->config['sitename']; ?> 
and its owners, managers, assigns, owning and/or controlling company(ies), and representatives (hereinafter, 'the Company') an irrevocable, perpetual, non-exclusive, transferrable, 
assignable, worldwide license to use this image in any and all ways the Company deems fit, including on this Website, on other Websites, 
and in advertising or marketing of any kind or for any other reason whatsoever. You also explicitly and without limitation agree such license 
includes your permission for Us to use, modify, re-use, or otherwise create any content using your Upload and that we shall owe you no licensing, 
royalty, usage, or any other fees whatsoever for any reason under such license and that we are not obligated for any reason or in any way to 
attribute the Upload we may use to you. You further agree to hold Us harmless over any claims which may arise over Our use of such Upload, 
including, by way of example and not limitation: intellectual property claims, lawsuits, and related damages. </p>

<h2>Data Entry</h2> 

<p>Data Entry, as used herein, shall mean the entry, editing, uploading, or other modification whatsoever by any user of this Website of any data, 
including, without limitation, text, images, documents, and binary- or text-based files for the usage, display, enjoyment, consideration, or 
any other use whatsoever by any and all other users on this Website. By continuing to use this system and by your acceptance of these 
Terms and Conditions, you agree that any Data Entry you may perform is merely a suggestion to Us for possible data to be added to our Website 
and its related systems and databases at some point in the future (a "Data Entry Suggestion"). Should We, at our own exclusive discretion, 
decide to accept your Data Entry Suggestion, whether by automated or manual means, You agree that your Data Entry Suggestion may be converted 
by Us from a mere suggestion into permanently stored data in our Website and its related systems and databases (the "Actual Data").  Such Actual 
Data may be stored in our Website and used by, modified by, displayed to, or deleted by Us, by any other user of this Website whatsoever, or by 
our owners, employees, affiliates, subsidiaries, and representatives for any purpose whatsoever, including for profit, in marketing or advertisting, 
or in any other way We, in our exclusive discretion, should decide to use the Actual Data. You agree that should we convert your Data Entry Suggestion 
into Actual Data, We have no duty to you whatsoever to notify you of this action or to notify you of any subsequent modification or deletion of 
such data. Additionally, you agree that any Data Entry Suggestion made was voluntary and made on behalf of Yourself only and that We owe you no 
compensation, royalties, penalties, or any other type of reimbursement whatsoever for such Data Entry Suggestions. You agree that your Data Entry 
Suggestions and their complete contents immediately belongs to Us upon your submission of such Data Entry Suggestions and You release any and all 
claims, moral and legal, to any copyright or other ownership and transfer such ownership voluntarily to Us immediately and irrevocably upon submission 
of any Data Entry Suggestion, whether such submission was made intentionally or not. You represent that any Data Entry Suggestion submitted to Us is 
entirely and without exception your own original content that does not infringe in any way on the intellectual property of anyone else whatsoever. 
You further agree to hold Us harmless over any claims which may arise over Our use of such Data Entry and Data Entry Suggestios, including, by way 
of example and without limitation: intellectual property claims, lawsuits, and related damages. Notwithstanding the foregoing, the terms and 
conditions of the section immediately above in this Agreement entitled "Copyright and Trademarks" shall also apply to all Data Entry or Data Entry 
Suggestions that may include images of any type or kind.



<h2>Links to Third-Party Websites</h2>

<p>Our Website may contain links to websites owned or operated by parties other than <?php echo $this->registry->config['sitename']; ?>. 
Such links are provided for your reference only. <?php echo $this->registry->config['sitename']; ?> does not monitor or control outside websites 
and is not responsible for their content. <?php echo $this->registry->config['sitename']; ?>’s inclusion of links to an outside website does not 
imply any endorsement of the material on our Website or, unless expressly disclosed otherwise, any sponsorship, affiliation or association with 
its owner, operator or sponsor, nor does <?php echo $this->registry->config['sitename']; ?> inclusion of the links imply that 
<?php echo $this->registry->config['sitename']; ?> is authorized to use any trade name, trademark, logo, legal or official seal, or 
copyrighted symbol that may be reflected in the linked Website.</p>


<h2>Notice of Non-Affiliation and Disclaimer</h2>
<p>Our Website may mention or make reference to the products, trademarks, and logos of other brands. We have no affiliation with these brands 
whatsoever and their appearance	on our website is not an endorsement by such brands and companies of our Website and/or related services. 
All trademarks and logos used remain the legal property of the brands and/or companies they represent. Specifically, We are not affiliated, 
associated, authorized, endorsed by, or in any way officially connected with Hallmark Cards, Inc., Hallmark Licensing, LLC, or any of its 
subsidiaries or its affiliates. The official Hallmark website can be found at http://www.hallmark.com. The names Hallmark, Hallmark Keepsake, 
and Hallmark Cards as well as related names, marks, emblems and images are registered trademarks of their respective owners.

<h2>Content Disclaimer</h2>

<p>Postings on our Website are made at such times as <?php echo $this->registry->config['sitename']; ?> determines in its discretion. 
	You should not assume that the information contained on our Website has been updated or otherwise contains current information. 
	<?php echo $this->registry->config['sitename']; ?> does not review past postings to determine whether they remain accurate and 
	information contained in such postings may have been superseded. THE INFORMATION AND MATERIALS IN OUR WEBSITE ARE PROVIDED 
	FOR YOUR REVIEW IN ACCORDANCE WITH THE NOTICES, LEGAL TERMS, AND THE TERMS AND CONDITIONS SET FORTH HEREIN. THESE MATERIALS 
	ARE NOT GUARANTEED OR REPRESENTED TO BE COMPLETE, CORRECT OR UP TO DATE. THESE MATERIALS MAY BE CHANGED FROM TIME TO TIME WITHOUT NOTICE.</p>

<p>Additionally, postings may be made on this website that were posted by other users and are not controlled, edited, monitored, 
	or otherwise moderated by <?php echo $this->registry->config['sitename']; ?>.  You agree to receive these messages at your own 
	risk and agree that you will indemnify, defend, and hold harmless <?php echo $this->registry->config['sitename']; ?> and its 
	owners, officers, directors, members, employees and assigns against any claims arising from your use of, reading of, or receiving 
	of any files, documents, text, messages, or other communications whatsoever posted by another user.</p>

<h2>Login and Password</h2>
<p>We issue accounts to individuals only (i.e., one person), not families, couples, or other groups. You agee that you will not violate this provision 
	by allowing others to use your account for any reason. You are responsible for keeping your login information, including your password, 
	private. You agree to never share your login information, including your password, with anyone, including Us. You agree to take
	reasonable steps to keep your account secure, including using Our offered two-factor authentication if you are able to receive text 
	(SMS) messages on any of your telephone devices. You also agree to use Our Website as it was intended to be used by a single user and 
	you agree not to attempt to misuse, damage, hack, reverse engineer, bypass security precautions or systems, access features and functions not
	ordinarily available to you, steal or otherwise gain access to data or systems that you would ordinarily not have access to, or otherwise 
	adversely affect our Website and related systems in any way	whatseover, including by accident. Should We discover that you have violated 
	any provision of this section, you agree that We may, and We reserve the right to, terminate your account immediately and/or contact 
	proper legal authorities to bring sufficient legal action against you to recover Our damages that you may have directly, indirectly, 
	or consequentially caused and that, should civil litigation be instituted against you for breach of Legal Terms, you explicitly and 
	irrevocably agree that We may seek immediate summary judgment based solely on the evidence We have against you and you further agree not
	to oppose such summary judgment so long as We can show reasonable likelihood that you had breached or were in default of Our Legal Terms.</p>

<h2>Email and Texts</h2>
<p>By using Our Website in any any way whatsoever, you agree, specifically, explicitly, irrevocably, and without limitation that We may
	email you at the email address(es) you provide to Us or that We may discover as a result of your usage of our Website. You further agree
	specifically, explicitly, irrevocably, and without limitation that We may text (SMS/MMS) you or call (voice) you at any phone number
	you may provide to Us or that We may discover as a result of your usage of our Website, and that you shall be responsible for any costs
	you may incur due to our communication in any way with you. We may agree to stop Our marketing communications with you via any method, 
	however, if you should email Us directly at our support email address and specifically ask Us to remove your phone and/or email address 
	from marketing communications. Should We agree to comply, which we are not obligated to do, you specifically, explicitly, irrevocably
	and without limitation agree that We may continue to email and/or text you regarding non-marketing-related subjects regarding your
	account or your usage of the Website.</p>


<h2>No Warranties; Exclusion of Liability; Indemnification</h2>

<p>You use the Website at your own risk and the Website and its services are provided "AS IS."</p>

<p>We are not saying that the Website will meet any of your specific needs or that you will get any particular results if you 
	use the Website. We are not responsible for your or other users' conduct on- or off-line. We encourage you to always 
	keep backups of any data entered or uploaded to the Website. We are not responsible if any data you entered or uploaded is deleted or 
	corrupted, or if any modification, suspension or discontinuation of the Website or services causes you to lose any 
	content—even if you pay for access to and/or usage of the Website.</p>

<p>While we make an effort to keep the Website up and running at all times, we are not responsible for any errors, omissions, interruptions, 
	deletions, defects, delays in operation or transmission, communications line failure, theft or destruction or unauthorized access to, 
	or alteration of, any user communication, whether the result of our maintenance of the Website or any problems or technical malfunction 
	of any network, servers, software or equipment, Internet traffic or any other failure of any nature.</p>

<p>OUR WEBSITE IS OPERATED ON AN "AS IS," "AS AVAILABLE" BASIS, WITHOUT 
REPRESENTATIONS OR WARRANTIES OF ANY KIND. TO THE FULLEST EXTENT PERMITTED BY LAW, THE COMPANY SPECIFICALLY DISCLAIMS ALL WARRANTIES AND 
CONDITIONS OF ANY KIND, INCLUDING ALL IMPLIED WARRANTIES AND CONDITIONS OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NONINFRINGEMENT FOR OUR WEBSITE AND ANY INFORMATION RECEIVED OR 
SERVICES YOU OBTAIN AND/OR PURCHASE THROUGH IT. THE COMPANY SHALL NOT HAVE ANY LIABILITY 
OR RESPONSIBILITY FOR ANY ERRORS OR OMISSIONS IN THE CONTENT OF OUR WEBSITE, FOR DATA STORED OR TRANSMITTED OR FOR SERVICES 
GIVEN OR SOLD THROUGH OUR WEBSITE, FOR YOUR ACTION OR INACTION IN CONNECTION WITH OUR WEBSITE OR FOR ANY DAMAGE TO YOUR 
COMPUTER OR DATA OR ANY OTHER DAMAGE YOU MAY INCUR IN CONNECTION WITH OUR WEBSITE. YOUR USE OF OUR WEBSITE AND ANY 
DATA, TEXT, OR SERVICES ARE AT YOUR OWN RISK. IN NO EVENT SHALL EITHER <?php echo $this->registry->config['sitename']; ?> OR 
ITS MEMBERS, OFFICERS, DIRECTORS, AGENTS, ASSIGNS, OR OWNING AND/OR CONTROLLING COMPANY(IES) BE LIABLE FOR ANY DIRECT, INDIRECT, 
PUNITIVE, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF OUR WEBSITE, 
CONTRACTS AND SERVICES PURCHASED THROUGH OUR WEBSITE, THE DELAY OR INABILITY TO USE OUR WEBSITE OR OTHERWISE ARISING IN 
CONNECTION WITH OUR WEBSITE, CONTRACTS OR RELATED SERVICES, WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, 
EVEN IF ADVISED OF THE POSSIBILITY OF ANY SUCH DAMAGES. IN NO EVENT SHALL THE COMPANY'S LIABILITY FOR ANY DAMAGE CLAIM EXCEED THE 
AMOUNT OF MONEY PAID BY YOU TO <?php echo $this->registry->config['sitename']; ?> IN ONE CALENDAR YEAR.</p>

<p>WITHOUT LIMITING THE FOREGOING, <?php echo $this->registry->config['sitename']; ?> DOES NOT REPRESENT OR WARRANT THAT THE 
INFORMATION ON THE WEBSITE IS ACCURATE, COMPLETE, RELIABLE, USEFUL, TIMELY OR CURRENT OR THAT OUR WEBSITE WILL OPERATE 
WITHOUT INTERRUPTION OR ERROR.</p>


<p><?php echo $this->registry->config['sitename']; ?> MAKES NO REPRESENTATION THAT CONTENT PROVIDED ON OUR WEBSITE, CONTRACTS, 
OR RELATED SERVICES ARE APPLICABLE OR APPROPRIATE FOR USE IN ALL JURISDICTIONS.</p>

<h2>Indemnification</h2>

<p>You agree to defend, indemnify and hold <?php echo $this->registry->config['sitename' ]; ?> and its owners, members, officers, directors, 
	agents, representatives, and assigns harmless from and against any and all claims, damages, costs and expenses, including attorneys' fees, 
	arising from or related to your use of our Website.</p>

<h2>Disputes and Resolution; Jurisdiction</h2>
<p>Our Website is hosted in the United States of America. Using the Website is at your own risk. These Terms are governed by, and interpreted in 
	keeping with, the laws of the State of Texas, regardless of any conflict of law provisions. You and We agree to submit 
	to the exclusive jurisdiction of the courts located within the State of Texas, County of Bexar, to resolve any dispute 
	arising out of the Agreement or the Services. </p>

<p>YOU AND WE SHALL MAKE A GOOD FAITH EFFORT TO RESOLVE ANY BREACH OF THESE TERMS, OR ANY OTHER DISPUTE ARISING UNDER OR IN 
	CONNECTION WITH THESE TERMS, THROUGH MEDIATION CONDUCTED WITHIN THIRTY (30) DAYS FOLLOWING FIRST ASSERTION OF THE BREACH 
	OR DISPUTE. IF EIGHT (8) HOURS OF SUCH MEDIATION DOES NOT FULLY RESOLVE THE BREACH OR DISPUTE, OR IF THE PARTIES DO NOT 
	CONDUCT SUCH MEDIATION, THE BREACH OR DISPUTE SHALL BE RESOLVED IN THE CITY OF HOUSTON, TEXAS BY BINDING ARBITRATION 
	UNDER THE ADMINISTRATION AND RULES OF JAMS (HTTP://WWW.JAMSADR.COM). NOTWITHSTANDING THE FOREGOING MEDIATION AND 
	ARBITRATION PROVISIONS, WE NEVER SHALL BE PRECLUDED OR DELAYED FROM SEEKING AND OBTAINING TEMPORARY, PRELIMINARY AND 
	PERMANENT INJUNCTIVE RELIEF AGAINST INFRINGEMENT OR OTHER VIOLATION OF ITS CLAIMED INTELLECTUAL PROPERTY RIGHTS, 
	OR OTHER RELIEF THAT IT DEEMS REASONABLY NECESSARY TO DEFEND OR ENFORCE ITS RIGHTS OR THESE TERMS, IN ANY COURT(S) 
	OF COMPETENT JURISDICTION. </p>


<p>A waiver on the part of Us of any term or condition of these Legal Terms shall not constitute a precedent or bind Us to a waiver 
of any succeeding breach of the same or any other terms or conditions of these Legal Terms.</p>

<h2>Captions and Headings</h2>
<p>Captions and headings used herein are inserted only for convenience of reference only and shall not operate to modify, interpret, 
alter, limit or define any provision hereof.</p>

<h2>Savings Clause</h2>
<p>In the event any provision herein shall be judicially interpreted or held to be invalid, illegal or otherwise unenforceable by reason 
of any rule of law or public policy, then (i) the other provisions of the Legal Terms shall remain in full force and effect, and (ii)the provision 
held to be invalid, illegal or unenforceable shall, to the fullest extent permitted by law, be reformed to the minimum extent necessary to 
render such provision valid, legal and enforceable and in such a manner as to preserve to the fullest extent possible Our original economic and 
legal intent.</p>

<h2>Entire Agreement - Waivers</h2>
<p>These Legal Terms embody the entire agreement between the you and Us relating to the subject matter hereof, and supersedes and 
replaces in their entirety all prior understandings and agreements relating to the subject matter hereof.</p>

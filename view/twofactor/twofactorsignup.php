<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script>
  $(document).ready(function(){
    // Make sure mobile number is only numbers
    $("#phone").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl+A, Command+A
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
           // Allow: home, end, left, right, down, up
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
    });
    });
</script>

<form class="form-signin" action="<?= BASE_URL ?>tfa" onsubmit="return validateForm()" method="post" name="tfa" id="tfa_form">

<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
	<div class="panel-heading">
	    <h3 class="panel-title">Two-Factor Authentication Management</h3>
	</div>
<div class="panel-body">
<?php
			/* LOGIC FOR RETURN MESSAGES */
			if (isset($_SESSION['returncode'])) {
				if ($_SESSION['returncode'] == 4) { $color = "alert-success"; } else { $color = "alert-danger"; }
				?>
					<div class="alert alert-danger col-md-6" role="alert">
        		<?php
   
				switch ($_SESSION['returncode']) {
					case 2:
						echo "<p>REQUEST FAILED: An internal error has occured.</p><p>Please try again in a few minutes, or contact your Administrator.</p>";
						break;
					case 3:
						echo "<p>REQUEST FAILED: Invalid Phone Number.</p><p>Phone numbers must be only 10 digits long and be only numbers.</p>";
						break;
					case 4:
						echo "<p>SUCCESS. Two-Factor Authentication has been successfully removed from your account.</p>";
						break;
					case 5:
						echo "<p>REQUEST FAILED: The code you entered was incorrect.</p><p>Try again if desired.</p>";
						break;
					default:
						echo "An unknown error has occurred.  Please contact your administrator.";
				}
				echo '</div>';
			}
		
	if ((isset($_SESSION['returncode']) && $_SESSION['returncode']<>2) || (!isset($_SESSION['returncode']))) {
?>			
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<p>To Activate Two-Factor Authentication on Your Account, please enter the 10-digit mobile phone number that you would like to use to receive two-factor codes via text message. This service is only available in the United States. NOTE: Your carrier's normal SMS rates will apply to each text message we send you.</p>
							<hr />
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<input type="tel" id="phone" name="phone" minlength="10" maxlength="10"  pattern=".{10,}" class="form-control" placeholder="10-Digit Mobile Phone Number" required autofocus>
							<label for="phone">&nbsp;Your 10-Digit Mobile Phone Number</label>
						</div>
					</div>
				</div>
			<button class="col-md-2 btn btn-primary btn-big-space" type="submit">Validate Phone</button>
<?php
	}
	else {
?>
	</form>
	<form action="<?= $this->registry->config['url'] ?>" method="POST">
		<button class="col-md-2 btn btn-primary btn-space" type="submit">Click here to continue</button>
		
<?php			
		}
?>
		</div>
	</form>
</div> <!-- /container -->

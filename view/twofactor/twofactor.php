<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script>
  $(document).ready(function(){
    // Make sure mobile number is only numbers
    $("#code").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl+A, Command+A
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
           // Allow: home, end, left, right, down, up
          (e.keyCode >= 35 && e.keyCode <= 40)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
    });
    });
</script>

<form class="form-signin" action="<?= BASE_URL ?>tfa/verify" onsubmit="return validateForm()" method="post" name="tfa" id="tfa_form">

<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
	<div class="panel-heading">
	    <h3 class="panel-title">VALIDATE: Enter your two-factor code</h3>
	</div>
<div class="panel-body">
<?php
			/* LOGIC FOR RETURN MESSAGES */
			if (isset($_SESSION['returncode'])) {
				if ($_SESSION['returncode'] == 1) { $color = "alert-success"; } else { $color = "alert-danger"; }
				?>
					<div class="alert alert-success col-md-6" role="alert">
        		<?php
   
				switch ($_SESSION['returncode']) {
					case 1:
						echo "SUCCESS: Validation code re-sent.";
						break;
					case 2:
						echo "FAILED: A system error occurred. Please try again, or contact your system administrator.";
						break;
					default:
						echo "An unknown error has occurred.  Please contact your administrator.";
				}
				echo '</div>';
			}
	if ($this->registry->config['twofactor_method'] == 1) { 
		$tfamethod = "email"; 
		$tfamsg = "your current email address on file.";
		$tfamsg2 = "NOTE: If you need to change your email address, please contact your system administrator.";
	} 
	else { 
		$tfamethod = "text"; 
		$tfamsg = "the mobile phone you used when you signed up for two-factor authentication";
		$tfamsg2 = "NOTE: Your carrier's normal SMS rates will apply to each text message we send you.";
	}	
	if ((isset($_SESSION['returncode']) && $_SESSION['returncode']<>15) || (!isset($_SESSION['returncode']))) {
?>			
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<p>To verify your identity, please enter the six-digit code we just sent via <?= $tfamethod ?> to <?= $tfamsg ?> and click "CONFIRM". This code is only valid for 10 minutes. <?= $tfamsg2 ?></p>
							<hr />
						</div>
					</div>
					<div class="row btn-big-space">
						<div class="col-md-4">
							<input type="text" id="code" name="code" minlength="6" maxlength="6"  pattern=".{6,}" class="form-control" placeholder="6-Digit Validation Code" required autofocus>
							<label for="phone">&nbsp;Enter the 6-digit validation code we sent you.</label>
						</div>
					</div>
					<div class="row">
						<div class="checkbox mb-3 col-md-6">         
					      <label>
					        <input type="hidden" name="remember" value="0" />           
					        <input type="checkbox" name="remember" value="1"> Optional: remember this device for 30 days before asking for another code (do not use this feature if on a public computer).
					      </label>       
			   		 	</div>	
			   		 </div>
				</div>	
			<button class="col-md-2 btn btn-primary" type="submit">CONFIRM</button>
			<p></p>
			<p><em>I didn't receive my code.  Please <a href="<?= $values['url'] ?>/tfa/verify/resend">send it again.</em></p>
<?php
	}
	else {
?>
	</form>
	<form action="<?= $values['url'] ?>" method="POST">
		<button class="col-md-2 btn btn-primary" type="submit">Click here to continue</button>
		
<?php			
		}
?>
		</div>
	</form>
</div> <!-- /container -->

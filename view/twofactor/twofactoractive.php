<form class="form-signin" action="<?= BASE_URL ?>tfa" method="post" name="tfa" id="tfa_form">

<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
	<div class="panel-heading">
	    <h3 class="panel-title">TWO FACTOR STATUS: ACTIVE</h3>
	</div>
<div class="panel-body">
<?php
			/* LOGIC FOR RETURN MESSAGES */
			if (isset($_SESSION['returncode'])) {
				if ($_SESSION['returncode'] == 1) { $color = "alert-success"; } else { $color = "alert-danger"; }
				?>
					<div class="alert alert-success col-md-6" role="alert">
        		<?php
   
				switch ($_SESSION['returncode']) {
					case 1:
						echo "<p>SUCCESS! Two-Factor Authentication is now active on your account.</p><p>You can <a href='".$url."'>click here</a> to continue.</p>";
						break;
					case 2:
						echo "<p>FAILED: Something went wrong.  Please try again later, or contact your system administrator for help.</p>";
						break;
					default:
						echo "An unknown error has occurred.  Please contact your administrator.";
				}
				echo '</div>';
			}
	if ($this->registry->config['twofactor_method'] == 1) { 
		$tfamethod = "email"; 
		$tfamsg = "your current email address on file.";
		$tfamsg2 = "If you need to change your email address, please contact your system administrator. If you";
	} 
	else { 
		$tfamethod = "text"; 
		$tfamsg = "the below number with a code for you to enter to verify your identity. NOTE: Your carrier's normal SMS/text message rates will apply to each text message we send you.";
		$tfamsg2 = "If you need to change your mobile phone number, or if you";
	}
	if ((isset($_SESSION['returncode']) && $_SESSION['returncode']<>15) || (!isset($_SESSION['returncode']))) {
?>			
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<p>Two-Factor Authentication is currently ACTIVE on your account. When logging in from an unknown device, we will send a <?= $tfamethod ?> message to <?= $tfamsg ?> </p>
							<p><?= $tfamsg2 ?> no longer want to use two-factor authentication, simply click "TURN OFF 2FA" to reset your account.</p>
							<hr />
						</div>
					</div>
					<?php
					if ($tfamethod == "text") {
					?>
					<div class="row">
						<div class="col-md-4">
							<input type="hidden" name="sitename" value = "<?= $sitename ?>"  />
							<input type="tel" id="phone" name="phone" minlength="10" maxlength="10"  pattern=".{10,}" class="form-control" placeholder="Your verified mobile number" value="<?= $values['phone'] ?>" disabled>
							<label for="phone">&nbsp;Your currently verified mobile number</label>
						</div>
					</div>
					<?php
					}
					?>
				</div>
			<button class="col-md-2 btn btn-primary btn-big-space" type="submit" name="submit" value="off" onclick="return confirm('Are you sure? This will turn off 2FA on your account.')">TURN OFF 2FA</button>
			<button class="col-md-2 btn btn-primary btn-big-space" type="submit" name="submit" value="cancel">RETURN HOME</button>
<?php
	}
	else {
?>
	</form>
	<form action="<?= $this->registry->config['url'] ?>" method="POST">
		<button class="col-md-2 btn btn-primary btn-space" type="submit">Click here to continue</button>
		
<?php			
		}
?>
		</div>
	</form>
</div> <!-- /container -->

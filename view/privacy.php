<h1><?php echo $this->registry->config['sitename'] ?> Privacy Policy</h1>

<p>This is the privacy policy regarding our website, <?php echo $this->registry->config['sitename']; ?> ("Website"). It was last 
posted on October 30, 2020. The terms, "We", "Us", and "Our" as used in this Privacy Policy refer to <?php echo $this->registry->config['sitename']; ?> 
and its owners and members as well as any owning, parent, or controlling company(ies).

<p>This Privacy Policy is meant to help you understand what data we collect, why we collect it, and what we do with it. 
	This is important; we hope you will take time to read it carefully.</p>


<h2>Information we collect</h2>

<p>We collect information to provide better services to all of our users – from your account balance to allowing
you to communicate securely with other users, to creating lists and collections of data that you entered, 
to participating in discussions - and much more. </p>

<p>We collect information in the following ways:</p>

<p><strong>Information you give us</strong>. We collect information that you directly give to us in person, 
	via telecommunications, or direcly on this site. For example: your name, email address, telephone number, 
	or credit card. </p>

<p><strong>Information we get from your use of our services</strong>. We collect information about the services that 
you use and how you use them, like when you post a message on a discussion board, visit any part of the website, or 
view and interact with our content. This information includes:</p>

<p><em>Device information</em></p>

<p>We collect device-specific information (such as your hardware model, operating system version, unique device identifiers, 
MAC address(es), and mobile network information including phone number). We may associate your device identifiers or phone number with 
your <?php echo $this->registry->config['sitename']; ?> Account.</p>

<p><em>Log information</em></p>

<p>When you use our services or view content provided by us, we automatically collect and store certain information 
in server logs. This includes, by way of example and not limitation: details of how you used our service, such as your 
search queries or what information you typed into any form on our site, whether you later delete it or not; telephony 
log information like your phone number, recorded contents of calls, calling-party number, forwarding numbers, time and 
date of calls, duration of calls, SMS routing information and types of calls when those calls go through any phone number 
owned or operated by us; your Internet protocol address (IP Address); cookies that may uniquely identify your browser or your Account.</p>


<p><em>Local storage</em></p>

<p>We may collect and store information (including personal information) locally on your device using mechanisms 
such as browser web storage (including HTML 5), cookies, and application data caches.</p>

<p><em>Cookies and similar technologies</em></p>

<p>We and our partners use various technologies to collect and store information when you visit a 
	<?php echo $this->registry->config['sitename']; ?> service, and this may include using cookies or similar technologies
	to identify your browser or device. We also use these technologies to collect and store information when you interact 
	with services we offer to our partners.</p>

<p>Information we collect when you are signed in to the Website, in addition to information we obtain about you from 
	partners, may be associated with your <?php echo $this->registry->config['sitename']; ?> Account. Additionally, 
	information about you that may be entered directly by members of our staff may be stored on our servers and
	associated with your <?php echo $this->registry->config['sitename']; ?> account, whether or not you are able to 
	see or view this inforation. Information of this type may include, by way of example and without limitation: 
	mailing address, phone numbers, privacy settings, notes about your interactions with Us or Our agents, officers, 
	members, or directors, details about financial transactions you make with us, vehicle information, date of birth 
	or other personal information of any type of kind whatsoever.  When information is associated with your Account,	
	we treat it as personal information	and take steps to make sure that your personal information stays private 
	unless you have elected otherwise.</p> 


<h2>How we use information we collect</h2>

<p>We use the information we collect from all of our services to provide, maintain, protect and improve them, to develop new ones, 
and to protect Us and Our users. We may use the username you provided or were given for your Profile across all of the services 
we offer that require an Account. In addition, we may replace past names associated with your Account so that you are 
represented consistently across all our services. If other users already have your email, or other information that identifies 
you, or if you choose to post messages to other users anywhere within our Website, we may show them your information, 
such as your name and email.</p>

<p>When you contact Us, we keep a record of your communication to help solve any issues you might be facing. We may use your 
email address or phone number to inform you about our services, such as letting you know about upcoming changes or improvements.</p>

<p>We use information collected from cookies and other technologies, like pixel tags, to improve your user experience and the
overall quality of our services. </p>

<p>Our automated systems analyze your content (including posts, emails, and internal messages) to provide you personally 
relevant product features, such as customized search results and filtering services, and to protect Us and other users.</p>

<p>We may combine personal information from one service with information, including personal information, from other services 
or with data we may collect in person from you, from public records, or from data collected about you from third parties.</p>

<p>We will ask for your consent before using information for a purpose where we are required by law to do so.</p>

<p>We may process your personal information on a server located outside the state or country where you live.</p>

<h2>Information you share</h2>

<p>Many of our services let you share information with others. Remember that when you share information on our Website, it 
may be recorded and archived indefinitely and used for any legal purpose even if you delete or edit the information later. </p>

<h2>Accessing and updating your personal information</h2>

<p>Whenever you use our services, we aim to provide you with access to your personal information. If that information is 
wrong, we strive to give you ways to update it quickly– unless we have to keep that information for legitimate business or 
legal purposes. When updating your personal information, we may ask you to verify your identity before we can act on your request.</p>

<p>We may reject requests that are unreasonably repetitive, require disproportionate technical effort (for example, developing 
a new system or fundamentally changing an existing practice), risk the privacy of others, or would be extremely impractical 
(for instance, requests concerning information residing on backup systems).</p>

<p>Where we can provide information access and correction, we will do so for free, except where it would require a 
disproportionate effort. We aim to maintain our services in a manner that protects information from accidental or 
malicious access or destruction. Because of this, after you delete information from our services, we may not delete 
residual copies from our active servers and may not remove information from our backup systems.</p>


<h2>Information we share</h2>

<p>We do not share personal information with companies, organizations and individuals outside of <?php echo $this->registry->config['sitename']; ?> 
unless one of the following circumstances applies:</p>

<p><em>With your consent</em></p>

<p>We will share personal information with companies, organizations or individuals outside of Us when we have your consent to do so. 
We require opt-in consent for the sharing of any sensitive personal information.</p>

<p><em>For external processing</em></p>

<p>We provide personal information to our affiliates or other trusted businesses or persons to process it for us, based on our 
instructions and in compliance with our Privacy Policy and any other appropriate confidentiality and security measures. This may include,
but is not limited to credit card processing, which we may not perform in-house for security reasons.</p>

<p><em>For marketing</em></p>
<p>Unless you specifically request otherwise (opt-out), we may use information that we have obtained about you to develop and deliver marketing,
including third-party marketing to you.</p>

<p>For legal reasons</p>

<p>We will share personal information with companies, organizations or individuals outside of Us if we have a good-faith 
belief that access, use, preservation or disclosure of the information is reasonably necessary to: meet any applicable law, 
regulation, legal process or enforceable governmental request; enforce applicable Terms of Use, including investigation of 
potential violations; detect, prevent, or otherwise address fraud, security or technical issues;  protect against harm to the 
rights, property or safety of <?php echo $this->registry->config['sitename']; ?>, our members, officers, directors, agents, users or 
the public as required or permitted by law.</p>

<h2>Information security</h2>

<p>We work hard to protect <?php echo $this->registry->config['sitename']; ?> and our users from unauthorized access to or 
	unauthorized alteration, disclosure or destruction of information we hold. In particular: We encrypt most of our services 
	using SSL; We review our information collection, storage and processing practices, including physical security measures, to guard 
	against unauthorized access to systems; We restrict access to personal information to <?php echo $this->registry->config['sitename']; ?> 
	employees, board members, officers, contractors and agents who need to know that information in order to process it for us, 
	and who are subject to strict contractual confidentiality obligations and may be disciplined or terminated if they fail to
	meet these obligations.</p>

<h2>When this Privacy Policy applies</h2>

Our Privacy Policy applies to all of the services offered by<?php echo $this->registry->config['sitename']; ?>, but excludes 
services that have separate privacy policies that do not incorporate this Privacy Policy or that are provided by third parties
not under the direct control or ownership of Us. Our Privacy Policy does not apply to services offered by other companies 
or individuals. Our Privacy Policy does not cover the information practices of other companies and organizations.

<h2>Changes</h2>

<p>Our Privacy Policy may change from time to time. We may reduce your rights under this Privacy Policy without your 
explicit consent. We will post any privacy policy changes on this page. We will also keep prior versions of this Privacy Policy 
in an archive for review.</p>


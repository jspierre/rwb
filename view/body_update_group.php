<script type="text/javascript">
    function selectAll() {
        var items = document.getElementsByName('module[]');
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox' && items[i].disabled == false)
                items[i].checked = true;
        }
        var items = document.getElementsByName('function[]');
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox' && items[i].disabled == false)
                items[i].checked = true;
        }
    }

    function UnSelectAll() {
        var items = document.getElementsByName('module[]');
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox' && items[i].disabled == false)
                items[i].checked = false;
        }
        var items = document.getElementsByName('function[]');
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox' && items[i].disabled == false)
                items[i].checked = false;
        }
    }     
</script>
<form class="form-horizontal" action="<?= BASE_URL ?>grpmgmt" method="post" name="editgroup" id="editgroup">
<fieldset>

<!-- Form Name -->
<div class="panel panel-primary">
<div class="panel-heading">
<?php
  //var_dump($_SESSION);
  //var_dump($_POST);
  //exit();
  //var_dump($values);
  
 
  // Determine what type of thing we are doing - new, display, or edit?
  if (isset($values['actiontype'])) {
    $actiontype = $values['actiontype'];
    extract($values['group']);
  }
  else {
    $actiontype = "NEW";
  }

  if ($actiontype !== "NEW") {
      echo '<h3 class="panel-title">Update Existing Group</h3>'; 
    }
    else {
      echo '<h3 class="panel-title">New Group Entry Form</h3>';
    }
?>
</div>
<div class="panel-body">
             
<?php
            /* LOGIC FOR RETURN MESSAGES */
            if (isset($_SESSION['returncode'])) {
                if ($_SESSION['returncode'] < 4) { 
                    $color = "alert-success"; 
                } 
                elseif ( $_SESSION['returncode'] == 17 )  { 
                    $color = "alert-warning"; 
                } 
                else { 
                    $color = "alert-danger"; 
                }
                echo '<div class="alert '.$color.' alert-dismissible" role="alert">';
                       
                switch ($_SESSION['returncode']) {
                    case 2:
                        echo "New Group Added. Please check settings to make sure all is correct!";
                        break;
                    case 3:
                        echo "Group Updated.";
                        break;
                    case 5:
                        $groupnum = $values['groupnum'];
                        $link = '<a href="'.BASE_URL.'grpmgmt/'.$groupnum.'">';
                        echo "ERROR: Group Already Exists - See ".$link."Group # $groupnum</a>";
                        extract($values['tempdata']);
                        break;
                    case 7:
                        echo "Users Moved Successfully.";
                        break;
                    case 8:
                        echo "ERROR: User Move FAILED.";
                        break;
                    default:
                        echo "An unknown error has occurred.  Please contact your administrator.";
                }
                echo '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
                echo '</div>';
            }                    
?>        


<?php 
    error_reporting(E_ALL & ~E_NOTICE);
?>

<?php 
if (isset($_SESSION['modulenum'])) {
  $groupnum = $_SESSION['groupnum'];
  $groupindex = $groupindex;
  echo '<input type="hidden" name="groupnum" value ='.$groupnum.' />';
}
$permissions = $values['permissions'];
$groups = $values['groups'];
?>


<!-- Select Basic -->
<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="groupindex">Group #</label>  
    <div class="col-md-2">
      <input id="groupindex" name="groupindex" type="text" placeholder="" class="form-control input-md" value="<?= $groupindex?>" disabled >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="groupname">Group Name</label>  
    <div class="col-md-2">
      <input id="groupname" name="groupname" type="text" placeholder="" class="form-control input-md" value="<?= $groupname?>" required >
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="isactive"><abbr title="Is this Group Active? WARNING! If NO, no one in the group will be able to log in!">Group Active?</abbr></label>
    <div class="col-md-2">
      <select id="isactive" name="isactive" class="form-select" >
        <option value="1" <?php if ($isactive==1) { echo "selected"; }?>>YES</option>
        <option value="0" <?php if ($isactive==0 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>NO</option>
      </select>
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="isadmin"><abbr title="Is this an Administrative Group? If Yes, will also automatically set STAFF to YES as well. WARNING! Setting this to YES will give the Group Admin abilities beyond those defined by role (e.g., can log in when in maintenance mode)">Admin Group?</abbr></label>
    <div class="col-md-2">
      <select id="isadmin" name="isadmin" class="form-select" >
        <option value="0" <?php if ($isadmin ==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($isadmin ==1 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>

  <div class="row">
    <label class="col-md-2 control-label" for="isstaff"><abbr title="Is this a Staff Group? This is used to differentiate staff from customers or regular users - it does not give them any special powers, but is used in determining some access and financial items.">Staff Group?</abbr></label>
    <div class="col-md-2">
      <select id="isstaff" name="isstaff" class="form-select" >
        <option value="0" <?php if ($isstaff ==0) { echo "selected"; }?>>NO</option>
        <option value="1" <?php if ($isstaff ==1 && ($actiontype !== "NEW" || isset($_SESSION['returncode']))) { echo "selected"; }?>>YES</option>
      </select>
    </div>
  </div>

<?php
  if ($actiontype == "NEW") {
?>
  <div class="row">
    <label class="col-md-2 control-label" for="copyfrom"><abbr title="If desired, you can copy the permissions from an existing Group to make it easier to work with - you'll have a chance to edit individual permissions on the next screen.">Copy From</abbr></label>
    <div class="col-md-2">
      <select id="copyfrom" name="copyfrom" class="form-control" >
        <option value="0" "selected">DO NOT COPY</option>
        <?php
        foreach ($groups as $group) {
          echo '        <option value="'.$group['groupindex'].'">'.$group['groupname'].'</option>'."\n";
        }
        ?>
      </select>
    </div>
  </div>
<?php
}
else {
?>
  <div class="row">
    <label class="col-md-2 control-label" for="moveto"><abbr title="If desired, you can Move all current users with this ACL group to another ACL group. Moves are exclusive: any other changes made to this form besides the move will be ignored.">Move All Users To</abbr></label>
    <div class="col-md-2">
      <select id="moveto" name="moveto" class="form-select" >
        <option value="0" "selected">DO NOT MOVE</option>
        <?php
        foreach ($groups as $group) {
          echo '        <option value="'.$group['groupindex'].'">'.$group['groupname'].'</option>'."\n";
        }
        ?>
      </select>
    </div>
  </div>
<?php
}
?>
</div>

<?php 
  if ($actiontype !== "NEW") {
?>
<div class="form-group">
  <div class="row">
    <div class="col-md-6 btn-space">
      <hr />
      <input type="button" class="btn btn-primary btn-sm" onclick='selectAll()' value="Select All"/>
      <input type="button" class="btn btn-primary btn-sm" onclick='UnSelectAll()' value="Unselect All"/>
     <?php if ($actiontype == "NEW") {
            //unset($_SESSION['modulenum']);
            ?><button id="submit" name="submit" value="savenew" class="btn btn-sm btn-primary">ADD NEW GROUP</button>
              <a class="btn btn-sm btn-primary" href="<?= BASE_URL ?>grpmgmt" role="button">CANCEL</a>
            <?php 
          }
          else {
            ?><button id="submit" name="submit" value="update" class="btn btn-sm btn-warning">UPDATE GROUP</button>
            <button id="submit" name="submit" value="grpmgmt" class="btn btn-sm btn-primary">CANCEL</button>
            <?php
            if ($members == 0 && $this->registry->security->checkFunction("deletegroup")) { ?>
              <button id="submit" name="submit" value="delete" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure? This group will be PERMANENTLY DELETED.')">DELETE GROUP</button> 
              <?php 
            }
          }
          ?>
          <hr />
        </div>
    </div>
</div>

<div class="form-group">
  <div class="row">
    <label class="col-md-2 control-label" for="permissions">Permissions</label>  
    <?php
      $x=0;
      foreach ($permissions['modAll'] as $module) {
        // First, display the main module and whether it is active or not
        if ($module['denyall'] !=  1) {
          $checked = "";
          $disabled = "";
          if ($module['allowall'] == 1) {
            $checked = "checked";
            $disabled = "disabled";
          }
          if ($permissions['modPerm']) {
            foreach ($permissions['modPerm'] as $modperm) {
              if ($modperm['modulenum'] == $module['moduleindex']) {
                $checked = "checked";
              }
            }
          }
          $x++;
          if ($x>1) {
            echo "</div>\n";
            echo '<div class = "row">';
            echo '  <label class = "col-md-2"></label>';
          }
          echo '<div class="col-md-8">'."\n";
          echo '  <div class="form-check">'."\n";
          echo '    <input type = "checkbox" class="form-check-input" name="module[]" value='.$module['moduleindex'].' '.$checked.' '.$disabled.' >';
          if ($disabled == "") {
            echo '<strong>'.$module['modulename']." (".$module['controller'].") </strong>";
          }
          else {
            echo '<strong style="color:lightgrey">'.$module['modulename']." (".$module['controller'].") </strong>"; 
          }
          echo '</input>'."\n"; 
          echo "  </div>\n";
          // echo " </div>\n";
          // Now, display any sub-functions if they exist
          // And they are related to the main module
          foreach ($permissions['funcAll'] as $func) {
              $checked = "";
              $disabled = "";
              $words = "";
            if ($func['modulenum'] == $module['moduleindex']) {
              if ($func['allowall'] == 1) {
              $checked = "checked";
              $disabled = "disabled";
            }
            if ($func['functionactive'] == 0) {
              $disabled = "disabled";
              $words = "[INACTIVE] ";
            }
              if ($permissions['funcPerm']) {
                  foreach ($permissions['funcPerm'] as $funcPerm) {
                  if ($funcPerm['functionnum'] == $func['funcnum']) {
                    $checked = "checked";
                  }
                }
              }
              echo '  <div class="col-md-10">'."\n";
              echo '    <div class="form-check">'."\n";
              echo '      <input type = "checkbox" class="form-check-input" name="function[]" value='.$func['funcnum'].' '.$checked.' '.$disabled.' >';
              if ($disabled == "disabled") {
                echo '<em style="color:lightgrey">';
              }
              else {
                echo '<em>';
              }
              echo $words.$func['funcdescr']." (".$func['funcname'].") </em>";
              echo '</input>'."\n";
              echo "    </div>\n";
              echo "  </div>\n"; 
            }
          }
        echo '</div>'."\n";
        }
      }
    ?>
  </div>
<?php
}
?>
</div>

<!-- Button -->
<div class="form-group">
  <div class="row">
    <div class="col-md-8 btn-space">
      <hr />
      <input type="button" class="btn btn-primary btn-sm" onclick='selectAll()' value="Select All"/>
      <input type="button" class="btn btn-primary btn-sm" onclick='UnSelectAll()' value="Unselect All"/>
      <?php if ($actiontype == "NEW") {
              unset($_SESSION['modulenum']);
              ?><button id="submit" name="submit" value="savenew" class="btn btn-sm btn-primary">ADD NEW GROUP</button>
                <a class="btn btn-sm btn-primary" href="<?= BASE_URL ?>grpmgmt" role="button">CANCEL</a>
              <?php 
            }
            else {
              ?><button id="submit" name="submit" value="update" class="btn btn-sm btn-warning">UPDATE GROUP</button>
              <button id="submit" name="submit" value="grpmgmt" class="btn btn-sm btn-primary">CANCEL</button>
              <?php
              if ($members == 0 && $this->registry->security->checkFunction("deletegroup")) { ?>
                <button id="submit" name="submit" value="delete" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure? This group will be PERMANENTLY DELETED.')">DELETE GROUP</button> 
                <?php 
              }
            }
            ?>
        <hr />
    </div>
  </div>
</div>


</fieldset>
</form>



<?php
?> 
<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $values['title'] ?></title>
		
    <!-- Bootstrap core CSS/JS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

    <!-- CSSS for handling date and time boxes -->
    <link href="<?= BASE_URL ?>css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Extra CSS for Framework -->
    <link href="<?= BASE_URL ?>css/framework.css" rel="stylesheet">
    <!-- CSS for Session Timeout -->
    <link href="<?= BASE_URL ?>js/timeout-dialog/css/timeout-dialog.css" rel="stylesheet" />
    
    <!-- JQuery and Related Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
	
  	<!-- If the payload from the prior page includes an additional HTML/PHP code file to include in the header, pull that file in now -->
    <?php if(isset($request['headerinclude'])) { require(SITE_PATH.'/view/'.$request['headerinclude']); }?>
  </head>

  <body>
    <!-- Start the Static Navbar -->
    <nav class="navbar bg-body-tertiary navbar-expand-lg" data-bs-theme="dark" >
      <div class="container-fluid">
        <!-- Display the Site's Name  -->
        <a class="navbar-brand" href="<?= BASE_URL ?>"><?= $sitename ?></a>

        <!-- If the menu has collapsed due to screen size, show a button to reveal menu -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- The Main Menu Area (collapsible Menu Items) Starts Here -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mea-uto mb-2 mb-lg-0">
            <?php
            if ($this->currentclass == $this->config['mainpage'] || $this->currentclass == $this->config['loginurl'] || $this->currentclass == $this->config['firstpage'] || $this->currentclass == "") 
					{ $homeactive='class="nav-item active"'; } else {$homeactive="";}
			?>
			     <li <?= $homeactive ?> ><a class="nav-link" href="<?= $this->registry->config['url'] ?>">Home</a></li>

        <!-- ADMIN Menu Dropdown -->
        <?php
          if ($this->registry->security->checkFunction("adminmenu")) { ?>
            <li class="nav-item dropdown">
              <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">Sys Admin</a>
              <ul class = "dropdown-menu">                
                <?php  if ($this->registry->security->checkFunction("modmgmt")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>modmgmt">ACL Module Management</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("fncmgmt")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>fncmgmt">ACL Function Mgmt</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("grpmgmt")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>grpmgmt">ACL Group Management</a></li>
                 <?php } ?>
                 <?php  if ($this->registry->security->checkFunction("bgtaskmgmt")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>bgtaskmgmt">Background Task Management</a></li>
                 <?php } ?>
                <?php  if ($this->registry->security->checkFunction("bans")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>bans">Banned IP Management</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("supportmaster")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>supportmastersearch">SupportMaster</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("sysadmin")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>sysadmin">System Administration</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("searchsysemails")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>searchsysemails">System Email Administration</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("usermgmt")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>searchuser">User Setup and Management</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("displaylog")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>viewsyslog">View System Log</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("displayuserlogins")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>loginstoday">View User Logins for Today</a></li>
                <?php } ?>
                <?php  if ($this->registry->security->checkFunction("loginsall")) { ?>
                <li><a class="dropdown-item" href="<?= BASE_URL ?>loginsall">View All User Logins Ever</a></li>
                <?php } ?>
              </ul>
            </li>
          <?php } ?>

          <!-- Settings Menu Dropdown -->
          <?php if ($this->loggedin) { ?>
          <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">Settings </a>
            <ul class = "dropdown-menu">   
            <?php
              $pwchg = BASE_URL.$this->registry->config['pwchgurl'];
            ?>
              <?php  if ($this->registry->security->checkFunction("pwchg")) { ?>
              <li><a class="dropdown-item" href="<?= $pwchg ?>">Change Your Password</a></li>
              <?php } ?>
              <?php  if ($this->registry->security->checkFunction("faq")) { ?>
              <li><a class="dropdown-item" href="<?= BASE_URL ?>faq">Frequently Asked Questions (FAQ)</a></li>
              <?php } ?>
              <?php  if ($this->registry->security->checkFunction("support")) { ?>
              <li><a class="dropdown-item" href="<?= BASE_URL ?>support">Help &amp; Support</a></li>
              <?php } ?>
              <li><a class="dropdown-item" href="<?= BASE_URL ?>tandc">Terms &amp; Conditions</a>
               <?php  if ($this->registry->security->checkFunction("tfa") && $this->registry->config['twofactor']) { ?>
              <li><a class="dropdown-item" href="<?= BASE_URL ?>tfa">Two-Factor Authentication</a></li>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>
          </ul>

    </div>
    <!-- End of Main Menu Area (collapsible Menu Items) -->

      <!-- Log In or Log Out Link -->
			<?php
				if ($this->loggedin) {
				?><a class="d-flex" href="<?= BASE_URL ?>logout" >Logout</a><?php 
				}
				else {
				?><a class="d-flex" href="<?= BASE_URL ?>login">Log In</a><?php
				}
			?>
      </div>
    </nav>
    <!-- End the Static Navbar -->

   <!-- Main Body of the Page Starts Here -->
   <main>

      <!-- If a system message (banner) needs to be displayed, display it -->
      <?php
        if (isset($_SESSION['systembanner'])) {
          $systembanner = $_SESSION['systembanner'];
          ?>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>NOTICE: </strong> <?= $systembanner ?>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>    
          <?php
          unset($_SESSION['systembanner']);
        }
      ?>


      <!-- If a return message (banner) from another module needs to be displayed, display it -->
      <?php
        if (isset($_SESSION['returnmessage'])) {
          $returnmessage = $_SESSION['returnmessage'];
          ?>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>NOTICE: </strong> <?= $returnmessage ?>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>    
          <?php
          unset($_SESSION['returnmessage']);
        }
      ?>

    
    <!-- Main Section Container forthe Main Body (closed in footer file) -->
    <section class="py-5 container">
	

<?php
?>
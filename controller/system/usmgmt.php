<?php

class usmgmt extends baseController {

    private $oldpost;

    public function index() {
        /* First, we call in the model we'll need */
        $this->model = new mUser($this->registry);

        // First, sanitize all POSTed data
        $this->oldpost = $_POST;
        $this->registry->security->sanitizePost();


        // If we are coming from another controller,
        // We pull the loginnumber from the Session and
        // Set it as if we were coming from search
        
        
        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if ($_POST['submit'] == "log") {
                if (isset($_SESSION['loginnum'])) {
                    $loginnum = $_SESSION['loginnum'];
                    return $this->doShowLog($loginnum);
                }
            }
            if ($_POST['submit'] == "syslog") {
                if (isset($_SESSION['loginnum'])) {
                    $loginnum = $_SESSION['loginnum'];
                    return $this->doShowSysLog($loginnum);
                }
            }

            if (substr($_POST['submit'],0,3) == "UNX") {
                $loginnum = substr($_POST['submit'],3);
                $_SESSION['loginnum'] = $loginnum;
                return $this->getUpdateUser($loginnum,'DISPLAY');
            }
            if ($_POST['submit'] == "new") {
                return $this->doUpdateUser('NEW');
            }
            if ($_POST['submit'] == "edit") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $_SESSION['loginnum'] = $_POST['loginnum'];
                    return $this->getUpdateUser($_POST['loginnum'], 'EDIT');
                }
            }
            if ($_POST['submit'] == "update") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    return $this->doUpdateUser('UPDATE');
                }
            }
            if ($_POST['submit'] == "associate") {
                if (isset($_SESSION['ticketno']) && isset($_POST['loginnum'])) {
                    $convno = $_SESSION['ticketno'];
                    $user = $_POST['loginnum'];
                    $support = new mSupport($this->registry);
                    $result = $support->associateUser($convno, $user);
                    if ($result) {
                        return $this->getUpdateUser($user, 'DISPLAY', 1);
                    } 
                    else {
                        return $this->getUpdateUser($user, 'DISPLAY', 11);
                    }
                }
            }
            if ($_POST['submit'] == "listtickets") {
                if (!isset($_SESSION['ticketno'])) {
                    $_SESSION['loginnum'] = $_POST['loginnum'];
                    $_SESSION['fromcontroller'] = "usmgmt";
                    $_SESSION['oldsearch'] = $_SESSION['searchterms'];
                    $this->registry->redirect("supportmastersearch");
                }
            }
            if ($_POST['submit'] == "delete") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $this->doDeleteUser($_POST['loginnum']);
                    $_SESSION['lastsearch'] = true;
                    $this->registry->redirect("searchuser");
                }
            }
            if ($_POST['submit'] == "notes") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    return $this->doShowNotes($loginnum, 'DISPLAY');
                }
            }
            if ($_POST['submit'] == "createticket") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    return $this->createTicket($loginnum);
                }
            }
            if ($_POST['submit'] == "submitticket") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    return $this->updateTicket($loginnum);
                }
            }
            if ($_POST['submit'] == "editnotes") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    return $this->doShowNotes($loginnum, 'EDIT');
                }
            }
            if ($_POST['submit'] == "savenotes") {
                if (isset($_SESSION['loginnum'])) {
                    $loginnum = $_SESSION['loginnum'];
                    $result = $this->doSaveNotes();
                    if ($result == true) { $results = 2; } else { $results = 5; }
                    $_SESSION['returncode'] = $results;
                    return $this->doShowNotes($loginnum, 'DISPLAY');
                }
            }
            if ($_POST['submit'] == "emailuser") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    return $this->doComposeEmail($loginnum, 'EDIT');
                }
            }
            if ($_POST['submit'] == "sendemail") {
                    return $this->doSendEmail();
            }
            if ($_POST['submit'] == "unbanuserip") {
                    //var_dump($_POST);
                    if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                        $ipaddress = $_POST['lastloginip'];
                        $loginnum = $_POST['loginnum'];
                        $this->model->checkIPIsBanned($ipaddress, true);
                        return $this->getUpdateUser($loginnum, 'DISPLAY', 3);
                    }
            }

            if ($_POST['submit'] == "vvsms" || $_POST['submit'] == "vvemail") {
                $loginnum = $_POST['loginnum'];
                if ($_POST['submit'] == "vvsms") {
                    $result = $this->doVoiceVerify("SMS");
                }
                else {
                    $result = $this->doVoiceVerify("EMAIL");
                }
                if ($result == false) {
                    return $this->getUpdateUser($loginnum, 'DISPLAY', 14);
                }
                else { 
                    return $this->getUpdateUser($loginnum, 'DISPLAY', 17, $result);
                }
            }
            if ($_POST['submit'] == "ticket") {
                if (isset($_SESSION['returnto'])) { unset($_SESSION['returnto']); }
                $_SESSION['fromcontroller'] = "usmgmt";
                $this->registry->redirect("supportmaster");
            }
            if ($_POST['submit'] == "search") {
                $_SESSION['lastsearch'] = true;
                $this->registry->redirect("searchuser");
            }
            if ($_POST['submit'] == "research") {
                $_SESSION['lastsearch'] = false;
                $this->registry->redirect("searchuser");
            }
            if ($_POST['submit'] == "impersonate") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    if (!$this->doImpersonateUser($loginnum)) {
                        return $this->getUpdateUser($loginnum, 'DISPLAY');
                    }
                }
            }
            if ($_POST['submit'] == "forceuserpw") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    $security = new mSecurity($this->registry);
                    if ($security->forcePasswordChange($loginnum)) {
                        return $this->getUpdateUser($loginnum, 'DISPLAY', 3);
                    }
                    else {
                        return $this->getUpdateUser($loginnum, 'DISPLAY', 14);
                    }
                }
            }
            if ($_POST['submit'] == "forcelogoff") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    $security = new mSecurity($this->registry);
                    if ($security->forceLogout($loginnum)) {
                        $creatornum = $_SESSION['usernum'];
                        $message = "FORCED LOGOFF OF USER";
                        $this->model->doLogUser($creatornum, $loginnum, $message);
                        return $this->getUpdateUser($loginnum, 'DISPLAY', 3);
                    }
                    else {
                        return $this->getUpdateUser($loginnum, 'DISPLAY', 14);
                    }
                }
            }
            if ($_POST['submit'] == "forceuser2fa") {
                if (isset($_POST['loginnum']) && $_POST['loginnum'] > 0) {
                    $loginnum = $_POST['loginnum'];
                    $security = new mSecurity($this->registry);
                    if ($security->force2FA($loginnum)) {
                        return $this->getUpdateUser($loginnum, 'DISPLAY', 3);
                    }
                    else {
                        return $this->getUpdateUser($loginnum, 'DISPLAY', 14);
                    }
                }
            }
            if ($_POST['submit'] == "usmgmt") {
                if (isset($_SESSION['loginnum'])) {
                    $loginnum = $_SESSION['loginnum'];
                    return $this->getUpdateUser($loginnum, 'DISPLAY');
                }
                else {
                    return $this->getUpdateUser();
                }
            }
            if (isset($this->registry->config['usr_plugins']) && $this->registry->config['usr_plugins'])  {
                $plugins = $this->registry->config['usr_plugins'];
                foreach ($plugins as $plugin) {
                    $butval = $plugin['button_submit_value'];
                    $butcon = $plugin['controller'];
                    if ($_POST['submit'] == $butval) {
                        $_SESSION['returnto'] = "usmgmt";
                        $_SESSION['loginnum'] = $_POST['loginnum'];
                        $this->registry->redirect($butcon);
                    }
                }
            }     
        }
        elseif (isset($_SESSION['loginnum']) && isset($_SESSION['fromcontroller'])) { 
            $loginnum = $_SESSION['loginnum'];
            unset($_SESSION['loginnum']);
            unset($_SESSION['fromcontroller']);
            return $this->getUpdateUser($loginnum,'DISPLAY');
        }
        else {
            if (isset($_SESSION['clientdata'])) { unset ($_SESSION['clientdata']); }
            if ($this->registry->passedVars) {
                if (is_numeric($this->registry->passedVars[0])) {
                    $_SESSION['searchterms'] = array($this->registry->passedVars[0], 0, 1);
                    return $this->getUpdateUser($this->registry->passedVars[0], 'DISPLAY');
                }
            }
            elseif ($this->registry->security->checkFunction("usradd")) {
                return $this->getUpdateUser();
            }
            else {
                $this->registry->security->doSecurity(false);
                exit();
            }
        }
    }

    public function getUpdateUser($loginnum=false, $action=false, $returncode=0, $data=false) {
        //var_dump($loginnum);
        // We'll need the security model for some of this, so let's instantiate it
        $security = new mSecurity($this->registry);

        $rvalue['type']='html';
        $rvalue['variables']['title']="User Management Tool";
        $aclgroups = $security->getAllACLGroups();
        $numtickets = $this->registry->db->runOtherMethod("mSupport", "getNumTickets", array($loginnum));
        $rvalue['variables']['numtickets']=$numtickets;
        $rvalue['variables']['aclgroups']=$aclgroups;
        $rvalue['variables']['returncode'] = $returncode;
        $rvalue['variables']['randompw'] = $this->model->randomPassword();
        $redisplay = false;

        // If we just added a user, we want to display the temporary 
        // Password
        if ($returncode == 2) {
            $rvalue['variables']['returnpass'] = $data['returnpass'];
        }
        // If we get a user already exists code, we need to display
        // The result differently
        if ($returncode == 5 || $returncode == 13) {
            $redisplay = true;
            $vars = $data[2]['tempdata'];           
            $rvalue['variables']['user']=$vars;
            $rvalue['variables']['user']['loginnum']=$loginnum;
        }

        // Other error codes also redisplay
        if ($returncode == 7 || $returncode == 8 || $returncode == 15) {
            $redisplay = true;
            $vars = $data[2]['tempdata'];
            $rvalue['variables']['user']=$vars;
        }

        // What about existing unit owner/renter?
        if ($returncode == 12) {
            $redisplay = true;
            $vars = $data[2]['tempdata'];           
            $rvalue['variables']['user']=$vars;
            $rvalue['variables']['actiontype']=$action;
            $rvalue['variables']['user']['dupnum']=$loginnum;
        }

        // If we asked to send a code, let's send it along
         if ($returncode == 17) {
            $rvalue['variables']['code']=$data;
        }
            

        // Otherwise, we simply do our display
        // If there was an error, we'll do that, otherwise, we'll
        // Do a standard display
        if ($loginnum && !$redisplay) {
            $vars = $this->model->getUserByLoginnum($loginnum); 
            $lastip = $vars['lastloginip'];
            $isbanned = $this->model->checkIPIsBanned($lastip);
            $vars['isbanned']=$isbanned;
            // If a user was actually pulled, set it up for display
            // If not, we won't set the variables and the add client screen will show instead with an error message
            if ($vars) {
                $rvalue['variables']['user']=$vars;
                $rvalue['variables']['actiontype']=$action;
                $rvalue['loginnum']=$loginnum;  
            }
            else {
                $rvalue['variables']['error']['loginnum'] = $loginnum;
                $rvalue['variables']['returncode'] = 6;
            }
        }
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_update_user.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }

    public function doUpdateUser($actiontype="NEW") {
        $vars=$_POST;
        $result = $this->model->UpdateUser($vars,$actiontype);
        if ($result[0] == false) {
            if ($result[1] == "ADD_USER_ERROR_DATABASE_ERROR_1" || $result[1] == "ADD_USER_ERROR_DATABASE_ERROR_2" || $result[1] == "ADD_USER_ERROR_DATABASE_ERROR_3") {
                $returncode=15;
                return $this->getUpdateUser(false,false,$returncode,$result);
            }
            if ($result[1] == "UPDATE_USER_ERROR_DATABASE_ERROR_1" || $result[1] == "UPDATE_USER_ERROR_DATABASE_ERROR_2" || $result[1] == "UPDATE_USER_ERROR_DATABASE_ERROR_3") {
                $returncode=15;
                return $this->getUpdateUser(false,false,$returncode,$result);
            }
            if ($result[1] == "ADD_USER_ERROR_DUPLICATE_USER") {
                $returncode=5;
                return $this->getUpdateUser($result[2]['loginnum'],false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_DUPLICATE_EMAIL") {
                $returncode=13;
                return $this->getUpdateUser($result[2]['loginnum'],false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_DUPLICATE_USER_CHANGE") {
                $returncode=12;
                return $this->getUpdateUser($result[2]['dupnum'],'EDIT',$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_INVALID_EMAIL1") {
                $returncode=7;
                return $this->getUpdateUser(false,false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_INVALID_EMAIL2") {
                $returncode=8;
                return $this->getUpdateUser(false,false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_INVALID_EMAIL3") {
                $returncode=9;
                return $this->getUpdateUser($result[2]['loginnum'],false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_INVALID_EMAIL4") {
                $returncode=10;
                return $this->getUpdateUser($result[2]['loginnum'],false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_UNIT_TAKEN") {
                $returncode=11;
                return $this->getUpdateUser($result[2]['loginnum'], false, $returncode, $result);
            }
        }
        if ($actiontype == "NEW") {
            $creatornum = $_SESSION['usernum'];
            $message = "ADDED NEW USER TO DATABASE";
            $loginnum = $result[2]['loginnum'];
            $useractive = $result[2]['data']['useractive']; 
            $_SESSION['searchterms'] = array($loginnum, $useractive, 1);
            $this->model->doLogUser($creatornum, $loginnum, $message);
            $returncode = 2;
            return $this->getUpdateUser($loginnum,'DISPLAY',$returncode,$result[2]['data']);
        }
        if ($actiontype == "EDIT") {
            return $this->getUpdateUser($loginnum,false,0,$result[2]['data']);
        }
        if ($actiontype == "UPDATE") {
            $loginnum = $result[2]['loginnum'];
            $creatornum = $_SESSION['usernum'];
            $message = "DATA UPDATE FORCED - NO CHANGES DETECTED";
            if (isset($result[2]['data']['returnpass'])) {
                $message = "RESET PASSWORD OF USER";
                $this->model->doLogUser($creatornum, $loginnum, $message);
            }
            if (isset($result[2]['data']['changed']) && count($result[2]['data']['changed']) > 0) {
                foreach ($result[2]['data']['changed'] as $message) {
                    $this->model->doLogUser($creatornum, $loginnum, $message);
                }
            }
            else {
                $this->model->doLogUser($creatornum, $loginnum, $message);
            }

            $returncode = 3;

            return $this->getUpdateUser($loginnum,'DISPLAY',$returncode,$result[2]['data']);
        }

    }

    public function createTicket($loginnum) {
        $rvalue['type']='html';
        $rvalue['variables']['title']="Create Ticket";
        $rvalue['variables']['loginnum']=$loginnum;
        $rvalue['variables']['action']="ADD";
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_create_user_ticket.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue; 
    }

    public function updateTicket($loginnum) {
        $support = new mSupport($this->registry);
        $result = $support->createNewTicket($_POST);
        $rvalue['type']='html';
        $rvalue['variables']['title']="Create Ticket";
        $rvalue['variables']['loginnum']=$loginnum;
        $rvalue['variables']['action']="DISPLAY";
        $rvalue['variables']['values']=$this->oldpost;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_create_user_ticket.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue; 
    }
    

    public function doShowSysLog($loginnum) {
        $system = new mSystem($this->registry);
        $logs = $system->getUserRecordData($loginnum);
        $rvalue['type']='html';
        $rvalue['variables']['title']="User Filtered System Log";
        $rvalue['variables']['results'] = $logs;   
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_user_system_log.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;      
    }

    public function doShowLog($loginnum) {
        $logs = $this->model->getUserLog($loginnum);
        $rvalue['type']='html';
        $rvalue['variables']['title']="User Log of Activity";
        $rvalue['variables']['results'] = $logs;   
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_user_log_activity.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;      
    }

    public function doShowNotes($loginnum, $action) {
        $notes = $this->model->getUserNotes($loginnum);        
        $rvalue['type']='html';
        $rvalue['variables']['title']="User Notes";
        $rvalue['variables']['notes']=$notes;
        $rvalue['variables']['action']=$action;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_update_user_notes.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue; 
    }


    public function doSaveNotes() {
        $loginnum = $_SESSION['loginnum'];
        $creatornum = $_SESSION['usernum'];
        $newnotes = $_POST['notes'];
        if ($this->model->updateUserNotes($loginnum, $newnotes)) {
            $message = "UPDATED USER NOTES FILE";
            $this->model->doLogUser($creatornum, $loginnum, $message);
            return true;
        }
        else {
            return false;
        }
    }

    public function doComposeEmail($loginnum, $action) {
        $usernum = $_SESSION['usernum'];
        $userinfo = $this->model->getUserByLoginnum($usernum);        
        $youremail = $userinfo['emailaddress'];
        $yourname = ucwords(strtolower($userinfo['firstname']))." ".ucwords(strtolower($userinfo['lastname']));
        if (empty($youremail)) { $youremail = $this->registry->config['siteemail']; }   
        if (!isset($emailbody)){ $emailbody = ""; }
        $rvalue['type']='html';
        $rvalue['variables']['title']="Email User";
        $rvalue['variables']['emailbody']=$emailbody;
        $rvalue['variables']['youremail']=$youremail;
        $rvalue['variables']['yourname']=$yourname;
        $rvalue['variables']['action']=$action;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_send_user_email.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue; 
    }

    public function doSendEmail() {
        $loginnum = $_SESSION['loginnum'];
        $creatornum = $_SESSION['usernum'];
        extract($_POST);
        $senderemail = false;
        $fromname = false;
        if ($sendfrom == 2) {
            $userinfo = $this->model->getUserByLoginnum($creatornum);
            $senderemail = $userinfo['emailaddress'];
            $fromname = ucwords(strtolower($userinfo['firstname']))." ".ucwords(strtolower($userinfo['lastname']));
        }
        $emailbody = $_POST['emailbody'];
        $model = new mEmails($this->registry);
        $success = $model->sendSingleEmail($loginnum, $sendfrom, $subjectline, $emailbody, $senderemail, $fromname);
        if ($success) {
            $message = "SENT EMAIL TO USER - SEE SYSTEM EMAILS FOR MORE INFO";
            $this->model->doLogUser($creatornum, $loginnum, $message);
            $explanation = "USER $creatornum SENT EMAIL WITH SUBJECT $subjectline TO USER $loginnum";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
            $result = true;
        }
        else {
            $explanation = "FAILED: USER $sendfrom FAILED EMAIL SEND TO USER $loginnum";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
            $result = false;
        }
        if ($result == true) { $results = 2; } else { $results = 5; }
        $usernum = $_SESSION['usernum'];
        $userinfo = $this->model->getUserByLoginnum($usernum);        
        $youremail = $userinfo['emailaddress'];
        $yourname = ucwords(strtolower($userinfo['firstname']))." ".ucwords(strtolower($userinfo['lastname']));
        if (empty($youremail)) { $youremail = $this->registry->config['siteemail']; }   
        $_SESSION['returncode'] = $results;
        $rvalue['type']='html';
        $rvalue['variables']['title']="Email User";
        $rvalue['variables']['emailbody']=$emailbody;
        $rvalue['variables']['youremail']=$youremail;
        $rvalue['variables']['yourname']=$yourname;
        $rvalue['variables']['sendfrom']=$sendfrom;
        $rvalue['variables']['subjectline']=$success['subjectline'];
        $rvalue['variables']['emailbody']=$success['emailbody'];
        $rvalue['variables']['action']="DISPLAY";
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_send_user_email.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue; 
    }

    // This generates a 3-digit code and sends it to the user by text or email
    // it is used to verify that you are speaking to the actual user when having 
    // a phone conversation about their account
    public function doVoiceVerify($type="EMAIL") {
        $chars = "123456789JKPQUVWXZ123456789";
        $code = substr( str_shuffle( $chars ), 0, 3 );
        if ($type=="EMAIL") {
            $loginnum = $_SESSION['loginnum'];
            $creatornum = $_SESSION['usernum'];
            $subjectline = "Your Customer Service Code";
            $emailbody = "YOUR CODE IS: $code\n\n Please give this code to the representative. If you did not request this code, please ignore this email.";
            $model = new mEmails($this->registry);
            if ($model->sendSingleEmail($loginnum, 0, $subjectline, $emailbody, false, false)) {
                $message = "SENT EMAIL TO USER WITH VOICE VERIFY CODE OF ".$code;
                $this->model->doLogUser($creatornum, $loginnum, $message);
                $explanation = "USER %%USER%% SENT EMAIL FOR VOICE VERIFY WITH CODE OF ".$code." TO USER $loginnum";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
                return $code;
            }
            else {
                $explanation = "FAILED: USER %%USER%% FAILED EMAIL VOICE VERIFY SEND TO USER $loginnum";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
                return false;
            }
        }
        if ($type=="SMS") {
            $loginnum = $_SESSION['loginnum'];
            $creatornum = $_SESSION['usernum'];
            $body = "Your cust svc code is: $code \nPlease give this code to the rep. If you did not request a code, please ignore.";
            $vars = $this->model->getUserByLoginnum($loginnum);
            $phone = $vars['mobilephone'];
            $model = new mTwilio($this->registry);
            if ($model->sendSMS($phone, $body)) {
                $message = "SENT TEXT TO USER WITH VOICE VERIFY CODE OF ".$code;
                $this->model->doLogUser($creatornum, $loginnum, $message);
                $explanation = "USER %%USER%% SENT TEXT FOR VOICE VERIFY WITH CODE OF ".$code." TO USER $loginnum";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
                return $code;
            }
            else {
                $explanation = "FAILED: USER %%USER%% FAILED TEXT VOICE VERIFY SEND TO USER $loginnum";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
                return false;
            }
        }
    }

    
    // This calls the delete user function
    public function doDeleteUser($usernum) {
        if ($this->model->deleteUser($usernum)) {
            return true;
        }
        else {
            return false;
        }
    }

    public function doImpersonateUser($usernum) {
        $loginnum = $_SESSION['loginnum'];
        $creatornum = $_SESSION['usernum'];
        $message = "IMPERSONATED THIS USER";
        $this->model->doLogUser($creatornum, $loginnum, $message);
        if ($this->model->impersonateUser($usernum)) {
            $this->registry->goHome();
        }
        else {
            return false;
        }
    }
}
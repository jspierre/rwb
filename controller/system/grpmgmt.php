<?php

class grpmgmt extends baseController {


    public function index() {
        /* First, we call in the model we'll need */
        $this->model = new mSecurity($this->registry);
        
        // If any POSTed data was sent, we need to sanitize it
        if (!$this->registry->passedVars) { $this->registry->security->sanitizePost(); }

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if (substr($_POST['submit'],0,3) == "GNX") {
                $groupnum = substr($_POST['submit'],3);
                return $this->getUpdateGroup($groupnum,'EDIT');
            }
            if ($_POST['submit'] == "new") {
                return $this->getUpdateGroup();
            }
            if ($_POST['submit'] == "savenew") {
                return $this->doUpdateGroup('NEW');
            }
            if ($_POST['submit'] == "update") {
                if (isset($_POST['moveto']) && $_POST['moveto'] > 0) {
                    if (isset($_SESSION['groupnum'])) {
                        $groupnum = $_SESSION['groupnum'];
                        $moveto = $_POST['moveto'];
                        return $this->doMoveGroup($groupnum, $moveto);
                    }
                }
                else if (isset($_SESSION['groupnum'])) {
                    $groupnum = $_SESSION['groupnum'];
                    return $this->doUpdateGroup('UPDATE');
                }
            }
            if ($_POST['submit'] == "delete" && $this->registry->security->checkFunction("deletegroup")) {
                $groupnum = $_SESSION['groupnum'];
                $groupname = $_POST['groupname'];
                if($this->model->deleteACLGroup($groupnum)) {
                    $explanation = "GROUP DELETED. USER: %%USER%% GROUP: $groupnum ($groupname)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 2;
                }
                else {
                    $explanation = "GROUP DELETE FAILED. USER: %%USER%% GROUP: $groupnum (groupname)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 5; 
                }
                return $this->getAllModules();
            }
            if ($_POST['submit'] == "grpmgmt") {
                   return $this->getAllModules();
            }
        }
        else {
             if ($this->registry->passedVars) {
                if (is_numeric($this->registry->passedVars[0])) {
                    return $this->getUpdateGroup($this->registry->passedVars[0], 'EDIT');
                }
            }
            return $this->getAllModules();
        }
    }

    // This method returns all modules available for editing and displays them in a list
    // on screen to be chosen from.  IT NEVER shows moduleindex #1 as that is for internal
    // Use only.
    public function getAllModules() {
        if ($this->registry->security->checkFunction("editadmins")) {
            $groups = $this->model->getAllACLGroups(false, true, true);    
        }
        else {
            $groups = $this->model->getAllACLGroups(false, false,true);    
        }
        $owngroup = $this->model->getUserGroup($_SESSION['usernum']);
        $rvalue['type']='html';
        $rvalue['variables']['title']="Group Management Tool";
        $rvalue['variables']['groups']=$groups;
        $rvalue['variables']['owngroup'] = $owngroup;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_show_groups.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }



    public function getUpdateGroup($groupnum=false, $action=false, $rvalues=false) {
        //var_dump($groupnum);
        $rvalue['type']='html';
        $rvalue['variables']['title']="Group Management: Edit";
        if ($action=="EDIT") {
            $rvalue['variables']['permissions'] = $this->model->getGroupPermissions($groupnum);
            $rvalue['variables']['groups'] = $this->model->getAllACLGroups(true);
        }
        else {
            $rvalue['variables']['groups'] = $this->model->getAllACLGroups(true);
        }
        $redisplay = false;
        if (isset($_SESSION['returncode'])) {
            if ($_SESSION['returncode'] == 5) {
                $redisplay = true;
            }
        }
        if ($groupnum && !$redisplay) {
            $vars = $this->model->getACLGroup($groupnum);
            if ($vars) {
                $rvalue['variables']['group']=$vars;
                $rvalue['variables']['actiontype']=$action;
                $_SESSION['groupnum']=$groupnum;    
            }
            else {
                $rvalue['variables']['error']['groupnum'] = $groupnum;
                $_SESSION['returncode'] = 6;
            }
        }
        elseif ($groupnum && $redisplay) {   
            //$_POST = $_SESSION['tempdata'];
            //$this->registry->security->sanitizePost();
            $vars=$_POST;           
            $rvalue['variables']['group']=$vars;
            $rvalue['variables']['actiontype']=$action;
            $rvalue['variables']['group']['groupnum']=$groupnum;
        }
        if ($rvalues) {
            foreach ($rvalues as $key=>$value) {
                $rvalue['variables'][$key]=$value;
            }
        }
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_update_group.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }

    public function doMoveGroup($groupnum, $moveto) {
        $result = $this->model->moveACLGroups($groupnum, $moveto);
        if ($result) {
            $_SESSION['returncode'] = 7;
        }
        else {
            $_SESSION['returncode'] = 8;   
        }
        $rvalues['groupnum'] = $groupnum;
        return $this->getUpdateGroup($groupnum, 'EDIT');
    }



    public function doUpdateGroup($actiontype="NEW") {
        // If This is a new entry, we first check for a duplicate, then do the add if none is found
        if ($actiontype == "NEW") {
            if (isset($_SESSION['groupnum'])) {unset($_SESSION['groupnum']);}
            $result = $this->model->newACLGroup($_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['groupnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateGroup(false, 'NEW', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateGroup(false, 'NEW', $rvalues);
                }
            }
            else {
                    $_SESSION['groupnum'] = $result[2];
                    $_SESSION['returncode'] = 2;
                    return $this->getUpdateGroup($result[2], 'EDIT');
            }
        }
        elseif ($actiontype == "UPDATE") {
            $groupnum = $_SESSION['groupnum'];
            // var_dump($_POST);
            $result = $this->model->updateACLGroup($groupnum, $_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['groupnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    $rvalues['tempdata']['groupindex'] = $groupnum;
                    return $this->getUpdateGroup($groupnum, 'EDIT', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['groupnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateGroup(false, 'EDIT', $rvalues);
                }
            }
            else {
                    $_SESSION['groupnum'] = $result[2];
                    $_SESSION['returncode'] = 3;
                    return $this->getUpdateGroup($result[2], 'EDIT');
            }
        }
    }

}
<?php

class modmgmt extends baseController {


    public function index() {
        /* First, we call in the model we'll need */
        $this->model = new mSecurity($this->registry);
        
        // If any POSTed data was sent, we need to sanitize it
        $this->registry->security->sanitizePost(); 

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if (substr($_POST['submit'],0,3) == "MNX") {
                $modulenum = substr($_POST['submit'],3);
                return $this->getUpdateModule($modulenum,'EDIT');
            }
            if ($_POST['submit'] == "new") {
                return $this->getUpdateModule();
            }
            if ($_POST['submit'] == "savenew") {
                return $this->doUpdateModule('NEW');
            }
            if ($_POST['submit'] == "update") {
                if (isset($_SESSION['modulenum'])) {
                    $modulenum = $_SESSION['modulenum'];
                    return $this->doUpdateModule('UPDATE');
                }
            }
            if ($_POST['submit'] == "delete" && $this->registry->security->checkFunction("deletemodule")) {
                $modulenum = $_SESSION['modulenum'];
                $controller = $_POST['controller'];
                $modulename = $_POST['modulename'];
                if($this->model->deleteModule($modulenum)) {
                    $explanation = "MODULE DELETED. USER: %%USER%% MODULE: $modulenum ($controller - $modulename)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 2;
                }
                else {
                    $explanation = "MODULE DELETE FAILED. USER: %%USER%% MODULE: $modulenum ($controller - $modulename)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 5; 
                }
                return $this->showAllModules();
            }
            if ($_POST['submit'] == "modmgmt") {
                   return $this->showAllModules();
            }
        }
        else {
             if ($this->registry->passedVars) {
                if (is_numeric($this->registry->passedVars[0])) {
                    return $this->getUpdateModule($this->registry->passedVars[0], 'EDIT');
                }
            }
            return $this->showAllModules();
        }
    }

    // This method returns all modules available for editing and displays them in a list
    // on screen to be chosen from.  IT NEVER shows moduleindex #1 as that is for internal
    // Use only.
    public function showAllModules() {
        $modules = $this->model->getAllModules();
        $rvalue['type']='html';
        $rvalue['variables']['title']="User Management Tool";
        $rvalue['variables']['modules']=$modules;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_show_modules.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }


    public function getUpdateModule($modulenum=false, $action=false, $rvalues=false) {
        $rvalue['type']='html';
        $rvalue['variables']['title']="Module Management: Edit";
        $redisplay = false;
        if (isset($_SESSION['returncode'])) {
            if ($_SESSION['returncode'] == 5) {
                $redisplay = true;
            }
        }
        if ($modulenum && !$redisplay) {
            $vars = $this->model->getModule($modulenum);
            // If a module was actually pulled, set it up for display
            // If not, we won't set the variables and the add module screen will show instead with an error message
            if ($vars) {
                $rvalue['variables']['module']=$vars;
                $rvalue['variables']['actiontype']=$action;
                $_SESSION['modulenum']=$modulenum;    
            }
            else {
                $rvalue['variables']['error']['modulenum'] = $modulenum;
                $_SESSION['returncode'] = 6;
            }
        }
        elseif ($modulenum && $redisplay) {   
            //$_POST = $_SESSION['tempdata'];
            //$this->registry->security->sanitizePost();
            $vars=$_POST;           
            $rvalue['variables']['module']=$vars;
            $rvalue['variables']['actiontype']=$action;
            $rvalue['variables']['module']['modulenum']=$modulenum;
        }
        if ($rvalues) {
            foreach ($rvalues as $key=>$value) {
                $rvalue['variables'][$key]=$value;
            }
        }
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_update_module.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }



    // ****************************************************
    // unfinished - need to LOG actions to main log file
    // ****************************************************

    public function doUpdateModule($actiontype="NEW") {
        // If This is a new entry, we first check for a duplicate, then do the add if none is found
        if ($actiontype == "NEW") {
            if (isset($_SESSION['modulenum'])) {unset($_SESSION['modulenum']);}
            $result = $this->model->newModule($_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['modulenum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateModule(false, 'NEW', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateModule(false, 'NEW', $rvalues);
                }
            }
            else {
                    $_SESSION['modulenum'] = $result[2];
                    $_SESSION['returncode'] = 2;
                    return $this->getUpdateModule($result[2], 'EDIT');
            }
        }
        elseif ($actiontype == "UPDATE") {
            $modulenum = $_SESSION['modulenum'];
            $result = $this->model->updateModule($modulenum, $_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['modulenum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    $rvalues['tempdata']['groupindex'] = $modulenum;
                    return $this->getUpdateModule($modulenum, 'EDIT', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['modulenum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateModule(false, 'EDIT', $rvalues);
                }
            }
            else {
                    $_SESSION['modulenum'] = $result[2];
                    $_SESSION['returncode'] = 3;
                    return $this->getUpdateModule($result[2], 'EDIT');
            }
        }
    }

}
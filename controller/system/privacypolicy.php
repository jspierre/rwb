<?php

class privacypolicy extends baseController {


	public function index() {
		$request = $this->registry->pageArray;
		if (!isset($request[1])) {
			$request[1] = "";
		} 
		return $this->output = $this->getValue($request[1]); 
	
	}
	
	public function getValue($pageattempt) {
		$rvalue['type']='html';
		$rvalue['variables']['title']="Privacy Policy - ".$this->registry->config['sitename'];
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "privacy.php";
		$rvalue['footerfile']="footer2.php";
		return $rvalue;
	}
}

?>

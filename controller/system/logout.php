<?php

class logout extends baseController {

	public function index() {
		
		/* If logins are not enabled, we should not be using this controller; send the user back to home */
		if ($this->registry->config['logins'] == FALSE) {
			$this->registry->goHome();
		}
		$this->doLogout();
	}

	public function doLogout() {
		/* First, check that a user is actually logged in - if not, send them home*/
		if ($this->registry->security->isLoggedIn() == FALSE) {	
			$this->registry->goHome();
		}
	
		/* Let's update their session info to inactivate */
		$this->registry->security->updateSession(false);

		/* Next, log it in the logfile */
		$explanation = "LOGOUT SUCCESSFUL FOR USER %%USER%% FROM %%IP%%";
		$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
		
		/* Now, let's get rid of any rogue or pending 2FA Codes that may be out there */
		$tfa = new mTwoFactor($this->registry);
		$usernum = $_SESSION['usernum'];
		$tfa->invalidate2FA($usernum);
		if (isset($_SESSION['returncode']) && $_SESSION['returncode'] = 9999) { $badlogout = true; } else { $badlogout = false; }
		
		/* Now, unset the most important data just to be sure */
		unset($_SESSION['userip']);
		unset($_SESSION['username']);
		unset($_SESSION['usernum']);
		
		/* Destroy & re-initialize the session.  Set the return code */
		session_destroy();
		session_start();
		if ($badlogout) { $_SESSION['returncode'] = 4; } else { $_SESSION['returncode'] = 2; }
		
		/* Finally, send the user back home.  It is the responsibility of the main controller */
		/* to send the user back to the login page if necessary. */
		$this->registry->goPostLogout();
			
	}
}

?>
<?php

class login extends baseController {

	private $posted;
	

	public function index() {
		/* Pull in the model for this controller */ 
		$this->model = new mSecurity($this->registry);

		/* If logins are not enabled, we should not be using this controller; send the user back to home */
		if ($this->registry->config['logins'] == FALSE) {
			$this->registry->goHome();
		}
		
		/* If POSTed data was sent, then we should do the login, if not, we should get the login from the user */
		if (isset($_POST['user'])) {
			return $this->doLogin();
		}
		else {
			return $this->getLogin();
		}
	}
	
	public function getLogin() {
		/* If the user just logged out, let's destroy his session and set the logout message - but only for one time. */
		if (isset($_SESSION['returncode']) && $_SESSION['returncode']==2) {
			if (!isset($_SESSION['returncount'])) {
				session_destroy();
				session_start();
				$_SESSION['returncode']=2;
				$_SESSION['returncount']=1;
			}
			else {
				if (isset($_SESSION['requestedurl'])) { $requestedurl = $_SESSION['requestedurl']; } else { $requestedurl = false; }
				session_destroy();
				session_start();
				if ($requestedurl) { $_SESSION['requestedurl'] = $requestedurl; }
			}
		}
		/* If the user is logged in, send him back to the main page */
		elseif (isset($_SESSION['userip']) && isset($_SESSION['username'])) {	
			$this->registry->goHome();
		}
		
		/* Set a session variable to disable POSTing from remote sites */
		$userip = $_SERVER['REMOTE_ADDR'];
		$_SESSION['login']=$userip;
		
		/* Display the login page and get the Login information */
		$rvalue['type']='html';
		$rvalue['variables']['title']="Please log in";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "bodylogin.php";
		$rvalue['footerfile']="footer2.php";
		return $rvalue;
	}
	
	public function doLogin() {
		$user = $_POST['user'];
		$pass = $_POST['pass'];
		$userip = $_SERVER['REMOTE_ADDR'];
		
		/* Let's check to see if the request was submitted from our app or not */
		if ((isset($_SESSION['login']) && $_SESSION['login'] !== $userip) || !isset($_SESSION['login'])) {
			$this->model->failAuthentication("XSS_ATTACK",3);
		}
		
		/* Now, if the user logged out and in, let's clear the return code to avoid weird stuff. */
		if (isset($_SESSION['returncode']) && $_SESSION['returncode']==2) {unset($_SESSION['returncode']);}
		
		/* Now, let's authenticate the user */
		$authenticated = $this->model->authenticateUser($user, $pass);

		/* IF for some reason all other safeguards fail, let's just set a default failure */
		if ($authenticated == false) {
			$this->model->failAuthentication();
			return false;
		}
		else {
			return true;
		}

	}
	
}

?>
<?php

class sysmaint extends baseController {


	public function index() {
		$request = $this->registry->pageArray;
		if (!isset($request[1])) {
			$request[1] = "";
		} 
		return $this->output = $this->getValue($request[1]);
	}
	
	public function getValue($pageattempt) {
		$rvalue['type']='html';
		$sitename=$this->registry->config['sitename'];
		$rvalue['variables']['title']="$sitename - SYSTEM MAINTENANCE IN PROGRESS";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "body_system_maintenance.php";
		$rvalue['footerfile']="footer.php";
		return $rvalue;
	}
}

?>

<?php

class bans extends baseController {

    // ********************************************************************************************                      
    // These private variables are ALL you should have to set any time you instantiate this class.
    // UNLESS you need you need to do additonal types of searches, then see ~line 181
    // ********************************************************************************************

    // This variable is the name of the model that you'll be working with. 
    private $modelname = "mBans";

    // Location and name of your VIEW files (one for search, one for results)
    private $viewloc = "body_bans_show.php";
    private $resultloc = "body_bans_show.php";

    // These variables contain some additional data to pass to your View.
    // $pagetitle - shows up in the browser tab for initial serach; 
    // $resulttitle - shows up in the browser tab for result pages;
    // $thingname - when viewing a record, THING # NNN will
    //    be shown.  Change $thingname to "USER" or "COURSE" or whatever as needed.
    private $pagetitle = "IP Ban Management";  
    private $resulttitle = "Currently Active Bans";
    private $thingname = "Banned IP";

    // ********************************************************************************************                      
    // ********************************************************************************************
    // ********************************************************************************************


    public function index() {
        /* First, we call in the model we'll need */
        $mname = $this->modelname;
        $this->model = new $mname($this->registry);
        $skipsanitize=false;

        // First, see if this is a special case triggered by GET data
        if (isset($this->registry->passedVars[0])) {
            $_POST['submit'] = "search";
            $skipsanitize=true;
        }

        // Next, sanitize all POSTed data
        if ($_POST && $skipsanitize==false) {
            $this->registry->security->sanitizePost();  
        }

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if ($_POST['submit']=="removeall") {
                $this->model->removeBan();
            }
            else {
                $selected = explode('-', $_POST['submit']);
                $bannumber = $selected[1];
                if (is_numeric($bannumber)) {
                    $this->model->removeBan($bannumber);
                }
            }
            return $this->getSearchRecords();    
        }
        else {
            return $this->getSearchRecords();
        }
    }

    public function getSearchRecords() {
        $rows=$this->model->doSearch();
        $rvalue['type']='html';
        $rvalue['variables']['title']=$this->pagetitle;
        $rvalue['variables']['thingname'] = $this->thingname;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = $this->viewloc;
        $rvalue['footerfile']="footer.php";
        $rvalue['variables']['results']=$rows;
        return $rvalue;
    }


}
<?php

class searchuser extends baseController {


    public function index() {
        /* First, we call in the model we'll need */
        $this->model = new mUser($this->registry);
        $skipsanitize=false;

        // First, see if this is a special case triggered by GET data
        if (isset($this->registry->passedVars[0])) {
            $_POST['submit'] = "search";
            $skipsanitize=true;
        }

        // Next, sanitize all POSTed data
        if ($_POST && $skipsanitize==false) {
            $this->registry->security->sanitizePost();   
        }

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit']) && $_POST['submit'] == "search") {
            if(isset($_SESSION['searchterms'])) { unset($_SESSION['searchterms']); }
            return $this->doSearchUser();
        }
        elseif (isset($_POST['submit']) && $_POST['submit'] ==  "clear") {
                if (isset($_SESSION['loginnum'])) { unset($_SESSION['loginnum']); }
                if (isset($_SESSION['returnto'])) { unset($_SESSION['returnto']); }
                if (isset($_SESSION['fromcontroller'])) { unset($_SESSION['fromcontroller']); }   
                if (isset($_SESSION['ticketno'])) { unset($_SESSION['ticketno']); }   
                $_SESSION['returncode'] = 3;
                $_SESSION['searchterm'] = "";
                $_SESSION['searchactive'] = "";
                $_SESSION['searchall'] = "";
                return $this->getSearchUser();
        }
        else {
            if (isset($_SESSION['lastsearch']) && $_SESSION['lastsearch'] == true) {
                unset($_SESSION['lastsearch']);
                return $this->doSearchUser();       
            }
            else {
                return $this->getSearchUser();
            }
        }
    }

    public function getSearchUser() {
        $rvalue['type']='html';
        $rvalue['variables']['title']="User Management Search";
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_search_user.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }

    public function doSearchUser() {
        if (!isset($_SESSION['searchterms'])) {
            $this->registry->security->sanitizePost();
            $vars=$_POST;
            extract ($_POST);
            $thedate = date("Y-m-d H:i:s");
            $searchterm = trim($searchterm);
            $searchterm = filter_var($searchterm, FILTER_SANITIZE_STRING);
            $safeterm = $this->registry->db->sanitize($searchterm);
            $_SESSION['searchterms'] = array($safeterm, $active, $searchall);
        }
        else {
            $safeterm = $_SESSION['searchterms'][0];
            $searchterm = $safeterm;
            $active = $_SESSION['searchterms'][1];
            $searchall = $_SESSION['searchterms'][2];
        }
        $result = $this->model->doUserSearch($safeterm, $active, $searchall);
        if ($result->num_rows < 1) {
            $_SESSION['returncode'] = 5;
            $_SESSION['searchterm'] = $searchterm;
            $_SESSION['searchactive'] = $active;
            $_SESSION['searchall'] = $searchall;
            return $this->getSearchUser();
        }
        else {
            $rows=$this->registry->db->getAllRows();
            $rvalue['type']='html';
            $rvalue['variables']['title']="User Search Results";
            $rvalue['variables']['results']=$rows;
            if (isset($_POST['searchall'])) { $rvalue['variables']['searchall']=$_POST['searchall']; } else { $rvalue['variables']['searchall']=1; }
            $rvalue['variables']['searchactive'] = $active;
            $rvalue['headerfile']="header.php";
            $rvalue['bodyfile'] = "users/body_search_user_results.php";
            $rvalue['footerfile']="footer.php";
            return $rvalue;      
        }
    }

}
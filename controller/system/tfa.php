<?php

class tfa extends baseController {


	public function index() {
		// First, if the 2FA is turned off, there's no need to be here
		// Send the user back
		if ($this->registry->config['twofactor'] == FALSE || $this->registry->config['logins'] == FALSE) {
			$this->registry->goHome();
		}

		//$request = $this->registry->pageArray;
		// return $this->output = $this->getValue($request[1]);
        $this->model = new mTwoFactor($this->registry);
		
		// First, sanitize all POSTed data
        $this->registry->security->sanitizePost(); 


        /* Is the user asking for a resent code? If so, let's do that first */
        if ($this->registry->passedVars && strtolower($this->registry->passedVars[0])=="resend") {
        	$explanation = "USER ASKED TO RESEND 2FA CODE: %%USER%% FROM %%IP%%";
			$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        	return $this->getValidationCode(false, true);
        }


        /* Is this user needing to verify themselves? If so, we send them to do that! */
        if ($this->registry->passedVars && strtolower($this->registry->passedVars[0])=="verify") {
        	$vars = $this->registry->passedVars;
        	$count = count($vars);
        	if ($count > 1 && strtolower($this->registry->passedVars[1])=="resend") {
        		$explanation = "USER ASKED TO RESEND 2FA CODE: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				$_SESSION['returncode'] = 1;
				return $this->getVerified(true);
        	}
        	else {
	        	$explanation = "USER REQUIRED TO VERIFY VIA 2FA NOW: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
	        	return $this->getVerified(false);
	        }
        }

		/* If POSTed data was sent, then we should do the validation, if not, we should get the login from the user */
		if (isset($_POST['phone'])) {
			return $this->getValidationCode();
		}
		else if (isset($_POST['code'])) {
			return $this->getFinalValidation();
		}
		else if (isset($_POST['submit'])) {
			if ($_POST['submit'] == "cancel") {
				$this->registry->goHome();
			}
			if ($_POST['submit'] == "off") {
				$usernum = $_SESSION['usernum'];
				if ($this->model->reset2FA($usernum)) {
					$_SESSION['returncode'] = 4;
					$explanation = "USER TURNED OFF 2FA: %%USER%% FROM %%IP%%";
					$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				}
				else {
					$_SESSION['returncode'] = 2;
				}
				$_SESSION['returnmessage'] = "Two-Factor Authentication Disabled Successfully.";
				$this->registry->goHome();
			}
		}
		else {
			return $this->getTwoFactorSignup();
		}

	}

	
	public function getTwoFactorSignup() {
				
		/* Find out if the user already has 2FA active.  */
		$usernum = $_SESSION['usernum'];
		$tfaactive = $this->model->tfacheck($usernum);
		if ($tfaactive == 1) {
				$rvalue['variables']['phone'] = $this->registry->template->prettyPhone($this->model->get2FAPhone($usernum));
				$rvalue['type']='html';
				$rvalue['variables']['title']="Two Factor Validation - Active";
				$rvalue['headerfile']="header.php";
				$rvalue['bodyfile'] = "twofactor/twofactoractive.php";
				$rvalue['footerfile']="footer.php";
				return $rvalue;
		}
		elseif ($tfaactive == 2) {
			return $this->getValidationCode(true);
		}

		/* If the 2FA is via email only, then we send them an emailed code and go directly to the validation step */
		if ($this->registry->config['twofactor_method'] ==1) {
			$emailcode = $this->getValidationCode();
			if ($emailcode) {
				return $this->getValidationCode(true);	
			}
			else {
				return false;
			}
		}


		/* Display the two factor page and get the 2FA information */
		$rvalue['type']='html';
		$rvalue['variables']['title']="Two-Factor Authentication";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "twofactor/twofactorsignup.php";
		$rvalue['footerfile']="footer.php";
		return $rvalue;
	}


	// This method takes in the phone number from step 1, sends it to the model
	// to get the 6-digit code sent via SMS, then displays the page that asks
	// the user to give back the 6-digit code.  If the user comes back to this
	// page later (while validating - $waiting==true), then it simply redisplays
	// the page without getting a new code.  Also, if the user needs their code
	// resent because it didn't go through $resend=true, this will invalidate
	// their prior code and give them a whole new one and start the expire timer over
	public function getValidationCode($waiting=false, $resend=false) {
		// Is this email only?  
		$tfatype = $this->registry->config['twofactor_method'];
		if ($tfatype == 1) { $phone = false; }

		if ($waiting == true) {
			if (isset($_SESSION['returncode'])) { unset($_SESSION['returncode']); }
			$rvalue['type']='html';
			$rvalue['variables']['title']="Confirm Two Factor Validation Code";
			$rvalue['headerfile']="header.php";
			$rvalue['bodyfile'] = "twofactor/twofactorvalidate.php";
			$rvalue['footerfile']="footer.php";
			$rvalue['variables']['url']=$this->registry->config['url'];
			return $rvalue;
		}
		else {
			if ($resend==true) { $phone = false; } else { if ($tfatype == 2) { $phone = $_POST['phone']; } 
		}
			$usernum = $_SESSION['usernum'];
		}
		if ($tfatype == 2 && $resend==false && (strlen($phone) < 10 || !is_numeric($phone))) {
			$_SESSION['returncode']=3;
			return $this->getTwoFactorSignup();
		}
		else {
			$result = $this->model->validate2FA($usernum, $phone);
			if ($result == false) {
				$_SESSION['returncode']=2;
				return $this->getTwoFactorSignup();
			}
			else {
				$explanation = "USER STARTED 2FA SETUP - CODE SENT: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				$_SESSION['returncode']=1;
				$rvalue['type']='html';
				$rvalue['variables']['title']="Confirm Two Factor Validation Code";
				$rvalue['headerfile']="header.php";
				$rvalue['bodyfile'] = "twofactor/twofactorvalidate.php";
				$rvalue['footerfile']="footer.php";
				$rvalue['variables']['url']=$this->registry->config['url'];
				return $rvalue;
			}
		}
	}
	
	public function getFinalValidation() {
		$code = $_POST['code'];
		$usernum = $_SESSION['usernum'];
		// First, let's check to see if the code is right.
		if ($this->model->check2FACode($usernum, $code)) {
			if ($this->model->set2FAValid($usernum)) {
				$_SESSION['returncode']=1;
				$explanation = "USER COMPLETED 2FA SETUP - 2FA ACTIVE: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				return $this->getTwoFactorSignup();
			}
			else {
				$_SESSION['returncode']=2;
				$explanation = "USER FAILED 2FA SETUP - SYSTEM ERROR: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				return $this->getValidationCode();
			}
		}
		else {
			// Let's undo the user's 2FA attempt and reset
			if ($this->model->reset2FA($usernum)) {
				$_SESSION['returncode'] = 5;
				$explanation = "USER FAILED 2FA SETUP - CODE INCORRECT: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
			}
			else {
				$_SESSION['returncode'] = 2;
				$explanation = "USER FAILED 2FA SETUP - SYSTEM ERROR: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
			}
			return $this->getTwoFactorSignup();
		}
	}

	// This method performs the actual 2FA process once the user is signed up
	// and needs to verify themselves on subsequent login
	public function getVerified($resend = false) {
		$usernum = $_SESSION['usernum'];
		/* Did the user already enter a code? If so, let's check it */
		if (isset($_POST['code'])) {
			$code = $_POST['code'];	
			$remember = $_POST['remember'];
			// If the code is good, let's send the user on their way
			// If the user asked to be remembered, lets do that as well
			if ($this->model->check2FACode($usernum, $code)) {
				if ($remember == 1) {
					$this->model->rememberDevice($usernum);
				}
				$_SESSION['twofactor_pass'] = true;
				$explanation = "USER PASSED 2FA VALIDATION: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				$this->registry->goHome();
			}
			// If the code if bad, let's send them AWAY
			else {
				unset($_SESSION['twofactor_pass']);
				$_SESSION['returncode'] = 9999;
				$explanation = "USER FAILED 2FA VALIDATION: %%USER%% FROM %%IP%%";
				$this->registry->security->reportAttack();
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				$this->registry->redirect('logout');
			}
		}
		/* Is there a code already pending? If so, no need to send another! */
		if ($this->model->tfaqueue($usernum) == false || $resend == true) {
			/* Now, let's generate the code */
			$result = $this->model->validate2FA($usernum, false, true);
			if ($result == false) {
				echo "A SERIOUS ERROR HAS OCCURRED IN getVerified() IN THE tfa MODULE - ALERT SYSTEM ADMIN."; 
				exit();
			}
		}
		/* Display the two factor page and get the 2FA information */
		$rvalue['type']='html';
		$rvalue['variables']['title']="Two-Factor Authentication";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "twofactor/twofactor.php";
		$rvalue['footerfile']="footer.php";
		$rvalue['variables']['url']=$this->registry->config['url'];
		return $rvalue;
	}
	
	
}

?>

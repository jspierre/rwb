<?php

class bgtaskmgmt extends baseController {


    public function index() {
        /* First, we call in the model we'll need */
        $this->model = new mSystem($this->registry);
        
        // If any POSTed data was sent, we need to sanitize it
        $this->registry->security->sanitizePost(); 

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if (substr($_POST['submit'],0,3) == "BGX") {
                $bgtasknum = substr($_POST['submit'],3);
                return $this->getUpdateTask($bgtasknum,'EDIT');
            }
            if ($_POST['submit'] == "new") {
                return $this->getUpdateTask();
            }
            if ($_POST['submit'] == "savenew") {
                return $this->doUpdateTask('NEW');
            }
            if ($_POST['submit'] == "update") {
                if (isset($_SESSION['bgtasknum'])) {
                    $bgtasknum = $_SESSION['bgtasknum'];
                    return $this->doUpdateTask('UPDATE');
                }
            }
            if ($_POST['submit'] == "delete" && $this->registry->security->checkFunction("deletetask")) {
                $bgtasknum = $_SESSION['bgtasknum'];
                $modulename = $_POST['modulename'];
                $friendlyname = $_POST['friendlyname'];
                if($this->model->deleteTask($bgtasknum)) {
                    $explanation = "BACKGROUND TASK DELETED. USER: %%USER%% MODULE: $bgtasknum ($modulename - $friendlyname)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 2;
                }
                else {
                    $explanation = "BG TASK DELETE FAILED. USER: %%USER%% MODULE: $bgtasknum ($modulename - $friendlyname)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 5; 
                }
                return $this->showAllTasks();
            }
            if ($_POST['submit'] == "bgtaskmgmt") {
                   return $this->showAllTasks();
            }
        }
        else {
             if ($this->registry->passedVars) {
                if (is_numeric($this->registry->passedVars[0])) {
                    return $this->getUpdateTask($this->registry->passedVars[0], 'EDIT');
                }
            }
            return $this->showAllTasks();
        }
    }

    // This method returns all modules available for editing and displays them in a list
    // on screen to be chosen from.  IT NEVER shows moduleindex #1 as that is for internal
    // Use only.
    public function showAllTasks() {
        $tasks = $this->model->getAllTasks();
        $rvalue['type']='html';
        $rvalue['variables']['title']="Background Task Management";
        $rvalue['variables']['tasks']=$tasks;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_show_tasks.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }


    public function getUpdateTask($bgtasknum=false, $action=false, $rvalues=false) {
        $rvalue['type']='html';
        $rvalue['variables']['title']="Background Task: Edit";
        $redisplay = false;
        if (isset($_SESSION['returncode'])) {
            if ($_SESSION['returncode'] == 5) {
                $redisplay = true;
            }
        }
        if ($bgtasknum && !$redisplay) {
            $vars = $this->model->getTask($bgtasknum);
            // If a module was actually pulled, set it up for display
            // If not, we won't set the variables and the add module screen will show instead with an error message
            if ($vars) {
                $rvalue['variables']['task']=$vars;
                $rvalue['variables']['actiontype']=$action;
                $_SESSION['bgtasknum']=$bgtasknum;    
            }
            else {
                $rvalue['variables']['error']['bgtasknum'] = $bgtasknum;
                $_SESSION['returncode'] = 6;
            }
        }
        elseif ($bgtasknum && $redisplay) {   
            //$_POST = $_SESSION['tempdata'];
            //$this->registry->security->sanitizePost();
            $vars=$_POST;           
            $rvalue['variables']['task']=$vars;
            $rvalue['variables']['actiontype']=$action;
            $rvalue['variables']['task']['bgtasknum']=$bgtasknum;
        }
        if ($rvalues) {
            foreach ($rvalues as $key=>$value) {
                $rvalue['variables'][$key]=$value;
            }
        }
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_update_task.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }


    public function doUpdateTask($actiontype="NEW") {
        // If This is a new entry, we first check for a duplicate, then do the add if none is found
        if ($actiontype == "NEW") {
            if (isset($_SESSION['bgtasknum'])) {unset($_SESSION['bgtasknum']);}
            $result = $this->model->newTask($_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['bgtasknum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateTask(false, 'NEW', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateTask(false, 'NEW', $rvalues);
                }
            }
            else {
                    $_SESSION['bgtasknum'] = $result[2];
                    $_SESSION['returncode'] = 2;
                    return $this->getUpdateTask($result[2], 'EDIT');
            }
        }
        elseif ($actiontype == "UPDATE") {
            $bgtasknum = $_SESSION['bgtasknum'];
            $result = $this->model->updateTask($bgtasknum, $_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['bgtasknum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    $rvalues['tempdata']['groupindex'] = $bgtasknum;
                    return $this->getUpdateTask($bgtasknum, 'EDIT', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['bgtasknum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateTask(false, 'EDIT', $rvalues);
                }
            }
            else {
                    $_SESSION['bgtasknum'] = $result[2];
                    $_SESSION['returncode'] = 3;
                    return $this->getUpdateTask($result[2], 'EDIT');
            }
        }
    }

}
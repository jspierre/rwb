<?php

class main extends baseController {

	public function index() {
		$request = $this->registry->pageArray;
		if (!isset($request[1])) {
			$request[1] = "";
		} 
		return $this->output = $this->getValue($request[1]);
	}


	
	public function getValue($pageattempt) {
		$loginnum = $_SESSION['usernum'];
		$rvalue['type']='html';
		$rvalue['variables']['title']=$this->registry->config['sitename']." - Welcome";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "body_main_logged_in.php";
		$rvalue['footerfile']="footer.php";
		return $rvalue;
	}
}

?>

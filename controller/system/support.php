<?php

class support extends baseController {

    private $oldpost;

    public function index() {
        /* Now, let's call in the model we'll need */
        $this->model = new mUser($this->registry);

        // First, sanitize all POSTed data
        $this->oldpost = $_POST;
        $this->registry->security->sanitizePost(); 
        
        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if ($_POST['submit'] == "gohome") {
                $this->registry->goHome();
                exit();
            }
            if ($_POST['submit'] == "submitticket") {
                    $usernum = $_SESSION['usernum'];
                    return $this->updateTicket($usernum);
            }
        }
        return $this->getSupportForm();

    }


    // Displays the support and help form
    public function getSupportForm($usernum=false, $returncode=false, $result=false) {
        $rvalue['variables']['usernum'] = $usernum;
        $rvalue['variables']['returncode'] = $returncode;
        $rvalue['variables']['result'] = $result;
        $rvalue['variables']['action'] = "ADD";
        $rvalue['type']='html';
        $rvalue['variables']['title']="Help and Support";
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_help_support.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }

    
    public function updateTicket($usernum) {
        $support = new mSupport($this->registry);
        $result = $support->createNewTicket($_POST);
        $rvalue['type']='html';
        $rvalue['variables']['title']="Help and Support";
        $rvalue['variables']['usernum']=$usernum;
        $rvalue['variables']['action']="DISPLAY";
        $rvalue['variables']['values']=$this->oldpost;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_help_support.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue; 
    }

    


}
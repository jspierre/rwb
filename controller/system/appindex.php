<?php

class appindex extends baseController {


	public function index() {
		$request = $this->registry->pageArray;
		if (!isset($request[1])) {
			$request[1] = "";
		} 
		
		// We must initialize the session here because it is a login page.
		// see the login controller for more info
		session_destroy();
		session_start();
		$userip = $_SERVER['REMOTE_ADDR'];
		$_SESSION['login']=$userip;

		// Now we can display our page
		return $this->output = $this->getValue($request[1]); 
	}
	
	public function getValue($pageattempt) {
		$rvalue['type']='html';
		$rvalue['variables']['title']=$this->registry->config['sitename']." - Welcome";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "body_main_logged_out.php";
		$rvalue['footerfile']="footer2.php";
		return $rvalue;
	}
}

?>

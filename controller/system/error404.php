<?php

class error404 extends baseController {


	public function index() {
		$request = $this->registry->pageArray;
		if (!isset($request[1])) {
			$request[1] = "";
		} 
		return $this->output = $this->getValue($request[1]);
	}
	
	public function getValue($pageattempt) {
		$rvalue['type']='html';
		$rvalue['variables']['title']="Error 404 - Page Not Found";
		$rvalue['headerfile']="header.php";
		$rvalue['bodytext'] = '<button type="button" class="btn btn-danger">ERROR 404 - PAGE NOT FOUND</button>';
		$rvalue['footerfile']="footer.php";
		$addon="";
		return $rvalue;
	}
}
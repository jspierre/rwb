<?php

class signup extends baseController {


    public function index() {
        // First, if the self-create users is turned off, there's no need to be here
        // Send the user back
        if ($this->registry->config['createself'] == FALSE) {
            $this->registry->goHome();
        }

        // If we are logged in, we cannot use the signup controller.  Back you go!
        if ($this->registry->security->isLoggedIn() == TRUE) { 
            $this->registry->goHome();
        }

        /* Now, let's call in the model we'll need */
        $this->model = new mUser($this->registry);

        // First, sanitize all POSTed data
        $this->registry->security->sanitizePost(); 
        
        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if ($_POST['submit'] == "new") {
                return $this->dosignupUser('NEW');
            }
        }
        else {
            return $this->getSignupForm();
        }
    }

    // Displays the signup form
    public function getSignupForm($usernum=false, $returncode=false, $result=false) {
        $rvalue['variables']['usernum'] = $usernum;
        $rvalue['variables']['returncode'] = $returncode;
        $rvalue['variables']['result'] = $result;
        $rvalue['type']='html';
        $rvalue['variables']['title']="New User Signup";
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_signup_user.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }

    public function doSignupUser($actiontype="NEW") {
        $vars=$_POST;
        $result = $this->model->UpdateUser($vars,$actiontype,true);
        if ($result[0] == false) {
            if ($result[1] == "ADD_USER_ERROR_DUPLICATE_USER") {
                $returncode=5;
                return $this->getSignupForm($result[2]['loginnum'],$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_DUPLICATE_EMAIL") {
                $returncode=13;
                return $this->getSignupForm($result[2]['loginnum'],$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_INVALID_USERNAME_EMAIL3" || $result[1] == "ADD_USER_ERROR_INVALID_EMAIL4") {
                $returncode=7;
                return $this->getSignupForm(false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_INVALID_OFFERCODE") {
                $returncode=8;
                return $this->getSignupForm(false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_ERROR_NOT_REAL_NAME") {
                $returncode=9;
                return $this->getSignupForm(false,$returncode,$result);        
            }
            if ($result[1] == "ADD_USER_TC_NON_AGREE") {
                $returncode=11;
                return $this->getSignupForm(false,$returncode,$result);
            }
        }
        if ($actiontype == "NEW") {
            //var_dump($result);
            //exit();
            $password = $result[2]['data']['returnpass'];
            $to = $result[2]['data']['emailaddress'];
            $name = $result[2]['data']['firstname'];
            $name = ucfirst(strtolower($name));
            $username = $result[2]['data']['username'];
            $lastname = $result[2]['data']['lastname'];
            if (isset($result[2]['data']['offercode']) && trim($result[2]['data']['offercode']) != "") {
                $offercode = trim(strtolower($result['2']['data']['offercode']));
                $usedoffer = true;
            }
            else { $usedoffer = false; }
            $lastname = ucfirst(strtolower($lastname));
            $fullname = $name." ".$lastname;
            $mail = new email($this->registry);
            $subject = "Welcome to ".$this->registry->config['sitename']." - Your Password Enclosed";
            $body = "Dear $name,\n\nYour account with ".$this->registry->config['sitename']." is now set up and ready for use!\nPlease see your new temporary password below, which you'll need to log in:\n\nTEMPORARY PASSWORD: ".$password."\nLOG IN AT: ".$this->registry->config['url']."\n\nNote that this password is CASE-SENSITIVE. As soon as you log in the first\ntime, you'll be forced to change it to something easier for you to remember.\n\nIf you did not sign up at our site for this account, please forward this email\nimmediately to us at ".$this->registry->config['siteemail']." and let us know, or\ncall us at ".$this->registry->config['sitephone']." and we can investigate further for you.\n\nSincerely,\n\nThe System Administration Team\n".$this->registry->config['sitename'];
            $mail->sendMail($to,false,$subject,$body);         
            $creatornum = $result[2]['loginnum'];
            $loginnum = $creatornum;
            if ($this->registry->config['notifyoncreate']) {
                $adminemails = $this->registry->config['notifyoncreate_emails'];
                foreach ($adminemails as $adminemail) {
                    if ($usedoffer) { $extra = "Offer Code: ".$offercode."\n\n"; } elseif ($this->registry->config['offercodeon']) { $extra = "Offer Code: --none--\n\n"; } else { $extra = "\n"; }
                    $mail = new email($this->registry);
                    $subject = "ALERT: New User signup to ".$this->registry->config['sitename'].".";
                    $body = "Hello!,\n\nThe self-signup system at ".$this->registry->config['sitename']." was just used to create a new account as follows:\n\nUsername: ".$username."\nEmail: ".$to."\nName: ".$fullname."\n".$extra."The user was emailed with their temporary first password for login.\n\nSincerely,\n\nThe Administration Team\n".$this->registry->config['sitename'];
                    $mail->sendMail($adminemail,false,$subject,$body);                  
                }
            }
            $message = "NEW USER SELF-ADDED";
            if ($usedoffer) { $message .= " USING OFFER CODE ".$offercode; }
            $this->model->doLogUser($creatornum, $loginnum, $message);
            $rvalue['type']='html';
            $rvalue['variables']['title']="New User Signup Successful";
            $rvalue['headerfile']="header.php";
            $rvalue['bodyfile'] = "users/body_signup_user_success.php";
            $rvalue['footerfile']="footer.php";
            return $rvalue;
        }
    }



}
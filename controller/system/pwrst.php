<?php

class pwrst extends baseController {


	public function index() {
		// First, if the password resets are turned off, there's no need to be here
		// Send the user back
		if ($this->registry->config['resetpw'] == FALSE) {
			$this->registry->goHome();
		}

		//$request = $this->registry->pageArray;
		// return $this->output = $this->getValue($request[1]);
        $this->model = new mSecurity($this->registry);
		
		if ($this->registry->config['logins'] == FALSE) {
			$this->registry->goHome();
		}

		// If we are logged in, we cannot use the password reset controller.  Back you go!
        if ($this->registry->security->isLoggedIn() == TRUE) { 
            $this->registry->goHome();
        }
		
		// First, sanitize all POSTed data
    	$this->registry->security->sanitizePost(); 

		
		/* If POSTed data was sent, then we should do the pwchange, if not, we should get the login from the user */
		if (isset($_POST['user'])) {
			return $this->doResetPassword();
		}
		else {
			return $this->getResetPassword();
		}
	}

	
	public function getResetPassword() {
		
		/* If the user IS logged in, send him back to the main page */
		if ($this->registry->security->isLoggedIn() == TRUE) {
			$this->registry->goHome();
		}
		
		/* Set a session variable to disable POSTing from remote sites */
		$userip = $_SERVER['REMOTE_ADDR'];
		$_SESSION['login']=$userip;
		
		/* Display the password page and get the password information */
		$rvalue['type']='html';
		$rvalue['variables']['title']="Password Reset";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "bodypwrst.php";
		$rvalue['footerfile']="footer.php";
		$rvalue['pwlength']=$this->registry->config['pwlength'];
		$rvalue['url']=$this->registry->config['url'];
		return $rvalue;
	}
	
	
	public function doResetPassword() {
		/* Get POST data and other variables required */
		$user = $_POST['user'];
		$userip = $_SERVER['REMOTE_ADDR'];
		
		/* Let's check to see if the request was submitted from our app or not */
		if ($_SESSION['login']<>$userip) {
			$this->model->failAuthentication("XSS_ATTACK",3);
		}

		/* Now let's attempt to send a password reset */
		$pwreset = $this->model->resetUserPassword($user);

		
		/* If successful, give success page. If not, give error */
		if($pwreset) {
			$rvalue['type']='html';
			$rvalue['variables']['title']="Password Reset Success!";
			$rvalue['headerfile']="header.php";
			$rvalue['bodyfile'] = "bodypwrst.php";
			$rvalue['footerfile']="footer.php";
			$rvalue['url']=$this->registry->config['url'];
			$_SESSION['returncode']=15;
			return $rvalue;
		}
		else {
			$rvalue['type']='html';
			$rvalue['variables']['title']="Password Reset FAILED!";
			$rvalue['headerfile']="header.php";
			$rvalue['bodyfile'] = "bodypwrst.php";
			$rvalue['footerfile']="footer.php";
			$rvalue['url']=$this->registry->config['url'];
			$_SESSION['returncode']=99;
			return $rvalue;
		}
	}
	
	
}

?>

<?php

class tandc extends baseController {


	public function index() {
		$request = $this->registry->pageArray;
		if (!isset($request[1])) {
			$request[1] = "";
		} 
		return $this->output = $this->getValue($request[1]); 
	
	}
	
	public function getValue($pageattempt) {
		$rvalue['type']='html';
		$rvalue['variables']['title']="Terms of Use - ".$this->registry->config['sitename'];
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "tandc.php";
		$rvalue['footerfile']="footer2.php";
		return $rvalue;
	}
}

?>

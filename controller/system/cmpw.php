<?php

class pwchg extends baseController {


	public function index() {
        $this->model = new mSecurity($this->registry);
		
		if ($this->registry->config['logins'] == FALSE) {
			$this->registry->goHome();
		}
		
		// First, sanitize all POSTed data
        if ($_POST) {
            $this->registry->security->sanitizePost();  
        }
		
		/* If POSTed data was sent, then we should do the pwchange, if not, we should get the login from the user */
		if (isset($_POST['oldpass'])) {
			return $this->doPasswordChange();
		}
		else {
			return $this->getPasswordChange();
		}
	}

	

	
	
	public function getPasswordChange() {
		
		/* If the user is NOT logged in, send him back to the main page */
		if ($this->registry->security->isLoggedIn() == FALSE) {
			session_destroy();
			session_start();
			$this->registry->goHome();
		}
		
		/* Set a session variable to disable POSTing from remote sites */
		$userip = $_SERVER['REMOTE_ADDR'];
		$_SESSION['login']=$userip;
		
		/* Display the password page and get the password information */
		$rvalue['type']='html';
		$rvalue['variables']['title']="Password Change";
		$rvalue['headerfile']="header.php";
		$rvalue['bodyfile'] = "bodypwchg.php";
		$rvalue['footerfile']="footer.php";
		$rvalue['pwlength']=$this->registry->config['pwlength'];
		$rvalue['url']=$this->registry->config['url'];
		return $rvalue;
	}
	
	
	public function doPasswordChange() {
		/* Get POST data and other variables required */
		$oldpass = $_POST['oldpass'];
		$newpass1 = $_POST['newpass1'];
		$newpass2 = $_POST['newpass2'];
		$userip = $_SERVER['REMOTE_ADDR'];
		$usernum = $_SESSION['usernum'];
		$username = $_SESSION['username'];
		
		/* Let's check to see if the request was submitted from our app or not */
		if ($_SESSION['login']<>$userip) {
			$this->model->failAuthentication("XSS_ATTACK",3);
		}

		/* Now let's attempt to change the password */
		$pwchange = $this->model->changeUserPassword($username, $oldpass, $newpass1, $newpass2);

		
		/* If successful, give success page. If not, give error */
		if($pwchange) {
			$rvalue['type']='html';
			$rvalue['variables']['title']="Password Change Success!";
			$rvalue['headerfile']="header.php";
			$rvalue['bodyfile'] = "bodypwchg.php";
			$rvalue['footerfile']="footer.php";
			$rvalue['url']=$this->registry->config['url'];
			$_SESSION['returncode']=15;
			return $rvalue;
		}
		else {
			$this->model->failPasswordChange();		
		}
	}
	
	
}

?>

<?php

class fncmgmt extends baseController {

    public function index() {
        /* First, we call in the model we'll need */
        $this->model = new mSecurity($this->registry);
        
        // If any POSTed data was sent, we need to sanitize it
        $this->registry->security->sanitizePost(); 

        /* If POSTed data was sent, then we should validate the data,  */
        /* if not, we should get the data from the user on a fresh form */
        if (isset($_POST['submit'])) {
            if (substr($_POST['submit'],0,3) == "FNX") {
                $funcnum = substr($_POST['submit'],3);
                return $this->getUpdateFunction($funcnum,'EDIT');
            }
            if ($_POST['submit'] == "new") {
                return $this->getUpdateFunction();
            }
            if ($_POST['submit'] == "savenew") {
                return $this->doUpdateFunction('NEW');
            }
            if ($_POST['submit'] == "update") {
                if (isset($_SESSION['funcnum'])) {
                    $funcnum = $_SESSION['funcnum'];
                    return $this->doUpdateFunction('UPDATE');
                }
            }
            if ($_POST['submit'] == "delete" && $this->registry->security->checkFunction("deletefunction")) {
                $funcnum = $_SESSION['funcnum'];
                $funcname = $_POST['funcname'];
                $funcdescr = $_POST['funcdescr'];
                if($this->model->deleteFunction($funcnum)) {
                    $explanation = "FUNCTION DELETED. USER: %%USER%% FUNCTION: $funcnum ($funcname - $funcdescr)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 2;
                }
                else {
                    $explanation = "FUNCTION DELETE FAILED. USER: %%USER%% FUNCTION: $funcnum ($funcname - $funcdescr)";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    $_SESSION['returncode'] = 5; 
                }
                return $this->getAllFunctions();
            }
            if ($_POST['submit'] == "funcmgmt") {
                   return $this->getAllFunctions();
            }
        }
        else {
             if ($this->registry->passedVars) {
                if (is_numeric($this->registry->passedVars[0])) {
                    return $this->getUpdateFunction($this->registry->passedVars[0], 'EDIT');
                }
            }
            return $this->getAllFunctions();
        }
    }

    // This method returns all modules available for editing and displays them in a list
    // on screen to be chosen from.  IT NEVER shouls moduleindex #1 as that is for internal
    // Use only.
    public function getAllFunctions() {
        $functions = $this->model->getAllFunctions();
        $rvalue['type']='html';
        $rvalue['variables']['title']="Module Function Granular Management Tool";
        $rvalue['variables']['functions']=$functions;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_show_functions.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }



    public function getUpdateFunction($funcnum=false, $action=false, $rvalues=false) {
        $rvalue['type']='html';
        $rvalue['variables']['title']="Module Function Granular Management: Edit"; 
        $redisplay = false;
        if (isset($_SESSION['returncode'])) {
            if ($_SESSION['returncode'] == 5) {
                $redisplay = true;
            }
        }
        if ($funcnum && !$redisplay) {
            $modules=$this->model->getAllModules(true, true);
            $rvalue['variables']['modules']=$modules;
            $vars=$this->model->getFunction($funcnum);
            if ($vars) {
                $rvalue['variables']['function']=$vars;
                $rvalue['variables']['actiontype']=$action;
                $_SESSION['funcnum']=$funcnum;    
            }
            else {
                $rvalue['variables']['error']['funcnum'] = $funcnum;
                $_SESSION['returncode'] = 6;
            }
        }
        elseif ($funcnum && $redisplay) {   
            //$_POST = $_SESSION['tempdata'];
            //$this->registry->security->sanitizePost();
            $modules=$this->model->getAllModules(true, true);
            $rvalue['variables']['modules']=$modules; 
            $vars=$this->model->getFunction($funcnum);
            $vars=$_POST;          
            $rvalue['variables']['function']=$vars;
            $rvalue['variables']['actiontype']=$action;
            $rvalue['variables']['function']['funcnum']=$funcnum;
        }
        else {
            $modules=$this->model->getAllModules(true, true);
            $rvalue['variables']['modules']=$modules; 
        }
        if ($rvalues) {
            foreach ($rvalues as $key=>$value) {
                $rvalue['variables'][$key]=$value;
            }
        }
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "body_update_function.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue;
    }


     public function doUpdateFunction($actiontype="NEW") {
        // If This is a new entry, we first check for a duplicate, then do the add if none is found
        if ($actiontype == "NEW") {
            if (isset($_SESSION['funcnum'])) {unset($_SESSION['funcnum']);}
            $result = $this->model->newFunction($_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'NEW', $rvalues);
                }
            }
            else {
                    $_SESSION['funcnum'] = $result[2];
                    $_SESSION['returncode'] = 2;
                    return $this->getUpdateFunction($result[2], 'EDIT');
            }
        }
        elseif ($actiontype == "UPDATE") {
            $funcnum = $_SESSION['funcnum'];
            $result = $this->model->updateFunction($funcnum, $_POST);
            if ($result[0] == false) {
                if ($result[1]=="DUPLICATE") {
                    $_SESSION['returncode'] = 5;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    $rvalues['tempdata']['groupindex'] = $funcnum;
                    return $this->getUpdateFunction($funcnum, 'EDIT', $rvalues);
                }
                else {
                    $_SESSION['returncode'] = 6;
                    $rvalues['funcnum'] = $result[2];
                    $rvalues['tempdata'] = $_POST;
                    return $this->getUpdateFunction(false, 'EDIT', $rvalues);
                }
            }
            else {
                    $_SESSION['funcnum'] = $result[2];
                    $_SESSION['returncode'] = 3;
                    return $this->getUpdateFunction($result[2], 'EDIT');
            }
        }
    }

}
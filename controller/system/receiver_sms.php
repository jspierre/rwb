<?php

class receiver_sms extends baseController {

    public function index() {
        // If we are logged in, we cannot be using this controller.  Back you go!
        if ($this->registry->security->isLoggedIn() == TRUE) { 
            $this->registry->goHome();
        }

        /* Now, let's call in the model we'll need */
        $this->model = new mSupport($this->registry);

        // This is coming from Twilio, no need to sanitize
        // $this->registry->security->sanitizePost(); 

        // Test data for testing
        //$_REQUEST['MessageSid'] = "ABC1234";
		//$_REQUEST['From'] = "+15042501616";
		//$_REQUEST['Body'] = "This shit sucks - please help";
        
        /* If TWILIO data was sent, then we should validate the data,  */
		if (isset($_REQUEST)) {
			if(isset($_REQUEST['MessageSid'])) { $sid = $_REQUEST['MessageSid']; } else { $this->registry->security->doSecurity(false); exit(); }
			if(isset($_REQUEST['From'])) { $from = $_REQUEST['From']; } else { $this->registry->security->doSecurity(false); exit(); }
			if(isset($_REQUEST['Body'])) { $body = $_REQUEST['Body']; } else { $body = ""; }
		}

		/* Okay, now we send the data straight into the database */
		$explanation = "INCOMING SUPPORT SMS FROM ".$_REQUEST['From']." : ".$_REQUEST['Body'];
		$this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
		$result = $this->model->incomingSMS($_REQUEST);
		if ($result) {
			$explanation = "RECORDED AND NOTIFIED INCOMING SMS FROM ".$_REQUEST['From']." : ".$_REQUEST['Body'];
		}
		else {
			$explanation = "FAILED WHILE RECORDING AND/OR NOTIFYING SMS FROM ".$_REQUEST['From']." : ".$_REQUEST['Body'];	
		}
		$this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
		exit();
	}



}

?>
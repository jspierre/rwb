<?php

class sysadmin extends baseController {

    /*  
       Payload Return (comes from MODEL)
            $returnPayload[0]   BOOL - was the operation successful? (true/false)
            $returnPayload[1]   INT - Level of severity (0=None [default], 1=Success, 2=Info, 3=Warning, 4=Danger, 5=BLACK)
            $returnPayload[2]   STRING - a short description of the success or failure
            $returnPayload[3]   STRING- The message the user will see in the alert box if shown
            $returnPayload[4]   ARRAY - All of the data that came from the VIEW on the FORM, so it can 
                                be redisplayed along with the error at top OR the data from the database
                                of those same fields for display
    */

    // ********************************************************************************************                      
    // These private variables are ALL you should have to set any time you instantiate this class.
    // UNLESS you need you need to send extra data to your view, then see ~line 184
    // ********************************************************************************************

    // These three variables contain the ACL "Function Names" of the functions that give access 
    // to each of these functions.  Set them before you continue.  If you don't have ACL functions,
    // create them through the Web Interface.  If you don't have security turned on, it won't matter. 
    private $acl_display = "crsdisplay";
    private $acl_add = "crsadd";
    private $acl_edit = "crsedit";

    // This variable is the name of the model that you'll be working with. 
    private $modelname = "mSecurity";

    // This is the name of the search controller, if any, that you are coming from
    // If you are not working with a search, set this to the controller that you
    // want anyone who presses "BACK" to go to.
    private $searchname = "main";

    // Location and name of your VIEW file
    private $viewloc = "body_update_system_settings.php";

    // These variables contain some additional data to pass to your View.
    // $pagetitle - shows up in the browser tab; $thingname - when viewing a record, THING # NNN will
    //    be shown.  Change $thingname to "USER" or "COURSE" or whatever as needed.
    private $pagetitle = "System Settings Management";  
    private $thingname = "Settings";


    // ********************************************************************************************                      
    // ********************************************************************************************
    // ********************************************************************************************



    public function index() {
        // First, we call in the model we'll need
        $mname = $this->modelname;
        $this->model = new $mname($this->registry);

        // Next, initialize some variables just in case
        $record = false;
        $action = "ADD";
        $posted = false;

        // Let's check for display only data.  This would come in the form of 
        // Passed Variables from the registry.  We read the variable and store it
        // as $record. We also set $edit to false to let the system know this is
        // display only.
        if ($this->registry->passedVars) {
            $record = $this->registry->passedVars[0];
        }

        // Now we can deal with POSTed data, if any.
        // If $record contains a record number, then EITHER $edit or $update can 
        // be TRUE, but NOT BOTH, so we'll automatically help out with that to avoid errors. 
        if (isset($_POST)) {     
            // First, we sanitize the POST data
            $this->registry->security->sanitizePost();  
            $posted = $_POST;

            // Next, we determine what action needs to be taken based 
            // on the POST data 
            if (isset($_POST['record']) && $_POST['record'] != false) {
                $record = $_POST['record'];
            }
            if (isset($_POST['action'])) {
                $action = $_POST['action'];
            }
            else {
                $action = "EDIT";
            }

            // If buttons were pushed without an edit (from Display, for instance)
            // The submit value will tell us what the user wants to do. 
            // Let's deal with that next.
            if (isset($_POST['submit'])) {
                if ($_POST['submit'] == "edit") {
                    $action = "EDIT";
                }
                 if ($_POST['submit'] == "update") {
                    $action = "UPDATE";
                }
                if ($_POST['submit'] == "display") {
                    $_SESSION['lastsearch'] = true;
                    $this->registry->redirect($this->searchname);
                }
            }
        }
        return $this->getCrudView($record,$action,$posted);
    }


    // $record = the record number (index number) of the database item to pull, may be false if none
    // $action = the action to take (ADD, DISPLAY, EDIT, UPDATE)
    // $posted = an array of all of the variables to be pushed through the model for add/update.
    public function getCrudView ($record=false, $action="ADD", $posted=false) {
        // Let's initialize the returnPayload, just in case we don't have one
        $returnPayload[0] = true;
        $returnPayload[1] = "";
        $returnPayload[2] = "";
        $returnPayload[3] = 0;
        $returnPayload[4] = "";

        // If $action = UPDATE, then we need to send the data to the model to be updated
        // If we're doing a DISPLAY or EDIT, we need to pull the record
        // Otherwise, we're doing an add, which really doesn't require anything.
        if ($action == "DISPLAY" || $action == "EDIT") {
            $returnPayload = $this->model->getRecordData();
        }
        if ($action == "UPDATE") {
            if($posted['forcepw'] == 1) { $this->model->forcePasswordChange(); }
            if($posted['forcelogout'] == 1) { $this->model->forceLogout(); }
            if($posted['force2fa'] == 1 ) {$this->model->force2FA(); }
            if($posted['showsysbanner'] == 1) {$this->model->showSystemBanner(true); }
            if($posted['showsysbanner'] == 2) {$this->model->showSystemBanner(false);}
            $returnPayload = $this->model->updateRecordData($posted, $action);
            $getpayload = $this->model->getRecordData();
            $morepayload = $getpayload[4];
            $returnPayload[4]['lastpwchange'] = $morepayload['lastpwchange'];
            $returnPayload[4]['lastforcelogout'] = $morepayload['lastforcelogout'];
            $returnPayload[4]['laststatuschange'] = $morepayload['laststatuschange'];
            $action = "EDIT";
        }

        
        // Before we finish, let's log anything worth logging
        if (!empty($returnPayload[2])) {
            $this->registry->logging->logEvent($this->registry->config['logging_cat_success'],$returnPayload[2]);   
        }


        // Okay, now we send the data to the VIEW to handle displaying everything.
        // Remember when dealing with VIEWS that ['variables'] end up in the view
        // simply as $values, so you address them in the VIEW as $values['action']
        // for instance.
        $rvalue['type']='html';
        $rvalue['variables']['action']= $action;
        $rvalue['variables']['record']= $record;
        $rvalue['variables']['title']=$this->pagetitle;
        $rvalue['variables']['thingname']=$this->thingname;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = $this->viewloc;
        $rvalue['footerfile']="footer.php";
        $rvalue['variables']['returnPayload'] = $returnPayload;
        // IF you need to send extra variables to the VIEW, here is the place to do it:
        //$classstyles = $this->model->getClassStyles();
        //$rvalue['variables']['classstyles']=$classstyles;
        return $rvalue;
    }




    // *** THE BELOW METHODS ARE NOT USED NOW, BUT MAY USE IN FUTURE TO ADD SOME 'SYSTEM ADMIN NOTES' FUNCTIONALITY
    // SO LEAVE THIS SHIT HERE FOR NOW ****

    public function doShowNotes($loginnum, $action) {
        $notes = $this->model->getUserNotes($loginnum);        
        $rvalue['type']='html';
        $rvalue['variables']['title']="User Notes";
        $rvalue['variables']['notes']=$notes;
        $rvalue['variables']['action']=$action;
        $rvalue['headerfile']="header.php";
        $rvalue['bodyfile'] = "users/body_update_user_notes.php";
        $rvalue['footerfile']="footer.php";
        return $rvalue; 
    }

    public function doSaveNotes() {
        $loginnum = $_SESSION['loginnum'];
        $creatornum = $_SESSION['usernum'];
        $newnotes = $_POST['notes'];
        if ($this->model->updateUserNotes($loginnum, $newnotes)) {
            $message = "UPDATED USER NOTES FILE";
            $this->model->doLogUser($creatornum, $loginnum, $message);
            return true;
        }
        else {
            return false;
        }
    }


}
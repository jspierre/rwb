#!/bin/bash

if [ -f main.inc.php ]; then
   echo "Configuration already initialized - exiting."
else
    cp sample.db.inc.php db.inc.php 
    mv sample.db.inc.php dev.db.inc.php 
    mv sample.logging.inc.php logging.inc.php
    cp sample.main.inc.php main.inc.php
    mv sample.main.inc.php dev.main.inc.php
    cp sample.smtp.inc.php smtp.inc.php
    mv sample.smtp.inc.php dev.smtp.inc.php
    cp sample.twilio.inc.php twilio.inc.php
    mv sample.twilio.inc.php dev.twilio.inc.php
    cp sample.storage.inc.php storage.inc.php
    mv sample.storage.inc.php dev.storage.inc.php
    cd ..
    mkdir storage
    cd storage
    mkdir temp
    cd ..
    cd includes
    echo "Configuration finished."
fi

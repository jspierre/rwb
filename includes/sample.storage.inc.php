<?php
/** 
 * STORAGE CONFIGURATION FILE
 * Configures the settings for your STORAGE locations
 * @author John St. Pierre
 * @ver 0.1
 *
 */


/********************/
/* GENERAL SETTINGS */
/*******************/

/* NUMBER OF RANDOM CHARACTERS */
/* This is an integer that represents how long to make the random file name */
/* when using random file name storage, which is recommended */
$app_config['storage_NORANDOM'] = 10;

/* MANDATORY TEMPORARY STORAGE */
/* This is an interim location where uploads and downloads are kept temporarily */
/* while being transferred. The path must exist and the user that this program runs */
/* as must have read & write permission to this folder.  The path must end in a slash '/' */
/* The below folder was created automatically for you, but you can change it and delelet it if desired */
$app_config['storage_TEMPLOC'] = SITE_PATH."/storage/temp/";

/* TIME TO CLEAR */
/* This is how old (in minutes) the files in temporary storage should be in ordeer to be automatically cleared (deleted) */
/* This only works if background tasks (cron job) are running and settings less than the cron job run schedule */
/* will be reset to only run at that interval.  Default is any files older than 5 minutes will be deleted */
/* TO TURN OFF (not recommended), set this to 0 */
$app_config['storage_DELINTERVAL'] = 5;


/*******************/
/* LOCAL SETTINGS  */
/*******************/
 
/* LOCAL STORAGE LOCATION ARRAY */
/* Local Storage is always 'on' by default */
/* Define the server path to the local folder where files are to be stored */
/* Make sure that the system user for this program has permission to read & write to this location! */
/* This is an array of arrays, where NAMEOFLOCATION is the name you will identify this location with */
/* 'CODENAMEFORLOCATION' => array (																		
	'locloc' => "/path/to/storage/with/slash/",			
	'url' => the public path from the internet to access this storage area - must be public 
			  can use => BASE.URL."/storage/"	for example.  MUST END WITH A SLASH / !
	'descr' => "Internal description of storage area for reference"													
	)																								*/
/* *********************************************************************************************** */
$app_config['storage_LOC_LOCS'] = array(
	'MPCLOC01' => array (	
		'locloc' => SITE_PATH."/storage/",
		'url' => BASE_URL."/storage/",
		'descr' => "Local Storage in storage folder"
	)
);



/*******************/
/* AWS S3 SETTINGS */
/*******************/

/* Should AWS S3 Storage Be Turned On? (true = On / false = Off ) */
/* If not using AWS S3, keep this as false to avoid unnecessary overhead */
$app_config['storage_AWS_ACTIVE'] = true;	


/* The location of your Amazon AWS aws.phar file */
/* Make sure to include the file name - e.g., SITE_PATH.'/thirdparty/aws/aws.phar' */
$app_config['storage_AWS_LOC'] = SITE_PATH.'/thirdparty/aws/aws.phar';	


/* AWS S3 STORAGE LOCATION ARRAY */
/* Define the variables to connect and use an Amazon S3 Bucket */
/* Make sure that the bucket exists and your object key has permission for both read & write! */
/* This is an array of arrays, where 'NAMEOFLOCATION' => array ('locloc' => "/path/to/storage/with/slash/") */
/* 'CODENAMEFORLOCATION' => array (																		
	'bucket' => "nameofbucket",				*** REMEMBER BUCKET NAMES ARE ALWAYS LOWERCASE!! ***
	'region' => "us-east-1",													
	'key' => "youraccesskeyasgivenbyAWS";
	'url' => the public path from the internet to access this storage area - must be public 
			 https://bucket.s3.us-east-1.amazonaws.com/ for e.g., BUT  MUST END WITH A SLASH / !
	'secret' => "yoursecretaccesskeyasgivenbyAWS";
	'descr' => "Internal description of storage area for reference"													
	)																								*/
/* *********************************************************************************************** */
$app_config['storage_AWS_LOCS'] = array(
	'MPCAWS01' => array (	
		'bucket' => "bucketname",
		'region' => "us-east-1",
		'url' => "https://bucketname.s3.us-east-1.amazonaws.com/",
		'key' => "ABCDEFG1234567ABCDEFG1234567",
		'secret' => "ABCDEFG1234567ABCDEFG1234567ABCDEFG1234567",
		'descr' => "AWS3 STORAGE IN US EAST 1"
	)
);

?>
<?php

/** 
 * MAIN INCLUDES FILE
 * @author John St. Pierre
 * @ver 0.1
 *
 */

// This is needed because the controller base class has a different class name from the file
require (SITE_PATH.'/application/controller.class.php');
require (SITE_PATH.'/application/basemodel.class.php');

// Autoload all other classes from the /application or /model folder as needed
spl_autoload_register(function($class) {
	$appfile = SITE_PATH.'/application/'.$class.'.class.php';
	$modfile = SITE_PATH.'/model/'.$class.'.class.php';
	//echo "APP:".$appfile;
	if (file_exists($appfile)) {  include $appfile; }
	if (file_exists($modfile)) { include $modfile; }
});

$registry = new registry;
$registry->db = new database($registry);
$registry->security = new security($registry);
$registry->logging = new logging($registry);
$registry->template = new template($registry);
$registry->mail = new email($registry);
$registry->model = new model($registry);
$registry->storage = new storage($registry);

?>
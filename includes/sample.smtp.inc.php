<?php
/** 
 * SMTP EMAIL SEND CONFIGURATION FILE
 * Configures the SMTP Server that will be used with this application
 * @author John St. Pierre
 * @ver 1.0
 *
 */
 
 
/* The FQDN of the smtp server (e.g., mail.myserver.com OR localhost) */
$app_config['smtp_server'] = 'localhost';
 
/* Is SMTP Authorization required to send email? */
$app_config['smtp_auth'] = TRUE;
 
/* What Port is the SMTP Server listening On */
$app_config['smtp_port'] = 25;

/* What is the SMTP Username for authorization? */
$app_config['smtp_user'] = 'outgoing@mycompanyname.com';

/* What is the SMTP Password for authorization? */
$app_config['smtp_pass'] = 'abc123password';

/* What is the default from email to use if not defined elsewhere? */
$app_config['smtp_from'] = 'support@mycompany.com';

/* What is the default 'FROM' name to use if not defined elsewhere? */
$app_config['smtp_frname'] = 'RWB Framework';


/* What X-Mailer Header to Use in emails (leave alone if you aren't sure */
$app_config['smtp_xmailer'] = "RWB Framework System Mailer 1.0";

/* How many emails AT MOST should the system attempt to send per hour? */
/* Be mindful of the limits of your ISP and server host - NOTE: This only */
/* applies to mass emails sent to groups of users - it does not apply to */
/* individual emails which are sent immediately - MAX VALUE: 100000 (one hundred thousand per hour) */
/* Default value is 120 if not specified */
/* Note that this will always be limited by your PHPs max execution time setting */
$app_config['smtp_mails_per_hour'] = 500;








// -------------------------------------------------------------------------------
// DO NOT TOUCH BELOW THIS LINE
// -------------------------------------------------------------------------------
$app_config['smtp_loc'] = SITE_PATH.'/thirdparty/phpmailer/src';


?>
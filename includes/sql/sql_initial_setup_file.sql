-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2024 at 11:11 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rwb`
--

-- --------------------------------------------------------

--
-- Table structure for table `background_tasks`
--

CREATE TABLE `background_tasks` (
  `bgtasknum` int(11) NOT NULL COMMENT 'The autoindex task number',
  `modulename` varchar(100) NOT NULL DEFAULT '' COMMENT 'The actual php file holding the task to run (without .php)',
  `friendlyname` varchar(50) NOT NULL DEFAULT '' COMMENT 'A friendly name for the task to show in logs etc',
  `minutes` int(11) NOT NULL DEFAULT 60 COMMENT 'The number of minutes between runs of this task',
  `isactive` int(1) NOT NULL DEFAULT 1 COMMENT 'Is this task active for running (0=No, 1=YES)',
  `lastrun` datetime DEFAULT NULL COMMENT 'The date and time this module last ran'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Background tasks to run via cron job';

--
-- Dumping data for table `background_tasks`
--

INSERT INTO `background_tasks` (`bgtasknum`, `modulename`, `friendlyname`, `minutes`, `isactive`, `lastrun`) VALUES
(1, 'securityreset', 'Reset Attacks after Time', 60, 1, '2024-06-29 15:52:38'),
(2, 'sysemailqueue', 'Queue System Emails For Sending', 15, 1, '2024-06-29 15:52:38'),
(3, 'sysemailsend', 'Send Queued Emails Out', 5, 1, '2024-06-29 16:03:43'),
(6, 'tempstorageclear', 'Removes Files in Temp Storage Older Than Defined', 5, 1, '2024-06-29 16:03:43'),
(8, 'sessionreset', 'Set stale user sessions to logged off', 5, 1, '2024-06-29 16:03:44'),
(9, 'getexchangerates', 'Pull Currency Exchange Rates and Place in DB Table', 1440, 0, '2020-11-06 10:40:09'),
(10, 'logrotate', 'Rotates the Main Log File if Size if Large Enough', 60, 1, '2024-06-29 16:03:44');

-- --------------------------------------------------------

--
-- Table structure for table `currency_exchange_rates`
--

CREATE TABLE `currency_exchange_rates` (
  `updateno` int(6) UNSIGNED NOT NULL COMMENT 'The automatic index number for this currency conversion rate',
  `country` varchar(3) NOT NULL DEFAULT '' COMMENT 'The 3-letter country code that is being referenced',
  `exchangerate` decimal(8,2) NOT NULL DEFAULT 0.00 COMMENT 'The exchange rate compared to US Dollars (USD-1)',
  `datepulled` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'The date and time that this rate was pulled from the API'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Currency Exchange Rates from USD to common countries ';

-- --------------------------------------------------------

--
-- Table structure for table `emails_system`
--

CREATE TABLE `emails_system` (
  `mailnum` int(10) UNSIGNED NOT NULL COMMENT 'The index number for the table',
  `createdby` int(6) UNSIGNED DEFAULT NULL COMMENT 'The usernum of the person who created this email OR LAST CHANGED IT (NULL=SYSTEM)',
  `groupnum` int(1) NOT NULL DEFAULT 0 COMMENT 'The OPTIONAL group number that the email is to go to, 0=NO GROUP or ALL GROUPS',
  `usernum` int(6) NOT NULL DEFAULT 0 COMMENT 'OPTIONAL usernum from the users table. If this is 0 and groupnum is 0, then the email goes to ALL users',
  `sendtime` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The earliest date and time to start sending',
  `subjectline` varchar(255) NOT NULL DEFAULT '' COMMENT 'The subject line of the email',
  `emailbody` longtext NOT NULL DEFAULT '' COMMENT 'The actual text of the email',
  `optinonly` int(1) NOT NULL DEFAULT 1 COMMENT 'Only send to users who opted in for emails? (0=No, 1=YES)',
  `sendfrom` int(1) NOT NULL DEFAULT 0 COMMENT 'Which address to set as the sender? (0=SYSTEM EMAIL, 1=Support Email, 2=Sending User)',
  `emailstatus` int(1) NOT NULL DEFAULT 1 COMMENT 'The status of the email send (0=Complete, 1=READY, 2=In Progress, 3=Terminated)',
  `datecreated` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time that the email was created',
  `lastchange` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'The date and time that the status of the send was last changed',
  `ccadmins` int(1) NOT NULL DEFAULT 0 COMMENT 'Send email to all admins as well (0=NO, 1=Yes)',
  `numberexpected` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The number of emails expected to be sent (0=Not yet Processed, Please Wait))',
  `numbersent` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The number of emails sent to date',
  `internalnotes` text NOT NULL DEFAULT '' COMMENT 'Any notes the user wanted to give about this email'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='System Emails and their status';

-- --------------------------------------------------------

--
-- Table structure for table `emails_system_queue`
--

CREATE TABLE `emails_system_queue` (
  `queuenum` bigint(20) UNSIGNED NOT NULL COMMENT 'The mail index number',
  `mailnum` int(10) UNSIGNED NOT NULL COMMENT 'The mail number from the emails_system table',
  `emailfrom` varchar(255) NOT NULL DEFAULT '' COMMENT 'The email address that the email is coming from',
  `emailto` varchar(255) NOT NULL DEFAULT '' COMMENT 'The email address to send the email to',
  `subjectline` varchar(255) NOT NULL DEFAULT '' COMMENT 'The email subject line',
  `emailbody` longtext NOT NULL DEFAULT '' COMMENT 'The body of the email to be sent',
  `queuedate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time that the email was enqueued'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='A temporary table of emails to be sent';

-- --------------------------------------------------------

--
-- Table structure for table `logins_users`
--

CREATE TABLE `logins_users` (
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'Autoindex User Number',
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT 'Username of the user (can be email address or other)',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT 'Encrypted Password for the user',
  `passthesalt` varchar(255) NOT NULL DEFAULT '' COMMENT 'Salt to be used with the user''s password',
  `passexpires` date DEFAULT NULL COMMENT 'The date that this password expires',
  `useractive` int(1) NOT NULL DEFAULT 1 COMMENT 'Is this user active (0=No, 1=Yes)',
  `userlocked` int(1) NOT NULL DEFAULT 0 COMMENT 'Is this user locked out (0=No, 1=Yes)',
  `forcelogout` int(1) NOT NULL DEFAULT 0 COMMENT 'Force the user to be logged off immediately (0=NO, 1=Yes)',
  `pwattempts` int(5) NOT NULL DEFAULT 0 COMMENT 'Number of wrong attempts at password since last login',
  `aclgroup` int(6) DEFAULT NULL COMMENT 'The index number of the ACL group this user belongs to, if any',
  `lastlogin` datetime DEFAULT NULL COMMENT 'The last date and time this user logged in',
  `lastloginip` varchar(50) NOT NULL DEFAULT '0.0.0.0' COMMENT 'The IP address of the last login',
  `showsysbanner` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Show the system banner to the user (0=No, 1=YES)',
  `datecreated` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time that the record was first created',
  `selfcreated` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Self-Created? (0=No, 1=Yes)',
  `lastchanged` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Date and Time of last change to this user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Users and Password Tables for built-in Logins functions';

--
-- Dumping data for table `logins_users`
--

INSERT INTO `logins_users` (`usernum`, `username`, `password`, `passthesalt`, `passexpires`, `useractive`, `userlocked`, `forcelogout`, `pwattempts`, `aclgroup`, `lastlogin`, `lastloginip`, `showsysbanner`, `datecreated`, `selfcreated`, `lastchanged`) VALUES
(5, 'admin@admin.admin', '6dcb6b567a1df5684d80496fff17d05a5998f3ace719938a34d29d09b0676dfa68b792029cccaed5d77c2d9e7a64b4a72a9f3cc180cad6dd3bc097f1b5db1dde', '191568013104', '2024-06-28', 1, 0, 0, 0, 2, '2024-06-29 15:51:06', '::1', 0, '2017-02-14 00:43:46', 0, '2024-06-29 21:10:09');

-- --------------------------------------------------------

--
-- Table structure for table `logins_users_details`
--

CREATE TABLE `logins_users_details` (
  `detailindex` int(6) UNSIGNED NOT NULL COMMENT 'The index number for this table',
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'The usernum from the logins_users table',
  `firstname` varchar(75) NOT NULL DEFAULT '' COMMENT 'User''s First name',
  `lastname` varchar(75) NOT NULL DEFAULT '' COMMENT 'User''s Last Name',
  `emailaddress` varchar(254) NOT NULL DEFAULT '' COMMENT 'The email for this entry (can have multiple entries in table)',
  `billingaddress1` varchar(75) NOT NULL DEFAULT '' COMMENT 'Billing Address Line 1',
  `billingaddress2` varchar(75) NOT NULL DEFAULT '' COMMENT 'Billing Address Line 2',
  `billingcity` varchar(75) NOT NULL DEFAULT '' COMMENT 'Billing City',
  `billingstate` varchar(2) NOT NULL DEFAULT '' COMMENT 'Billing State',
  `billingzip` varchar(10) NOT NULL DEFAULT '' COMMENT 'Billing Zip',
  `billingphone` varchar(10) NOT NULL DEFAULT '' COMMENT 'Billing Phone',
  `okaytoemail` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=NO, 1=Yes',
  `oktotext` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=NO, 1=YES, Okay to Text',
  `mailingaddr1` varchar(255) NOT NULL DEFAULT '' COMMENT 'Mailing Address ONLY if different from unit address',
  `mailingaddr2` varchar(255) NOT NULL DEFAULT '' COMMENT 'Mailing address line 2',
  `mailingcity` varchar(255) NOT NULL DEFAULT '' COMMENT 'Mailing city',
  `mailingstate` varchar(2) NOT NULL DEFAULT '' COMMENT 'Mailing State',
  `mailingzip` varchar(10) NOT NULL DEFAULT '' COMMENT 'Mailing Zip',
  `mobilephone` varchar(10) NOT NULL DEFAULT '' COMMENT 'Mobile phone number',
  `tfalogin` int(1) NOT NULL DEFAULT 0 COMMENT 'Does the user have 2FA Active? (0=NO, 1=Yes, 2=Pending)',
  `tfaphone` varchar(10) NOT NULL DEFAULT '' COMMENT 'The phone number to use with two-factor auth (ten digits only)',
  `tfaverified` datetime DEFAULT NULL COMMENT 'The date and time that 2FA was last setup',
  `tcagree` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Did the user agree to T&C (0=NO, 1=Yes)',
  `lasttcagree` datetime DEFAULT NULL COMMENT 'The last time the user agreed to the T&Cs',
  `lastupdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contact and other details associated with logins_users';

--
-- Dumping data for table `logins_users_details`
--

INSERT INTO `logins_users_details` (`detailindex`, `usernum`, `firstname`, `lastname`, `emailaddress`, `billingaddress1`, `billingaddress2`, `billingcity`, `billingstate`, `billingzip`, `billingphone`, `okaytoemail`, `oktotext`, `mailingaddr1`, `mailingaddr2`, `mailingcity`, `mailingstate`, `mailingzip`, `mobilephone`, `tfalogin`, `tfaphone`, `tfaverified`, `tcagree`, `lasttcagree`, `lastupdate`) VALUES
(6, 5, 'SYSTEM', 'ADMINISTRATOR', 'admin@admin.admin', '', '', '', '', '', '', 1, 1, '', '', '', '', '', '', 0, '', NULL, 1, '2024-06-24 12:17:10', '2024-06-24 17:17:10');

-- --------------------------------------------------------

--
-- Table structure for table `logins_users_log`
--

CREATE TABLE `logins_users_log` (
  `lognum` int(8) NOT NULL COMMENT 'The index number for the log entry',
  `creatornum` int(6) UNSIGNED NOT NULL COMMENT 'The user who caused the action on another user',
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'The user this log entry goes to',
  `logtime` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time of the log entry',
  `logmessage` text NOT NULL DEFAULT '' COMMENT 'The log message or activity',
  `isprivate` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is this log entry private (FOR HR/EXEC VIEW ONLY? 0=NO, 1=Yes)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Log of actions taken or caused by a client or user';

--
-- Dumping data for table `logins_users_log`
--

INSERT INTO `logins_users_log` (`lognum`, `creatornum`, `usernum`, `logtime`, `logmessage`, `isprivate`) VALUES
(123, 5, 5, '2019-12-14 10:39:04', 'New User Created', 0);

-- --------------------------------------------------------

--
-- Table structure for table `logins_users_logins`
--

CREATE TABLE `logins_users_logins` (
  `loginlognum` bigint(15) UNSIGNED NOT NULL COMMENT 'Auto index number to log entry',
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'The user number that logged in',
  `ipaddress` varchar(255) NOT NULL DEFAULT '' COMMENT 'The IP address logged in from',
  `loggedintime` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time logged in '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='A simple table of each successful login that occurs';

--
-- Dumping data for table `logins_users_logins`
--

INSERT INTO `logins_users_logins` (`loginlognum`, `usernum`, `ipaddress`, `loggedintime`) VALUES
(1, 5, '::1', '2021-01-12 15:54:09'),
(2, 5, '::1', '2021-06-15 21:03:33'),
(3, 5, '::1', '2022-08-21 00:14:48'),
(4, 5, '::1', '2024-06-24 17:16:50'),
(5, 5, '::1', '2024-06-24 17:43:22'),
(6, 5, '::1', '2024-06-24 20:02:17'),
(7, 5, '::1', '2024-06-25 15:58:14'),
(8, 5, '::1', '2024-06-25 17:26:55'),
(9, 5, '::1', '2024-06-25 18:32:40'),
(10, 5, '::1', '2024-06-25 18:41:14'),
(11, 5, '::1', '2024-06-27 20:39:07'),
(12, 5, '::1', '2024-06-29 13:51:33'),
(13, 5, '::1', '2024-06-29 20:51:06');

-- --------------------------------------------------------

--
-- Table structure for table `logins_users_notes`
--

CREATE TABLE `logins_users_notes` (
  `notenum` int(8) NOT NULL COMMENT 'The note number in the file',
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'The user these notes belongs to',
  `usernotes` longtext NOT NULL DEFAULT '' COMMENT 'The notes for this user',
  `lastupdate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Holds the client note file';

-- --------------------------------------------------------

--
-- Table structure for table `logins_users_session`
--

CREATE TABLE `logins_users_session` (
  `sesindex` int(11) NOT NULL COMMENT 'Auto Index for Session Identifier',
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'The usernum in question from logins_users',
  `sessionstart` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time the current session started if known',
  `lastalive` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Last Date and Time the user confirmed session',
  `ipaddress` varchar(255) NOT NULL DEFAULT '0.0.0.0' COMMENT 'The last User IP the user came from ',
  `module` varchar(255) NOT NULL DEFAULT 'never_logged_in' COMMENT 'The last module the user successfully accessed',
  `isalive` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Is the session still alive (0=No, 1 =YES) - May be inaccurate!'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table to keep track of users who have live sessions ';

--
-- Dumping data for table `logins_users_session`
--

INSERT INTO `logins_users_session` (`sesindex`, `usernum`, `sessionstart`, `lastalive`, `ipaddress`, `module`, `isalive`) VALUES
(2, 5, '2024-06-29 16:10:12', '2024-06-29 21:10:12', '::1', 'pwchg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `logins_users_tfa`
--

CREATE TABLE `logins_users_tfa` (
  `tfnum` int(11) NOT NULL COMMENT 'The index number for the table',
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'The user number associated with this 2fa attempt',
  `phonenumber` varchar(10) NOT NULL DEFAULT '' COMMENT 'The phone number used to text this 2FA session',
  `code` varchar(255) NOT NULL DEFAULT '' COMMENT 'The hashed 6-digit code for login',
  `salt` varchar(255) NOT NULL DEFAULT '' COMMENT 'The salt used in the has for the 6-digit code',
  `expires` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The time that the code expires and can no longer be used',
  `isvalid` int(1) NOT NULL DEFAULT 1 COMMENT 'Is the code still valid for use (0=No, 1=YES)',
  `codestatus` int(1) NOT NULL DEFAULT 2 COMMENT 'The status of the code currently (0=Failed, 1=Success, 2=Pending, 3=Expired)',
  `usedate` datetime DEFAULT NULL COMMENT 'The date and time the code was used or attempted to be used, i.e., its expiration time'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Two Factor Authentication Live Table';

-- --------------------------------------------------------

--
-- Table structure for table `logins_users_tfa_cookies`
--

CREATE TABLE `logins_users_tfa_cookies` (
  `cookienum` int(6) UNSIGNED NOT NULL COMMENT 'The index number for the table',
  `usernum` int(6) UNSIGNED NOT NULL COMMENT 'The user number associated with this cookie',
  `cookie` varchar(255) NOT NULL DEFAULT '' COMMENT 'The contents of the cookie sent',
  `ipaddress` varchar(255) NOT NULL DEFAULT '' COMMENT 'The IP Address that the cookie is associated with',
  `dateissued` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The time and date that the cookie was issued',
  `dateexpires` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time that the cookie expires',
  `isrevoked` int(1) NOT NULL DEFAULT 0 COMMENT 'Is this cookie revoked (0=NO, 1=Yes)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Two Factor Authorization Cookies';

-- --------------------------------------------------------

--
-- Table structure for table `security_acl_access`
--

CREATE TABLE `security_acl_access` (
  `rulenum` int(9) NOT NULL COMMENT 'The access rule numbers',
  `groupnum` int(6) NOT NULL COMMENT 'The group that this rule applies to',
  `modulenum` int(9) NOT NULL COMMENT 'The module that the group can access',
  `ruleactive` int(1) NOT NULL DEFAULT 1 COMMENT 'Is the rule active (1=Yes, 0=No)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The access rules that apply to ACL access';

--
-- Dumping data for table `security_acl_access`
--

INSERT INTO `security_acl_access` (`rulenum`, `groupnum`, `modulenum`, `ruleactive`) VALUES
(1278, 2, 34, 1),
(1279, 2, 29, 1),
(1280, 2, 50, 1),
(1281, 2, 49, 1),
(1282, 2, 15, 1),
(1283, 2, 14, 1),
(1284, 2, 11, 1),
(1285, 2, 45, 1),
(1286, 2, 44, 1),
(1287, 2, 27, 1),
(1288, 2, 31, 1),
(1289, 2, 30, 1),
(1290, 2, 32, 1),
(1291, 2, 42, 1),
(1292, 2, 10, 1),
(1293, 2, 9, 1),
(1294, 5, 29, 1),
(1295, 5, 50, 1),
(1296, 5, 49, 1),
(1297, 5, 45, 1),
(1298, 5, 44, 1),
(1299, 5, 31, 1),
(1300, 5, 30, 1),
(1301, 5, 10, 1),
(1302, 5, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `security_acl_functions`
--

CREATE TABLE `security_acl_functions` (
  `funcnum` int(9) NOT NULL COMMENT 'The index number of the function',
  `modulenum` int(9) NOT NULL COMMENT 'The module the function belongs to',
  `funcname` varchar(255) NOT NULL DEFAULT '' COMMENT 'The name of the method being protected',
  `funcdescr` varchar(255) NOT NULL DEFAULT '' COMMENT 'A short description of the function''s job',
  `functionactive` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Is the function active? (1=YES, 0=No)',
  `allowall` int(1) NOT NULL DEFAULT 0 COMMENT 'Allow all users to access this function? (0=NO, 1=Yes)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Granular Functions available to main modules';

--
-- Dumping data for table `security_acl_functions`
--

INSERT INTO `security_acl_functions` (`funcnum`, `modulenum`, `funcname`, `funcdescr`, `functionactive`, `allowall`) VALUES
(6, 1, 'adminmenu', 'MENU: Administration', 1, 0),
(10, 10, 'useredit', 'Edit User Primary Information', 1, 0),
(11, 10, 'userresetpw', 'Reset User Passwords', 1, 0),
(12, 10, 'usernotes', 'View User Notes', 1, 0),
(13, 10, 'userlog', 'View User Log', 1, 0),
(14, 10, 'usernotesedit', 'Edit User Notes', 1, 0),
(20, 10, 'editownacct', 'Edit Own Account', 1, 0),
(21, 14, 'editownacct', 'Edit Own Group', 1, 0),
(23, 1, 'usermgmt', 'SUBMENU: User Management', 1, 0),
(24, 10, 'adminedit', 'Edit Admin Accounts', 1, 0),
(33, 9, 'usradd', 'Add a New User', 1, 0),
(35, 1, 'sysadmin', 'SUBMENU: System Administration', 1, 0),
(36, 10, 'impersonate', 'Impersonate Another User', 1, 0),
(37, 1, 'tfa', 'SUBMENU: Two-Factor Authentication', 1, 1),
(38, 10, 'forceuserpw', 'Force User to Change Password Immediately', 1, 0),
(39, 10, 'forceuser2fa', 'Force User to Re-verify 2FA Code on next login', 1, 0),
(40, 1, 'modmgmt', 'SUBMENU: Module Management', 1, 0),
(41, 1, 'fncmgmt', 'SUBMENU: Function Management', 1, 0),
(42, 1, 'grpmgmt', 'SUBMENU: Group Management', 1, 0),
(43, 1, 'bans', 'SUBMENU: Banned IP Management', 1, 0),
(44, 1, 'searchsysemails', 'SUBMENU: Mass Email Administration', 1, 0),
(45, 31, 'emgmtdisplay', 'Display Individual Email Records', 1, 0),
(46, 31, 'emgmtadd', 'Add New Emails to Queue', 1, 0),
(47, 31, 'emgmtedit', 'Edit Emails Already in Queue', 1, 0),
(48, 1, 'pwchg', 'SUBMENU: Change Password', 1, 1),
(49, 10, 'useremailsend', 'Send Email to a User', 1, 0),
(50, 1, 'displaylog', 'SUBMENU: Display System Log', 1, 0),
(51, 32, 'displaylog', 'View the System Log', 1, 0),
(52, 32, 'archivelog', 'Archive System Logs', 1, 0),
(53, 10, 'vvsms', 'Voice Verify Users by SMS', 1, 0),
(54, 10, 'vvemail', 'Voice Verify Users by Email', 1, 0),
(55, 10, 'createadmins', 'Create Admin-Level Users or Upgrade to Admin', 1, 0),
(56, 14, 'editadmins', 'Edit Admin Groups as well as Non-Admin Groups', 1, 0),
(57, 14, 'deletegroup', 'Delete A Group with No Members', 1, 0),
(58, 15, 'deletefunction', 'Delete Functions', 1, 0),
(62, 11, 'deletemodule', 'Delete Modules', 1, 0),
(63, 1, 'bgtaskmgmt', 'SUBMENU: Background Task Management', 1, 0),
(64, 34, 'deletetask', 'Delete Existing Background Tasks', 1, 0),
(66, 10, 'forcelogoff', 'Force User to Immediately be Logged Off', 1, 0),
(67, 9, 'searchonline', 'Search and Display of Users Currently Logged In', 1, 0),
(68, 10, 'usersystemlog', 'View System Log Filtered by User', 1, 0),
(69, 10, 'usradd', 'Add New Users into the System', 1, 0),
(72, 43, 'notifyescalated', 'Notify of new Escalated Messages or Replies', 1, 0),
(73, 43, 'notifysupport', 'Notify of new Support Messages (not escalated)', 1, 0),
(74, 1, 'supportmaster', 'SUBMENU: SupportMaster', 1, 0),
(75, 10, 'associate', 'Associate a Support Ticket with Selected User', 1, 0),
(76, 10, 'listtickets', 'Show all Support Tickets for Selected User', 1, 0),
(77, 10, 'createticket', 'Create new support ticket for a user', 1, 0),
(78, 45, 'supportdisplay', 'TIcket Basic Display', 1, 0),
(79, 45, 'supportedit', 'Edit Support Ticket Data', 1, 0),
(80, 45, 'respondsupport', 'Respond to Regular Support Tickets', 1, 0),
(81, 45, 'respondescalated', 'Respond to Escalated Support Tickets', 1, 0),
(82, 45, 'notatesupport', 'View and Add Internal Notes to Tickets', 1, 0),
(83, 45, 'supportassign', 'Assign staff member to unassigned ticket', 1, 0),
(84, 45, 'supportreassign', 'Reassign staff members to support tickets', 1, 0),
(85, 45, 'supportassoc', 'Associate a client with an unassociated ticket', 1, 0),
(86, 45, 'supportreassoc', 'Associate another client with already associated ticket', 1, 0),
(87, 45, 'supportdelete', 'Delete tickets with no responses yet (eg spam)', 1, 0),
(88, 1, 'support', 'SUBMENU: Help and Support', 1, 1),
(89, 46, 'submitticketself', 'User can submit a support request through Help page', 1, 0),
(90, 1, 'faq', 'SUBMENU: Frequently Asked Questions', 1, 1),
(91, 1, 'displayuserlogins', 'SEE SUBMENU: Display User Logins', 1, 0),
(92, 1, 'loginsall', 'SEE SUBMENU: View User Logins for All Time', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `security_acl_functions_access`
--

CREATE TABLE `security_acl_functions_access` (
  `rulenum` int(6) NOT NULL COMMENT 'The rule index number',
  `functionnum` int(9) NOT NULL COMMENT 'The function that this rule is regarding',
  `groupnum` int(6) NOT NULL COMMENT 'The group that has access to this detailed function',
  `ruleactive` int(1) NOT NULL DEFAULT 1 COMMENT 'Is this rule active (1=YES, 0=NO)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='What granular functions do which groups have access to?';

--
-- Dumping data for table `security_acl_functions_access`
--

INSERT INTO `security_acl_functions_access` (`rulenum`, `functionnum`, `groupnum`, `ruleactive`) VALUES
(2843, 89, 7, 1),
(2844, 6, 2, 1),
(2845, 91, 2, 1),
(2846, 92, 2, 1),
(2847, 63, 2, 1),
(2848, 43, 2, 1),
(2849, 50, 2, 1),
(2850, 41, 2, 1),
(2851, 42, 2, 1),
(2852, 44, 2, 1),
(2853, 40, 2, 1),
(2854, 74, 2, 1),
(2855, 35, 2, 1),
(2856, 23, 2, 1),
(2857, 64, 2, 1),
(2858, 58, 2, 1),
(2859, 57, 2, 1),
(2860, 56, 2, 1),
(2861, 21, 2, 1),
(2862, 89, 2, 1),
(2863, 72, 2, 1),
(2864, 73, 2, 1),
(2865, 62, 2, 1),
(2866, 83, 2, 1),
(2867, 85, 2, 1),
(2868, 86, 2, 1),
(2869, 87, 2, 1),
(2870, 79, 2, 1),
(2871, 84, 2, 1),
(2872, 81, 2, 1),
(2873, 80, 2, 1),
(2874, 78, 2, 1),
(2875, 82, 2, 1),
(2876, 46, 2, 1),
(2877, 45, 2, 1),
(2878, 47, 2, 1),
(2879, 52, 2, 1),
(2880, 51, 2, 1),
(2881, 69, 2, 1),
(2882, 75, 2, 1),
(2883, 55, 2, 1),
(2884, 77, 2, 1),
(2885, 24, 2, 1),
(2886, 20, 2, 1),
(2887, 14, 2, 1),
(2888, 10, 2, 1),
(2889, 38, 2, 1),
(2890, 66, 2, 1),
(2891, 39, 2, 1),
(2892, 36, 2, 1),
(2893, 11, 2, 1),
(2894, 49, 2, 1),
(2895, 76, 2, 1),
(2896, 68, 2, 1),
(2897, 13, 2, 1),
(2898, 12, 2, 1),
(2899, 54, 2, 1),
(2900, 53, 2, 1),
(2901, 33, 2, 1),
(2902, 67, 2, 1),
(2903, 6, 5, 1),
(2904, 91, 5, 1),
(2905, 92, 5, 1),
(2906, 43, 5, 1),
(2907, 44, 5, 1),
(2908, 74, 5, 1),
(2909, 23, 5, 1),
(2910, 89, 5, 1),
(2911, 73, 5, 1),
(2912, 85, 5, 1),
(2913, 79, 5, 1),
(2914, 80, 5, 1),
(2915, 78, 5, 1),
(2916, 82, 5, 1),
(2917, 45, 5, 1),
(2918, 75, 5, 1),
(2919, 77, 5, 1),
(2920, 14, 5, 1),
(2921, 10, 5, 1),
(2922, 38, 5, 1),
(2923, 66, 5, 1),
(2924, 39, 5, 1),
(2925, 11, 5, 1),
(2926, 49, 5, 1),
(2927, 76, 5, 1),
(2928, 68, 5, 1),
(2929, 13, 5, 1),
(2930, 12, 5, 1),
(2931, 54, 5, 1),
(2932, 53, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `security_acl_groups`
--

CREATE TABLE `security_acl_groups` (
  `groupindex` int(6) NOT NULL COMMENT 'Auto Index for group',
  `groupname` varchar(25) NOT NULL DEFAULT '' COMMENT 'Short name for the group',
  `isactive` int(1) NOT NULL DEFAULT 1 COMMENT 'Group active? (0=No, 1=Yes)',
  `isadmin` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is this an admin group (0=NO, 1=Yes)',
  `isstaff` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is this person a staff (non-customer) user? (0=NO, 1=Yes) - NOTE: If Admin, then ALSO staff automatically.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='For ACL Usage - All allowed groups';

--
-- Dumping data for table `security_acl_groups`
--

INSERT INTO `security_acl_groups` (`groupindex`, `groupname`, `isactive`, `isadmin`, `isstaff`) VALUES
(2, 'Full Admin', 1, 1, 1),
(5, 'Limited Admin', 1, 1, 1),
(7, 'User', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `security_acl_modules`
--

CREATE TABLE `security_acl_modules` (
  `moduleindex` int(6) NOT NULL COMMENT 'The module autoindex number',
  `controller` varchar(255) NOT NULL COMMENT 'The controller at /controller (file name without .php)',
  `modulename` varchar(255) NOT NULL DEFAULT '' COMMENT 'A short description of the module - optional',
  `allowall` int(1) NOT NULL DEFAULT 0 COMMENT 'Allow all authenticated users to access? (0=No, 1=Yes)',
  `denyall` int(1) NOT NULL DEFAULT 0 COMMENT 'Block all authenticated users from access (0=No, 1=Yes)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The controllers that are subject to ACL rules';

--
-- Dumping data for table `security_acl_modules`
--

INSERT INTO `security_acl_modules` (`moduleindex`, `controller`, `modulename`, `allowall`, `denyall`) VALUES
(1, 'appindex', 'MAIN CONTROLLER FUNCTIONS', 1, 0),
(5, 'pwchg', 'Password Change', 1, 0),
(9, 'searchuser', 'Users - Search', 0, 0),
(10, 'usmgmt', 'Users - General Management', 0, 0),
(11, 'modmgmt', 'Module Management (Controller Access)', 0, 0),
(14, 'grpmgmt', 'Group Management (ACL Groups & Permissions)', 0, 0),
(15, 'fncmgmt', 'Function Management (ACL Granular Functions)', 0, 0),
(17, 'main', 'Main Page after Login', 1, 0),
(19, 'pwrst', 'Password Reset', 1, 0),
(20, 'signup', 'Users - Self Sign Up Form', 1, 0),
(25, 'tandc', 'Terms and Conditions Page', 1, 0),
(26, 'sysmaint', 'System Down - Maintenance Display Page', 1, 0),
(27, 'sysadmin', 'System Advanced Administration', 0, 0),
(28, 'tfa', 'Two Factor Authentication', 1, 0),
(29, 'bans', 'Banned IP Management', 0, 0),
(30, 'searchsysemails', 'System Emails - Search', 0, 0),
(31, 'emailmgmt', 'System Emails - General Management', 0, 0),
(32, 'viewsyslog', 'System Log - View', 0, 0),
(34, 'bgtaskmgmt', 'Background Task Management', 0, 0),
(42, 'test', 'Testing File for Development', 0, 0),
(43, 'receiver_sms', 'Incoming SMS Processor (for Twilio Only)', 1, 0),
(44, 'supportmastersearch', 'SupportMaster Search Tickets', 0, 0),
(45, 'supportmaster', 'SupportMaster', 0, 0),
(46, 'support', 'Help and Support for Logged In Users', 1, 0),
(47, 'keepalive', 'Called internally to extend sessions.  This was installed with the framework. LEAVE THIS SET AS-IS!', 1, 0),
(48, 'faq', 'Frequently Asked Questions Page', 1, 0),
(49, 'loginstoday', 'Display User Logins for Today', 0, 0),
(50, 'loginsall', 'Display All User Logins', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `security_attacks`
--

CREATE TABLE `security_attacks` (
  `attacknum` int(6) NOT NULL COMMENT 'Autoindex Value for the Attack',
  `ipaddress` varchar(50) NOT NULL DEFAULT '' COMMENT 'The IP Address of the Attacker',
  `attempts` int(4) NOT NULL DEFAULT 1 COMMENT 'How many attempts this IP Address has made',
  `lastattempt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time of the last attempt'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Used by the Security Modules to Record Attacks on Your App';

-- --------------------------------------------------------

--
-- Table structure for table `security_bans`
--

CREATE TABLE `security_bans` (
  `bannumber` int(6) NOT NULL COMMENT 'Autoindex of the banned IP',
  `ipaddress` varchar(50) NOT NULL DEFAULT '' COMMENT 'The IP Address that is banned',
  `bandate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date and time the IP Address was banned'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Used by the Security Modules to permanently block banned addresses';

-- --------------------------------------------------------

--
-- Table structure for table `settings_system`
--

CREATE TABLE `settings_system` (
  `settingindex` tinyint(4) NOT NULL COMMENT 'The index - this should never go above 1',
  `goduser` int(6) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The usernumber of the GOD user - has access to everything always',
  `runstatus` int(1) NOT NULL DEFAULT 0 COMMENT 'The status of the system (0=Normal Run, 1=Maintenance-Allow Admin Login, 2=Maintenance-Deny All, 3=DOWN',
  `laststatuschange` datetime DEFAULT NULL COMMENT 'The last time the run status was changed',
  `lastpwchange` datetime DEFAULT NULL COMMENT 'The last time all users were forced to change their passwords at once',
  `lastforcelogout` datetime DEFAULT NULL COMMENT 'The last time all users were forcefully logged out',
  `lastforce2FA` datetime DEFAULT NULL COMMENT 'The last time ALL users were forced to revalidate 2FA',
  `systembanner` varchar(255) NOT NULL DEFAULT '' COMMENT 'The current system banner that may display to users at the top of their page'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='System Settings';

--
-- Dumping data for table `settings_system`
--

INSERT INTO `settings_system` (`settingindex`, `goduser`, `runstatus`, `laststatuschange`, `lastpwchange`, `lastforcelogout`, `lastforce2FA`, `systembanner`) VALUES
(1, 0, 0, '2019-11-09 15:08:37', '2019-11-08 12:45:38', '2019-11-09 15:08:37', '2019-11-08 12:45:38', 'Replace this with your text');

-- --------------------------------------------------------

--
-- Table structure for table `support_conversations`
--

CREATE TABLE `support_conversations` (
  `convno` bigint(20) UNSIGNED NOT NULL COMMENT 'The number that identifies this conversation, automatically defined',
  `convtype` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'The conversation type (0=SMS, 1=Email)',
  `convstart` timestamp NULL DEFAULT current_timestamp() COMMENT 'The date and time the conversation started',
  `convsubject` varchar(255) NOT NULL DEFAULT '' COMMENT 'The subject of the email, if any',
  `userstart` int(6) UNSIGNED DEFAULT NULL COMMENT 'The user number who started the conversation, if known',
  `useremail` varchar(255) NOT NULL DEFAULT '' COMMENT 'The email address associated with the conv',
  `userphone` varchar(12) NOT NULL DEFAULT '' COMMENT 'The phone number associated with this conv',
  `assignedto` int(6) UNSIGNED DEFAULT NULL COMMENT 'The person this conversation is assigned to',
  `isescalated` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Has this conversation been escalated to management (0=NO, 1=Yes)',
  `isclosed` tinyint(1) DEFAULT 0 COMMENT 'Is this ticket / conversation closed? (0=NO, 1=Yes)',
  `lastaction` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'The last time this conversation had an update.',
  `lastactionparty` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'What party last acted on this ticket (0=CLIENT, 1=Staff)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='A table of all conversations (support tickets)';

-- --------------------------------------------------------

--
-- Table structure for table `support_log`
--

CREATE TABLE `support_log` (
  `logno` bigint(20) UNSIGNED NOT NULL COMMENT 'The automatic index number of this log entry',
  `convno` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'The conversation that this log entry is associated with',
  `isescalated` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Is this item part of an escalated conv - reg users won''t see it (0=NO, 1=Yes)',
  `logtime` datetime DEFAULT NULL COMMENT 'The date and time this log message occurred',
  `loguser` int(6) UNSIGNED DEFAULT NULL COMMENT 'The user who caused this log entry',
  `logmessage` varchar(500) NOT NULL DEFAULT '' COMMENT 'The actual log entry'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Log of activity on each conversation';

-- --------------------------------------------------------

--
-- Table structure for table `support_messages`
--

CREATE TABLE `support_messages` (
  `messageno` bigint(20) UNSIGNED NOT NULL COMMENT 'The internal number assigned to this message',
  `convno` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'The conversation this message is associated with',
  `messagetime` timestamp NULL DEFAULT current_timestamp() COMMENT 'The date and time this message was received',
  `messagetype` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'What type of message (0=SMS, 1=Email, 2=Note Only)',
  `sendernum` int(6) UNSIGNED DEFAULT NULL COMMENT 'The usernum of the sender, if any',
  `isincoming` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Is the message incoming (0=Outgoing, 1=INCOMING)',
  `messagebody` text NOT NULL DEFAULT '' COMMENT 'The body of the message sent or received',
  `internalnotes` text NOT NULL DEFAULT '' COMMENT 'Admin notes about this messsage - cannot be seen by User'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='The messages assoc with each conversation';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `background_tasks`
--
ALTER TABLE `background_tasks`
  ADD PRIMARY KEY (`bgtasknum`),
  ADD KEY `modulename` (`modulename`,`lastrun`);

--
-- Indexes for table `currency_exchange_rates`
--
ALTER TABLE `currency_exchange_rates`
  ADD PRIMARY KEY (`updateno`),
  ADD KEY `country` (`country`);

--
-- Indexes for table `emails_system`
--
ALTER TABLE `emails_system`
  ADD PRIMARY KEY (`mailnum`),
  ADD KEY `groupnum` (`groupnum`),
  ADD KEY `sendtime` (`sendtime`),
  ADD KEY `emailstatus` (`emailstatus`),
  ADD KEY `createdby` (`createdby`);

--
-- Indexes for table `emails_system_queue`
--
ALTER TABLE `emails_system_queue`
  ADD PRIMARY KEY (`queuenum`),
  ADD KEY `mailnum` (`mailnum`);

--
-- Indexes for table `logins_users`
--
ALTER TABLE `logins_users`
  ADD PRIMARY KEY (`usernum`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `aclgroup` (`aclgroup`);

--
-- Indexes for table `logins_users_details`
--
ALTER TABLE `logins_users_details`
  ADD PRIMARY KEY (`detailindex`),
  ADD UNIQUE KEY `usernum_2` (`usernum`),
  ADD KEY `usernum` (`usernum`,`emailaddress`),
  ADD KEY `lastname` (`lastname`),
  ADD KEY `okaytoemail` (`okaytoemail`),
  ADD KEY `billingzip` (`billingzip`);

--
-- Indexes for table `logins_users_log`
--
ALTER TABLE `logins_users_log`
  ADD PRIMARY KEY (`lognum`),
  ADD KEY `clientnum` (`usernum`),
  ADD KEY `creatornum` (`creatornum`);

--
-- Indexes for table `logins_users_logins`
--
ALTER TABLE `logins_users_logins`
  ADD PRIMARY KEY (`loginlognum`),
  ADD KEY `usernum` (`usernum`),
  ADD KEY `loggedintime` (`loggedintime`);

--
-- Indexes for table `logins_users_notes`
--
ALTER TABLE `logins_users_notes`
  ADD PRIMARY KEY (`notenum`),
  ADD UNIQUE KEY `clientnum_2` (`usernum`),
  ADD KEY `clientnum` (`usernum`);

--
-- Indexes for table `logins_users_session`
--
ALTER TABLE `logins_users_session`
  ADD PRIMARY KEY (`sesindex`),
  ADD UNIQUE KEY `usernum` (`usernum`);

--
-- Indexes for table `logins_users_tfa`
--
ALTER TABLE `logins_users_tfa`
  ADD PRIMARY KEY (`tfnum`),
  ADD KEY `usernum` (`usernum`);

--
-- Indexes for table `logins_users_tfa_cookies`
--
ALTER TABLE `logins_users_tfa_cookies`
  ADD PRIMARY KEY (`cookienum`),
  ADD KEY `usernum` (`usernum`),
  ADD KEY `cookie` (`cookie`);

--
-- Indexes for table `security_acl_access`
--
ALTER TABLE `security_acl_access`
  ADD PRIMARY KEY (`rulenum`),
  ADD KEY `groupnum` (`groupnum`,`modulenum`),
  ADD KEY `modulenum` (`modulenum`);

--
-- Indexes for table `security_acl_functions`
--
ALTER TABLE `security_acl_functions`
  ADD PRIMARY KEY (`funcnum`),
  ADD KEY `modulenum` (`modulenum`,`funcname`,`functionactive`),
  ADD KEY `ruleactive` (`functionactive`);

--
-- Indexes for table `security_acl_functions_access`
--
ALTER TABLE `security_acl_functions_access`
  ADD PRIMARY KEY (`rulenum`),
  ADD KEY `functionnum` (`functionnum`,`groupnum`),
  ADD KEY `groupnum` (`groupnum`);

--
-- Indexes for table `security_acl_groups`
--
ALTER TABLE `security_acl_groups`
  ADD PRIMARY KEY (`groupindex`),
  ADD UNIQUE KEY `groupname` (`groupname`),
  ADD KEY `isstaff` (`isstaff`);

--
-- Indexes for table `security_acl_modules`
--
ALTER TABLE `security_acl_modules`
  ADD PRIMARY KEY (`moduleindex`),
  ADD UNIQUE KEY `controller` (`controller`);

--
-- Indexes for table `security_attacks`
--
ALTER TABLE `security_attacks`
  ADD PRIMARY KEY (`attacknum`),
  ADD KEY `ipaddress` (`ipaddress`,`lastattempt`);

--
-- Indexes for table `security_bans`
--
ALTER TABLE `security_bans`
  ADD PRIMARY KEY (`bannumber`),
  ADD KEY `ipaddress` (`ipaddress`,`bandate`);

--
-- Indexes for table `settings_system`
--
ALTER TABLE `settings_system`
  ADD PRIMARY KEY (`settingindex`),
  ADD UNIQUE KEY `goduser` (`goduser`);

--
-- Indexes for table `support_conversations`
--
ALTER TABLE `support_conversations`
  ADD PRIMARY KEY (`convno`),
  ADD KEY `convtype` (`convtype`),
  ADD KEY `userstart` (`userstart`),
  ADD KEY `userphone` (`userphone`),
  ADD KEY `useremail` (`useremail`),
  ADD KEY `assignedto` (`assignedto`),
  ADD KEY `isclosed` (`isclosed`),
  ADD KEY `lastactionparty` (`lastactionparty`);

--
-- Indexes for table `support_log`
--
ALTER TABLE `support_log`
  ADD PRIMARY KEY (`logno`),
  ADD KEY `convno` (`convno`),
  ADD KEY `isescalated` (`isescalated`),
  ADD KEY `loguser` (`loguser`);

--
-- Indexes for table `support_messages`
--
ALTER TABLE `support_messages`
  ADD PRIMARY KEY (`messageno`),
  ADD KEY `convno` (`convno`),
  ADD KEY `messagetype` (`messagetype`),
  ADD KEY `sendernum` (`sendernum`),
  ADD KEY `isincoming` (`isincoming`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `background_tasks`
--
ALTER TABLE `background_tasks`
  MODIFY `bgtasknum` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The autoindex task number', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `currency_exchange_rates`
--
ALTER TABLE `currency_exchange_rates`
  MODIFY `updateno` int(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The automatic index number for this currency conversion rate', AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `emails_system`
--
ALTER TABLE `emails_system`
  MODIFY `mailnum` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The index number for the table', AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `emails_system_queue`
--
ALTER TABLE `emails_system_queue`
  MODIFY `queuenum` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The mail index number', AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `logins_users`
--
ALTER TABLE `logins_users`
  MODIFY `usernum` int(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Autoindex User Number', AUTO_INCREMENT=117089;

--
-- AUTO_INCREMENT for table `logins_users_details`
--
ALTER TABLE `logins_users_details`
  MODIFY `detailindex` int(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The index number for this table', AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `logins_users_log`
--
ALTER TABLE `logins_users_log`
  MODIFY `lognum` int(8) NOT NULL AUTO_INCREMENT COMMENT 'The index number for the log entry', AUTO_INCREMENT=221;

--
-- AUTO_INCREMENT for table `logins_users_logins`
--
ALTER TABLE `logins_users_logins`
  MODIFY `loginlognum` bigint(15) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Auto index number to log entry', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `logins_users_notes`
--
ALTER TABLE `logins_users_notes`
  MODIFY `notenum` int(8) NOT NULL AUTO_INCREMENT COMMENT 'The note number in the file', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `logins_users_session`
--
ALTER TABLE `logins_users_session`
  MODIFY `sesindex` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Index for Session Identifier', AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `logins_users_tfa`
--
ALTER TABLE `logins_users_tfa`
  MODIFY `tfnum` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The index number for the table', AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `logins_users_tfa_cookies`
--
ALTER TABLE `logins_users_tfa_cookies`
  MODIFY `cookienum` int(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The index number for the table', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `security_acl_access`
--
ALTER TABLE `security_acl_access`
  MODIFY `rulenum` int(9) NOT NULL AUTO_INCREMENT COMMENT 'The access rule numbers', AUTO_INCREMENT=1303;

--
-- AUTO_INCREMENT for table `security_acl_functions`
--
ALTER TABLE `security_acl_functions`
  MODIFY `funcnum` int(9) NOT NULL AUTO_INCREMENT COMMENT 'The index number of the function', AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `security_acl_functions_access`
--
ALTER TABLE `security_acl_functions_access`
  MODIFY `rulenum` int(6) NOT NULL AUTO_INCREMENT COMMENT 'The rule index number', AUTO_INCREMENT=2933;

--
-- AUTO_INCREMENT for table `security_acl_groups`
--
ALTER TABLE `security_acl_groups`
  MODIFY `groupindex` int(6) NOT NULL AUTO_INCREMENT COMMENT 'Auto Index for group', AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `security_acl_modules`
--
ALTER TABLE `security_acl_modules`
  MODIFY `moduleindex` int(6) NOT NULL AUTO_INCREMENT COMMENT 'The module autoindex number', AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `security_attacks`
--
ALTER TABLE `security_attacks`
  MODIFY `attacknum` int(6) NOT NULL AUTO_INCREMENT COMMENT 'Autoindex Value for the Attack', AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `security_bans`
--
ALTER TABLE `security_bans`
  MODIFY `bannumber` int(6) NOT NULL AUTO_INCREMENT COMMENT 'Autoindex of the banned IP', AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `settings_system`
--
ALTER TABLE `settings_system`
  MODIFY `settingindex` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'The index - this should never go above 1', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `support_conversations`
--
ALTER TABLE `support_conversations`
  MODIFY `convno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The number that identifies this conversation, automatically defined', AUTO_INCREMENT=10153;

--
-- AUTO_INCREMENT for table `support_log`
--
ALTER TABLE `support_log`
  MODIFY `logno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The automatic index number of this log entry', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `support_messages`
--
ALTER TABLE `support_messages`
  MODIFY `messageno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The internal number assigned to this message', AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emails_system`
--
ALTER TABLE `emails_system`
  ADD CONSTRAINT `emails_usernum` FOREIGN KEY (`createdby`) REFERENCES `logins_users` (`usernum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `emails_system_queue`
--
ALTER TABLE `emails_system_queue`
  ADD CONSTRAINT `emails_system_queue_ibfk_1` FOREIGN KEY (`mailnum`) REFERENCES `emails_system` (`mailnum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logins_users`
--
ALTER TABLE `logins_users`
  ADD CONSTRAINT `logins_users_ibfk_1` FOREIGN KEY (`aclgroup`) REFERENCES `security_acl_groups` (`groupindex`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `logins_users_details`
--
ALTER TABLE `logins_users_details`
  ADD CONSTRAINT `logins_users_details_ibfk_1` FOREIGN KEY (`usernum`) REFERENCES `logins_users` (`usernum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logins_users_log`
--
ALTER TABLE `logins_users_log`
  ADD CONSTRAINT `logins_users_log_ibfk_1` FOREIGN KEY (`usernum`) REFERENCES `logins_users` (`usernum`),
  ADD CONSTRAINT `logins_users_log_ibfk_2` FOREIGN KEY (`creatornum`) REFERENCES `logins_users` (`usernum`);

--
-- Constraints for table `logins_users_logins`
--
ALTER TABLE `logins_users_logins`
  ADD CONSTRAINT `usernum_logins` FOREIGN KEY (`usernum`) REFERENCES `logins_users` (`usernum`);

--
-- Constraints for table `logins_users_session`
--
ALTER TABLE `logins_users_session`
  ADD CONSTRAINT `session_usernum` FOREIGN KEY (`usernum`) REFERENCES `logins_users` (`usernum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logins_users_tfa`
--
ALTER TABLE `logins_users_tfa`
  ADD CONSTRAINT `TFA_Usernum` FOREIGN KEY (`usernum`) REFERENCES `logins_users` (`usernum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logins_users_tfa_cookies`
--
ALTER TABLE `logins_users_tfa_cookies`
  ADD CONSTRAINT `Cookie_Usernum` FOREIGN KEY (`usernum`) REFERENCES `logins_users` (`usernum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `security_acl_access`
--
ALTER TABLE `security_acl_access`
  ADD CONSTRAINT `security_acl_access_ibfk_1` FOREIGN KEY (`groupnum`) REFERENCES `security_acl_groups` (`groupindex`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `security_acl_access_ibfk_2` FOREIGN KEY (`modulenum`) REFERENCES `security_acl_modules` (`moduleindex`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `security_acl_functions`
--
ALTER TABLE `security_acl_functions`
  ADD CONSTRAINT `security_acl_functions_ibfk_1` FOREIGN KEY (`modulenum`) REFERENCES `security_acl_modules` (`moduleindex`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `security_acl_functions_access`
--
ALTER TABLE `security_acl_functions_access`
  ADD CONSTRAINT `security_acl_functions_access_ibfk_1` FOREIGN KEY (`functionnum`) REFERENCES `security_acl_functions` (`funcnum`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `security_acl_functions_access_ibfk_2` FOREIGN KEY (`groupnum`) REFERENCES `security_acl_groups` (`groupindex`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `support_conversations`
--
ALTER TABLE `support_conversations`
  ADD CONSTRAINT `support _assigned` FOREIGN KEY (`assignedto`) REFERENCES `logins_users` (`usernum`),
  ADD CONSTRAINT `support_userstart` FOREIGN KEY (`userstart`) REFERENCES `logins_users` (`usernum`);

--
-- Constraints for table `support_log`
--
ALTER TABLE `support_log`
  ADD CONSTRAINT `log_convno` FOREIGN KEY (`convno`) REFERENCES `support_conversations` (`convno`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `log_usernum` FOREIGN KEY (`loguser`) REFERENCES `logins_users` (`usernum`);

--
-- Constraints for table `support_messages`
--
ALTER TABLE `support_messages`
  ADD CONSTRAINT `message_convno` FOREIGN KEY (`convno`) REFERENCES `support_conversations` (`convno`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_sender` FOREIGN KEY (`sendernum`) REFERENCES `logins_users` (`usernum`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

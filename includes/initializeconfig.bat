@echo off
cls
if exist main.inc.php (
    echo Configuration already initialized - exiting.
) else (
    copy sample.db.inc.php db.inc.php 
    copy sample.db.inc.php dev.db.inc.php 
    copy sample.logging.inc.php logging.inc.php
    copy sample.main.inc.php main.inc.php
    copy sample.main.inc.php dev.main.inc.php
    copy sample.smtp.inc.php smtp.inc.php
    copy sample.smtp.inc.php dev.smtp.inc.php
    copy sample.twilio.inc.php twilio.inc.php
    copy sample.twilio.inc.php dev.twilio.inc.php
    copy sample.storage.inc.php storage.inc.php
    copy sample.storage.inc.php dev.storage.inc.php
    cd ..
    mkdir storage
    cd storage
    mkdir temp
    cd ..
    cd includes
    echo Configuration finished.  Same config files still exist so they will not delete from framework!
)

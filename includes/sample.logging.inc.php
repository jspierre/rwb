<?php
/** 
 * LOGGING CONFIGURATION FILE
 * Configures the SMTP Server that will be used with this application
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
 /* VARIABLES YOU CAN USE IN SUBJECT, PRE-, POST-, LONG & SHORT MESSAGES:
 /* 	%%USER%% = THE USER NAME LOGGED IN
 /*		%%IP%% = THE IP ADDRESS OF THE USER
 /* 	%%SUBJECT%% - THE SUBJECT TEXT DEFINED EITHER BELOW OR ON METHOD CALL
  /* 	%%PREMESSAGE%% - THE PRE-MESSAGE TEXT DEFINED BELOW 
 /* 	%%SHORTMESSAGE%% - THE SHORT MESSAGE TEXT DEFINED EITHER BELOW OR ON METHOD CALL
 /* 	%%LONGMESSAGE%% - THE LONG MESSAGE TEXT DEFINED EITHER BELOW OR ON METHOD CALL
 /* 	%%POSTMESSAGE%% - THE POST-MESSAGE TEXT DEFINED BELOW 
 
 /* LOG ROTATION SCHEDULE */
/* At what size (in MB) should the log be rotated and archived? 0=Never Rotate (NOT RECOMMENDED) */
$app_config['log_rotate_size'] = 2;

/* COMMUNICATION GROUPS */
$app_config['logging_groups'] = array (
	'SYS_ADMIN' => array(
		'method' => 'emailSend',
		'data' => array ('admin1@mycompanyname.com','admin2@mycompanyname.com','admin3@mycompanyname.com')
	),
	'default' => array(
		'method' => 'emailSend',
		'data' => array ('webmaster@mycompanyname.com')
	)
);
	
/* USER DEFINITION */
/*	If we alert the user, what table is the user lookup in (db_table)?  What column name should match the usernum from the logins_users table (db_match_column)?
/*	Which column holds the value for the user's address (db_value_column)?  
/*  Which column is used to test whether the user has this method "on" (db_active_column) (true/1 = ON and 0/false = OFF - important for opt-out,etc)
/*  Which communication method should be used with this data (notify_method)?
/*
/*  NOTE: The default SQL import file at includes/sql/security.mysql.sql already includes a default table for this purpose called
/*  logins_users_emails.  It is set up below to work out-of-the-box if you wish to use it this way.  If you don't use this table and change the
/*  values below, you can drop the logins_users_emails table without harm.
/*
/*	You can list any number of methods to send notices to the user (Email, text, etc), and each one will be tried in turn - for each "ON" value, 
/*	the system will send a notice 
*/
$app_config['logging_user'] = array (
	0 => array (
		'db_table' => 'logins_users_emails',
		'db_match_column' => 'usernum',
		'db_value_column' => 'emailaddress',
		'db_active_column' => 'isactive',
		'notify_method' => 'emailSend'
	)
);
	

 
 
 /* NOTIFICATION LEVELS */
 /* Default levels are: 0=EMERGENCY, 1=CRITICAL, 2=HIGH, 3=MEDIUM, 4=LOW, 8=WARNING, 9=INFORMATIONAL, 99=NOTIFY USER ONLY */
 $app_config['logging_levels'] = array (
	0 => array (
		'name' =>		'EMERGENCY',
		'applog' =>		TRUE,
		'commgroup' =>	array ('SYS_ADMIN'),
		'commother' =>	array ('emailSend'=>array('abuse@mycompanyname.com','attorney@mycompanyname.com') )
	),
	9 => array (
		'name' =>		'INFORMATION',
		'applog' =>		TRUE,
		'commgroup' =>	FALSE,
		'commother' =>	FALSE
	),
	99 => array (
		'name' =>		'NOTIFYUSER',
		'applog' =>		FALSE,
		'commgroup' =>	FALSE,
		'commother' =>	FALSE
	)
	
);
		

/* DEFAULT NOTIFICATION LEVEL */
/* If something screws up with your categories or you set a logging level that doesn't exist, what level should the system attempt to use instead? */
	$app_config['logging_level_default'] = 9;
	
  
 /* LOGGING CATEGORIES */
 /* 	Notes:	$subject = The subject to be used in any email or other long-form communication
				$premessage = A template message or introduction to be used before the $longmsg in any email or long-form communication
				$shortmsg = The default message for log and short-form communication (i.e., texts) IF NO $shortmessage IS SENT directly through the caller method
							$shortmsg is limited to 120 Characters automatically.
				$longmsg = The default message text for emails or long-form communication IF NO $longmessage IS SENT directly through the caller message
				$postmessage= A template message or introduction to be used after the $longmsg in any email or long-form communication
				$loglevel = The logging_level number as defined in that setting below
				$attack = Should the security module record the IP address as an attack for this (for blocking purposes)? TRUE/FALSE
				$notifyuser = If logged in, should the system attempt to copy the user (e.g., via email) with the same message? TRUE/FALSE
		The 'default' category is applied if code does not call a specific category and is usually used for generic, information or log-only messages
		NOTE: 	THE INITIAL CATEGORIES BELOW ARE IN USE INITIALLY BY THE SYSTEM.  IF YOU DELETE THESE CATEGORIES, BE SURE TO REMOVE REFERENCES TO THEM
				IN THE NEXT SECTION WHERE THEY ARE CALLED.
*/
 $app_config['logging_categories'] = array(
	'attack_and_log' => array (		
		'subject' => 	'',
		'premessage' => "",
		'shortmsg' =>	"",
		'longmsg' =>	"",
		'postmessage'=>	"",
		'loglevel' => 	9,
		'attack' => 	TRUE,
		'notifyuser' =>	FALSE
	),
	'log_only' => array (		
		'subject' => 	'',
		'premessage' => "",
		'shortmsg' =>	"",
		'longmsg' =>	"",
		'postmessage'=>	"",
		'loglevel' => 	9,
		'attack' => 	FALSE,
		'notifyuser' =>	FALSE
	),
	'default' => array (		
		'subject' => 	'',
		'premessage' => "",
		'shortmsg' =>	"",
		'longmsg' =>	"",
		'postmessage'=>	"",
		'loglevel' => 	9,
		'attack' => 	FALSE,
		'notifyuser' =>	FALSE
		),
	/************************************/
	/* USER-DEFINED CATEGORIES GO BELOW */
	/************************************/
	'user_notice' => array (		
		'subject' => 	'An important notice',
		'premessage' => "Dear user,\n\nPlease be advised of the following information:\n\n",
		'shortmsg' =>	"",
		'longmsg' =>	"%%SHORTMESSAGE%%\n\n",
		'postmessage'=>	"Thank you!\n\nSincerely,\nThe System",
		'loglevel' => 	9,
		'attack' => 	FALSE,
		'notifyuser' => TRUE
		)
	);
	
	
	
	/******************************************************************************************************/
	/* SYSTEM CATEGORIES GO BELOW - CHANGE AS NEEDED TO MATCH DEFINED CATEGORIES ABOVE OR YOUR OWN TASTES */
	/******************************************************************************************************/
	/* Logins: If system logins are used, what category should be used for successful login/logout attempts or logout due to expired session */
	$app_config['logging_cat_loginout'] = 'log_only';
	
	/* Logins: If system logins are used, category for FAILED login attempts */
	$app_config['logging_cat_loginfail'] = 'attack_and_log';
		
	/* Attempts to Access Non-Existent Pages */
	$app_config['logging_cat_error404'] = 'log_only';
	
	/* Successful access granted to pages */
	$app_config['logging_cat_success'] = 'log_only';
	
	/* Access Denied due to ACL */
	$app_config['logging_cat_acldeny'] = 'log_only';
	
	/* Access Denied because User is missing an ACL group in his record OR ACL group is set to inactive */
	$app_config['logging_cat_aclmissing'] = 'log_only';
	
	/* Messages returned by View to Router for logging */
	$app_config['logging_cat_viewmsg'] = 'log_only';
 
	/* Security: Access Fail due to IP address checking fail (not valid IP, IP doesn't match session data,etc.) */
	$app_config['logging_cat_secipfail'] = 'attack_and_log';
	
	/* Security: Access Fail due to IP is already banned by security systems */
	$app_config['logging_cat_secipban'] = 'attack_and_log';
	
	/* Security: Address is newly banned by security systems */
	$app_config['logging_cat_secipbannew'] = 'log_only';
	
	/* Security: Access Fail due to session data not matching data from client */
	$app_config['logging_cat_secsessfail'] = 'attack_and_log';
	
	/* Security: Access fail due to user being locked out */
	$app_config['logging_cat_userlocked'] = 'attack_and_log';
	
	/* Security: Access fail due to user being set 'inactive' (banned) by Admin */
	$app_config['logging_cat_userinactive'] = 'attack_and_log';
	
	/* Security: Password Expired, sent for update */
	$app_config['logging_cat_userpass'] = 'log_only';
	
	/* Security: Access fail - unauthenticated user attempt to access non-public area of site */
	$app_config['logging_cat_unuathaccess'] = 'attack_and_log';
	
	/* Passwords: Cross-site posting attempt to change user password */
	$app_config['logging_cat_passcsp'] = 'attack_and_log';
	
	/* Passwords: Old password is incorrect when trying to change password */
	$app_config['logging_cat_passold'] = 'attack_and_log';
	
	/* Passwords: Password Change successful */
	$app_config['logging_cat_passchgd'] = 'log_only';

	/* VBX: Attack recorded against VBX */
	$app_config['logging_cat_vbxattack'] = 'log_only';

	/* Users: Delete User Completely */
	$app_config['logging_cat_deleteuser'] = 'log_only';

	/* General: General - Logging of various items for information only */
	$app_config['logging_cat_general'] = 'log_only';	
?>
<?php
/** 
 * MAIN CONFIGURATION FILE
 * Configures the main application setup information that will be used with this application
 * @author John St. Pierre
 * @ver 1.0
 *
 */


/* ------------------------------------------------------------------------------- */
/* BASIC SETTINGS AND URL CONFIGURATION
/* ------------------------------------------------------------------------------- */

/* DEV MODE ON?   KEEP THIS ON FALSE UNLESS YOU WANT YOUR APP TO BE UNSAFE!!! DO NOT USE DEV MODE ON PRODUCTION SERVER!! */
/* ***IMPORTANT*** */
/* IF SET TO TRUE, THE CONFIGURATOR WILL USE THE dev.xxxx.inc.php FILES INSTEAD OF THE PRODUCTION ONES. */
/* BE VERY CAREFUL TO SET THIS CORRECTLY */
$app_config['devmode'] = false;

/* THE EXACT PHP TIMEZONE TO USE FOR YOUR LOGS AND DATE FUNCTIONS (SEE http://www.php.net/manual/en/timezones.php ) */
$app_config['timezone'] = 'America/Chicago';

/* The base domain that your app operates from.  If your site is at */
/* http://mysite.com/app, you would put http://mysite.com */
/* This is the domain WITH NO SUBDIRECTORIES */
$app_config['basedomain'] = "https://localhost";
 
/* The full url of the app's main page (usually just http://my.app.com or https://my.app.com, but could be http://my.app.com/subdir ) 
/* NOTE: There is NO trailing slash in this setting!
/* NOTE: If you turn on 'ssl' below, make SURE this is an https:// address!! 	*/
$app_config['url'] = 'https://localhost';

/* The URL base from which your app operates (if you are using a subdirectory it would be '/subdirectory/', */
/* EXAMPLES: www.myapp.com = "/"; myapp.com/app = "/app/"; myapp.com/src/app = "/src/app/"
/* IMPORTANT NOTE: There IS a trailing slash in this setting!
/* If your app is in the public_html root (web root), it would just be '/' */
$app_config['baseurl'] = '/';

/* Is SSL required always? */
/* Should the system force all users to receive pages over https? */
/* The default is now TRUE and should be left like this unless you have good reason not to */
$app_config['ssl'] = TRUE;



/* ------------------------------------------------------------------------------- */
/* SITE AND COMPANY DETAILS CONFIGURATION
/* ------------------------------------------------------------------------------- */

/* Your site's name - this will appear in various places throughout the site. */
/* DO NOT INCLUDE any descriptors or other elements - just the name of your site (usually under 25 char) */
$app_config['sitename'] = "My Company Name";

/* Your site's primary help/support email address.  This will appear in emails and other places. */
/* Enter ONE email address ONLY */
$app_config['siteemail'] = "support@mycompanyname.com";

/* Your site's email address for sending system emails.  This is usually a "DO NOT REPLY" email address */
/* that can be used for situations when you don't want replies to the emails being sent out.  This  */
/* MUST BE SET, even if it is the same emal as the 'siteemail' setting above */
$app_config['systememail'] = "donotreply@mycompanyname.com";

/* Your site's primary help/support phone number.  This will appear in various emails and other places. */
/* This should be formatted to your taste. */
$app_config['sitephone'] = "888-123-4567";

/* THE DEFAULT US STATE (2 LETTER ABBREVIATION) TO USE IN DROPDOWN LISTS, ETC. IF NONE, YOU CAN LEAVE BLANK ("")*/
/* AND THE SELECTION WILL START AT THE TOP OF THE LIST OF STATES INSTEAD */
$app_config['us_state'] = "TX";

/* THE DEFAULT DISPLAY STYLE FOR PHONE NUMBERS */
/* ""= NO FORMATTING, "PARENTHESIS" = (xxx) xxx-xxxx, "DASHES" = xxx-xxx-xxxx */
$app_config['phone_format'] = "PARENTHESIS";



/* ------------------------------------------------------------------------------- */
/* DEFAULT SITE PAGES (CONTROLLERS) 
/* ------------------------------------------------------------------------------- */

/* The main page of your site that users are sent to when they go to your main URL */
/* This is set to 'appindex' by default and shouldn't be changed unless you know what you are doing */
$app_config['firstpage'] = 'appindex';

/* What page to display when the user requests a non-existent page.  This seves the same purposes as an Error 404 Not Found Page */
/* This is set to 'error404' by default - For most uses, this setting can be left alone */
$app_config['404page'] = 'error404';

/* What page to display when the system is under maintenance mode? */
/* This can usually be left alone, although feel free to customize the view for it */
$app_config['maintpage'] = 'sysmaint';

/* What page do users log in from?.  For most apps, this setting can be left alone */
$app_config['loginurl'] = "login";

/* What page do users go to in order to logout?  For most apps, this can be left alone */
$app_config['logouturl'] = "logout";
		
/* What page do users go to once they've logged in?  */
$app_config['mainpage'] = 'main';
		
/* What page do users go to once they've logged out? For most apps, this can be left alone */
$app_config['postlogout'] = "login";

/* IF LOGINS = TRUE, Set the password change page controller name.  For most, this can be left alone */
$app_config['pwchgurl'] = "pwchg";



/* ------------------------------------------------------------------------------- */
/* PAGES EXEMPT FROM LOGIN
/* ------------------------------------------------------------------------------- */

/* This is an array of all the pages (in /controller) that DO NOT require a */
/* logged-in user to be able to access it - BE CAREFUL WITH THIS AS IT OVERRIDES EVERYTHING ELSE */
/* Make sure you list every page that users may need to access BEFORE they log in */
/* For example: terms, privacy policy, home page, etc.  WE STRONGLY RECOMMEND LEAVING AT LEAST THE
/* PAGES INITIALLY LISTED BELOW IN THE ARRAY (tandc, appindex, privacypolicy, pwrst, signup, sysmaint). */
$app_config['loginexempt'] = array(
	'tandc',
	'appindex',
	'privacypolicy',
	'pwrst', 
	'signup', 
	'sysmaint',
	'receiver_sms'
);
		


/* ------------------------------------------------------------------------------- */
/* SECURITY AND BRUTE-FORCE PROTECTION SETTINGS
/* ------------------------------------------------------------------------------- */

/* Brute Force: how many 'bad' attempts will be allowed from an IP Address until your */
/* app permanently bans an attacker? */
$app_config['security_attempts'] = 5;
	
/* How long (in hours) between resetting of attempts against the app? */
$app_config['security_reset'] = 24;
	
/* Should 404 Errors considered an attack? */
/* Some hackers will try different page names attempting to find an unprotected way in, */
/* so 404 errors could in some cases mean an attack */
$app_config['security_404attacks'] = TRUE;



/* ------------------------------------------------------------------------------- */
/* LOG-IN AND USER SETTINGS
/* ------------------------------------------------------------------------------- */

/* Should Users log in with email addresses or usernames? */
/* to use EMAIL, set "TRUE"; to use USERNAMES, set "FALSE" */
$app_config['loginwithemail'] = true;

/* If Users are logging in with USERNAMES, then is it okay to */
/* have multiple usernames with the same email address? */
/* Is so, set to TRUE; if not, FALSE.  WARNING: FALSE is strongly recommended! */
$app_config['allowduplicateemails']	= false;

/* Should the login IDs (either userid or email) be case-sensitive? */
/* Case-Sensitive = True; Non-Case-Sensitive = False; FALSE is strongly recommended */
$app_config['logincase'] = FALSE;
		
/* Allow new users to create their own account (through Create One Now! link)? */
/* false = Do not allow users to create accounts (admin must create); true = Allow users to create accounts */
$app_config['createself'] = true;

/* If users can create their own accounts, should the system send an email to someone */
/* to alert them of each new user signup? true = yes, send email on self-signup; false = no */
/* If true, also give email addresses (as an array) of each email to send the notice to */
$app_config['notifyoncreate'] = true;
$app_config['notifyoncreate_emails'] = array(
	'person1@mycompanyname.com',
	'person2@mycompanyname.com'
);

/* If users can create their own account, what security group should they belon to upon setup?  */
/* This is from the "Group #" field on the "ACL Group Management" Administration Screen */
/* This can ONLY be a number.  The default number is set to the "User" Group with limited access */
$app_config['defaultaclgroup'] = 7;

/* If users create their own account, you can activate an "Offer Code" that will allow the */
/* user to be granted a higher acl group automatically - this can be used for special offers */
/* or limited time promotions.  If you don't want to use this, just set it to false */
/* 'offercodeon' = true/false - turn offer code on or off */
/* 'offercode' =  "string" - the offercode itself.  Max of 255 Characters. It is NOT case sensitive */
/* 'offeraclgroup' = the Group # that the offercode will give the user */
$app_config['offercodeon'] = false;
$app_config['offercode'] = "BETATESTER2020";
$app_config['offeraclgroup'] = 7;



/* ------------------------------------------------------------------------------- */
/* PASSWORD SETTINGS
/* ------------------------------------------------------------------------------- */

/* How many attempts with the wrong password before a user is locked out? 
/* This must be an integer.  If you do not want to lock out users, set this to FALSE) */
$app_config['maxpwattempts'] = 5;

/* Allow users to reset their own password if they forget (via I Forgot My Password link)? */
/* false = Do not allow self-reset; true = Allow self-reset */
$app_config['resetpw'] = false;

/* What is the minimum password length required.  Minimum is 1 */
$app_config['pwlength'] = 6;
		
/* How many days until a user's password expires? Must be an integer */
/* If user passwords should NEVER expire, set to FALSE (not recommended) */
$app_config['pwexpires'] = 90;
		
/* IF LOGINS = TRUE, Set the number of idle minutes before a session expires. */
/* This must be an integer - for most, this can be left alone */
$app_config['sessiontime'] = 60;

/* Allow two-factor authentication? */
$app_config['twofactor'] = true;

/* If two-factor auth is allowed, should the code be sent via email or via text? */
/* FOR EMAIL, set to 1.  For TEXT WITH FALLBACK TO EMAIL, set to 2
/* NOTE!!! TEXT REQUIRES TWILIO TO WORK! IF YOU SET TO 2 MAKE SURE TO FILL OUT THE /includes/twilio.inc.php file with details! */
$app_config['twofactor_method'] = 1;
		
		


/* ------------------------------------------------------------------------------- */
/* SYSTEM LOG SETTINGS
/* ------------------------------------------------------------------------------- */

/* Use built-in Notifications and System Logging Features? */
$app_config['notifications'] = TRUE;		

/* If using Notifications, also use built-in logging features? */
$app_config['logging'] = TRUE;

/* If using built-in logging, where should system log be kept? (Full system path or relative path)*/
/* Include the name of the log file as well (recommend main.log) */
/* We Recommended leaving the default:  'logs/main.log'                  */
$app_config['logging_location'] = 'logs/main.log';

/* If using built-in logging, where should archived logs be kept (full or relative path ONLY) */
/* Include a trailing slash BUT DO NOT INCLUDE A FILE NAME - this will be generated automatically */
/* We Recommended leaving the default:  'logs/archives/'               */
$app_config['logging_archives'] = 'logs/archives/';
	


/* ------------------------------------------------------------------------------- */
/* MAIL SETTINGS  
/* ------------------------------------------------------------------------------- */
/* NOTE: ALSO see includes/smtp.inc.php file to set up mail server settings */

/* When sending system mails (Mass Email to All Users), what is the preferred time of day to start sending, in full 24 hour format */
/* WITH LEADING ZEROES IF ANY: e.g., for 3:30 a.m. = "03:30", for 7:15 p.m. = "19:15".  Leave as is to start */
/* the process at the recommended time: 1 a.m. when system load is likely low. */
/* NOTE: This can be overriden in the Email Management Module to send "ASAP" when needed */
$app_config['systememailtime'] = "01:00";

/* If using built-in Mail Features: */
/* What is the default 'From' Email address to show on outgoing messages? */
$app_config['smtp_from'] = 'support@mycompanyname.com';

/* If using built-in Mail Features: */
/* What is the default 'From Name'  to show on outgoing messages? */
$app_config['smtp_frname'] = 'My Company Name Customer Support';



/* ------------------------------------------------------------------------------- */
/* USER FILE STORAGE
/* ------------------------------------------------------------------------------- */

/* What directory are files to be stored in that may need to be downloaded or accessed by users? */
/* Full System path or Relative Path is fine */
$app_config['file_location'] = 'fileshare';


/* ------------------------------------------------------------------------------- */
/* PLUGINS
/* ------------------------------------------------------------------------------- */

/* The User Module has  allows you to extend the funcionality of the panel by adding */
/* buttons to the row(s) that already exist that give additonal functionality.  They */
/* simply hook into a new controller that doesn't get edited.  This way, if the main */
/* User controller gets upgraded, you won't lose your additions. */

/* Set to false if not using, or...

/* Just create this array like in the example and your plugin button will appear. */
/* The user in question will always be stored in $_SESSION['loginnum'] and you */
/* can return to the user simply by redirecting back to usmgmt due to this. */
/* The system will also set $_SESSION['returnto'] = 'usmgmt'

/* 
$app_config['usr_plugins'] = array(
	'plugin1' => array (						(this can be anything, but best to use plugin1, plugin2, etc.)
		'button_name' => 'List Collections',  (the name that you want on the button)
		'button_description' => 'List all Collections this user owns', (the tooltip to show)
		'button_function' => 'vvemail',	   (the ACL function the user must have to view this button)
		'button_submit_value' => 'listcollection',    (the value that the button will submit back to usmgmt)
		'button_color_class' => 'btn-primary',	   (the color you want the button to be - Bootstrap class)
		'controller' => 'collcollmgmt',         (the controller you want called when the button is pressed)
	),
	'plugin2' => array (						
		'button_name' => 'List Collections',  
		'button_description' => 'List all Collections this user owns', 
		'button_function' => 'vvemail',	   
		'button_submit_value' => 'listcollection',    
		'button_color_class' => 'btn-primary',	   
		'controller' => 'collcollmgmt',         
	)
);
*/

$app_config['usr_plugins'] = false;


/* ------------------------------------------------------------------------------- */
/* DEVELOPER (YOUR) ADDITIONAL SETTINGS
/* ------------------------------------------------------------------------------- */

/* Have a set of config rules specific to your app? */
/* Define it here, and then add a file with $app_config['appname_var'] = 'value'; */
/* Put 'appname' before the variable name to AVOID CONFLICTS WITH OTHER CONFIGURATIONS IN THIS FILE & ELSEWHERE! */
/* SEE EXAMPLE FILE NAMED BELOW */

// require (SITE_PATH.'/includes/myappname.inc.php');

















/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
/* DO NOT TOUCH BELOW THIS LINE! - THESE SETTINGS ARE CRUCIAL TO PROPER */
/* OPERATION OF THE SYSTEM AND COULD BREAK ITEMS COMPLETELY - YOU HAVE BEEN WARNED! */
/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
$app_config['pepper'] = "H6Oo98-hIgq-&>FSuwC|gF6+IDwdD?<($[y#w=kVX|Rv<koN+7?Qu(#5e6UJdh%";
$app_config['sessions'] = TRUE;
$app_config['logins'] = TRUE;
$app_config['logtable'] = 'logins_users';
$app_config['security'] = TRUE;
$app_config['acl'] = TRUE;
$app_config['acl_access']='security_acl_access';
$app_config['acl_groups']='security_acl_groups';
$app_config['acl_modules']='security_acl_modules';
$app_config['acl_functions']='security_acl_functions';
$app_config['acl_functions_rules'] = 'security_acl_functions_access';

?>
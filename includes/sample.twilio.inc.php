<?php
/** 
 * TWILIO CALL API CONFIGURATION FILE
 * Configures the settings for your TWILIO account.
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
/* Your Twilio ACCOUNT SID */
$app_config['twilio_SID'] = 'ABC123039ejj73712321123DEFAULT';
 
/* Your Twilio ACCOUNT AUTH TOKEN */
$app_config['twilio_AUTH'] = 'ABC123039ejj73712321123DEFAULT';

/* The location of your Twilio Third Party autoload.php file */
$app_config['twilio_LOC'] = SITE_PATH.'/thirdparty/twilio-php/src/Twilio/autoload.php';

/* Your Twilio Default Phone Number - Used on Outgoing Calls & SMS */
/* Put this in international format: i.e., +12105551212 */
$app_config['twilio_DEFAULT_PHONE'] = "+11234567890";


/*********************************************************/
/* NOTE: IF YOU PLAN TO USE TWILIO FOR THE SUPPORTMASTER */
/* FEATURE, YOU --MUST-- SET YOUR TWILIO PHONE NUMBER */
/* THAT WILL RECEIVE TEXT MESSAGES TO:
/* https://yourwebsite.com/receiver_sms */
/*********************************************************/
?>
<?php
/** 
 * CONFIGURATOR
 * Pulls together all configuration files 
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
// First pull in the Main Configuration File
// If we're in dev mode, overwrite that file now to avoid errors 
require (SITE_PATH.'/includes/main.inc.php');
if ($app_config['devmode'] == true) { 
	require (SITE_PATH.'/includes/dev.main.inc.php');
}

// Now, define our BASE_URL constant
define ('BASE_URL', $app_config['baseurl']); 

// Now we can pull in the rest of our PRD config file
require (SITE_PATH.'/includes/db.inc.php');
require (SITE_PATH.'/includes/smtp.inc.php');
require (SITE_PATH.'/includes/logging.inc.php');
require (SITE_PATH.'/includes/twilio.inc.php');
require (SITE_PATH.'/includes/storage.inc.php');

// BUT, we then overwrite Config File with DEV values if DEVMODE
// Is set to TRUE in main config file
if ($app_config['devmode'] == true) {
	require (SITE_PATH.'/includes/dev.db.inc.php');
	require (SITE_PATH.'/includes/dev.smtp.inc.php');
	require (SITE_PATH.'/includes/dev.twilio.inc.php');
	require (SITE_PATH.'/includes/dev.storage.inc.php');
}

?>

<?php
/** 
 * APPLICATION SPECIFIC CONFIG FILE
 * Options specific to the application - not part of original framework
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
 /* This is an example seting - note that use of 'myappname' before the */
 /* Variable name - this is to avoid conflicts with other configuration variables */
 $app_config['myappname_myvar'] = "SOME VALUE";
 


?>
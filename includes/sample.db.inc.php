<?php
/** 
 * DATABASE CONFIGURATION FILE
 * Configures the database that will be used with this application
 * @author John St. Pierre
 * @ver 0.1
 *
 */
 
 // What type of database are we using? Right now, only 'mysql' is supported
 $app_config['dbtype'] = 'mysql';
 
 
 // What is the hostname of the database - usually 'localhost', but can also be a remote hostname
 // if needed or an IP address
 $app_config['dbhost'] = 'localhost';
 
 
 // What is the name of the database that this application will use?
 $app_config['dbname'] = 'dbname';
 
 
 // Under what user should the application access the database?
 $app_config['dbuser'] = 'dbuser';
 
 
 // What is the password of the database user to access the database?
 $app_config['dbpass'] = 'dbpassword';
 
 
 // Should the system log all database calls and responses to a logfile?
 // ***BE CAREFUL!*** THIS OPTION MAY EXPOSE SENSITIVE INFORMATION AND
 // IS ALSO VERY RESOURCE-INTENSIVE. IT SHOULD ONLY BE USED TEMPORARILY
 // FOR  DEBUGGING PURPOSES.
 $app_config['dblogging'] = false;
 
 // If database logging is turned on, where should the log be stored?
 // There is no need to change the default unless you need to.
 $app_config['dbglogging_location'] = 'logs/database.log';
 
 
 ?>
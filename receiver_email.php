<?php

/*** error reporting on ***/
error_reporting(E_ALL);

/*** define the site path constant ***/
$site_path = realpath(dirname(__FILE__));
define ('SITE_PATH', $site_path);


/*** include the init.php file ***/
require(SITE_PATH.'/includes/init.php');
require(SITE_PATH.'/application/background.class.php');

/* Now, let's call in the model we'll need */
$model = new mSupport($registry);

// Okay, now we can accept the incoming message
$fd = fopen("php://stdin", "r");
$now = date("Y-m-d H:i:s");
$explanation = "INCOMING SUPPORT EMAIL INITIATED";
$registry->logging->logEvent($registry->config['logging_cat_general'], $explanation);
$email = "";
while (!feof($fd)) {
	$email .= fread($fd, 1024);
}
fclose($fd);

if (!$email || $email == "") {
	$explanation = "FAILED UPON EMAIL RECEIPT.";
	$registry->logging->logEvent($registry->config['logging_cat_general'],$explanation);
	exit();
}

/* Okay, now we send the data straight into the database */
$result = $model->incomingEmail($email);
if ($result) {
	$explanation = "RECORDED AND NOTIFIED INCOMING SUPPORT EMAIL - SUCCCESS";
}
else {
	$explanation = "FAILED WHILE RECORDING AND/OR NOTIFYING SUPPORT EMAIL";	
}
$registry->logging->logEvent($registry->config['logging_cat_general'],$explanation);
exit();


?>
<?php

class mSecurity extends baseModel {
    
    // This method returns ALL data from the DB about ALL ACL Groups
    // ACTIVE = return only active groups? (TRUE); ADMIN = return admin groups AND regular groups? (FALSE)
    public function getAllACLGroups($active=false, $admin=true, $count=false) {
        if ($count) { 
            $sqladd = ", (SELECT count(*) FROM logins_users WHERE logins_users.aclgroup  = security_acl_groups.groupindex) AS members "; 
        }
		else { 
            $sqladd = ""; 
        }
        if ($active) {
            if ($admin) {
                $sql = "SELECT security_acl_groups.* ".$sqladd."FROM security_acl_groups WHERE isactive = 1 ORDER BY groupname ASC";    
            }
            else {
                $sql = "SELECT security_acl_groups.* ".$sqladd."FROM security_acl_groups WHERE isactive = 1 AND isadmin = 0 ORDER BY groupname ASC"; 
            }
        }
        else {
            if ($admin) {
                $sql = "SELECT security_acl_groups.* ".$sqladd."FROM security_acl_groups ORDER BY groupname ASC";    
            }
            else {
                $sql = "SELECT security_acl_groups.* ".$sqladd."FROM security_acl_groups WHERE isadmin = 0 ORDER BY groupname ASC";
            }
            
        }
        $this->registry->db->query($sql);
        $groups = $this->registry->db->getAllRows();
        return $groups;
    }

    // This method moves all users with one ACL Group to another ACL group
    public function moveACLGroups($groupnum, $moveto) {
        if ($groupnum == $moveto) {
            return false;
        }
        $sql = "UPDATE logins_users SET aclgroup = $moveto WHERE aclgroup = $groupnum";
        $result = $this->registry->db->query($sql);
        if ($result) { return true; } else { return false; }
    }

    // This method returns ALL data from the DB about ALL Modules
    // if $showall = True, show the main module as well (caution)
    // if $removepermissions = True, only return the index and names, not the allow or deny permissions
    public function getAllModules($showall=false, $removepermissions=false, $sortbyname=false) {
        if ($sortbyname && $showall) { $sort = "ORDER BY CASE WHEN moduleindex = 1 THEN 1 ELSE 2 END, modulename ASC"; }
        elseif ($sortbyname && $showall == false) { $sort = "ORDER BY modulename ASC"; }
        else { $sort = "ORDER BY controller ASC"; }
        if ($showall) {
            if ($removepermissions) {
                $sql = "SELECT moduleindex, controller, modulename FROM security_acl_modules ".$sort;
            }
            else {
                $sql = "SELECT * FROM security_acl_modules ".$sort;
            }
        }
        else {
            if ($removepermissions) {
                $sql = "SELECT moduleindex,controller, modulename FROM security_acl_modules WHERE moduleindex > 1 ".$sort;
            }
            else {
                $sql = "SELECT * FROM security_acl_modules WHERE moduleindex > 1 ".$sort;    
            }
        }
        $this->registry->db->query($sql);
        $modules = $this->registry->db->getAllRows();
        return $modules;
    }

    // This method returns ALL data from the DB about ALL granular Functions
    public function getAllFunctions($sortbyname=false) {
        if ($sortbyname) { $sort = "ORDER BY modulename, funcdescr ASC"; }
        else { $sort = "ORDER BY modulenum, funcnum ASC"; }
        $sql = "SELECT security_acl_functions.*, security_acl_modules.moduleindex, security_acl_modules.controller, security_acl_modules.modulename FROM security_acl_functions LEFT JOIN security_acl_modules ON security_acl_functions.modulenum = security_acl_modules.moduleindex ".$sort;
        $this->registry->db->query($sql);
        $modules = $this->registry->db->getAllRows();
        return $modules;
    }

    // This method returns all modules, all functions and which ones have permission for the 
    // Given Group
    public function getGroupPermissions($groupnum=false) {
        $showall=true;
        $modulesList = $this->getAllModules($showall, false, true);
        if ($groupnum) {
            $sql = "SELECT modulenum FROM security_acl_access WHERE groupnum = $groupnum";
            $this->registry->db->query($sql);
            $modulesPermission = $this->registry->db->getAllRows();
            $functionsList = $this->getAllFunctions(true);
            $sql = "SELECT functionnum FROM security_acl_functions_access WHERE groupnum = $groupnum";
            $this->registry->db->query($sql);
            $functionsPermission = $this->registry->db->getAllRows();
        }
        else {
            $modulesPermission = false;
            $functionsPermission = false;
        }
        $permissions['modAll'] = $modulesList;
        $permissions['modPerm'] = $modulesPermission;
        $permissions['funcAll'] = $functionsList;
        $permissions['funcPerm'] = $functionsPermission;
        return $permissions;
    }



    // This method returns ALL data from the DB about ONE particular ACL Group
    public function getACLGroup($groupnum=false) {
        if ($groupnum) {
            $sql = "SELECT security_acl_groups.*, (SELECT count(*) FROM logins_users WHERE logins_users.aclgroup = 
            security_acl_groups.groupindex) AS members FROM security_acl_groups WHERE groupindex = $groupnum LIMIT 1";
            $data = $this->registry->db->oneRowQuery($sql);
            return $data;
        }   
        else {
            return false;
        }
    }
   
    // This method deletes an ACL Group completely UNLESS it is currently 
    // in use by one or more users. Returns TRUE if successful, FALSE if not
    public function deleteACLGroup($groupnum) {
        // First, check to see if the group is really empty
        $sql = "SELECT * FROM logins_users WHERE aclgroup = $groupnum";
	    $groupuse = $this->registry->db->queryGetRows($sql);
	    if ($groupuse > 0) { return false; }
	    // Next, delete the group
        $sql = "DELETE FROM security_acl_groups WHERE groupindex = $groupnum LIMIT 1";
        if ($this->registry->db->query($sql)) { return true; } else { return false; }
    }

    

    public function getModule($modnum=false, $fullurl=false) {
        if ($modnum) {
            $sql = "SELECT * FROM security_acl_modules WHERE moduleindex = $modnum LIMIT 1";
            $vars=$this->registry->db->oneRowQuery($sql);
            if ($vars) {
                if ($fullurl) {
                    $baseurl = $this->registry->config['url'];
                    $fullurl = $baseurl."/".$vars['controller'];
                    return $fullurl;
                }
                else {
                    return $vars;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    // This method deletes an ACL Function completely  
    // Returns TRUE if successful, FALSE if not
    public function deleteModule($modulenum) {
        // Delete the function from all access rules
        $sql = "DELETE FROM security_acl_access WHERE modulenum = $modulenum";
        if ($this->registry->db->query($sql)) { 
            // If that was successful, now let's remove the main function from the table
            $sql = "DELETE FROM security_acl_modules WHERE moduleindex = $modulenum LIMIT 1";
            if ($this->registry->db->query($sql)) { return true; } else { return false; } 
        }
        else { 
            return false; 
        }
        return true;
    }

    public function getFunction($funcnum=false) {
        if ($funcnum) {
            $sql = "SELECT security_acl_functions.*, security_acl_modules.moduleindex, security_acl_modules.controller, security_acl_modules.modulename FROM security_acl_functions LEFT JOIN security_acl_modules ON security_acl_functions.modulenum = security_acl_modules.moduleindex WHERE funcnum = $funcnum LIMIT 1";
            $this->registry->db->query($sql);
            $vars=$this->registry->db->oneRowQuery($sql);
            if ($vars) {
                return $vars;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    // This method deletes an ACL Function completely  
    // Returns TRUE if successful, FALSE if not
    public function deleteFunction($funcnum) {
        // First, delete the function from all access rules
        $sql = "DELETE FROM security_acl_functions_access WHERE functionnum = $funcnum";
        if ($this->registry->db->query($sql)) { 
            // If that was successful, now let's remove the main function from the table
            $sql = "DELETE FROM security_acl_functions WHERE funcnum = $funcnum LIMIT 1";
            if ($this->registry->db->query($sql)) { return true; } else { return false; } 
        }
        else { 
            return false; 
        }
        return true;
    }



    // This method checks for duplicate entries.  It returns the index number if a duplicate is found, or false if
    // no duplicate is found.  The first parameter $groupname is required and it will attempt to match a groupname
    // case-insensitive (due to DB collation of _ci).  The second parameter is optional; if supplied it will return 
    // a hit ONLY IF the duplicate is not found at $groupnum (in other words, you're making sure that the user isn't)
    // Updating an existing field name to a name that already exists.)
    private function checkGroupDups($groupname=false, $groupnum=false) {
        if ($groupname) {
            if ($groupnum) {
                $sql = "SELECT groupindex FROM security_acl_groups WHERE groupname = '$groupname'  AND groupindex != $groupnum LIMIT 1"; 
            }
            else {
                $sql = "SELECT groupindex FROM security_acl_groups WHERE groupname LIKE '$groupname' LIMIT 1";
            }
            $groupnum = $this->registry->db->oneRowQuery($sql);
            if ($groupnum) {
                return $groupnum['groupindex'];
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    private function checkModuleDups($controller=false, $modnum=false) {
        if ($controller) {
            if ($modnum) {
                $sql = "SELECT moduleindex FROM security_acl_modules WHERE controller = '$controller' AND moduleindex != $modnum LIMIT 1"; 
            }
            else {
                $sql = "SELECT moduleindex FROM security_acl_modules WHERE controller = '$controller' LIMIT 1";
            }
            $modnum = $this->registry->db->oneRowQuery($sql);
            if ($modnum) {
                return $modnum['moduleindex'];
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    private function checkFunctionDups($funcname=false, $modulenum=false, $funcnum=false) {
        if ($funcname) {
            if ($funcnum) {
                $sql = "SELECT funcnum FROM security_acl_functions WHERE funcname = '$funcname' AND modulenum = $modulenum  AND funcnum != $funcnum LIMIT 1";
            }
            else {
                $sql = "SELECT funcnum FROM security_acl_functions WHERE funcname = '$funcname' AND modulenum = $modulenum LIMIT 1";
            }
            $funcnum = $this->registry->db->oneRowQuery($sql);
            if ($funcnum) {
                return $funcnum['funcnum'];
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }


    public function newModule($data) {
        // First, sanitize the incoming data
        // ***NOT NECESSARY - SANITIZED IN CONTROLLER**** //
        //$data=$this->registry->security->sanitizeArray($data);
        
        // Next, extract it all to individual variables
        extract($data);
        
        // Now, check that all variables needed exist
        if (!isset($controller) || empty ($controller) || !isset($modulename) || empty($modulename)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        else {
            // Let's make sure that the controller name stays lowercase
            $controller = strtolower($controller);
            
            // You can't set both deny all and allow all.  Deny overrides allow, so we set that here if deny is set
            if ($denyall == 1) { $allowall = 0; }

            // Next, check for duplicate entries   
            $exists = $this->checkModuleDups($controller);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }
            // Next, do the insert
            else {
                $sql = "INSERT INTO security_acl_modules (controller, modulename, allowall, denyall) VALUES ('$controller', '$modulename', $allowall, $denyall)";      
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $modulenum = $this->registry->db->getInsertId();
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $modulenum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = false;
                }
            }
        }
        return $result;
    }
    

    // This method creates a new ACL Group Entry from an array containing key=>value pairs of the table variables needed
    // Returns an array containing success (TRUE, FALSE); result message (SUCCESS, DUPLICATE, FAILURE); index number (new or duplicate)
    public function newACLGroup($data) {
        // First, sanitize the incoming data
        // ***NOT NEEDED - DONE IN CONTROLLER***
        //$data=$this->registry->security->sanitizeArray($data);

        // Next, extract it all to individual variables
        extract($data);
        // Then check that all variables needed exist
        if (!isset($groupname) || !isset($isactive) || empty($groupname)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        // Next, check for duplicate entries
        else {
            $exists = $this->checkGroupDups($groupname);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }
            // Next, do the insert
            else {
                // First, if ADMIN is YES, BUT STAFF is NO, this is wrong.  ADMIN is automatically staff, so fix this.
                if ($isadmin == 1) { $isstaff = 1; }
                $sql = "INSERT INTO security_acl_groups (groupname, isactive, isadmin, isstaff) VALUES ('$groupname', $isactive, $isadmin, $isstaff)";      
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $groupnum = $this->registry->db->getInsertId();
                    // If we are copying another profile to make this one, let's do the copying now:
                    if ($copyfrom > 0) {
                        $this->copyPermissions($groupnum, $groupname, $isactive, $isadmin, $isstaff, $copyfrom);
                    }
                    // Okay, let's send back success
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $groupnum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = false;
                }
            }
        }
        return $result;
    }

    // This method copys the permissions of the group number $copyfrom onto the 
    // permissions of $groupnum
    public function copyPermissions($groupnum, $groupname, $isactive, $isadmin, $isstaff, $copyfrom) {
        // First, set some variables we'll need
        $data['groupnum']=$groupnum;
        $data['groupname']=$groupname;
        $data['isactive']=$isactive;
        $data['isadmin']=$isadmin;
        $data['isstaff']=$isstaff;

        // Next, get the permissions of the group being copied
        $permissions = $this->getGroupPermissions($copyfrom);
        $modules=false;
        $functions=false;
        if (isset($permissions['modPerm']) && count($permissions['modPerm'])>0) {
            foreach ($permissions['modPerm'] as $mod => $perm) {
                $modules[]=$perm['modulenum'];
            }
        }
        if (isset($permissions['funcPerm']) && count($permissions['funcPerm'])>0) {
            foreach ($permissions['funcPerm'] as $func => $perm) {
                $functions[]=$perm['functionnum'];
            }
        }
        $data['module'] = $modules;
        $data['function'] = $functions;

        // Next, let's call the ACLGroup update function to perform the magic
        $this->updateACLGroup($groupnum, $data);
        return true;
    }

    // This method creates a new granular function entry from an array containing key=>value pairs of the table variables needed
    // Returns an array containing success (TRUE, FALSE); result message (SUCCESS, DUPLICATE, FAILURE); index number (new or duplicate)
    public function newFunction($data) {
        // First, sanitize the incoming data
        // ***NOT NEEDED - DONE IN CONTROLLER***
        //$data=$this->registry->security->sanitizeArray($data);

        // Next, extract it all to individual variables
        extract($data);
        // Then check that all variables needed exist
        if (!isset($funcname) || !isset($funcdescr)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        // Next, check for duplicate entries
        else {
            $exists = $this->checkFunctionDups($funcname, $modulenum);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }
            // Next, do the insert
            else {
                $sql = "INSERT INTO security_acl_functions (modulenum, funcname, funcdescr, functionactive) VALUES ($modulenum, '$funcname', '$funcdescr', $functionactive)";      
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $funcnum = $this->registry->db->getInsertId();
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $funcnum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = false;
                }
            }
        }
        return $result;
    }

    // This method attempts to update an ACL Group Entry (given as $groupnum) from an array ($data) containing key=>value pairs of the table variables needed
    // Returns an array containing success (TRUE, FALSE); result message (SUCCESS, DUPLICATE, FAILURE); index number (new or duplicate)
    public function updateACLGroup($groupnum, $data) {
        // First, sanitize the incoming data
        // ***NOT NECESSARY - SANITIZED IN CONTROLLER**** //
        //$data=$this->registry->security->sanitizeArray($data);

        // Next, extract it all to individual variables
        extract($data);
        // Then check that all variables needed exist
        if (!isset($groupname) || !isset($isactive) || empty ($groupnum) || empty($groupname)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        // Next, check for duplicate entries
        else {
            $exists = $this->checkGroupDups($groupname, $groupnum);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }

            // Next, do the update
            else {
                // First, if ADMIN is YES, BUT STAFF is NO, this is wrong.  ADMIN is automatically staff, so fix this.
                if ($isadmin == 1) { $isstaff = 1; }
                $sql = "UPDATE security_acl_groups SET groupname='$groupname', isactive=$isactive, isadmin=$isadmin, isstaff=$isstaff WHERE groupindex = $groupnum LIMIT 1";     
                $success = $this->registry->db->query($sql);
                // Remove All prior ACL entries from the modules table
                $sql = "DELETE FROM security_acl_access WHERE groupnum = $groupnum";
                $success = $this->registry->db->query($sql);
                // Add all selected new ACL entries into the modules table
                if(isset($module) && $module != false) {
                    foreach ($module as $m) {
                        $sql = "INSERT INTO security_acl_access (groupnum, modulenum, ruleactive) VALUES ($groupnum, $m, 1)";
                        $success = $this->registry->db->query($sql);
                    } 
                }

                // Remove All prior ACL entries from the functions table
                $sql = "DELETE FROM security_acl_functions_access WHERE groupnum = $groupnum";
                $success = $this->registry->db->query($sql);
                // Add all selected new ACL entries into the functions table
                if (isset($function) && $function != false) {
                    foreach ($function as $f) {
                        $sql = "INSERT INTO security_acl_functions_access (functionnum, groupnum, ruleactive) VALUES ($f, $groupnum, 1)";
                        $success = $this->registry->db->query($sql);
                    }
                }

                if ($success) {
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $groupnum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = $groupnum;
                }
            }
        }
        return $result;
    }


    public function updateModule($modulenum, $data) {
        // First, sanitize the incoming data
        // ***NOT NECESSARY - SANITIZED IN CONTROLLER**** //
        //$data=$this->registry->security->sanitizeArray($data);
        
        // Next, extract it all to individual variables
        extract($data);
        
        // Now, check that all variables needed exist
        if (!isset($controller) || empty ($controller) || !isset($modulename) || empty($modulename)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        
        // Next, check for duplicate entries
        else {
            // Let's make sure that the controller name stays lowercase
            $controller = strtolower($controller);
            
            // You can't set both deny all and allow all.  Deny overrides allow, so we set that here if deny is set
            if ($denyall == 1) { $allowall = 0; }
            
            $exists = $this->checkModuleDups($controller, $modulenum);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }

            // Next, do the update
            else {
                $sql = "UPDATE security_acl_modules SET controller='$controller', modulename='$modulename', allowall=$allowall, denyall=$denyall WHERE moduleindex = $modulenum LIMIT 1";
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $modulenum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = $modulenum;
                }
            }
        }
        return $result;
    }




    public function updateFunction($funcnum, $data) {
         // First, sanitize the incoming data
        // ***NOT NECESSARY - SANITIZED IN CONTROLLER**** //
        //$data=$this->registry->security->sanitizeArray($data);
        
        // Next, extract it all to individual variables
        extract($data);
        
        // Now, check that all variables needed exist
        if (!isset($funcname) || !isset($funcdescr) || !isset($funcnum)) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        else {
            // Let's make sure that the granular function name stays lowercase
            $funcname = strtolower($funcname);

            // Next, check for duplicate entries
            $exists = $this->checkFunctionDups($funcname, $modulenum, $funcnum);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }
            // Next, do the update
            else {
                $sql = "UPDATE security_acl_functions SET modulenum=$modulenum, funcname='$funcname', funcdescr='$funcdescr', functionactive=$functionactive, allowall=$allowall WHERE funcnum = $funcnum LIMIT 1";
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $funcnum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = $funcnum;
                }
            }
        }
        return $result;
    }

    


    /************************************************************************************************/
    /************************************************************************************************/
    /*                       AUTHENTICATION METHODS                                                 */
    /************************************************************************************************/
    /************************************************************************************************/

    
    public function authenticateUser($username, $password) {

        // Set some variables
        $userip = $this->userip;

        // Check 1 - Is the password and username at least 1 character long?
        if (strlen($username) < 1 || strlen($password) <1) {
            if (strlen($username) < 1 & strlen($password) < 1 ) { $txt = "NO_USERNAME_PASSWORD"; } 
            elseif (strlen($username) < 1) { $txt = "NO_USERNAME"; } 
            else {$txt = "NO_PASSWORD"; }
            $this->failAuthentication($txt, 3);
            return false;
        }

        // Check 2 - Clean up the username and if it is supposed to be an email, make sure it is one
        $validuser = $this->validateUsername($username);
        if ($validuser[0] == false) {
                $this->failAuthentication("NOT_VALID_EMAIL", 5, $username);
                return false;
        }


        // Check 3 - Does the user exist?  If so, pull his data
        $cleanuser = $validuser[2];
        //$sql = "SELECT * FROM ".$this->registry->config['logtable']." WHERE username = '$cleanuser' LIMIT 1";
        $sql = "SELECT logins_users.*, security_acl_groups.isactive AS aclgroupactive FROM logins_users LEFT JOIN security_acl_groups ON 
        logins_users.aclgroup = security_acl_groups.groupindex WHERE logins_users.username='$cleanuser' LIMIT 1";
        $userinfo = $this->registry->db->oneRowQuery($sql);
        if ($userinfo==FALSE) { 
            $this->failAuthentication("USER_NOT_EXIST", 3, $cleanuser);
            return false;
        }

        // Check 4 - Check the given password against the DB password
        $pwmatch = $this->verifyPassword($cleanuser, $password);
        if ($pwmatch == false) {
            $usernum = $userinfo['usernum'];
            $this->failAuthentication("PASSWORD_INVALID", 3, $cleanuser);
            return false;
        }

        // Check 5 - Is this User the GOD user?
        $isgod = $this->isGod($userinfo['usernum']);
        
        // Check 6 - Is the user locked out or inactive,
        // Or is his ACL or Deptartment inactive?
        if (!$isgod) {
            if ($userinfo['useractive']==0) { 
                $this->failAuthentication("ACCOUNT_INACTIVE",7, $cleanuser);
                return false;
            }
            if ($userinfo['userlocked']==1) {
                $this->failAuthentication("ACCOUNT_LOCKED",6, $cleanuser);
                return false;
            }
            if ($userinfo['aclgroupactive']==0) {
                $this->failAuthentication("ACL_GROUP_INACTIVE",9, $cleanuser);
                return false;
            }
        }

        // CHECKS PASSED - Authentication successful!
        /* ADD ANY SESSION VARIABLES YOU WANT SET HERE */
        $usernum = $userinfo['usernum'];
        $datetime = date("Y-m-d H:i:s");
        $sql = "UPDATE ".$this->registry->config['logtable']." SET lastlogin = '$datetime', lastloginip = '$userip', pwattempts = 0, showsysbanner = 0 WHERE usernum='$usernum'";
        $this->registry->db->query($sql);
        $sql = "INSERT INTO logins_users_logins (usernum, ipaddress) VALUES ($usernum, '$userip')";
        $this->registry->db->query($sql);
        $_SESSION['userip']=$userip;
        $_SESSION['usertime'] = $datetime;
        $_SESSION['username'] = $userinfo['username'];
        $_SESSION['usernum'] = $usernum;
        $explanation = "LOGIN SUCCESSFUL FOR USER: %%USER%% FROM: %%IP%%";
        $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        if ($isgod) {
            $explanation = "***GOD USER*** STATUS GRANTED TO USER: %%USER%% FROM: %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        }
        // If the user was trying to get to a certain page before log in, send him there now.
        if (isset($_SESSION['requestedurl'])) { 
                $requestedurl = $_SESSION['requestedurl']; 
                unset($_SESSION['requestedurl']);
                $url = $requestedurl;
                header("Location: $url");
                exit();
        }
        // Otherwise, send him to his home page
        $this->registry->goHome();
        return true;
    }


    // This method takes a user name and makes sure it meets all set requirements
    // For a valid username (email/non-email not a blank, etc.)
    // Then, it cleans it up and lowercases it if necessary and also validates it 
    // if it is an email.  It returns a clean username if the process is successful
    // or FALSE with an error code if not successful
    private function validateUsername($user) {
        /* Let's clean up the user name and validate it */
        /* First, If not case-sensitive, lowercase the whole damned thing */
        if ($this->registry->config['logincase']==FALSE) {$user=strtolower($user); }    
        /* Send it to the sanitizer */
        $cleanuser=$this->registry->db->sanitize($user);  
        $result[0] = true;
        $result[1] = "SUCCESS";
        $result[2] = $cleanuser;
        /* If login should be an email, make sure it is! */
        if ($this->registry->config['loginwithemail']==TRUE) {                      
            $cleanuser=$this->registry->db->validateEmail($cleanuser); 
            if ($cleanuser==FALSE) {
                $result[0] = false;
                $result[1] = "NOT_VALID_EMAIL";
                $result[2] = $username;
             }
        }
        return $result;
    }

    private function passwordAttack($username) {
        $userip = $this->userip;
        $sql = "SELECT * FROM ".$this->registry->config['logtable']." WHERE username = '$username' LIMIT 1";
        $userinfo = $this->registry->db->oneRowQuery($sql);
        $usernum = $userinfo['usernum'];
        $pwattempts = $userinfo['pwattempts']+1;
        $userlocked = $userinfo['userlocked'];
        /* Too many log attempts?  Lock out the user */
        if (($this->registry->config['maxpwattempts'] !== FALSE) && ($pwattempts >= $this->registry->config['maxpwattempts']) && $userlocked < 1) { $userlocked=1; $newlock=1;}
        /* Update the database */   
        $sql = "UPDATE ".$this->registry->config['logtable']." SET userlocked='$userlocked', pwattempts = (pwattempts + 1) WHERE usernum='$usernum'";
        $this->registry->db->query($sql);
        // If the user was locked out, we should log that
        if (isset($newlock) && $newlock > 0) {
            $explanation = "USER LOCKED OUT DUE TO MULTIPLE LOGIN FAILURES- USER: ".$username." IP: ".$userip;    
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        }
        elseif ($userlocked == 1) {
            $explanation = "ATTEMPT TO LOG IN TO LOCKED OUT ACCT WITH WRONG PASSWORD- USER: ".$username." IP: ".$userip;
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);    
        }
        return true;
    }

    public function changeUserPassword($username, $oldpass="", $newpass1="", $newpass2="", $tcagree="", $usernum) {
        // Check 1 - Were we given a validly formed username?
        $validuser = $this->validateUsername($username);
        if ($validuser[0] == false) {
            $txt = "INVALID_USERNAME";
            $this->failPasswordChange($txt,99,$username);
        }
        else {
            $username = $validuser[2];
        }

        // Check 2 - Was any field left blank on the change form?
        if (strlen($oldpass) < 1 || strlen($newpass1) < 1 || strlen($newpass2) < 1) {
            $txt = "MISSING_PW_FIELDS";
            $this->failPasswordChange($txt, 12,$username);
            return false;
        }
        
        // Check 3 - Do the new password and verification match each other?
        if ($newpass1 <> $newpass2) {
            $txt = "NEW_PW_MISMATCH";
            $this->failPasswordChange($txt,14,$username);
        }
        
        // Check 4 - Does the new password meet minimum length requirements?
        if (strlen($newpass1) < $this->registry->config['pwlength']) {
            $txt = "PW_LENGTH_FAIL";
            $this->failPasswordChange($txt,13,$username);
        }
        
        // Check 5 - Does the old password match the one in the database?
        $pwmatch = $this->verifyPassword($username, $oldpass);
        if ($pwmatch == false) {
            $txt = "OLD_PASSWORD_FAIL";
            $this->failPasswordChange($txt,11,$username);
        }
        
        // Check 6 - Is the NEW password DIFFERENT from the OLD password?
        if ($oldpass == $newpass1 || $oldpass == $newpass2 || strtolower($oldpass) == strtolower($newpass1) || strtolower($oldpass) == strtolower($newpass2)) {
            $txt = "PW_DIFFERENCE_FAIL";
            $this->failPasswordChange($explanation,16);
        }

        // Check 7 - Did the User Agree to the current T&Cs?
        if ($tcagree == 0) {
            $txt = "NO_TC_AGREE";
            $this->failPasswordChange($txt, 17);
        }
        
        // All Checks Pass.  Let's encrypt the new password and get the new salt
        $newpassinfo = $this->registry->db->encrypt($newpass1);
        $newpassword = $newpassinfo['pw'];
        $newsalt = $newpassinfo['salt'];

        // Let's grab a date for the user agreeing to our T&Cs and update that database table first
        $lasttcagree = date("Y-m-d H:i:s");
        $sql = "UPDATE ".$this->registry->config['logtable']."_details SET tcagree=1, lasttcagree='$lasttcagree' WHERE usernum=$usernum LIMIT 1";
        $this->registry->db->query($sql);

        // Now, we need to calculate a new expiration date for the password based on configuration rules
        if ($this->registry->config['pwexpires'] !== FALSE) {
            $expwords = date("Y-m-d")." + ".$this->registry->config['pwexpires']." days";
            $newexpire = date('Y-m-d', strtotime($expwords));
            $sql = "UPDATE ".$this->registry->config['logtable']." SET password='$newpassword', passthesalt='$newsalt', passexpires='$newexpire' WHERE usernum=$usernum LIMIT 1";
        }
        else {
            $sql = "UPDATE ".$this->registry->config['logtable']." SET password='$newpassword', passthesalt='$newsalt' WHERE usernum=$usernum LIMIT 1";
        }
            
        // Let's update the database and log everything
        if($this->registry->db->query($sql)) {
            $explanation="PASSWORD CHANGE SUCCESSFUL FOR %%USER%% FROM %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_passchgd'],$explanation);
            $explanation="T&CS AGREED TO EXPLICITLY BY %%USER%% FROM %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_passchgd'],$explanation);
            return true;
        }
        else {
            return false;
        }
    }

    private function verifyPassword($username, $password) {
        // First, we pull the user's password and salt from the database
        $sql = "SELECT password,passthesalt FROM ".$this->registry->config['logtable']." WHERE username = '$username' LIMIT 1";
        $oldpassinfo = $this->registry->db->oneRowQuery($sql);
        $dbpass = $oldpassinfo['password'];
        $dbsalt = $oldpassinfo['passthesalt'];
        
        /* We need to encrypt the user-entered password so we can compare it in a second */
        $oldpassenc = $this->registry->db->recrypt($password,$dbsalt);
        
        /* If the old passwords don't match, fail */
        if ($oldpassenc <> $dbpass) {
            return false;
        }
        else {
            return true;
        }
    }

    public function failAuthentication($description=false, $returncode=3, $username="") {
        session_destroy();
        session_start();
        $userip = $this->userip;
        $pwattempt = false;
        switch ($description) {
            case 'XSS_ATTACK':
                $explanation="CROSS-SITE POSTING ATTEMPT FROM USER AT ".$userip;
                break;
            case 'NO_USERNAME':
                $explanation="IMPROPER INPUT ON LOGIN PAGE: USERNAME BLANK - FROM: ".$userip;
                break;
            case 'NO_PASSWORD':
                $explanation="IMPROPER INPUT ON LOGIN PAGE: PASSWORD BLANK - FROM: ".$userip;
                break;
            case 'NO_USERNAME_PASSWORD':
                $explanation="IMPROPER INPUT ON LOGIN PAGE: USERNAME & PASSWORD BLANK - FROM: ".$userip;
                break;
            case 'NOT_VALID_EMAIL':
                $explanation="IMPROPER INPUT ON LOGIN PAGE: USERNAME ".$username." NOT A VALID EMAIL ADDRESS - FROM: ".$userip;
                break;
            case 'USER_NOT_EXIST':
                $explanation="INVALID USERNAME ATTEMPT: ".$username." DOESN'T EXIST - FROM: ".$userip;
                break;
            case 'PASSWORD_INVALID':
                $explanation="INVALID PASSWORD ATTEMPT ON USER: ".$username." - FROM: ".$userip;
                $pwattempt = true;
                break;
            case 'ACCOUNT_INACTIVE':
                 $explanation="ATTEMPT TO LOG IN TO INACTIVE ACCOUNT - USER: ".$username." - FROM: ".$userip;
                 break;
            case 'ACCOUNT_LOCKED':
                 $explanation="ATTEMPT TO LOG IN TO LOCKED OUT ACCT WITH CORRECT PASSWORD - USER: ".$username." - FROM: ".$userip;
                 break;
            case 'DEPT_INACTIVE':
                 $explanation="ATTEMPT TO LOG IN WHILE DEPARTMENT DEACTIVATED - USER: ".$username." - FROM: ".$userip;
                 break;
            case 'ACL_GROUP_INACTIVE':
                 $explanation="ATTEMPT TO LOG IN WHILE ACL GROUP DEACTIVATED - USER: ".$username." - FROM: ".$userip;
                 break;
            default:
                $explanation="UNKNOWN ERROR OCCURRED! USERNAME ".$username." - FROM: ".$userip;
                break;
        }
        $_SESSION['returncode'] = $returncode;
        $this->registry->logging->logEvent($this->registry->config['logging_cat_loginfail'],$explanation);
        if ($pwattempt) { $this->passwordAttack($username); }
        $url = $this->registry->config['url']."/".$this->registry->config['loginurl'];
        header("Location: $url");
        exit();
    }

    private function failPasswordChange($description=false, $returncode=99, $username=false) {
        $userip = $this->userip;
        switch ($description) {
            case 'XSS_ATTACK':
                $explanation="CROSS-SITE POSTING ATTEMPT FROM USER AT ".$userip;
                break;

            case 'INAVLID_USERNAME':
                $explanation="PWCHG ERROR: INVALID USERNAME SUBMITTED- ".$username." FROM: ".$userip;
                break;

            case 'MISSING_PW_FIELDS':
                $explanation="PWCHG ERROR: ONE OR MORE FIELDS LEFT BLANK FOR USER: ".$username." FROM: ".$userip;
                break;

            case 'NEW_PW_MISMATCH':
                $explanation="PWCHG ERROR: NEW PASSWORDS DO NOT MATCH FOR USER: ".$username." FROM: ".$userip;
                break;

            case 'PW_LENGTH_FAIL':
                $explanation="PWCHG ERROR: NEW PASSWORD DOES NOT MEET LENGTH REQUIREMENT- USER: ".$username." FROM: ".$userip;
                break;

            case 'OLD_PASSWORD_FAIL':
                $explanation="PWCHG ERROR: OLD PASSWORD DOES NOT MATCH CURRENT PASSWORD FOR USER: ".$username." FROM: ".$userip;
                break;    

            case 'PW_DIFFERENCE_FAIL':
                $explanation="PWCHG ERROR: OLD PASSWORD & NEW PASSWORD ARE THE SAME FOR USER: ".$username." FROM: ".$userip;
                break;

            default:
                $explanation="PWCHG ERROR: UNKNOWN ERROR OCCURRED! USERNAME ".$username." - FROM: ".$userip;
                break;
        }
        
        if ($returncode==99) {
            session_destroy();
            session_start();
            $this->registry->logging->logEvent($this->registry->config['logging_cat_passcsp'],$explanation);
        }
        if ($returncode == 11) {
            $this->registry->logging->logEvent($this->registry->config['logging_cat_passold'],$explanation);
        }
        $_SESSION['returncode'] = $returncode;
        $url = $this->registry->config['url']."/".$this->registry->config['pwchgurl'];
        header("Location: $url");
        exit();
    }

    // This function simply returns the group number a user belongs to
    public function getUserGroup($usernum=false) {
        if ($usernum==false) {
            return false;
        }
        else {
            $sql = "SELECT aclgroup FROM logins_users WHERE usernum = $usernum LIMIT 1";
            $result = $this->registry->db->oneRowQuery($sql);
            $group = $result['aclgroup'];
            return $group;
        }
        return false;
    }

    // This function checks whether or not a user is the GOD user in the system
    // Returns TRUE if the given user is the GOD user, or FALSE otherwise
    public function isGod($usernum=false) {
        if ($usernum==false) {
            return false;
        }
        else {
           $sql = "SELECT goduser FROM settings_system WHERE settingindex = 1 LIMIT 1";
            $result=$this->registry->db->query($sql);
            if ($result) {
                $row = $this->registry->db->getRow();
                $goduser = $row['goduser'];
                if ($goduser == $usernum) {
                    $isgod = true;
                }
                else {
                    $isgod = false;
                } 
            }
        }
        return $isgod;
    }


    // This functions sends a password reset to the user if the username exists
    public function resetUserPassword($user) {
            $user = strtolower($user);

            // WE DON'T LET INACTIVE USERS RESET THEIR PASSWORDS!
            $sql = "SELECT usernum FROM logins_users WHERE username = '$user' AND useractive = 1 LIMIT 1";
            $result = $this->registry->db->query($sql);
            if ($result) { 
                $row = $this->registry->db->getRow();
                $usernum = $row['usernum'];
                // If the username is not an email, we need to pull that from another table.
                if ($this->registry->config['loginwithemail'] == false) {
                    $sql = "SELECT emailaddress FROM logins_users_details WHERE usernum = $usernum LIMIT 1";
                    $result = $this->registry->db->query($sql);
                    if ($result) {
                        $row = $this->registry->db->getRow();    
                        $user = $row['emailaddress'];
                    }
                    else {
                        return false;
                    }
                }
                $newpassword = $this->randomPassword();
                $mail = new email($this->registry);
                $to = $user;
                $subject = "YOUR PASSWORD RESET REQUEST";
                $body = "Dear user,\n\nAs you requested, your password has been reset.  Please see your new temporary password below:\n\nTEMPORARY PASSWORD: ".$newpassword."\n\nThis password is CASE-SENSITIVE and is only valid for one login, at which\npoint you'll be immediately required to change your password.\n\nIf you did not request this change, please forward this email immediately\nto us at ".$this->registry->config['siteemail']." so we can investigate further for you.\n\nSincerely,\n\nThe System Administration Team\n".$this->registry->config['sitename'];
                
                if ($mail->sendMail($to,false,$subject,$body)) {
                    // Encrypt and salt the new password
                    $newpassinfo = $this->registry->db->encrypt($newpassword);
                    $newpw = $newpassinfo['pw'];
                    $newsalt = $newpassinfo['salt'];
                    // Set the expiration date to yesterday
                    $date = new DateTime();
                    $date->add(DateInterval::createFromDateString('yesterday'));
                    $passexpires = $date->format('Y-m-d');
                    // NEED TO WIPE OUT 2FA IF IT EXISTS SO USER IS FORCED TO RE-2FA.
                    $this->force2FA($usernum);
                    // Update the database
                    $sql = "UPDATE logins_users SET password='$newpw', passthesalt='$newsalt', passexpires='$passexpires', pwattempts = 0 WHERE usernum = $usernum LIMIT 1";
                    $result = $this->registry->db->query($sql);
                    if ($result) {
                        $explanation = "PASSWORD RESET SUCCESSFUL FOR USER: $user FROM: %%IP%%";
                        $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);
                        return true;
                    }
                    else {
                        $explanation = "PASSWORD RESET FAILED AT SEC 1 FOR USER: $user FROM: %%IP%%";
                        $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);   
                        return false;
                    }
                }
        }
        else {
            $explanation = "PASSWORD RESET FAILED AT SEC 3 FOR USER: $user FROM: %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);   
            return false;
        }
    }

    // This function generates a random password of a desired length
    public function randomPassword( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    // This function immediately sets passwords to expired to force a password change.  If usernum
    // is given, affects only the given user.  If no usernum is given, FORCES ALL USERS to change PW
    // This can be used in times of crisis or hacks, etc. or when everyone needs to agree to a new
    // set of t&cs
    public function forcePasswordChange($usernum=0) {
        // Set the expiration date to yesterday
        $date = new DateTime();
        $date->add(DateInterval::createFromDateString('yesterday'));
        $passexpires = $date->format('Y-m-d');
        // Update the database
        if ($usernum==0) {
            $sql = "UPDATE logins_users SET passexpires='$passexpires'";
            $explain = "OF ALL USERS";
        }
        else {
            $sql = "UPDATE logins_users SET passexpires='$passexpires' WHERE usernum = $usernum LIMIT 1";  
            $explain = "OF USER # ".$usernum; 
        }
        $result = $this->registry->db->query($sql);
        if ($result) {
            $explanation = "FORCE PASSWORD CHANGE $explain SET SUCCESSFULLY";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);
            return true;
        }
        else {
            $explanation = "FORCE PASSWORD CHANGE $explain FAILED";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);   
            return false;
        }
    }

    // This function immediately logs out the specified user, or if NO user is specified,
    // immediately logs out ALL users. 
    public function forceLogout($usernum=0) {
        if ($usernum==0) {
            $sql = "UPDATE logins_users SET forcelogout = 1";
            $explain = "OF ALL USERS";
        }
        else {
            $sql = "UPDATE logins_users SET forcelogout = 1 WHERE usernum = $usernum LIMIT 1";
            $explain = "OF USER # ".$usernum;
        }
        $result = $this->registry->db->query($sql);
        if ($result) {
            $explanation = "IMMEDIATE LOG OUT $explain SET SUCCESSFULLY BY USER %%USERNUM%% (%%USER%%) FROM %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);
            return true;
        }
        else {
            $explanation = "IMMEDIATE LOG OUT $explain SETTING FAILED BY USER %%USERNUM%% (%%USER%%) FROM %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);   
            return false;
        }
    }

    // This function immediately revokes all 2FA cookies for the given user, OR if no user is specified, revokes
    // ALL 2FA cookies, forcing the user(s) to re-verfify themselves on next login.
    public function force2FA($usernum=0) {
        if ($usernum==0) {
            $sql = "UPDATE logins_users_tfa_cookies SET isrevoked = 1";
            $explain = "OF ALL USERS";
        }
        else {
            $sql = "UPDATE logins_users_tfa_cookies SET isrevoked = 1 WHERE usernum = $usernum";
            $explain = "OF USER # ".$usernum;
        }
        $result = $this->registry->db->query($sql);
        if ($result) {
            $explanation = "INVALIDATE 2FA SESSIONS $explain SET SUCCESSFULLY";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);
            return true;
        }
        else {
            $explanation = "INVALIDATE 2FA SESSIONS $explain FAILED";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);   
            return false;
        }
    }

    // This function sets every user to either see or not see the System Banner Message
    public function showSystemBanner($show) {
        if ($show) { $showsysbanner = 1; $showword = "ON"; } else { $showsysbanner = 0; $showword = "OFF"; }
        $sql = "UPDATE logins_users SET showsysbanner = ".$showsysbanner;
        $result = $this->registry->db->query($sql);
        if ($result) {
            $explanation = "SET SHOW SYSTEM BANNER TO ".$showword." SET SUCCESSFULLY";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);
            return true;
        }
        else {
            $explanation = "SET SHOW SYSTEM BANNER TO ".$showword." FAILED!";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_userlocked'],$explanation);   
            return false;
        }


    }

    // This function pulls all data about the system administration for use in System Admin Functions
    public function getRecordData() {
        $sql = "SELECT * FROM settings_system LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars) {
            $returnPayload[0] = true;
            $returnPayload[1] = 1;
            $returnPayload[2] = "getRecordData for ".$this->registry->controller." SUCCESS";
            $returnPayload[3] = "";
            $returnPayload[4] = $vars;
        }
        else { 
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "getRecordData for ".$this->registry->controller." FAILURE";
            $returnPayload[3] = "Database retrieval failure. Try again or contact Support.";
            $returnPayload[4] = $vars;
        }
        return $returnPayload;
    }

    // This function updates the system administration settings
    public function updateRecordData($posted, $action=false) {
        // If $action remains false or is not valid, let's error out now.
        if ($action == false || ($action != "UPDATE")) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "updateRecordData for ".$this->registry->controller." called with missing or incorrect ACTION ($action).";
            $returnPayload[3] = "There was a fatal error calling the database.  Please contact support.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }

        // First, extract all the data to individual variables
        // We sanitized it in the controller, so there's no need to do it again.
        // We also need the current date and time
        extract($posted);
        $datetime = date("Y-m-d H:i:s");

        // We need to compare current data to data sent to see what changed
        $sql = "SELECT * FROM settings_system LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars['runstatus'] == $runstatus) { $lsc = ""; } else { $lsc = ", laststatuschange = '".$datetime."'"; }
        if ($forcelogout == 0) { $lfl = ""; } else { $lfl = ", lastforcelogout = '".$datetime."'"; }
        if ($forcepw == 0) { $lpw = ""; } else { $lpw = ", lastpwchange = '".$datetime."'"; }
        if ($force2fa == 0) { $l2F = ""; } else { $l2F = ", lastforce2FA = '".$datetime."'"; }
        if ($showsysbanner == 0) { $ssb = "";  } else { $ssb = ", systembanner = '".$systembanner."'"; }

        // Since there's no module for runtime changes,  let's set what happened for logging
        $explanation = "";
        if ($runstatus == 0) { $statusmeans = "NORMAL RUN "; }
        if ($runstatus == 1) { $statusmeans = "SYS MAINT ADMINS ONLY "; }
        if ($runstatus == 2) { $statusmeans = "SYS MAINT DENY ALL "; }
        if ($runstatus == 3) { $statusmeans = "SYSTEM DOWN "; }
        if ($lsc !== "") { $explanation .= "- RUN STATUS CHANGED TO ".$statusmeans; }
        if ($lfl !== "") { $explanation .= "- FORCED LOGOUT "; }
        if ($lpw !== "") { $explanation .= "- FORCED PASSWORD CHANGE "; }
        if ($l2F !== "") { $explanation .= "- REVOKED 2FA COOKIES "; }
        if ($showsysbanner == 1) { $explanation .= "- SYSTEM BANNER TURNED ON "; }
        if ($showsysbanner == 2) { $explanation .= "- SYSTEM BANNER TURNED OFF "; }

        // Now, we set the SQL code to either add a new record or update the existing record depending upon
        // what was sent
        if ($action == "UPDATE") {
            $sql = "UPDATE settings_system SET runstatus = '$runstatus' $lsc $lfl $lpw $l2F $ssb WHERE settingindex = 1 LIMIT 1";
        }

        // We run the transaction - if the insert/update fails, we send back an error
        if($this->registry->db->query($sql) == false) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "INSERT/UPDATE FAILED: ".$this->registry->controller." - DATABASE ERROR when SQL ran";
            $returnPayload[3] = "FAILED: An error has occurred.  Please contact your system administrator.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }
        

        // That's all there is to it! Let's send back the data and be done!
        $returnPayload[0] = true;
        $returnPayload[1] = 1;
        $returnPayload[2] = "*** SYSTEM WIDE ".$explanation."*** BY %%USER%% FROM %%IP%%";
        $returnPayload[3] = "SYSTEM SETTINGS UPDATED SUCCESFULLY.";
        $returnPayload[4] = $posted;
        return $returnPayload;
    }

    // Pull certain login user records for reporting
    // Returns all rows found if successful or false if not
    public function doLoginSearch($all=false) {
        $today = date("Y-m-d");
        if ($all == false) { $sqlextra = "WHERE loggedintime LIKE '$today%'"; } else { $sqlextra = ""; }
        $sql = "SELECT logins_users_logins.*, logins_users.username, logins_users_details.firstname, logins_users_details.lastname FROM 
            logins_users_logins LEFT JOIN logins_users ON logins_users_logins.usernum = logins_users.usernum LEFT JOIN 
            logins_users_details ON logins_users_logins.usernum = logins_users_details.usernum $sqlextra ORDER BY
            logins_users_logins.loggedintime ASC"; 
        //echo $sql;   
        $result=$this->registry->db->query($sql);
        $result=$this->registry->db->getAllRows();
        if ($result) {
            return $result;
        }
        return false;
    }

}

?>
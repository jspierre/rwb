<?php
use Twilio\Rest\Client;

class mTwilio extends baseModel {

	private $AccountSID;
	private $AccountAuth;


	public function setVars() {
		$this->AccountAuth = $this->registry->config['twilio_AUTH'];
		$this->AccountSID = $this->registry->config['twilio_SID'];
		require_once($this->registry->config['twilio_LOC']);
	}
	
	/* This method pulls the incoming data from Twilio, validates it, then */
	/* sends all of the data regarding the call back to the calling code */
	/* If any part of it fails, it will return false, else an array with the */
	/* call data will be returned */
	public function getCall() {
		if ($this->signatureValidate()) {
			if ($_POST) {
				$postVars = $_POST;
			}
			else {
				return false;
			}
			return $postVars;
		}
		else {
			return false;
		}
	}


	// This module pulls all available phone numbers for use
	// From Twilio and sends them back to the user
	// If there are none or an error occurs, FALSE is sent back
	public function getAvailablePhones($prettynumbers=false) {
		$this->setVars();
		$client = new Services_Twilio($this->AccountSID, $this->AccountAuth);
		$x=0;
		foreach ($client->account->incoming_phone_numbers as $number) {
			$availablePhones[$x]['sid'] = $number->sid;
			$phonenumber = $number->phone_number;
			$availablePhones[$x]['number'] = $phonenumber;
			if (substr($phonenumber,0,1)=="+") {
				if (substr($phonenumber,0,2)=="+1") {
					$newphone=substr($phonenumber,2);
				}
				else {
					$newphone=substr($phonenumber, 1);
				}
			}
			if (strlen($newphone) == 10) {
				if ($this->registry->config['phone_format'] == "PARENTHESIS") {
					$newphone = "(".substr($newphone,0,3).") ".substr($newphone,3,3)."-".substr($newphone,6,4);
				}
				elseif ($this->registry->config['phone_format'] == "DASHES") {
					$newphone = substr($newphone,0,3)."-".substr($newphone,3,3)."-".substr($newphone,6,4);
				}
			}
			$availablePhones[$x]['prettynumber'] = $newphone;
			$availablePhones[$x]['name'] = $number->friendly_name;
			$x++;
		}
		return $availablePhones;
	}


	// This module pushes calling module information onto the Twilio API so that 
	// Deity modules are linked up with certain phone numbers
	public function setPhoneModule($phonesid, $urltomodule) {
		$this->setVars();
		$client = new Services_Twilio($this->AccountSID, $this->AccountAuth);
		$number = $client->account->incoming_phone_numbers->get($phonesid);
		$number->update(array(
			"VoiceUrl" => $urltomodule,
			"VoiceMethod" => "POST",
			"StatusCallback" => $urltomodule,
			"StatusCallbackMethod" => "POST"));
		return true;
	}

	// This module wipes out the Twilio routing data from Twilio so that
	// A phone number no longer routes
	public function removePhoneModule($phonesid) {
		$this->setVars();
		$client = new Services_Twilio($this->AccountSID, $this->AccountAuth);
		$number = $client->account->incoming_phone_numbers->get($phonesid);
		$number->update(array(
			"VoiceUrl" => "",
			"StatusCallback" => ""));
		return true;	
	}


	// This method matches a PHONE SID with an actual phone number and sends back
	// The actual phone number or FALSE if none
	public function getPhoneFromSid($phonesid) {
		$this->setVars();
		$client = new Services_Twilio($this->AccountSID, $this->AccountAuth);
		$numberobject = $client->account->incoming_phone_numbers->get($phonesid);
		$number = $numberobject->phone_number;
		if ($number) {
			return $number;
		}
		return false;
	}

	// This method gets data about a particular call based on its Sid 
	public function getCallData($sid) {
		$this->setVars();
		$client = new Services_Twilio($this->AccountSID, $this->AccountAuth);
		$call = $client->account->calls->get($sid);		
		if ($call) {
			return $call;
		}
		return false;	
	}
	
	// This method does simply sends a single text $message to the phone
	// number given as $phone via Twilio API Call.
	public function sendSMS($phone, $message) {
			$this->setVars();
            $twilio = new Client($this->AccountSID, $this->AccountAuth);
            if (substr($phone, 0,2) != "+1") { $cc = "+1"; } else { $cc = ""; }
            $tonumber = $cc.$phone;
            $fromnumber = $this->registry->config['twilio_DEFAULT_PHONE'];
            $message = $twilio->messages->create($tonumber, array("from" => $fromnumber, "body" => $message));
            return true;
	}

 
	/* This method determines if a request coming from Twilio is actually from */
	/* The Twilio service or somewhere else.  It will return TRUE for a true Twilio */
	/* Request and FALSE for anything else. */
	public function signatureValidate () {
		$this->setVars();
		// Your auth token from twilio.com/user/account
		$authToken = $this->AccountAuth;
		 
		// Request the Validator
		$validator = new Services_Twilio_RequestValidator($authToken);

		// The Twilio request URL. You may be able to retrieve this from 
		// $_SERVER['SCRIPT_URI']
		if (isset($_SERVER['REQUEST_URI'])) { $uri = $_SERVER['REQUEST_URI']; } else { $uri = false; }
		$baseurl = $this->registry->config['basedomain'];
		$url = $baseurl.$uri; 
		 
		// The post variables in the Twilio request. You may be able to use 
		// $postVars = $_POST
		if (isset($_POST)) { $postVars = $_POST; } else { $postVars = false; }
		
		 
		// The X-Twilio-Signature header - in PHP this should be 
		// $_SERVER["HTTP_X_TWILIO_SIGNATURE"];
		if (isset($_SERVER["HTTP_X_TWILIO_SIGNATURE"])) { 
			$signature = $_SERVER["HTTP_X_TWILIO_SIGNATURE"];
		}
		else {
			$signature = false;
		}

		if ($validator->validate($signature, $url, $postVars)) {
		    return true;
		} else {
		    return false;
		}
	}

}
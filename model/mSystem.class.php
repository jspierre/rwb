<?php

class mSystem extends baseModel {

	// This method simply pulls everything from the current system log and sends it back
	public function getRecordData() {
		$filename =  $this->registry->config['logging_location'];
		if (file_exists($filename)) {
			$systemLog = file_get_contents($filename);
			if ($systemLog == false) {
				return false; 
			}
			return $systemLog;
		}
		else {
			return false;
		}
	}

    //This method pulls the system log line by line and greps it for just the user requested
    public function getUserRecordData($usernum = false) {
        if ($usernum == false) { return false; }
        $userinfo = $this->registry->security->getUserName($usernum);
        if ($userinfo == false) { return false; }
        $username = $userinfo['username'];
        $filename =  $this->registry->config['logging_location'];
        if (file_exists($filename)) {
            $file = fopen($filename, "r");
            $userlog = "";
            while(! feof($file)) {
                $line = fgets($file);
                if (strpos($line, $username)) { $userlog .= $line; }
            }
            fclose($file); 
            if ($userlog == "") { return false; }
            else {
                return $userlog;
            }
        }
        else {
            return false;
        }
    }

	// This method copies the log into a date and time named archive and starts a new log
	public function archiveLog() {
        $archiveloc=$this->registry->config['logging_archives'];
        // if archive location does not exist, create it
        if (!file_exists($archiveloc)) {
            mkdir($archiveloc);
        }

		// Sets newname to YYYYMMDDHHMMSS_LOG_ARCHIVE.txt
		$newname = $this->registry->config['logging_archives'].date("YmdHis")."_LOG_ARCHIVE.log";
		$oldname = $this->registry->config['logging_location'];
		if (rename($oldname, $newname)) {
			return true;
		}
		else {
			return false;
		}
	}

	// This method returns all Background Tasks
	public function getAllTasks() {
		$sql = "SELECT * FROM background_tasks ORDER BY modulename ASC";
		$this->registry->db->query($sql);
        $tasks = $this->registry->db->getAllRows();
        return $tasks;
	}

	// This method returns one particular background task
	public function getTask($tasknum) {
		$sql = "SELECT * FROM background_tasks WHERE bgtasknum = $tasknum LIMIT 1";
		$task = $this->registry->db->oneRowQuery($sql);
        return $task;
	}

	// This method inserts a new task into the database
	public function newTask($data) {
        // First, sanitize the incoming data
        $data=$this->registry->security->sanitizeArray($data);
        
        // Next, extract it all to individual variables
        extract($data);
        
        // Now, check that all variables needed exist
        if (!isset($modulename) || empty ($modulename) || !isset($friendlyname) || empty($friendlyname ) || !isset($minutes) || empty ($minutes) ) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        else {
            // Let's make sure that the controller name stays lowercase
            $modulename = strtolower($modulename);
            
            // Next, check for duplicate entries   
            $exists = $this->checkTaskDups($modulename);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }
            // Next, do the insert
            else {
                $lastrun = $this->registry->getDateTime();
                $sql = "INSERT INTO background_tasks (modulename, friendlyname, minutes, isactive, lastrun) VALUES 
                ('$modulename', '$friendlyname', $minutes, $isactive, '$lastrun')";   
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $tasknum = $this->registry->db->getInsertId();
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $tasknum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = false;
                }
            }
        }
        return $result;
    }

 private function checkTaskDups($modulename=false, $bgtasknum=false) {
        if ($modulename) {
            if ($bgtasknum) {
                $sql = "SELECT bgtasknum FROM background_tasks WHERE modulename = '$modulename' AND bgtasknum != $bgtasknum LIMIT 1"; 
            }
            else {
                $sql = "SELECT bgtasknum FROM background_tasks WHERE modulename = '$modulename' LIMIT 1";
            }
            $bgtasknum = $this->registry->db->oneRowQuery($sql);
            if ($bgtasknum) {
                return $bgtasknum['bgtasknum'];
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    // This method simply deletes the given task. Returns true if success, false if not
    public function deleteTask($bgtasknum) {
    	 $sql = "DELETE FROM background_tasks WHERE bgtasknum = $bgtasknum LIMIT 1";
         if ($this->registry->db->query($sql)) { return true; } else { return false; } 
    }

	 public function updateTask($bgtasknum, $data) {
         // First, sanitize the incoming data
        $data=$this->registry->security->sanitizeArray($data);
        
        // Next, extract it all to individual variables
        extract($data);
        
        // Now, check that all variables needed exist
        if (!isset($modulename) || empty ($modulename) || !isset($friendlyname) || empty($friendlyname ) || !isset($minutes) || empty ($minutes) ) {
            $result[0] = false;
            $result[1] = "MISSING_DATA";
            $result[2] = false;
        }
        
        // Next, check for duplicate entries
        else {
            // Let's make sure that the controller name stays lowercase
            $modulename = strtolower($modulename);
            
            
            $exists = $this->checkTaskDups($modulename, $bgtasknum);
            if ($exists) {
                $result[0] = false;
                $result[1] = "DUPLICATE";
                $result[2] = $exists;
            }

            // Next, do the update
            else {
                $sql = "UPDATE background_tasks SET modulename='$modulename', friendlyname='$friendlyname', minutes=$minutes, isactive=$isactive WHERE bgtasknum = $bgtasknum LIMIT 1";
                $success = $this->registry->db->query($sql);
                if ($success) {
                    $result[0] = true;
                    $result[1] = "SUCCESS";
                    $result[2] = $bgtasknum;       
                }
                else {
                    $result[0] = false;
                    $result[1] = "DATABASE_ERROR";
                    $result[2] = $bgtasknum;
                }
            }
        }
        return $result;
    }

}
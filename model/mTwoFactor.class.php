<?php

class mTwoFactor extends baseModel {

	// This method simply checks to see whether a given user has 2FA active
	// or not on their account.  It returns the code from the database, where
	// 0=Inactive, 1=Active, and 2=Pending Activation. Returns FALSE on error.
	// If it returns 2 (Pending), then it goes ahead and checks to see if there 
	// are any inactive codes and, if so, sets the user back to 0 (Inactive)
	public function tfacheck($usernum) {
		$sql="SELECT tfalogin FROM logins_users_details WHERE usernum = $usernum LIMIT 1";
		$result = $this->registry->db->oneRowQuery($sql);
		if ($result) {
			if ($result['tfalogin'] == 2) {
				$this->tfaexpire();
				if ($this->tfaqueue($usernum) == false) {
					$sql = "UPDATE logins_users_details SET tfalogin = 0 WHERE usernum = $usernum LIMIT 1";
					$this->registry->db->query($sql);
					$result['tfalogin']=0;
				}
			}
			return $result['tfalogin'];
		}
		else {
			return false;
		}
	}
	
	// This method simply sets the codes to invalid of all codes that have expired.
	public function tfaexpire() {
		$sql = "UPDATE logins_users_tfa SET isvalid = 0, codestatus = 3 WHERE isvalid = 1 AND expires < CURRENT_TIMESTAMP";
		$this->registry->db->query($sql);
		return true;
	}
	
	// This method sends back TRUE if a user has a valid waiting 2FA Code in queue
	// that is unexpired, and FALSE if the user does not.
	public function tfaqueue($usernum) {
		$sql = "SELECT * FROM logins_users_tfa WHERE isvalid = 1 AND codestatus = 2 AND expires > CURRENT_TIMESTAMP AND usernum = $usernum";
		$result = $this->registry->db->queryGetRows($sql);
		if ($result > 0) { return true; } else { return false; }		
	}
	
	// This method returns a random six-digit number for use during 2FA,
	// Along with encrypted version, salt, and 10-minute expiration date
	// returned as $codes['code'] ['encrypted'] ['salt'] ['expires']
	public function get2FACode() {
		$chars = "123456789";
        $code = substr( str_shuffle( $chars ), 0, 6 );
        $newpassinfo = $this->registry->db->encrypt($code);
        $expwords = date("Y-m-d H:i:s")." + 10 minutes";
        $codes['code'] = $code;
        $codes['encrypted'] = $newpassinfo['pw'];
        $codes['salt'] = $newpassinfo['salt'];
        $codes['expires'] = date('Y-m-d H:i:s', strtotime($expwords));
        return $codes;
	}

	// This method looks for and invalidates any existing codes for a user
	// This should be called whenever obtaining a new code
	public function invalidate2FA($usernum) {
		$sql = "UPDATE logins_users_tfa SET isvalid = 0, codestatus=3 WHERE isvalid = 1 AND usernum = $usernum";
		if ($this->registry->db->query($sql) == false) { return false; }
		return true;
	}

	// If $recheck = false, this method initializes 2FA for the given user and inserts the 
	// Phone and other data in the database, setting their validation to "pending"
	// If $recheck = true, then we're just checking 2FA, not setting it up, and we just
	// generate the code and put it in the tfa codes table
	public function validate2FA($usernum, $phone=false, $recheck=false) {
		if ($this->invalidate2FA($usernum) == false) { return false; }
		// Is this email only?  If so, we need to change some things
		$tfatype = $this->registry->config['twofactor_method'];
		if ($tfatype == 1) {
			$phone = 1;
		}

		// If no phone was given, try to discover it
		if ($phone == false) {
			$sql = "SELECT tfaphone FROM logins_users_details WHERE usernum = $usernum LIMIT 1";
			$result = $this->registry->db->oneRowQuery($sql);
			if ($result == false) { return false; }
			$phone = $result['tfaphone'];
		}
		$codes = $this->get2FACode();
		$code = $codes['code'];
		$newpassword = $codes['encrypted'];
		$newsalt = $codes['salt'];
		$expires = $codes['expires'];
		$sql = "INSERT INTO logins_users_tfa (usernum, phonenumber, code, salt, expires, isvalid, codestatus) VALUES ('$usernum', '$phone', '$newpassword', '$newsalt', '$expires', 1, 2)";
		if ($this->registry->db->query($sql) == false) { return false; }
		if ($recheck == false) {
			if ($tfatype == 1) {
				$sql = "UPDATE logins_users_details SET tfalogin = 2 WHERE usernum = $usernum LIMIT 1";	
			}
			else {
				$sql = "UPDATE logins_users_details SET oktotext = 1, tfalogin = 2, mobilephone = '$phone', tfaphone = '$phone' WHERE usernum = $usernum LIMIT 1";
			}
			if ($this->registry->db->query($sql) == false) { return false; }
		}
		if ($tfatype == 1 || $phone == false) {
			$emailer = new mEmails($this->registry);
			$appname = $this->registry->config['sitename'];
			$subject = "Your Requested Code";
			$body = "The code you requested for ".$appname." is: ".$code.".\n\nDo NOT share this code with anyone - our staff will never directly ask you for this code.";
			$fromname = $appname." Validation";
			if ($emailer->sendSingleEmail($usernum, 0, $subject, $body, false, $fromname)) {
				return true;
			}
			else {
				return false;
			}

		}
		else {
			$message = $this->registry->config['sitename']." Security Code: ".$code.".";
			$twilio = new mTwilio($this->registry);
			$twilio->sendSMS($phone, $message);
		}
		return true;
	}

	/* This method checks the given code against the 2FA database for the user and */
	/* returns true if its a match and false if not.  The method also sorts the valid */
	/* codes, in the odd event there's more than one, in order of most recently issued */
	/* Whether the code matches or not, the database code is invalidated and cannot be used again */
	public function check2FACode($usernum, $code) {
		// Let's get the active code for the user
		$sql = "SELECT * FROM logins_users_tfa WHERE usernum = $usernum AND isvalid = 1 AND codestatus = 2 AND expires > CURRENT_TIMESTAMP ORDER BY expires DESC";
		$result = $this->registry->db->query($sql);
		if ($result == false || $result->num_rows == 0) { return false; }
		$codecheck = $this->registry->db->getRow();
		$hash = $codecheck['code'];
		$salt = $codecheck['salt'];
		$tfnum = $codecheck['tfnum'];
		// Now let's check the hashed db code against what the user entered
		// If it matches, send back true, if it doesn't, false
		$checkhash = $this->registry->db->recrypt($code, $salt);
		if ($hash == $checkhash) { $codestatus=1; } else { $codestatus=0; }
		// Before going on, let's invalidate this code so it can't be used again
		$sql = "UPDATE logins_users_tfa SET isvalid = 0, codestatus = $codestatus, usedate = CURRENT_TIMESTAMP WHERE tfnum = $tfnum LIMIT 1";
		if ($this->registry->db->query($sql) == false) { return false; }
		// Finally we return the final true or false
		if ($codestatus == 1) { return true; } else { return false; }
	}

	// This method simply marks the user as valid and active for 2FA
	public function set2FAValid($usernum) {
		$sql = "UPDATE logins_users_details SET tfalogin = 1, tfaverified = CURRENT_TIMESTAMP WHERE usernum = $usernum LIMIT 1";
		if ($this->registry->db->query($sql)) { return true; } else { return false; }
	}

	// This method pulls the 2FA phone number of a valid 2FA user
	public function get2FAPhone($usernum) {
		$sql = "SELECT tfaphone FROM logins_users_details WHERE usernum = $usernum AND tfalogin = 1 LIMIT 1";
		$result = $this->registry->db->oneRowQuery($sql);
		if ($result) {
			return $result['tfaphone'];
		}
		else {
			return false;
		}
	}

	// This method completely turns off 2FA for the given user
	// Returns true if successful, false if not
	public function reset2FA($usernum) {
		if ($this->invalidate2FA($usernum)) {
			$sql = "UPDATE logins_users_details SET tfalogin = 0, tfaphone= '' WHERE usernum = $usernum LIMIT 1";
			if ($this->registry->db->query($sql)) { return true; } else { return false; }
		}
		else {
			return false;
		}
	}

	// This method checks the user and returns TRUE if the user needs to
	// 2FA Verify NOW, or FALSE if the user does not need to 2FA verify right now
	public function check2FANeeded ($usernum) {
		// Let's get the data about the user first
		$ipaddress = $_SESSION['userip'];
		$sql = "SELECT tfalogin FROM logins_users_details WHERE usernum = $usernum LIMIT 1";
		$result = $this->registry->db->oneRowQuery($sql);
		if ($result == false) { echo "CRITICAL ERROR IN check2FANeeded() IN mTwoFactor - ALERT ADMINISTRATOR!"; exit(); }
		// If the user doesn't have 2FA Active, or if they are in the process of enabling 2FA, they don't need to verify!
		if ($result['tfalogin'] == 0 || $result['tfalogin'] == 2) { $_SESSION['twofactor_pass'] = true; return false; }
		// If the user has 2FA active, but they chose to remember the device and that cookie is present and valid,
		// then no need to verify again
		if(isset($_COOKIE["DeviceCode"])) {
			$devicecode = $_COOKIE['DeviceCode'];
			$sql = "SELECT cookienum FROM logins_users_tfa_cookies WHERE usernum = $usernum AND cookie = '$devicecode' AND ipaddress = '$ipaddress' AND isrevoked = 0 AND dateexpires > CURRENT_TIMESTAMP";
			$validcookie = $this->registry->db->query($sql);
			if ($validcookie == false) { echo "CRITICAL ERROR IN check2FANeeded() IN mTwoFactor - ALERT ADMINISTRATOR!"; exit(); } 
			if ($validcookie->num_rows > 0) {
				$explanation = "USER 2FA ACTIVE BUT HAS VALID 2FA COOKIE SET - NO VERIFY NEEDED: %%USER%% FROM %%IP%%";
				$this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
				$_SESSION['twofactor_pass'] = true;
				return false;
			}
		}
		// Otherise, the user needs to verify themselves, so return true
		$_SESSION['twofactor_pass'] = false;
		return true;
	}

	// The method sets the necessary database entries and sends a cookie to the user's 
	// computer to 'remember' their device
	public function rememberDevice($usernum) {
		$ipaddress = $_SESSION['userip'];
		$expwords = date("Y-m-d H:i:s")." + 30 days";
        $dateexpires = date('Y-m-d H:i:s', strtotime($expwords));
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $cookie = substr( str_shuffle( $chars ), 0, 200 );
        $domain = $_SERVER['HTTP_HOST'];
        $sql = "INSERT INTO logins_users_tfa_cookies (usernum, cookie, ipaddress, dateexpires) VALUES ($usernum, '$cookie', '$ipaddress', '$dateexpires')";
		if ($this->registry->db->query($sql) == false) { return false; }
		setcookie("DeviceCode", $cookie, time()+2592000, "/", $domain, false, true);
		return true;
	}

}
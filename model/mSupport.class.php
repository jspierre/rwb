<?php

class mSupport extends baseModel {


    // Fill this in to keep logs, etc. up to date.
    public $classname = "mSupport";

    public function incomingSMS($request) {
        // First get the data out
        $sid = $request['MessageSid'];
        $from = $request['From'];
        $body = $request['Body'];

        // Next, let's see if a conversation already exists
        if (isset($from) && $from) {
            if (strlen($from) > 10) { $phoneno = substr($from, -10); }
            $sql = "SELECT * FROM support_conversations WHERE userphone = '$phoneno' AND isclosed = 0 LIMIT 1";
            $results = $this->registry->db->oneRowQuery($sql);
            if ($results) {
                $convexists = true;
                extract($results);
            }
            else {
                $convexists = false;
            }
        }
        else {
            $explanation = "FAIL IN mSupport SECTION 25";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false;
        }

        // Next, if we don't know who this belongs to, let's search and see if we can find out
        $theuser=false;
        if($convexists == false || $userstart == "") {
            if (strlen($from) > 10) { 
                $phoneno = substr($from, -10); 
                $sql = "SELECT usernum, firstname, lastname FROM logins_users_details WHERE billingphone = '$phoneno' OR mobilephone = '$phoneno' OR tfaphone = '$phoneno'";
                $possibles = $this->registry->db->query($sql);
                if ($possibles && $possibles->num_rows == 1) {
                    $theuser = $this->registry->db->getRow();
                }
             }
        }

        // Let's log what we know into the Conversations Table
        if ($theuser && (!isset($userstart) || $userstart == "")) { 
            $sqlextra = ", userstart = ".$theuser['usernum']; 
            $userstart = $theuser['usernum']; 
        } 
        elseif (!isset($userstart) || $userstart == "") { 
            $userstart = "NULL";
        }
        if ($convexists) {
            $sql = "UPDATE support_conversations SET lastaction = CURRENT_TIMESTAMP, lastactionparty = 0, userstart = $userstart WHERE convno = $convno LIMIT 1";
            $update = $this->registry->db->query($sql);
            if ($update == false) { 
                $explanation = "FAIL IN mSupport SECTION 55";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                return false; 
            }
        }
        else {
            $sql = "INSERT INTO support_conversations (convtype, userstart, userphone, lastactionparty) VALUES (0, $userstart, '$phoneno', 0)";
        }
        $result = $this->registry->db->query($sql);
        if ($result == false) { 
            $explanation = "FAIL IN mSupport SECTION 65: $sql";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false; 
        }
        else {
            if ($convexists == false) { $convno = $this->registry->db->getInsertId(); }
        }

        // Now, let's put the message into the messages table
        $sql = "INSERT INTO support_messages (convno, messagetype, sendernum, isincoming, messagebody) VALUES ($convno, 0, 
            $userstart, 1, '$body')";
        $result2 = $this->registry->db->query($sql);
        if ($result2 == false) { 
            $explanation = "FAIL IN mSupport SECTION 76";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false; 
        }
         

        // If this is assigned to someone, we send a note only to that person.  Otherwise,
        // we need to pull a list of everyone with support desk permission based on escalated or
        // non-escalated status
        if (!isset($assignedto) || $assignedto == "") {
            if (isset($isescalated) && $isescalated == 1) { $searchfunc = "notifyescalated"; } else { $searchfunc = "notifysupport"; }
            $emailinfo = $this->registry->security->getUserPerFunction($searchfunc);
            if ($emailinfo) {
                foreach ($emailinfo as $ei) {
                    $emails[] = $ei['usernum'];
                }
            }
            else {
            $explanation = "FAIL IN mSupport SECTION 93";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false;
            }
        }
        else {
            $emails = array($assignedto);
        }

        // Now we set up and send the emails
        if ($convexists) { $text1 = "REPLY TICKET ".$convno; $text2 = "reply"; } else { $text1 = "NEW TICKET ".$convno; $text2 = "new"; }
        $subject = "ALERT: $text1 SUPPORT TEXT RECEIVED";
        $emailbody = "Hello from ".$this->registry->config['sitename']."!\n\nA $text2 text message was just received as follows:\n\n";
        $emailbody .= "TICKET NO: ".$convno."\n"; 
        $emailbody .= "FROM: ".$phoneno."\n";
        if ($userstart != "NULL") { 
            $ouruser = $this->registry->security->getUserName($userstart);
            $emailbody .= "USER: ".$ouruser['firstname']." ".$ouruser['lastname']." (User # ".$userstart.")\n";
        }
        else { $emailbody .= "USER: Unknown (unable to match)\n"; }
        $emailbody .= "MESSAGE: ".$body."\n\n";
        $emailbody .= "Please log in and respond through SupportMaster at your earliest convenience.\n\n\nThank you,";
        $emailbody .= "\n\n".$this->registry->config['sitename']."\nWebsite: ".$this->registry->config['url']."\n";
        $errorsend = false;
        $emailsend = new mEmails($this->registry);
        foreach ($emails as $email) {
            $tryer = $emailsend->sendSingleEmail($email, 0, $subject, $emailbody);
            if ($tryer==false) {
                $errorsend = true;
            }
        }
        if ($errorsend) {
            $explanation = "FAIL IN mSupport SECTION 124";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false;
        }
        else {
            return true;
        }

    }
 
 
 
    // This takes an incoming email and parses it, saves it in the databse, and send necessary email notifications
    // Returns true if successful or false if not.
    public function incomingEmail($email) {
        // Now, get some variables from the downloaded email.
        $gotfrom = false;
        $gotsubject = false;
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $email) as $line){
            // This gets the "FROM" Email address
            if (substr($line, 0, 5) == "From:" && $gotfrom == false) {
                preg_match('/\b[^\s]+@[^\s]+/', $line, $match);
                $str = strtolower($match[0]);
                $str = str_replace('<', '', $str);
                $str = str_replace('>', '', $str);
                $from = $str;
                $gotfrom = true;
            }   
    
            // This gets the SUBJECT, which will contain the phone number
            if (strtolower(substr($line, 0, 8)) == "subject:" && $gotsubject == false) {
                $exploded = explode(" ", $line);
                $subject = $exploded[1];
                $gotsubject = true;
            }
        }
    
        // NOW WE LOOK FOR ATTACHMENTS & THE BODY OF THE MESSAGE (IN PLAIN TEXT)
        $downloading = false;
        $mimepart = false;
        $mimecheck = "";
        $downstep = 1;
        $files = array();
        $body = "";
        $x = -1;
        $blanked = false;

        foreach(preg_split("/((\r?\n)|(\r\n?))/", $email) as $line) {
            // This looks for a MIME PART
            // If we already completed downloading that MIME Part, though, we skip it
            if ($mimepart == false) {
                if (substr($line, 0,2) == "--") {
                    $mimepart = true;
                    $mimeid = str_replace("-", "", $line);
                }
            }
            // Okay, we've found a MIME Part we haven't processed yet
            // We are disabling file downloads for security purposes so
	        // Those instructions are being commented out here.
            elseif ($downloading == false) {
	            if (substr($line, 0, 24) == "Content-Type: text/plain") {
		            $downloading = true;
                    $downstep = 25;
	            }
            }
            	        
            // If this mime type is the body of the email, we save that differently
	        elseif ($downstep == 25) {
	            // Skip the blank line
                if ($line == "" && $blanked == false) {
                    $blanked = true;
                    continue;
                }
                elseif ($blanked == false) {
                    continue;
                }
                // Check to see if we've reached the end of the MIME part
                // If so, we break out of the loop.
                if (substr($line, 0,2) == "--") {
                    $mimecheck = str_replace("-", "", $line);   
                    if ($mimecheck == $mimeid) {
                        $downstep = 3;
                        continue;
                    }
                }
                // Save the line to the body contents variable
                $downstep = 25;
                $body .= $line."\n";		
	        }
            // We reached the end of a mime part/file.
            // Clear the variables and get the next file if any
            elseif ($downstep == 3) {
                $mimepart = true;
                $downloading = false;
                $blanked = false;
                $downstep = 1;
                $mimecheck = "";
            }
        
        }
    
        // LET'S SEE IF THIS IS AN EXISTING TICKET THAT IS CLOSED AND NEEDS TO REOPEN
        $reopen = false;
        if (isset($subject) && $subject) {
            $subpcs = explode(" ", $subject);
            if ($subpcs && count($subpcs) > 3) {
                $subpc1 = substr($subpcs[3], 0, -1);
                $subpc2 = $subpcs[3];
                if (is_numeric($subpc2)) { $piece = $subpc2; } else { $piece = $supbc1; }
                if (is_numeric($piece)) {
                    $tickno = $subpcs[2];
                    $sql = "SELECT * FROM support_conversations WHERE convno = $tickno AND isclosed = 1 LIMIT 1";
                    $results = $this->registry->db->oneRowQuery($sql);
                    if ($results) {
                        $convexists = true;
                        $reopen = true;
                        extract($results);
                    }
                    else {
                        $convexists = false;
                    }
                }    
            }
        }

       // NOW, WE HAVE THE FROM, SUBJECT, AND BODY.  WE'RE GOOD TO PROCEED!
       // Next, let's see if a conversation already exists
        if (isset($from) && $from) {
            $sql = "SELECT * FROM support_conversations WHERE useremail = '$from' AND isclosed = 0 LIMIT 1";
            $results = $this->registry->db->oneRowQuery($sql);
            if ($results) {
                $convexists = true;
                extract($results);
            }
            else {
                $convexists = false;
            }
        }
        else {
            $explanation = "FAIL IN mSupport SECTION 25";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false;
        }

        // Next, if we don't know who this belongs to, let's search and see if we can find out
        $theuser=false;
        if($convexists == false || $userstart == "") {
            if (strlen($from) > 10) { 
                $phoneno = substr($from, -10); 
                $sql = "SELECT usernum, firstname, lastname FROM logins_users_details WHERE emailaddress = '$from'";
                $possibles = $this->registry->db->query($sql);
                if ($possibles && $possibles->num_rows == 1) {
                    $theuser = $this->registry->db->getRow();
                }
             }
        }

        // Let's log what we know into the Conversations Table
        if ($theuser && (!isset($userstart) || $userstart == "")) { 
            $sqlextra = ", userstart = ".$theuser['usernum']; 
            $userstart = $theuser['usernum']; 
        } 
        elseif (!isset($userstart) || $userstart == "") { 
            $userstart = "NULL";
        }
        if ($convexists) {
            $sql = "UPDATE support_conversations SET lastaction = CURRENT_TIMESTAMP, lastactionparty = 0, userstart = $userstart, isclosed = 0 WHERE convno = $convno LIMIT 1";
            $update = $this->registry->db->query($sql);
            if ($update == false) { 
                $explanation = "FAIL IN mSupport SECTION 55";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                return false; 
            }
        }
        else {
            $sql = "INSERT INTO support_conversations (convtype, convsubject, userstart, useremail, lastactionparty) VALUES (1, '$subject', $userstart, '$from', 0)";
        }
        $result = $this->registry->db->query($sql);
        if ($result == false) { 
            $explanation = "FAIL IN mSupport SECTION 65: $sql";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false; 
        }
        else {
            if ($convexists == false) { $convno = $this->registry->db->getInsertId(); }
        }

        // This removes that weird =2E error that only appears in K-9 Mail
        // And also deals with the weird curly apostrophe that Windows and iPhones send through
        $body = str_replace("=2E", ".", $body);
        $body = str_replace("=\n", "", $body);
        $body = str_replace("=92", "'", $body);

        // This sanitized the body since it was never done in the receiver_email controller
        $cleanbody = $this->registry->db->sanitize($body);


        // Now, let's put the message into the messages table
        $sql = "INSERT INTO support_messages (convno, messagetype, sendernum, isincoming, messagebody) VALUES ($convno, 1, 
            $userstart, 1, '$cleanbody')";
        $result2 = $this->registry->db->query($sql);
        if ($result2 == false) { 
            $explanation = "FAIL IN mSupport SECTION 76";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false; 
        }
         

        // If this is assigned to someone, we send a note only to that person.  Otherwise,
        // we need to pull a list of everyone with support desk permission based on escalated or
        // non-escalated status
        if (!isset($assignedto) || $assignedto == "") {
            if (isset($isescalated) && $isescalated == 1) { $searchfunc = "notifyescalated"; } else { $searchfunc = "notifysupport"; }
            $emailinfo = $this->registry->security->getUserPerFunction($searchfunc);
            if ($emailinfo) {
                foreach ($emailinfo as $ei) {
                    $emails[] = $ei['usernum'];
                }
            }
            else {
            $explanation = "FAIL IN mSupport SECTION 93";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false;
            }
        }
        else {
            $emails = array($assignedto);
        }

        // Now we set up and send the emails
        if ($convexists) { $text1 = "REPLY TICKET ".$convno; $text2 = "reply"; } else { $text1 = "NEW TICKET ".$convno; $text2 = "new"; }
        $subject = "ALERT: $text1 SUPPORT EMAIL RECEIVED";
        if ($reopen) { $text1 = "TICKET REOPENED BY USER: ".$convno; $text2 = "user just reopened this ticket - a new"; }
        $emailbody = "Hello from ".$this->registry->config['sitename']."!\n\nA $text2 email message was just received as follows:\n\n";
        if ($convexists) { $emailbody .= "TICKET NO: ".$convno."\n"; }
        $emailbody .= "FROM: ".$from."\n";
        if ($userstart != "NULL") { 
            $ouruser = $this->registry->security->getUserName($userstart);
            $emailbody .= "USER: ".$ouruser['firstname']." ".$ouruser['lastname']." (User # ".$userstart.")\n";
        }
        else { $emailbody .= "USER: Unknown (unable to match)\n"; }
        $emailbody .= "MESSAGE: \n---------------------------------------------------------------------\n".$body."\n----------------------------------------------------------------------\n\n";
        $emailbody .= "Please log in and respond through SupportMaster at your earliest convenience.\n\n\nThank you,";
        $emailbody .= "\n\n".$this->registry->config['sitename']."\nWebsite: ".$this->registry->config['url']."\n";
        $errorsend = false;
        $emailsend = new mEmails($this->registry);
        foreach ($emails as $email) {
            $tryer = $emailsend->sendSingleEmail($email, 0, $subject, $emailbody);
            if ($tryer==false) {
                $errorsend = true;
            }
        }
        if ($errorsend) {
            $explanation = "FAIL IN mSupport SECTION 124";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            return false;
        }
        else {
            return true;
        }         

    }



    // This function gets ONE specific record
    public function getRecordData($record) {
        $sql = "SELECT support_conversations.*, logins_users_details.* FROM support_conversations LEFT JOIN 
            logins_users_details ON support_conversations.userstart = logins_users_details.usernum WHERE 
            convno = $record LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars) {
            $returnPayload[0] = true;
            $returnPayload[1] = 1;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record SUCCESS";
            $returnPayload[3] = "";
            $returnPayload[4] = $vars;
        }
        else { 
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record FAILURE";
            $returnPayload[3] = "Database retrieval failure. Try again or contact Support.";
            $returnPayload[4] = $vars;
        }
        return $returnPayload;
    }

    // This function gets every message associated with a conversation and returns it
    // As an array of one message per row.
    public function getMessages($convno) {
        $sql = "SELECT * FROM support_messages WHERE convno = $convno ORDER BY messagetime ASC";
        $result = $this->registry->db->query($sql);
        if ($result) {
            $messages = $this->registry->db->getAllRows();
            return $messages;
        }
        else {
            return false;
        }
    }


    // $posted is every field needed to update the necessary record
    // $action can be "ADD" or "UPDATE".  Anything else results in error
    public function updateRecordData($posted, $action=false) {
        

        // If $action remains false or is not valid, let's error out now.
        if ($action == false || ($action != "ADD" && $action != "UPDATE" && $action !="RESPOND" && $action != "NOTATE")) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "updateRecordData for ".$this->classname." called with missing or incorrect ACTION ($action).";
            $returnPayload[3] = "There was a fatal error calling the database.  Please contact support.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }

        // First, extract all the data to individual variables
        // We sanitized it in the controller, so there's no need to do it again.
        extract($posted);
        $error = 0;

        // If a response if blank, that's an error, too
        if (($action == "RESPOND" || $action == "NOTATE") && trim($messagebody) == "") {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "updateRecordData for ".$this->classname." called blank response field.";
            $returnPayload[3] = "SEND FAILED: You must write a message in the Message area below.";
            $returnPayload[4] = $posted;
            return $returnPayload; 
        }


         // Set any other variables or final touches (default or extra db values, etc) here
        $enteredon = $this->registry->getDateTime();
        $usernum = $_SESSION['usernum'];
        $curruser = $this->registry->security->getUserName($usernum);



        // Now do any data validation needed that can be done without a DB call.  For example, cleaning up
        // and verifying email addresses, etc.  e.g.,:
        if ($action != "RESPOND" && $action != "NOTATE") {
            if ($wasassigned == "") { $wasassigned = "NULL"; }
            if (($isclosed <> $wasclosed) && ($isescalated <> $wasescalated) && $isclosed == 1) { $isescalated = $wasescalated; }
            if (($isclosed <> $wasclosed) && ($assignedto <> $wasassigned) && $isclosed == 1) { $assignedto = $wasassigned; }
            if ($isescalated <> $wasescalated) { $assignedto = "NULL"; }
        }
       

        // Now, we set the SQL code to either add a new record or update the existing record depending upon
        // what was sent
        if ($action == "RESPOND") {
            $sql = "INSERT INTO support_messages (convno, messagetype, sendernum, isincoming, messagebody) VALUES
                ($record, $convtype, $usernum, 0, '$messagebody'); UPDATE support_conversations SET lastactionparty = 1 
                WHERE convno = $record LIMIT 1;";
        }
        elseif ($action == "NOTATE") {
            $sql = "INSERT INTO support_messages (convno, messagetype, sendernum, isincoming, internalnotes) VALUES
                ($record, 2, $usernum, 0, '$messagebody');";
        }
        elseif ($action == "UPDATE") {
            $sql = "UPDATE support_conversations SET convsubject = '$convsubject', assignedto = $assignedto, isescalated = $isescalated,
                isclosed = $isclosed WHERE convno = $record LIMIT 1;";
        }

        // We run the transaction - if the insert/update fails, we send back an error
        if($this->registry->db->multiQuery($sql) == false) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "INSERT/UPDATE FAILED: ".$this->classname." - DATABASE ERROR when SQL ran";
            $returnPayload[3] = "FAILED: An error has occurred.  Please contact your system administrator.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }
        
        // If this was an ADD we pull the new record number to give back to the system
        if ($action == "RESPOND" || $action == "NOTATE") { 
            $newmsg = $this->registry->db->getInsertId(); 
            $sql = "SELECT messagebody FROM support_messages WHERE messageno = $newmsg LIMIT 1";
            $cleanmsg = $this->registry->db->oneRowQuery($sql);
            $messagebody = $cleanmsg['messagebody'];
        }

        // Let's set some variables we may need next
        $emailenduser = false;
        $emailass = false;
        $emailgroup = false;
        $logmessage = false;
        $changed = false;
        $emails = false;

        // If we responded, we need to send the actual message
        // If this was not a response, let's notify anyone needed of the changes we made
        if ($action == "RESPOND") {
            $convinfo = $this->getRecordData($record);
            $userphone = $convinfo[4]['userphone'];
            $useremail = $convinfo[4]['useremail'];
            $messagebody = trim($messagebody); 
            if ($convtype == 0 && $messagebody != "") {
                if (strlen($messagebody) > 160) { $messagebody = substr($messagebody, 0, 160); }
                $message = $this->registry->security->unSanitize($messagebody);
                $twilio = new mTwilio($this->registry);
                $twilio->sendSMS($userphone, $message);
            }
            elseif ($convtype == 1 && $messagebody != "") {
                $subject = "RE: Ticket # ".$record." - ".$convinfo[4]['convsubject'];
                $emailbody = "Hello from ".$this->registry->config['sitename']."! The following reply was just sent to you on the subject support ticket:";
                $emailbody .= "\n\n----------------------------------------------------------------------\n\n";
                $emailbody .= $messagebody;
                $emailbody .= "\n\n----------------------------------------------------------------------\n\n";
                $emailbody .= "If you would like to respond to this ticket at any time, you can simply reply to this email.\n\n\nThank you,";
                $emailbody .= "\n\n".$this->registry->config['sitename']."\nPhone: ".$this->registry->config['sitephone']."\nWebsite: ".$this->registry->config['url']."\n";
                $emailsend = new email($this->registry);
                $fromname = $this->registry->config['sitename'];
                $emailfrom = $this->registry->config['siteemail'];
                if (!$emailsend->sendMail($useremail,false,$subject,$emailbody,$emailfrom,$fromname)) {
                    $explanation = "FAIL IN mSupport SECTION 582";
                    $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                }
            }
        }
        elseif ($action != "NOTATE") {
            if ($wasassigned == "") {$wasassigned = "NULL";}
            if ($wasassigned == "NULL") {$prior = "--Not Previously Assigned--";}
            else {
                $p = $this->registry->security->getUserName($wasassigned);
                $prior = $p['firstname']." ".$p['lastname'];
            }

            // If the ticket changed status, we need to let the right parties know
            if ($wasclosed <> $isclosed ) {
                $changed = true;
                if ($isclosed==1) { $sub = "Closed"; $opening = "The following support ticket has been closed:"; $emailenduser = true; $logmessage = "CLOSED TICKET";}
                else { $sub = "Re-Opened"; $opening = "The following support ticket has been re-opened:"; $emailenduser = false; $logmesage = "RE-OPENED TICKET";}
            }
            elseif ($wasescalated <> $isescalated) { 
                $changed = true;
                if ($isescalated == 1) { $sub = "Escalated"; $opening = "The following support ticket has been escalated to management for further handling:"; $emailenduser = true; $emailass = true; $logmessage = "ESCALATED TICKET"; }
                else { $sub = "De-Escalated"; $opening = "The following support ticket was de-escalated for handling by first-level support staff:"; $emailenduser = false; $emailass = true; $logmessage = "DE-ESCALATED TICKET";}
            }
            elseif ($wasassigned <> $assignedto) {
                $changed = true;
                if ($assignedto == "NULL") { $sub = "Unassigned - Handling Required"; $opening = "The following support ticket was unassigned and is now back in the open queue for handling:"; $emailenduser = false; $emailass = true; $logmessage = "REMOVED TICKET ASSIGNMENT"; }
                else {
                    $a = $this->registry->security->getUserName($assignedto);
                    $sub = "Assigned to You"; $opening = "The following support ticket has been assigned to you for handling:"; $emailenduser = false; $logmessage = "ASSIGNED TICKET TO USER #".$assignedto." (".$a['firstname']." ".$a['lastname'].")"; 
                }
            }
            if ($usernum <> $assignedto && $assignedto != "NULL" && $changed) { $emailass = true; }
            if ($emailass == true && $assignedto == "NULL" && $changed) { $emailass = false; $emailgroup = true; }

            // Now, let's set up all the emails we need to send
            if ($emailenduser || $emailass || $emailgroup) {
                if (isset($isescalated) && $isescalated == 1) { $searchfunc = "notifyescalated"; } else { $searchfunc = "notifysupport"; }
                if ($emailgroup == true) {
                    $emailinfo = $this->registry->security->getUserPerFunction($searchfunc);
                    if ($emailinfo) {
                        foreach ($emailinfo as $ei) {
                            $emails[] = $ei['usernum'];
                        }
                    }
                    else {
                        $explanation = "FAIL IN mSupport SECTION 557";
                        $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    } 
                }
                if ($emailass == true) {
                    $emails[] = $assignedto;
                }               
                $youruser = $this->registry->security->getUserName($usernum);
                if ($convsubject == "") {$cs = "No Subject Set"; } else { $cs = $convsubject; }
                $subject = "ALERT: Support Ticket # ".$record." ".$sub;
                $emailbody = "Hello from ".$this->registry->config['sitename']."! ".$opening."\n\n";
                $emailbody .= "TICKET NO: ".$record."\n"; 
                if ($userstart != "NULL") { 
                    $ouruser = $this->registry->security->getUserName($userstart);    
                    $emailbody .= "FROM USER: ".$ouruser['firstname']." ".$ouruser['lastname']." (User # ".$userstart.")\n";
                }
                else { $emailbody .= "FROM USER: Unknown (unable to match)\n"; }
                $emailbody .= "SUBJECT: ".$cs."\n";
                $emailbody .= "CHANGED BY: ".$youruser['firstname']." ".$youruser['lastname']."\n";
                $emailbody .= "PRIOR ASIGNEE: ".$prior."\n\n";
                $emailbody .= "Please log in and respond through SupportMaster at your earliest convenience.\n\n\nThank you,";
                $emailbody .= "\n\n".$this->registry->config['sitename']."\nWebsite: ".$this->registry->config['url']."\n";
                $errorsend = false;
                $emailsend = new mEmails($this->registry);
                // Send to staff first if needed
                if ($emails) {
                    foreach ($emails as $email) {
                        $tryer = $emailsend->sendSingleEmail($email, 0, $subject, $emailbody);
                        if ($tryer==false) { $errorsend = true; }
                    }
                    if ($errorsend) {
                        $explanation = "FAIL IN mSupport SECTION 587";
                        $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    }
                }
                // Send to end user if needed
                if ($emailenduser) {
                    $emailbody = "Hello from ".$this->registry->config['sitename']."! ".$opening."\n\n";
                    $emailbody .= "TICKET NO: ".$record."\n"; 
                    $emailbody .= "SUBJECT: ".$cs."\n\n";
                    $emailbody .= "This is an informational email - no action is required on your part. Please do not respond to this email as it will be bounced back to you. If you want to respond to this ticket, please send an email to ".$this->registry->config['siteemail']."\n\n\nThank you,";
                    $emailbody .= "\n\n".$this->registry->config['sitename']."\nWebsite: ".$this->registry->config['url']."\n";
                    $email = $userstart;
                    $tryer = $emailsend->sendSingleEmail($email, 0, $subject, $emailbody);
                    if ($tryer==false) { $errorsend = true; }   
                    if ($errorsend) {
                       $explanation = "FAIL IN mSupport SECTION 601";
                        $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    }
                }

            }
        }

        // Insert Support Log Entries if any
        if ($logmessage) {
            if ($this->doLogEntry($record, $isescalated, $usernum, $logmessage)) {
                $errorlog = false;
            }
            else {
                $errorlog = true; 
            }
            if ($errorlog) {
                $explanation = "FAIL IN mSupport SECTION 625";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);   
            }
        } 
        
        // Did we change the subject?
        if (($action != "RESPOND" && $action !="NOTATE") && ($convsubject != $wassubject))  {
            $logmessage = "SUBJECT CHANGED FROM '".$wassubject."'";
            $logmessage = $this->registry->db->sanitize($logmessage);
            if (!$this->doLogEntry($record, $isescalated, $usernum, $logmessage)) {
                $explanation = "FAIL IN mSupport SECTION 634";
                $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);   
            }
        }
            

        // That's all there is to it! Let's send back the data and be done!
        $returnPayload[0] = true;
        $returnPayload[1] = 1;
        $returnPayload[2] = "$action RECORD completed successfully ".$this->classname." - RECORD # $record";
        $returnPayload[3] = "$action completed successfully (Record# $record) - please check the data below.";
        $returnPayload[4] = $posted;
        return $returnPayload;
    }



    // This method inserts entries into the support log table for the ticket in question
    public function doLogEntry($convno, $isescalated, $loguser, $logmessage) {
        $sql = "INSERT INTO support_log (convno, isescalated, loguser, logmessage) VALUES ($convno, $isescalated, $loguser, '$logmessage')";
        $result = $this->registry->db->query($sql);
        if ($result) { return true; } else { return false; }
    }

    // This method cleanly deletes an entire support record - very careful with this one!
    public function deleteRecord($convno) {
        $loguser = $_SESSION['usernum'];
        $logmessage = "DELETED TICKET # ".$convno;
        $sql = "DELETE FROM support_log WHERE convno = $convno; DELETE FROM support_messages WHERE convno = $convno; DELETE 
            FROM support_conversations WHERE convno = $convno LIMIT 1; INSERT INTO support_log (loguser, logmessage) 
            VALUES ($loguser, '$logmessage');";
        $result = $this->registry->db->multiQuery($sql);
        if ($result) { return true; } else { return false; }
    }

    // This method associates the given user's profile with the given support ticket.  Also,
    // If the user doesn't have a mobile phone set, it will set it and turn on permissions
    public function associateUser($convno, $usernum) {
        $sql = "SELECT convtype, userphone FROM support_conversations WHERE convno = $convno LIMIT 1";
        $ct = $this->registry->db->oneRowQuery($sql);
        if ($ct && $ct['convtype'] == 0) {
            $sql = "SELECT mobilephone from logins_users_details WHERE usernum = $usernum LIMIT 1";
            $phoneinfo = $this->registry->db->oneRowQuery($sql);
            if ($phoneinfo && $phoneinfo['mobilephone']== "") {
                $mobilephone = $ct['userphone'];
                $sql = "UPDATE logins_users_details set mobilephone = '$mobilephone', oktotext = 1 WHERE usernum = $usernum LIMIT 1";
                $phoneupdate = $this->registry->db->query($sql);
            }
        }
        $sql = "UPDATE support_conversations SET userstart = $usernum WHERE convno = $convno LIMIT 1";
        $result = $this->registry->db->query($sql);
        if ($result) { return true; } else { return false; }
    }

    // This method simply counts the number of open and closed tickets associated with the given user
    // Returns number of tickets as an array (open, closed, total).
    public function getNumTickets($usernum) {
        $sql = "SELECT COUNT(convno) AS numtickets FROM support_conversations WHERE userstart = $usernum AND isclosed = 0";
        $result1 = $this->registry->db->oneRowQuery($sql);
        $sql = "SELECT COUNT(convno) AS numtickets FROM support_conversations WHERE userstart = $usernum AND isclosed = 1";
        $result2 = $this->registry->db->oneRowQuery($sql);
        if ($result1) { $a = $result1['numtickets']; } else { $a = 0; }
        if ($result2) { $b = $result2['numtickets']; } else { $b = 0; }
        $count = array($a, $b, ($a+$b));
        return $count;
    }

    // This method creates a brand new ticket based on staff-entered data 
    // And sends emails as appropriate.
    public function createNewTicket($data) {
        $usernum = $_SESSION['usernum'];
        extract($data);
        $userdata = $this->registry->security->getUserName($loginnum);
        if ($usernum == $loginnum) { $ownticket = true; } else { $ownticket = false; }
        $useremail = $userdata['emailaddress'];
        $sql = "INSERT INTO support_conversations (convtype, convsubject, userstart, useremail) VALUES
            (1, '$convsubject', $loginnum, '$useremail')";
        $result = $this->registry->db->query($sql);
        if (!$result) { $_SESSION['returncode'] = 5; return false; }
        $convno = $this->registry->db->getInsertId();
        $creator = $this->registry->security->getUserName($usernum);
        if ($ownticket) {
            $newbody = $messagebody;
        }
        else {
            $newbody = "######################################################################\n";
            $newbody .= "NOTE: THIS TICKET WAS CREATED ON USER BEHALF BY \n".$creator['firstname']." ".$creator['lastname']. " (USER # ".$usernum.")\n";
            $newbody .= "######################################################################\n\n";
            $newbody .= $messagebody; 
        }
        $sql = "INSERT INTO support_messages (convno, messagetype, sendernum, messagebody) VALUES
            ($convno, 1, $loginnum, '$newbody')";
        $result = $this->registry->db->query($sql);
        if (!$result) { 
            $sql = "DELETE FROM support_conversations WHERE convno = $convno LIMIT 1";
            $result = $this->registry->db->query($sql);
            $_SESSION['returncode'] = 5; 
            return false; 
        }
        $sql = "SELECT support_conversations.*, support_messages.* FROM support_conversations LEFT JOIN support_messages ON
            support_conversations.convno = support_messages.convno WHERE support_conversations.convno = $convno LIMIT 1";
        $cleanresult = $this->registry->db->oneRowQuery($sql);
        if (!$cleanresult) { $_SESSION['returncode'] = 5; return false; }
        $cleansubject = $cleanresult['convsubject'];
        $cleanmessage = $cleanresult['messagebody'];

        // Now send email to the end user
        $subject = "TICKET # ".$convno.": ".$cleansubject. " - opened on your behalf";
        $emailbody = "Hello from ".$this->registry->config['sitename']."! The following support ticket was opened on your behalf:\n\n";
        $emailbody .= "TICKET NO: ".$convno."\n"; 
        $emailbody .= "SUBJECT: ".$cleansubject."\n\n";
        $emailbody .= "We will get back to you soon! You can simply reply to this email if you have additional questions or informations related to this ticket.\n\n\nThank you,";
        $emailbody .= "\n\n".$this->registry->config['sitename']."\nWebsite: ".$this->registry->config['url']."\n";
        $email = $loginnum;
        $emailsend = new email($this->registry);
        $fromname = $this->registry->config['sitename']; 
        $emailfrom = $this->registry->config['siteemail'];
        if (!$emailsend->sendMail($useremail,false,$subject,$emailbody,$emailfrom,$fromname)) {
            $explanation = "FAIL IN mSupport SECTION 601";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
        }

        // Now we set up and send the staff emails
        $emailinfo = $this->registry->security->getUserPerFunction("notifysupport");
        if ($emailinfo) {
            foreach ($emailinfo as $ei) {
                $emails[] = $ei['usernum'];
            }
        }
        else { 
            $explanation = "FAIL IN mSupport SECTION 846";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            $_SESSION['returncode'] = 5; 
            return false; 
        }
        $text1 = "NEW TICKET ".$convno; $text2 = "new"; 
        $subject = "ALERT: $text1 CREATED";
        $emailbody = "Hello from ".$this->registry->config['sitename']."!\n\nA new support ticket was just created internally as follows:\n\n";
        $emailbody .= "TICKET NO: ".$convno."\n"; 
        if ($ownticket) {
            $emailbody .= "SUBMITTED BY USER: ".$userdata['firstname']." ".$userdata['lastname']." (User # ".$loginnum.")\n";    
        }
        else {
            $emailbody .= "ON BEHALF OF USER: ".$userdata['firstname']." ".$userdata['lastname']." (User # ".$loginnum.")\n";    
        }
        $emailbody .= "SUBJECT: ".$cleansubject."\n\n";
        $emailbody .= "MESSAGE: \n---------------------------------------------------------------------\n".$cleanmessage."\n----------------------------------------------------------------------\n\n";
        $emailbody .= "Please log in and respond through SupportMaster at your earliest convenience.\n\n\nThank you,";
        $emailbody .= "\n\n".$this->registry->config['sitename']."\nWebsite: ".$this->registry->config['url']."\n";
        $errorsend = false;
        $emailsend = new mEmails($this->registry);
        foreach ($emails as $email) {
            $tryer = $emailsend->sendSingleEmail($email, 0, $subject, $emailbody);
            if ($tryer==false) {
                $errorsend = true;
            }
        }
        if ($errorsend) {
            $explanation = "FAIL IN mSupport SECTION 874";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
        }

        // Now log the new ticket
        $record = $convno; $isescalated = 0;
        if ($ownticket) { $logmessage = "SELF-CREATED SUPPORT TICKET"; } else { $logmessage = "OPENED TICKET ON USER BEHALF"; }
        if ($this->doLogEntry($record, $isescalated, $usernum, $logmessage)) {
            $errorlog = false;
        }
        else {
            $errorlog = true; 
        }
        if ($errorlog) {
            $explanation = "FAIL IN mSupport SECTION 625";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);   
        }

         $_SESSION['returncode'] = 2; 
         return true;

    }

    /*  ********************************************************************** */
    /*  ***************** SEARCH METHOD FOR SEARCH CONTROLLER **************** */
    /*  ********************************************************************** */
    // You will need to adjust your SQL here to fit your needs for the search
    // You would also add other wiring to do different limiters or types of search
    public function doSearch($searchterms, $active=0, $searchall=0) {
        $terms = explode(" ", $searchterms);
        $numterms = count($terms);
        $x=0;
        $sql = "SELECT support_conversations.*, logins_users_details.* FROM support_conversations LEFT JOIN
            logins_users_details ON support_conversations.userstart = logins_users_details.usernum WHERE ";
         if ($active==1) {
            $sql .= "support_conversations.isclosed = 0 AND ";
        }
        elseif ($active==2) {
            $sql .= "support_conversations.isclosed = 1 AND ";
        }
        foreach ($terms as $term) {
            $x++;
            if ($searchall==1) {
                $sql .= "(convsubject LIKE '%$term%' OR convno LIKE '%$term%' OR useremail LIKE '%$term%' OR userphone LIKE 
                    '%$term%' OR firstname LIKE '%$term%' OR lastname LIKE '%$term%')";
            }
            elseif ($searchall==2) {
                $sql .= "(convsubject LIKE '%$term%' OR convno LIKE '%$term%' OR useremail LIKE '%$term%' OR userphone LIKE 
                    '%$term%' OR firstname LIKE '%$term%' OR lastname LIKE '%$term%') AND userstart IS NULL";
            }
            elseif ($searchall==3) {
                $sql .= "(convsubject LIKE '%$term%' OR convno LIKE '%$term%' OR useremail LIKE '%$term%' OR userphone LIKE 
                    '%$term%' OR firstname LIKE '%$term%' OR lastname LIKE '%$term%') AND assignedto IS NULL";
            }
            elseif ($searchall==5) {
                $sql .= "userstart = $term";
            }
            else {
                $sql .= "(convno LIKE '%$term%')";                
            }
            if ($x!==$numterms) {
                $sql .= " AND ";
            }
        }
        /*  **********************************************************************************
            Add additional code here to limit the search further or perform other cool things 
            **********************************************************************************
        */
        if ($searchall == 5) {
            $sql.=" ORDER BY isclosed ASC, lastaction ASC";    
        }
        else {
            $sql.=" ORDER BY lastaction ASC";
        }
        
        //echo $sql;
        //die();
        $result=$this->registry->db->query($sql);
        if ($result) {
            return $result;
        }
        return false;
    }




}


?>
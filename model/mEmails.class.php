<?php

// Remove this before using.
   /*
    Payload Return (USE FOR ALL non-"list-only" returns IN THE MODEL!!)
            $returnPayload[0]   BOOL - was the operation successful? (true/false)
            $returnPayload[1]   INT - Level of severity (0=None [default], 1=Success, 2=Info, 3=Warning, 4=Danger, 5=BLACK)
            $returnPayload[2]   STRING - a short description of the success or failure
            $returnPayload[3]   STRING- The message the user will see in the alert box if shown
            $returnPayload[4]   ARRAY - All of the data that came from the VIEW on the FORM, so it can 
                                be redisplayed along with the error at top OR the data from the database
                                of those same fields for display
    */



class mEmails extends baseModel {

    // Fill this in to keep logs, etc. up to date.
    public $classname = "mEmails";



    public function getRecordData($record) {
        $sql = "SELECT emails_system.*, security_acl_groups.groupname, logins_users_details.emailaddress, 
                logins_users_details.firstname, logins_users_details.lastname FROM emails_system LEFT JOIN 
                security_acl_groups ON emails_system.groupnum = security_acl_groups.groupindex LEFT JOIN 
                logins_users_details on emails_system.usernum = logins_users_details.usernum WHERE 
                mailnum = $record LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars) {
            $returnPayload[0] = true;
            $returnPayload[1] = 1;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record SUCCESS";
            $returnPayload[3] = "";
            $returnPayload[4] = $vars;
        }
        else { 
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "getRecordData for ".$this->classname." $record FAILURE";
            $returnPayload[3] = "Database retrieval failure. Try again or contact Support.";
            $returnPayload[4] = $vars;
        }
        return $returnPayload;
    }




    // $posted is every field needed to update the necessary record
    // $action can be "ADD" or "UPDATE".  Anything else results in error
    public function updateRecordData($posted, $action=false) {

        // If $action remains false or is not valid, let's error out now.
        if ($action == false || ($action != "ADD" && $action != "UPDATE" && $action !== "TERMINATE")) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "updateRecordData for ".$this->classname." called with missing or incorrect ACTION ($action).";
            $returnPayload[3] = "There was a fatal error calling the database.  Please contact support.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }

        // First, we extract the data to its individual variables
        //$clean = $this->registry->security->sanitizeArray($posted, false);
        extract($posted);

        // Who is the current user?
        $createdby = $_SESSION['usernum'];

        // Let's get the date set correctly
        if (isset($sendnow) && $sendnow) {
            $sendnow = 1;
            $sendtime=$this->registry->getDateTime();
        }
        elseif (isset($sendtime)) {
            $sendnow = 0;
            $y=substr($sendtime, -4);
            $m=substr($sendtime, 0, 2);
            $d=substr($sendtime, 3, 2);
            $ctime = $this->registry->config['systememailtime'];
            if (strlen($ctime) < 5 || substr($ctime,2,1) !== ":") { $ctime = "01:00"; }
            if (substr($ctime,0,2) < 0 || substr($ctime, 0,2) > 24) { $ctime = "01:00"; }
            if (substr($ctime,3,2) < 0 || substr($ctime, 0,2) > 59) { $ctime = "01:00"; }
            $sendtime = $y."-".$m."-".$d." ".$ctime.":00";
        }

        // Now, we set the SQL code to either add a new record or update the existing record depending upon
        // what was sent
        if ($action == "ADD") {
            $sql = "INSERT INTO emails_system (createdby, groupnum, sendtime, subjectline, emailbody, 
            optinonly, sendfrom, emailstatus, ccadmins, internalnotes) VALUES 
            ($createdby, $groupnum, '$sendtime', '$subjectline', '$emailbody', $optinonly, $sendfrom, 1,
             $ccadmins, '$internalnotes')";
        }
        elseif ($action == "UPDATE") {
            $sql = "UPDATE emails_system SET createdby = $createdby, groupnum = $groupnum, sendtime = '$sendtime', 
            subjectline = '$subjectline', emailbody = '$emailbody', optinonly = $optinonly,
            sendfrom = $sendfrom, ccadmins = $ccadmins, internalnotes = '$internalnotes'
            WHERE mailnum = $record LIMIT 1";
            $posted['lastchange'] = $this->registry->getDateTime();
            $posted['sendtime'] = $sendtime;
        }
        elseif ($action == "TERMINATE") {
            $sql = "UPDATE emails_system SET emailstatus = 3 WHERE mailnum = $record LIMIT 1"; 
            $this->emptyQueue($record);            
            $posted['emailstatus'] = 3;
        }


        // We run the transaction - if the insert/update fails, we send back an error
        if($this->registry->db->query($sql) == false) {
            $returnPayload[0] = false;
            $returnPayload[1] = 4;
            $returnPayload[2] = "INSERT/UPDATE FAILED: ".$this->classname." - DATABASE ERROR when SQL ran";
            $returnPayload[3] = "FAILED: An error has occurred.  Please contact your system administrator.";
            $returnPayload[4] = $posted;
            return $returnPayload;
        }
        
        // If this was an ADD we pull the new record number to give back to the system
        if ($action == "ADD") { 
            $record = $this->registry->db->getInsertId(); 
            $posted['record'] = $record;
        }

        // That's all there is to it! Let's send back the data and be done!
        $returnPayload[0] = true;
        $returnPayload[1] = 1;
        $returnPayload[2] = "$action RECORD completed successfully ".$this->classname." - RECORD # $record";
        $returnPayload[3] = "$action completed successfully (Record# $record) - please check the data below.";
        $returnPayload[4] = $posted;
        return $returnPayload;
    }




    /*  ********************************************************************** */
    /*  ***************** SEARCH METHOD FOR SEARCH CONTROLLER **************** */
    /*  ********************************************************************** */
    // You will need to adjust your SQL here to fit your needs for the search
    // You would also add other wiring to do different limiters or types of search
    public function doSearch($searchterms, $active=0, $searchall=0) {
        $terms = explode(" ", $searchterms);
        $numterms = count($terms);
        $x=0;
        $sql = "SELECT emails_system.*, security_acl_groups.groupname, logins_users_details.emailaddress, 
                logins_users_details.firstname, logins_users_details.lastname FROM emails_system LEFT JOIN 
                security_acl_groups ON emails_system.groupnum = security_acl_groups.groupindex LEFT JOIN logins_users_details 
                on emails_system.usernum = logins_users_details.usernum WHERE ";
        if ($active == 1) {
            $sql .= "emails_system.usernum = 0 AND ";
        }
        if ($active == 2) {
            $sql .= "emails_system.emailstatus = 1 AND ";
        }
        foreach ($terms as $term) {
            $x++;
            if ($searchall==1) {
                $sql .= "(emails_system.subjectline LIKE '%$term%' OR emails_system.emailbody LIKE '%$term%' OR emails_system.sendtime LIKE '%$term%' 
                OR emails_system.datecreated LIKE '%$term%' OR logins_users_details.emailaddress LIKE '%$term%' OR logins_users_details.firstname LIKE 
                '%$term%' OR logins_users_details.lastname LIKE '%$term%' OR security_acl_groups.groupname LIKE '%$term%')";
            }
            else {
                $sql .= "(emails_system.subjectline LIKE '%$term%' OR emails_system.emailbody LIKE '%$term%')";                
            }
            if ($x!==$numterms) {
                $sql .= " AND ";
            }
        }
        /*  **********************************************************************************
            Add additional code here to limit the search further or perform other cool things 
            **********************************************************************************
        if ($searchall==2 || $searchall==3 || $searchall==4 || $searchall==5 || $searchall==6) { 
            $sql.=" ORDER BY resident_units.unitnumber";
        }
        else {
            $sql.=" ORDER BY lastname, firstname ASC";
        }
        */
        $sql.=" ORDER BY emails_system.sendtime ASC";
        $result=$this->registry->db->query($sql);
        if ($result) {
            return $result;
        }
        return false;
    }



    public function getAllGroups() {
        $sql = "SELECT * FROM security_acl_groups ORDER BY groupname ASC";
        $result=$this->registry->db->query($sql);
        if ($result->num_rows > 0) {
            $result=$this->registry->db->getAllRows();
            return $result;
        }
        return false;
    }

    // This function checks for any emails that need to be enqueued and sends back
    // an array containing the data or false if none
    public function checkNewEmails() {
        $sql = "SELECT * FROM emails_system WHERE emails_system.emailstatus = 1 AND emails_system.usernum = 0 AND sendtime < CURRENT_TIMESTAMP()";
        $result=$this->registry->db->query($sql);
        if ($result->num_rows > 0) {
            $result=$this->registry->db->getAllRows();
            return $result;
        }
        return false;   
    }

    public function getEmailsQueue($groupnum=0, $optinonly=1, $ccadmins=0) {
        $addsql = "";
        if ($groupnum > 0) { 
            if ($ccadmins == 1) {
                $addsql .= " AND (logins_users.aclgroup = $groupnum OR security_acl_groups.isadmin = 1)"; 
            }
            else {
                $addsql .= " AND logins_users.aclgroup = $groupnum"; 
            }
        }
        if ($optinonly == 1) { $addsql .= " AND logins_users_details.okaytoemail = 1"; }
        $sql = "SELECT logins_users.username, logins_users_details.*, security_acl_groups.isadmin FROM 
                logins_users LEFT JOIN logins_users_details ON logins_users_details.usernum = logins_users.usernum 
                LEFT JOIN security_acl_groups ON logins_users.aclgroup = security_acl_groups.groupindex WHERE 
                logins_users.useractive = 1".$addsql; 
        $result=$this->registry->db->query($sql);
        if ($result->num_rows > 0) {
            $result=$this->registry->db->getAllRows();
            return $result;
        }
        return false; 
    }

    // This method simply inserts a mail to be sent into the temporary mail queue
    public function queueEmail($mailnum, $emailfrom, $emailto, $subjectline, $emailbody) {
        $sql = "INSERT INTO emails_system_queue (mailnum, emailfrom, emailto, subjectline, emailbody) VALUES
                ($mailnum, '$emailfrom', '$emailto', '$subjectline', '$emailbody')";
        $result=$this->registry->db->query($sql);
        if ($result) {
            return true;
        }
        else {
            return false;
        }
    }

    // This method updates the status of a system mail with various stats
    public function updateEmailStatus($mailnum, $status=false, $numberexpected=false, $numbersent=false) {
        $terms=0;
        $addsql = "";
        if ($status !== false) { $addsql .= " emailstatus = $status"; $terms++;}
        if ($numberexpected) { if ($terms) {$addsql .= ","; } $addsql .= " numberexpected = $numberexpected"; $terms++; }
        if ($numbersent) { if ($terms) {$addsql .= ","; } $addsql .= " numbersent = numbersent + $numbersent"; $terms++; }
        $sql = "UPDATE emails_system SET".$addsql." WHERE mailnum = $mailnum LIMIT 1";
        $result=$this->registry->db->query($sql);
        if ($result) {
            return true;
        }
        else {
            return false;
        }      
    }

    // This method removes all queued emails for a particular mailnum
    public function emptyQueue($mailnum) {
        $sql = "DELETE FROM emails_system_queue WHERE mailnum = $mailnum";
        $result=$this->registry->db->query($sql);
        if ($result) {
            return true;
        }
        else {
            return false;
        }   
    }

    // This method pulls emails waiting to be sent and sends back the 
    // correct number of mails to send to match the "emails per hour"
    // setting in the SMTP configuration file
    public function checkSendEmails() {
        // First, how many times per hour are we checking for email
        $emailsperhour = $this->registry->config['smtp_mails_per_hour'];
        if ($emailsperhour < 0 || $emailsperhour > 100000 || empty($emailsperhour) || !is_numeric($emailsperhour)) { $emailsperhour = 120; }
        $sql = "SELECT minutes FROM background_tasks WHERE modulename = 'sysemailsend' LIMIT 1";
        $result = $this->registry->db->oneRowQuery($sql);
        if ($result) {
            $minutes = $result['minutes'];
            if ($minutes > 60) { $minutes = 60; }
            $xperhour = round(60/$minutes);
        }
        else {
            $xperhour = 4;
        }
        $numtosend = round($emailsperhour/$xperhour);
        $sql = "SELECT * FROM emails_system_queue ORDER BY queuedate ASC LIMIT ".$numtosend;
        $result = $this->registry->db->query($sql);
        if ($result) {
            if ($result->num_rows == 0) {
                return false;
            }
            $sendmails = $this->registry->db->getAllRows();
            return $sendmails;
        }
        else {
            return false;
        }
    }

    // This method removes a queued individual email from the queue 
    public function removeSend($queuenum) {
        $sql = "DELETE FROM emails_system_queue WHERE queuenum = $queuenum LIMIT 1";
        $result = $this->registry->db->query($sql);
        if ($result) { return true; } else { return false; }
    }

    // This method returns the number of emails still waiting to be sent
    // from the mail send queue for a particular mailnum or all waiting if no mailnum is given
    public function countSend($mailnum=false) {
        if ($mailnum) { $sql = "SELECT queuenum FROM emails_system_queue WHERE mailnum = $mailnum"; } else { $sql = "SELECT queuenum FROM emails_system_queue"; }
        $result = $this->registry->db->query($sql);
        if ($result) {$numrows = $result->num_rows; return $numrows; } else { return false; }
    }

    // This method sends a single email to a single user immediately and inserts the necessary data
    // into the database to record it.
    // $sendfrom: 0 = System Email Address, 1 = Site Email Address, 2 = specific address given in $senderemail
    public function sendSingleEmail($loginnum, $sendfrom, $subjectline, $emailbody, $senderemail=false, $fromname=false) {
        $model = new mEmails($this->registry);
        $sql = "SELECT emailaddress FROM logins_users_details WHERE usernum = $loginnum LIMIT 1";
        $result = $this->registry->db->oneRowQuery($sql);
        if ($result) { $emailto = $result['emailaddress']; } else { return false; }
        if (!$fromname) { $fromname = $this->registry->config['sitename']; }
        if (empty($subjectline)) { $subjectline = "Email from ".$fromname; }
        if ($sendfrom == 0) { $emailfrom = $this->registry->config['systememail']; } 
        elseif ($sendfrom == 1) { $emailfrom = $this->registry->config['siteemail']; }
        elseif ($sendfrom == 2) { $emailfrom = $senderemail; }
        else { return false; }
        if (isset($_SESSION['usernum'])) { $createdby = $_SESSION['usernum']; } else { $createdby = "NULL"; }
        $rsubjectline = $this->registry->security->unSanitize($subjectline);
        $remailbody = $this->registry->security->unSanitize($emailbody);
        $sql = "INSERT INTO emails_system (createdby, usernum, subjectline, emailbody, optinonly, sendfrom, 
                emailstatus, numberexpected, numbersent) VALUES ($createdby, $loginnum, '$subjectline', 
                '$emailbody', 0, $sendfrom, 0, 1, 1)";
        if (!$this->registry->db->query($sql)) {
            $explanation = "FAILED TO INSERT RECORD OF EMAIL FROM $createdby TO $loginnum";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        }
        else {
            $newmsg = $this->registry->db->getInsertId(); 
            $sql = "SELECT subjectline, emailbody FROM emails_system WHERE mailnum = $newmsg LIMIT 1";
            $cleanmsg = $this->registry->db->oneRowQuery($sql);
            $emailbody = $cleanmsg['emailbody'];
            $subjectline = $cleanmsg['subjectline'];
        }
        $mail = new email($this->registry);
        if ($mail->sendMail($emailto,false,$subjectline,$emailbody,$emailfrom,$fromname)) {   
            $cleanmail['emailbody'] = $emailbody;
            $cleanmail['subjectline'] = $subjectline;
            return $cleanmail;
        }
        return false;
    }



}   


?>
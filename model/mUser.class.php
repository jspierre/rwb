<?php

class mUser extends baseModel {
    
    public function getUserByLoginnum($loginnum) {
        $sql = "SELECT logins_users.*, logins_users_details.*, logins_users_session.* FROM logins_users 
        LEFT JOIN logins_users_details ON logins_users.usernum = logins_users_details.usernum  
        LEFT JOIN logins_users_session ON logins_users.usernum = logins_users_session.usernum
        WHERE logins_users.usernum = $loginnum LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars) {
            return $vars;
        }
        else { 
            return false;
        }
    }

    // Checks an IP address against the bans table.  
    // If $remove is set to true, the banned IP address will be removed
    // Returns true IP is banned and false if it isn't
    public function checkIPIsBanned($ipaddress, $remove=false) {
        $sql = "SELECT * FROM security_bans WHERE ipaddress='$ipaddress' LIMIT 1";
        $vars=$this->registry->db->oneRowQuery($sql);
        if ($vars) {
            if ($remove == true) {
                $sql = "DELETE FROM security_bans WHERE ipaddress = '$ipaddress'";
                $this->registry->db->query($sql);
                $sql = "DELETE FROM security_attacks WHERE ipaddress = '$ipaddress'";
                $this->registry->db->query($sql);
            }
            return true;
        }
        else { 
            return false;
        }
    }

    // Given a function name, this method returns all users who have that specific power.
    public function getUsersByFunction($funcname) {
        $sql = "SELECT DISTINCTROW logins_users.usernum, logins_users.*, logins_users_details.*, security_acl_functions.*, security_acl_functions_access.* 
        FROM logins_users LEFT JOIN logins_users_details ON logins_users.usernum = logins_users_details.usernum LEFT JOIN
        security_acl_functions_access ON logins_users.aclgroup = security_acl_functions_access.groupnum LEFT JOIN 
        security_acl_functions ON security_acl_functions_access.functionnum = security_acl_functions.funcnum WHERE 
        security_acl_functions.funcname = '$funcname'";
        $vars = $this->registry->db->query($sql);
        if ($vars) {
            $usersinfo = $this->registry->db->getAllRows();
            return $usersinfo;
        }
        else {
            return false;
        }
    }


    public function updateUser($data, $actiontype="NEW", $selfsignup=false) {
        
        // Let's check for an invalid offercode first since it doesn't use a database
        if ($selfsignup) { $aclgroup = $this->registry->config['defaultaclgroup']; }
        if ($selfsignup && $this->registry->config['offercodeon'] && trim($data['offercode']) != "") {
            $useroffercode = trim(strtolower($data['offercode']));
            $realoffercode = trim(strtolower($this->registry->config['offercode']));
            if ($realoffercode == $useroffercode) {
                $aclgroup = $this->registry->config['offeraclgroup'];
            }
            else {
                $result[0] = false;
                $result[1] = "ADD_USER_ERROR_INVALID_OFFERCODE";
                $result[2] = array(
                'tempdata' => $data,
                );
                 return $result; 
            }
        }

        // First, sanitize the incoming data
        $data=$this->registry->security->sanitizeArray($data);
        
        // Because a passsword may have been set, we DO NOT want to set that to upper case
        // Although we want everything else to be, so let's check for an updated password
        // and pull that out if that's the case
        if (isset($data['password']) && !empty($data['password'])) { $passwd = $data['password']; }
 
        // Now we can go ahead and turn the whole array of data into uppercase
        $data=$this->registry->security->upperArray($data);
        
        // Next, extract it all to individual variables
        extract($data);

        // Email address should be lowercase
        if (isset($emailaddress)) {$emailaddress = strtolower($emailaddress); $data['emailaddress'] = $emailaddress; }

        // We also want our username to be lowercase.  Damn, we're picky.
        if (isset($username)) { $username = strtolower($username); $data['username'] = $username; }
        
        // If this is a self-signup and the user didn't agree to the T&Cs, send them back
        if ($selfsignup && $tcagree == 0) {
            $result[0] = false;
            $result[1] = "ADD_USER_TC_NON_AGREE";
            $result[2] = array(
                'tempdata' => $data,
            );
            return $result;
        }

        // Did this user enter a real name or some fake shit?  Let's see
        $firstname=trim($firstname);
        $lastname=trim($lastname);
        if (!isset($firstname) || $firstname == "" || strlen($firstname) < 2 || !isset($lastname) || $lastname == "" || strlen($lastname) < 2 || ((strlen($firstname) + strlen($lastname)) < 4)) {
            $result[0] = false;
            $result[1] = "ADD_USER_ERROR_NOT_REAL_NAME";
            $result[2] = array(
                'tempdata' => $data,
            );
            return $result;
        }
        
        // Now, let's put the password back into the variable it goes to if it was changed
        if (isset($data['password']) && !empty($data['password'])) { $password = $passwd; $data['password']=$passwd; } else {$password = false;}
        
        // Now, if the username is an email address, let's clean it up and make sure it's valid
        // Also, if email is set as the username, let's define the username as the emailaddress
            if ($this->registry->config['loginwithemail'] && $username !== "" ) {
                $username = $this->cleanEmail($username);
                $emailaddress = $username;
                if ($username == false) {
                    $result[0] = false;
                    $result[1] = "ADD_USER_ERROR_INVALID_USERNAME_EMAIL3";
                    $result[2] = array(
                        'tempdata' => $data,
                    );
                    return $result;
                }
            }

        // Same with the actual email address field
            if ($emailaddress !== "" ) {
                $emailaddress = $this->cleanEmail($emailaddress);
                if ($emailaddress == false) {
                    $result[0] = false;
                    $result[1] = "ADD_USER_ERROR_INVALID_EMAIL4";
                    $result[2] = array(
                        'tempdata' => $data,
                    );
                    return $result;
                }
            }



        // Let's grab the date in a proper format for use later
        $thedate = $this->registry->getDateTime();

        // If we're creating a new user, let's do that
        if ($actiontype == "NEW") {
            
            // First, let's make sure that we're starting from a clean slate
            if (isset($_SESSION['loginnum'])) {unset($_SESSION['loginnum']);}
            
            // Now, let's check to see if the user already exists that we're trying 
            // to create.  If so, we send 'em back!
            $sql = "SELECT usernum FROM logins_users WHERE username = '$username' LIMIT 1";
            $exists = $this->registry->db->oneRowQuery($sql);
            if ($exists) {
                $result[0] = false;
                $result[1] = "ADD_USER_ERROR_DUPLICATE_USER";
                $result[2] = array(
                    'loginnum' => $exists['usernum'],
                    'tempdata' => $data,
                );
                return $result;
            }

            // If multiple accounts with the same email are not allowed, let's make sure that the email
            // address is not a duplicate
            if ($this->registry->config['loginwithemail'] == false && $username !== "" && $this->registry->config['allowduplicateemails'] == false ) {
                $sql = "SELECT usernum FROM logins_users_details WHERE emailaddress = '$emailaddress' LIMIT 1";
                $exists = $this->registry->db->oneRowQuery($sql);
                if ($exists) {
                    $result[0] = false;
                    $result[1] = "ADD_USER_ERROR_DUPLICATE_EMAIL";
                    $result[2] = array(
                        'loginnum' => $exists['usernum'],
                        'tempdata' => $data,
                    );
                    return $result;
                }    
            }
            
            // If the user doesn't exist yet, we're going to go ahead and add
            // the user to the database
            else {              
                // IF this user created themselves, we need to create a password for them
                // and initiate some variables they won't have yet
                if ($selfsignup) { 
                    $password = $this->randomPassword(); 
                    $useractive = 1; $userlocked=0; $selfcreated=1;
                    $billingaddress1 =""; $billingaddress2=""; $billingcity=""; $billingstate=""; $billingzip="";
                    $billingphone=""; $okaytoemail = 1; $oktotext = 0; $mailingaddr1=""; $mailingaddr2=""; $mailingcity="";
                    $mailingstate=""; $mailingzip=""; $mobilephone=""; 
                }


                // IF this is NOT a self-signup, we set some variables for the agent automatically
                if ($selfsignup == false) { $tcagree = 0; $selfcreated = 0;}

                // If the user happened to agree to T&Cs, let's set the time and date for the record
                if ($tcagree == 1) { $lasttcagree = "'".date("Y-m-d H:i:s")."'"; } else { $lasttcagree = "NULL"; }

                // FIRST, we need to get the password encrypted
                $originalpw = $password;
                $encarray = $this->registry->db->encrypt($password);
                $password = $encarray['pw'];
                $passthesalt = $encarray['salt'];
                // NEXT, we get yesterday's date to set the password to expire immediately
                $date = new DateTime();
                $date->add(DateInterval::createFromDateString('yesterday'));
                $passexpires = $date->format('Y-m-d');
                $originaldate = date("Y-m-d");
                

                // Now, we create the SQL to insert the user into the basic users table
                $sql = "INSERT INTO logins_users (username, password, passthesalt, passexpires, useractive, userlocked,
                 pwattempts, aclgroup, selfcreated) VALUES ('$username', '$password', '$passthesalt', '$passexpires', 
                 '$useractive', '$userlocked', 0, $aclgroup, $selfcreated)";
                //echo $sql;
                //exit();
                // We run the transaction - if the insert failed, we send back an error
                if($this->registry->db->query($sql) == false) {
                    $result[0] = false;
                    $result[1] = "ADD_USER_ERROR_DATABASE_ERROR_1";
                    $result[2] = false;
                    return $result;
                }
                
                // If the insert worked, we pull the new user number
                $loginnum = $this->registry->db->getInsertId();



                // And we set the SQL to insert the extra details about the
                // user into the details table
                $sql = "INSERT INTO logins_users_details (usernum, firstname, lastname, emailaddress, billingaddress1, 
                    billingaddress2, billingcity, billingstate, billingzip, billingphone, okaytoemail, oktotext, 
                    mailingaddr1, mailingaddr2, mailingcity, mailingstate, mailingzip, mobilephone, tcagree, lasttcagree ) VALUES ($loginnum, 
                    '$firstname', '$lastname', '$emailaddress', '$billingaddress1', '$billingaddress2', 
                    '$billingcity', '$billingstate', '$billingzip', '$billingphone', $okaytoemail, $oktotext, 
                    '$mailingaddr1', '$mailingaddr2', '$mailingcity', '$mailingstate', '$mailingzip', '$mobilephone', $tcagree, $lasttcagree)";
                

                // We run the transaction - if it fails, we delete the work we've
                // done so far and return an error
                if ($this->registry->db->query($sql) == false) {
                    $sql = "DELETE FROM logins_users WHERE usernum=$loginnum LIMIT 1";
                    $this->registry->db->query($sql);
                    $result[0] = false;
                    $result[1] = "ADD_USER_ERROR_DATABASE_ERROR_2";
                    $result[2] = false;
                    return $result;
                }

                // And now we set up the session table for the user
                $sql = "INSERT INTO logins_users_session (usernum, isalive) VALUES ($loginnum, 0)";

                // We run the transaction - if it fails, we delete the work we've
                // done so far and return an error
                if ($this->registry->db->query($sql) == false) {
                    $sql = "DELETE FROM logins_users_details WHERE usernum=$loginnum LIMIT 1";
                    $this->registry->db->query($sql);
                    $sql = "DELETE FROM logins_users WHERE usernum=$loginnum LIMIT 1";
                    $this->registry->db->query($sql);
                    $result[0] = false;
                    $result[1] = "ADD_USER_ERROR_DATABASE_ERROR_3";
                    $result[2] = false;
                    return $result;
                }

                // Let's just pull out our user's first name because we'll need it a few times soon.
                $name = ucfirst(strtolower($firstname));

                // At this point, the new user has been recorded.  If the admin asked for the user's credentials 
                // to be emailed to the user, let's go ahead and do that
                
                if ($selfsignup==false && $sendcredentials==1) {
                    $mail = new email($this->registry);
                    $subject = "Welcome to ".$this->registry->config['sitename']." - Login Credentials Enclosed";
                    $body = "Dear $name,\n\nYour account with ".$this->registry->config['sitename']." is now set up and ready to use!\n\nPlease see your new login id and temporary password below, which you'll need\nto log in:\n\nUSERNAME: ".$username."\nTEMPORARY PASSWORD: ".$originalpw."\n\nNote that this password is CASE-SENSITIVE. As soon as you log in the first time,\nyou'll be forced to change it to something easier for you to remember.\n\nThis account was created for you by our administration team. If you believe\nthis account was created in error, please contact us immediately at\n".$this->registry->config['siteemail']." and let us know, or call us at\n".$this->registry->config['sitephone']." and we would be happy to assist you.\n\nSincerely,\n\nThe System Administration Team\n".$this->registry->config['sitename'];
                    if($mail->sendMail($emailaddress,false,$subject,$body)) {
                        $explanation = "NEW CREDENTIALS EMAIL SENT SUCCESSFULLY TO USER $username (at $emailaddress) SYSTEM";
                        $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);    
                    }   
                    else {
                        echo " NOTE: SETUP EMAIL FAILED! SEND EMAIL MANUALLY TO USER!";
                        $explanation = "NEW CREDENTIALS EMAIL ###FAILED### FOR USER $username (at $emailaddress) BY SYSTEM.";
                        $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                    }      
                }

                // No errors means a SUCCESS - let's send back that data
                $result[0] = true;
                $result[1] = "USER_ADD_SUCCESS";
                $result[2]['data']=$data;
                $result[2]['loginnum']=$loginnum;
                $result[2]['data']['returnpass'] = $originalpw; 
                if ($this->registry->config['loginwithemail'] && $selfsignup) {
                    $result[2]['data']['emailaddress'] = $username;
                }
                if ($selfsignup == true) {$crby = "SELF-SIGNUP"; } else { $crby = "USER # %%USERNUM%% (%%USER%%)";}
                $explanation = "***NEW USER*** USER # $loginnum WITH USER ID: $username ($name) CREATED BY ".$crby;
                $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
                return $result;               
            }
        }
        elseif ($actiontype == "UPDATE") {
            // SO, WE DON'T HAVE A NEW USER, LET'S UPDATE AN EXISTING ONE!

            // First, if the user is trying to bump up another user to admin level and doesn't have
            // the right to, we just leave them as-is - also if they are trying to move an admin between
            // admin groups and they don't have the right to, we leave it as-is.
            $sql = "SELECT usernum, aclgroup, isadmin FROM logins_users LEFT JOIN security_acl_groups ON 
                    logins_users.aclgroup = security_acl_groups.groupindex WHERE usernum = $loginnum LIMIT 1";
            $admindata = $this->registry->db->oneRowQuery($sql);
            $isadmin = $admindata['isadmin'];
            $oldaclgroup = $admindata['aclgroup'];
            $sql = "SELECT * FROM security_acl_groups WHERE groupindex = $aclgroup LIMIT 1";
            $groupdata = $this->registry->db->oneRowQuery($sql);
            $isgroupadmin = $groupdata['isadmin'];
            if (!$this->registry->security->checkFunction("createadmins") && $isgroupadmin == 1 && $isadmin == 0) {
                $aclgroup = $oldaclgroup;
                $explanation = "USER %%USER%% TRIED TO UPGRADE OTHER USER WITHOUT PERMISSION: ".$loginnum;
                $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            }
            elseif (!$this->registry->security->checkFunction("createadmins") && $isadmin == 1 && ($aclgroup != $oldaclgroup)) {
                $aclgroup = $oldaclgroup;   
                $explanation = "USER %%USER%% TRIED TO ADJUST ADMIN USER WITHOUT PERMISSION: ".$loginnum;
                $this->registry->logging->logEvent($this->registry->config['logging_cat_general'],$explanation);
            }

            // If we reset the user's password, we need to deal with
            // That first
            if ($password) {

                // We encrypt the new password and store the unencrypted version temporarily
                $originalpw = $password;
                $encarray = $this->registry->db->encrypt($password);
                $password = $encarray['pw'];
                $passthesalt = $encarray['salt'];
            
                // NEXT, we get yesterday's date to set the password to expire immediately
                $originaldate = date("Y-m-d");
                $mindate=explode("-",$originaldate);
                $miny=$mindate[0];
                $minm=$mindate[1];
                $mind=$mindate[2];
                $mind=$mind-1;
                if ($mind==0) {
                        $minm=$minm-1;
                        if ($minm==0) { $minm=12; $miny=$miny-1;}
                        if ($minm==4 || $minm==6 || $minm==9 || $minm==11) { $mind=30; } else { $mind=31; }
                        if ($minm==2) { if (($miny/4)==intval(($miny/5))) { $mind=29; } else { $mind=28;} }
                }
                $passexpires=$miny."-".$minm."-".$mind;
            }

            // Now, if we changed the username, we need to make sure there are no duplicates:
            $sql = "SELECT usernum FROM logins_users WHERE username = '$username' AND usernum != $loginnum LIMIT 1";
            $exists = $this->registry->db->oneRowQuery($sql);
            if ($exists) {
                $result[0] = false;
                $result[1] = "ADD_USER_ERROR_DUPLICATE_USER_CHANGE";
                $result[2] = array(
                    'dupnum' => $exists['usernum'],
                    'loginnum' => $loginnum,
                    'tempdata' => $data,
                );
                return $result;
            }

            // Let's check for main user details that were changed so we can let our Controller know and log these
            $changed = array();
            $sql = "SELECT * FROM logins_users WHERE usernum=$loginnum LIMIT 1";
            $result = $this->registry->db->oneRowQuery($sql);
            if ($result['username'] !== $username) { $changed[] = "USER NAME CHANGED FROM ".$result['username']." TO $username"; }
            if ($result['passexpires'] !== $passexpires) { $changed[] = "PASSWORD EXPIRATION CHANGED FROM ".$result['passexpires']." TO $passexpires"; }
            if ($result['useractive'] !== $useractive) { 
                if ($useractive == 1) { $changed[] = "USER ACCOUNT RE-ENABLED (SET ACTIVE)"; } else { $changed[] = "USER ACCOUNT DISABLED (SET INACTIVE)";}
            }
            if ($result['userlocked'] !== $userlocked) { 
                if ($userlocked == 0) { $changed[] = "USER ACCOUNT MANUALLY UNLOCKED"; } else { $changed[] = "USER ACCOUNT MANUALLY LOCKED";}
            }
            if ($result['aclgroup'] !== $aclgroup) { 
                $sql = "SELECT groupname FROM security_acl_groups WHERE groupindex = $aclgroup LIMIT 1";
                $nu = $this->registry->db->oneRowQuery($sql);
                $newgroup = $nu['groupname'];
                $oldgroupnumber = $result['aclgroup'];
                $sql = "SELECT groupname FROM security_acl_groups WHERE groupindex = $oldgroupnumber LIMIT 1";
                $ou = $this->registry->db->oneRowQuery($sql);
                $oldgroup = $ou['groupname'];
                $changed[] = "ACCESS LEVEL CHANGED FROM ".$oldgroup." TO $newgroup"; 
            }


            if ($result['pwattempts'] !== $pwattempts) { $changed[] = "PASSWORD ATTEMPTS CHANGED FROM ".$result['pwattempts']." TO $pwattempts"; }


            // If a new password was set, we update the database with the new password
            if ($password) {
                $sql = "UPDATE logins_users SET username='$username', password='$password', passthesalt='$passthesalt', passexpires='$passexpires', useractive=$useractive, userlocked=$userlocked, pwattempts=$pwattempts, aclgroup=$aclgroup, lastchanged='$thedate' WHERE usernum=$loginnum LIMIT 1";
            }
            else {
                $sql = "UPDATE logins_users SET username='$username', passexpires='$passexpires', useractive=$useractive, userlocked=$userlocked, pwattempts=$pwattempts, aclgroup=$aclgroup, lastchanged='$thedate' WHERE usernum=$loginnum LIMIT 1";                
            }
            if($this->registry->db->query($sql) == false) {
                $result[0] = false;
                $result[1] = "UPDATE_USER_ERROR_DATABASE_ERROR_1";
                $result[2] = array(
                    'loginnum' => $loginnum,
                    'tempdata' => $data,
                );
                return $result;
            } 

            // Let's now check for important details that were changed so we can let our Controller know and log these
            $sql = "SELECT * FROM logins_users_details WHERE usernum = $loginnum LIMIT 1";
            $result = $this->registry->db->oneRowQuery($sql); 
            if ($result['firstname'] !== $firstname) { $changed[] = "FIRST NAME CHANGED FROM ".$result['firstname']." TO $firstname"; }
            if ($result['lastname'] !== $lastname) { $changed[] = "LAST NAME CHANGED FROM ".$result['lastname']." TO $lastname"; }
            if ($result['emailaddress'] !== $emailaddress) { $changed[] = "EMAIL CHANGED FROM ".$result['emailaddress']." TO $emailaddress"; }
            if ($result['okaytoemail'] !== $okaytoemail) { 
                if ($okaytoemail == 1) { $changed[] = "EMAIL STATUS CHANGED TO ALLOW EMAILS TO USER"; } else { 
                    $changed[] = "EMAIL STATUS CHANGED TO DO NOT EMAIL USER"; }
            }
            if ($result['mobilephone'] !== $mobilephone) { $changed[] = "MOBILE PHONE CHANGED FROM ".$result['mobilephone']." TO $mobilephone"; }
            if ($result['oktotext'] !== $oktotext) { 
                if ($oktotext == 1) { 
                    $changed[] = "MOBILE TEXT STATUS CHANGED TO ALLOW TEXTS"; 
                } 
                else { 
                    // If the user opts out of texts, they can't be in 2FA, so remove that capability as well.
                    $changed[] = "MOBILE TEXT STATUS CHANGED TO DO NOT TEXT USER"; 
                    $changed[] = "2FA STATUS SET TO OFF RESULTING FROM DO NOT TEXT"; 
                    $tfa = new mTwoFactor($this->registry);
                    $tfa->reset2FA($loginnum);
                }
            }
            if ($result['mailingaddr1'] !== $mailingaddr1 ) { $changed[] = "MAILING ADDR LINE 1 CHANGED FROM ".$result['mailingaddr1']." TO $mailingaddr1"; }
            if ($result['mailingaddr2'] !== $mailingaddr1 ) { $changed[] = "MAILING ADDR LINE 2 CHANGED FROM ".$result['mailingaddr2']." TO $mailingaddr2"; }
            if ($result['mailingcity'] !== $mailingcity ) { $changed[] = "MAILING ADDR CITY CHANGED FROM ".$result['mailingcity']." TO $mailingcity"; }
            if ($result['mailingstate'] !== $mailingstate ) { $changed[] = "MAILING ADDR STATE CHANGED FROM ".$result['mailingstate']." TO $mailingstate"; }
            if ($result['mailingzip'] !== $mailingzip) { $changed[] = "MAILING ADDR ZIP CHANGED FROM ".$result['mailingzip']." TO $mailingzip"; }
            if ($result['billingaddress1'] !== $billingaddress1 ) { $changed[] = "BILLING ADDR LINE 1 CHANGED FROM ".$result['billingaddress1']." TO $billingaddress1"; }
            if ($result['billingaddress2'] !== $billingaddress2 ) { $changed[] = "BILLING ADDR LINE 2 CHANGED FROM ".$result['billingaddress2']." TO $billingaddress2"; }
            if ($result['billingcity'] !== $billingcity ) { $changed[] = "BILLING ADDR CITY CHANGED FROM ".$result['billingcity']." TO $billingcity"; }
            if ($result['billingstate'] !== $billingstate ) { $changed[] = "BILLING ADDR STATE CHANGED FROM ".$result['billingstate']." TO $billingstate"; }
            if ($result['billingzip'] !== $billingzip) { $changed[] = "BILLING ADDR ZIP CHANGED FROM ".$result['billingzip']." TO $billingzip"; }
            if ($result['billingphone'] !== $billingphone) { $changed[] = "BILLING PHONE CHANGED FROM ".$result['billingphone']." TO $billingphone"; }

            // Now, we update the remaining details of the user
            $sql = "UPDATE logins_users_details SET firstname='$firstname', lastname='$lastname', emailaddress='$emailaddress', 
            okaytoemail=$okaytoemail, mobilephone='$mobilephone', oktotext=$oktotext, mailingaddr1='$mailingaddr1', 
            mailingaddr2='$mailingaddr2', mailingcity='$mailingcity', mailingstate='$mailingstate', mailingzip='$mailingzip', 
            billingaddress1='$billingaddress1', billingaddress2='$billingaddress2', billingcity='$billingcity', 
            billingstate='$billingstate', billingzip='$billingzip', billingphone='$billingphone' WHERE usernum=$loginnum 
            LIMIT 1";
            //echo $sql;
           // exit();
            if($this->registry->db->query($sql) == false) {
                $result[0] = false;
                $result[1] = "UPDATE_USER_ERROR_DATABASE_ERROR_2";
                $result[2] = array(
                    'loginnum' => $loginnum,
                    'tempdata' => $data,
                );
                return $result;
            }

            // No errors means a SUCCESS - let's send back that data
                $result[0] = true;
                $result[1] = "USER_UPDATE_SUCCESS";
                $result[2]['data']=$data;
                $result[2]['loginnum']=$loginnum;
                if (isset($originalpw)) { $result[2]['data']['returnpass'] = $originalpw; }
                if (isset($changed)) {$result[2]['data']['changed'] = $changed; }
                return $result;

            //if (isset($_SESSION['clientdata'])) { 
            //    unset ($_SESSION['clientdata']); 
            //    $sql = "SELECT * FROM clients WHERE loginnum = $loginnum LIMIT 1";
            //    $vars=$this->registry->db->oneRowQuery($sql);
                //$_SESSION['clientdata'] = $vars;     
            //    $_SESSION['loginnum'] = $vars['loginnum'];
            }
    }

    public function getUserNotes($loginnum) {
        $sql = "SELECT usernotes from logins_users_notes WHERE usernum = $loginnum LIMIT 1";
        $result = $this->registry->db->oneRowQuery($sql);
        if ($result == false) {
            $notes = false;
        }
        else {
            $notes = $result['usernotes'];
        }
        return $notes;
    }

    public function getUserLog($loginnum) {
        $sql = "SELECT logins_users_log.lognum, logins_users_log.creatornum, logins_users_log.logtime, logins_users.username, logins_users_log.logmessage, logins_users_log.isprivate FROM logins_users_log JOIN logins_users on logins_users_log.creatornum = logins_users.usernum WHERE logins_users_log.usernum = $loginnum ORDER BY lognum DESC";
        $result=$this->registry->db->query($sql);
        if ($result->num_rows > 0) {
            $result=$this->registry->db->getAllRows();
            return $result;
        }
        return false;        
    }

    public function updateUserNotes($loginnum, $notes) {
        $notes = $this->registry->db->sanitize($notes);
        $todays_date = date("Y-m-d H:i:s");
        $sql = "SELECT usernotes from logins_users_notes WHERE usernum = $loginnum LIMIT 1";
        $result = $this->registry->db->oneRowQuery($sql);
        if ($result == false) {
            $sql = "INSERT into logins_users_notes (usernum, usernotes, lastupdate) VALUES ($loginnum, '$notes', '$todays_date')";
        }
        else {                
                $sql = "UPDATE logins_users_notes SET usernotes = '$notes', lastupdate = '$todays_date' WHERE usernum = $loginnum LIMIT 1";
            }
        $result = $this->registry->db->query($sql);
        if ($result) {
            return true;
        }
        return false;
    }

    public function doLogUser($creatornum, $loginnum, $message, $isprivate=0) {
        if ($isprivate != 0 && $isprivate !=1) { $isprivate = 0;}
        $todays_date = date("Y-m-d H:i:s");
        $sql = "INSERT INTO logins_users_log (creatornum, usernum, logtime, logmessage, isprivate) VALUES ($creatornum, $loginnum, '$todays_date', '$message', $isprivate)";
        if ($this->registry->db->query($sql)) {
            return true;    
        }
        return false;
    }

    public function doUserSearch($searchterms, $active=0, $searchall=0) {
        $terms = explode(" ", $searchterms);
        $numterms = count($terms);
        $x=0;
        $goduser = $this->registry->goduser;
        if ($this->registry->isgod) {
            $sql = "SELECT logins_users.*, logins_users_details.*, logins_users_session.*, security_acl_groups.* FROM logins_users 
            LEFT JOIN logins_users_details ON logins_users.usernum = logins_users_details.usernum 
            LEFT JOIN logins_users_session ON logins_users.usernum = logins_users_session.usernum 
            LEFT JOIN security_acl_groups ON security_acl_groups.groupindex = logins_users.aclgroup WHERE ";
        }
        else {
            $sql = "SELECT logins_users.*, logins_users_details.*, logins_users_session.*, security_acl_groups.* FROM logins_users 
            LEFT JOIN logins_users_details ON logins_users.usernum = logins_users_details.usernum 
            LEFT JOIN logins_users_session ON logins_users.usernum = logins_users_session.usernum 
            LEFT JOIN security_acl_groups ON security_acl_groups.groupindex = logins_users.aclgroup WHERE logins_users.usernum != $goduser AND ";
        }
        if ($active==1) {
            $sql .= "logins_users.useractive = 1 AND ";
        }
        elseif ($active==2) {
            $sql .= "logins_users_session.isalive = 1 AND ";
        }
        elseif ($active==3) {
            $sql .= "logins_users.userlocked = 1 AND logins_users.useractive = 1 AND ";
        }
        elseif ($active==4) {
            $sql .= "logins_users.useractive = 0 AND ";
        }
        foreach ($terms as $term) {
            $x++;
            if ($searchall==1) {
                $sql .= "(logins_users.usernum LIKE '%$term%' OR username LIKE '%$term%' OR firstname LIKE '%$term%' OR lastname 
                    LIKE '%$term%' OR emailaddress LIKE '%$term%' OR mobilephone LIKE '%$term%' OR billingaddress1 LIKE '%$term%' OR 
                    billingaddress2 LIKE '%$term%' OR billingcity LIKE '%$term%' OR billingstate LIKE '%$term%' OR billingzip LIKE '%$term%'
                    OR billingphone LIKE '%$term%' OR mailingaddr1 LIKE '%$term%' OR mailingaddr2 LIKE '%$term%' OR mailingcity LIKE '%$term%' OR 
                    mailingstate LIKE '%$term%' OR mailingzip LIKE '%$term%' OR mobilephone LIKE '%$term%')";
            }
            else if ($searchall==2) {
                $sql .= "(aclgroup LIKE '%$term%' OR groupname LIKE '%$term%')";
            }
            else {
                $sql .= "(firstname LIKE '%$term%' OR lastname LIKE '%$term%')";                
            }
            if ($x!==$numterms) {
                $sql .= " AND ";
            }
        }
        $sql.=" ORDER BY lastname, firstname ASC";
        //echo $sql;
        //die();
        $result=$this->registry->db->query($sql);
        if ($result) {
            return $result;
        }
        return false;
    }



    // This method returns the usernumber, first and last name of all users, either active (true) or not
    public function getAllUsers($active=true) {
        if ($active) {
            $addsql = " WHERE logins_users.useractive=1";
        }
        else {
            $addsql ="";
        }
        $sql = "SELECT logins_users.usernum, logins_users.username, logins_users_details.firstname, 
        logins_users_details.lastname FROM logins_users LEFT JOIN logins_users_details ON 
        logins_users.usernum = logins_users_details.usernum".$addsql." ORDER BY lastname, firstname ASC";
        $this->registry->db->query($sql);
        $users = $this->registry->db->getAllRows();
        return $users;
    }


    // This method simply sanitizes an email address and makes sure it is a valid email
    // Returns clean email if valid or FALSE if not
    public function cleanEmail($emailaddress) {
        $cleanvar = filter_var($emailaddress, FILTER_SANITIZE_EMAIL);
            if (filter_var($cleanvar, FILTER_VALIDATE_EMAIL)) {
                      $emailaddress = $cleanvar;
                      return $emailaddress;
            }
            else {
                return false;
            }
    }

    // This method takes a unitnumber and validates it to make sure that no more than
    // One Owner and/or One Resident have an active account for a given unit.
    // Requires the DB Unit index number and either "RENT" or "OWN" to check against
    // Returns an array [0]=pass/fail test (true/false), [1]=usernumoftakenowner/renter or FALSE
        public function getUnitStatus($unitnumber, $isowner=0, $currentuser=false) {
        // First, check for an Owner
        $sql = "SELECT usernum FROM logins_users_details WHERE unitnumber = $unitnumber and isowner > 0 LIMIT 1";
        $ownerresult = $this->registry->db->oneRowQuery($sql);      
        if ($ownerresult) {
            $owner = $ownerresult['usernum'];
        }
        else {
            $owner = false;
        }
        // Next, check for a Renter
        $sql = "SELECT usernum FROM logins_users_details WHERE unitnumber = $unitnumber and isowner = 0 LIMIT 1";
        $renterresult = $this->registry->db->oneRowQuery($sql);      
        if ($renterresult) {
            $renter = $renterresult['usernum'];
        }
        else {
            $renter = false;
        }
        // Finally, put together the results
        $usernum = false;
        if ($isowner == 0) {
            if ($renter == false || $renter == $currentuser) {
                $passfail = true;
            }
            else {
                $passfail=false;
                $usernum = $renter;
            }
        }
        else {
            if ($owner == false || $owner == $currentuser) {
                $passfail=true;
            }
            else {
                $passfail=false;
                $usernum = $owner;
            }
        }
        $unitstatus = array($passfail, $usernum);
        return $unitstatus;
    }


    // This method deletes a user
    public function deleteUser($usernum) {
        // Let's get the user's name
        $details=$this->getUserByLoginnum($usernum);
        $first = $details['firstname'];
        $last = $details['lastname'];
        // First, we need to delete all entries in the users_notes table
        $sql = "DELETE FROM logins_users_notes WHERE usernum=$usernum";
        if ($this->registry->db->query($sql)) {
            // Okay, now we need to delete all entries in the users_log table
            $sql = "DELETE FROM logins_users_log WHERE usernum=$usernum OR creatornum=$usernum";
            if ($this->registry->db->query($sql)) {
                // Okay, now we delete the user's details
                $sql = "DELETE FROM logins_users_details WHERE usernum=$usernum LIMIT 1";
                if ($this->registry->db->query($sql)) {
                    // Finally, we delete the main user entry
                    $sql = "DELETE FROM logins_users WHERE usernum=$usernum LIMIT 1";
                    if ($this->registry->db->query($sql)) {
                        // Now, we need to log this
                         $this->registry->logging->logEvent($this->registry->config['logging_cat_deleteuser'],"USER ".$usernum." (".$first." ".$last.") DELETED BY USER# %%USERNUM%% (%%USER%%) FROM %%IP%%");   
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
        return false;
    }

    public function randomPassword( $length = 12 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    // This function transforms the current user into another user
    // Note that this is destructive in that once transforms, the user becomes
    // the other user for the rest of their session and must LOG OUT and back IN to resume their prior role
    public function impersonateUser($usernum) {
        $adminuser = $_SESSION['username'];
        $userip = $_SESSION['userip'];
        $datetime = date("Y-m-d H:i:s");
        $sql = "SELECT username FROM logins_users WHERE usernum = $usernum LIMIT 1";
        $userinfo = $this->registry->db->oneRowQuery($sql);
        if (!$userinfo) { return false; }
        $_SESSION['userip']=$userip;
        $_SESSION['usertime'] = $datetime;
        $_SESSION['username'] = $userinfo['username'];
        $_SESSION['usernum'] = $usernum;
        $explanation = "### IMPERSONATE ### $adminuser BECAME USER: %%USER%% FROM: %%IP%%";
        $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        return true;
    }

}

?>
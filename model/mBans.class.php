<?php

class mBans extends baseModel {


    public function doSearch() {
        $sql = "SELECT * FROM security_bans ORDER BY bandate ASC";
        $result=$this->registry->db->query($sql);
        $result=$this->registry->db->getAllRows();
        if ($result) {
            return $result;
        }
        return false;
    }


    public function removeBan($bannumber=false) {
        // If bannumber is false, remove all bans and all attacks
        if ($bannumber == false) {
            $sql = "DELETE FROM security_bans";
            $this->registry->db->query($sql);
            $sql = "DELETE FROM security_attacks";
            $this->registry->db->query($sql);
            $explanation = "ALL SECURITY BANS AND ATTACKS DELETED BY %%USER%% FROM %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        }
        else {
            // First, let's get the info on this ban so we have everything we need
            $sql = "SELECT * FROM security_bans WHERE bannumber = $bannumber LIMIT 1";
            $result = $this->registry->db->oneRowQuery($sql);
            if ($result == false) { return false; }
            $ipaddress = $result['ipaddress'];
            // Now, let's get rid of the ban
            $sql = "DELETE FROM security_bans WHERE bannumber = $bannumber";
            $this->registry->db->query($sql);
            // And, we get rid of all attacks from that IP Address (otherwise, it will just get rebanned immediately)
            $sql = "DELETE FROM security_attacks WHERE ipaddress = '$ipaddress'";
            $this->registry->db->query($sql);
            $explanation = "SECURITY BANS AND ATTACKS ON IP ADDRESS $ipaddress DELETED BY %%USER%% FROM %%IP%%";
            $this->registry->logging->logEvent($this->registry->config['logging_cat_loginout'],$explanation);
        }
        return true;
    }


}


?>